from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class HrDepartment(models.Model):
    """Add analytics into departments:
    - Departments may contain analytic fields.
    """

    __metaclass__ = MetaAnalytic

    _analytic = 'hr.department'

    _inherit = 'hr.department'

class account_invoice_line_analytic(models.Model):
    __metaclass__ = MetaAnalytic
    _name = "account.invoice.line"
    _inherit = "account.invoice.line"

    _analytic = 'account_invoice_line'