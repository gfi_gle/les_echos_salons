# -*- coding: utf-8 -*-

from openerp import models, fields, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        invoice_vals = super(SaleOrder, self)._prepare_invoice(cr, uid, order, lines, context=context)
        invoice_vals.update(order._update_invoice_header(invoice_vals))
        return invoice_vals

    @api.multi
    def _update_invoice_header(self, data={}):
        data.update({
            'partner_id': self.partner_id and self.partner_id.id or False,
            'stand_id': self.stand_id and self.stand_id.id or False,
            'contact_client_id': self.contact_client_id and self.contact_client_id.id or False,
            'contact_business_ids': self.contact_business_id and [(6, 0, [self.contact_business_id.id])] or [
                (6, 0, [])],
            'commercial_ids': [(6, 0, self.mapped('ca_commision_ids.commercial_id.id'))],
            'invoice_address_ids': self.partner_invoice_id and [(6, 0, [self.partner_invoice_id.id])] or [(6, 0, [])],
            'advance_payment_method': self.env.context.get('advance_payment_method', False),
            'ca_commision_ids': [(6, 0, self.ca_commision_ids.mapped('id'))] if self.env.context.get(
                'advance_payment_method', False) not in ['fixed', 'percentage'] else False,
            'discount_type_invoice': self.discount_type_invoice,
            'wording_grouping': self.wording_grouping,
            'function_grouping': self.function_grouping,
            'partner_vat': self.partner_id.vat or False,
            'date_invoice': fields.Date.context_today(self)
        })
        return data


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        """Save the layout when converting to an invoice line."""

        invoice_vals = super(SaleOrderLine, self)._prepare_order_line_invoice_line(cr, uid, line, account_id=account_id, context=context)
        if line.function_grouping:
            invoice_vals['function_grouping'] = line.function_grouping
        if context.get('function_grouping', False):
            invoice_vals['function_grouping'] = True
        return invoice_vals
