# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class AccountTax(models.Model):
    """"""
    _inherit = 'account.tax'

    code_tva_gle = fields.Char("Code Tva Les Echos")
