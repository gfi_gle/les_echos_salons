# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', required=True, default=lambda self: self.env['product.pricelist'].search([('id', '=', 1)]) , help="The pricelist sets the currency used for this purchase order. It also computes the supplier price for the selected products/quantities.")
    is_invoiced = fields.Boolean(string='is_invoiced',compute='_compute_bdc_is_invoiced',default=False)


    def _compute_bdc_is_invoiced(self):
        """ check if BDC has been invoiced - is his number in account_invoice"""
        for line in self:
            xselfname = line.name
            if xselfname:
                xfind_invoice = self.env['account.invoice'].search([('origin', '=', xselfname)])
                if xfind_invoice:
                    line.is_invoiced = True





class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    code_tva_gle = fields.Char("Code Tva Les Echos", compute='_compute_code_tva_gle_purchase', store=True)


    @api.depends('taxes_id')
    def _compute_code_tva_gle_purchase(self):
        """"""
        i = 0
        for line in self:
            for tax_single in line.taxes_id:
                i += 1
                xsearch_code_tva_gle = self.env['account.tax'].search([('id', '=', tax_single.id)])
                xreturn_code_tva_gle = xsearch_code_tva_gle.code_tva_gle
        if i > 1 or i == 0:
            line.code_tva_gle = ''
        else:
            if xreturn_code_tva_gle:
                line.code_tva_gle = xreturn_code_tva_gle
            else:
                line.code_tva_gle = ''