# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.addons.decimal_precision import decimal_precision as dp
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class AccountInvoice(models.Model):

    _inherit = 'account.invoice'


    intercos = fields.Boolean('Interco', default=False)

    stand_id = fields.Many2one('gle_salon.stand', string="Stand")

    contact_client_id = fields.Many2one('res.partner', string="Contact client", help="Nom et prénom")
    contact_business_ids = fields.Many2many(
        comodel_name='res.users',
        relation='account_invoice_contact_business_rel',
        column1='invoice_id',
        column2='user_id',
        string='Contact les echos Business', )
    commercial_ids = fields.Many2many(
        comodel_name='res.users',
        relation='account_invoice_commercial_rel',
        column1='invoice_id',
        column2='user_id',
        string='Commerciaux', )
    partner_vat = fields.Char(string='N° de TVA', size=64, )
    partner_internal_ref = fields.Char(string='Référence Interne Client', size=64, )

    annotation_reglement = fields.Text(string='Annotation règlement',
                                       default=lambda self: self.env.user.company_id.annotation_reglement, )
    annotation_penalite_retard = fields.Text(string='Annotation pénalités de retard',
                                             default=lambda self: self.env.user.company_id.annotation_penalite_retard)

    discount_type_invoice = fields.Selection([
        ('line', "Remise / Ligne"),
        ('total', "Remise total")
    ], string="Type de remise", default=lambda self: 'total', )

    ca_commision_ids = fields.Many2many(comodel_name='gle_adv.ca_commision',
                                        relation='gle_adv_ca_commision_account_invoice_relation',
                                        column1='invoice_id',
                                        column2='ca_commision_id',
                                        string="Répartition du CA commissionnable")


    # Jira 233 plus de multi adresse de facturation > modif champ

    invoice_address_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='account_invoice_address_rel',
        column1='invoice_id',
        column2='partner_id',
        string='Adresses de Facturation', )
    # invoice_address_ids = fields.Many2one(comodel_name='res.partner', string='Adresses de Facturation', )



    acompte_recu = fields.Float(string='Acompte reçu', digits=dp.get_precision('Account'),
                                compute='_compute_acompte_recu', store=True, )
    invoice_total_without_acompte = fields.Float(string='Total TTC sans acompte', digits=dp.get_precision('Account'),
                                                 compute='_compute_invoice_total_without_acompte',
                                                 store=True, )
    invoice_total_untaxed_without_acompte = fields.Float(string='Total HT sans acompte',
                                                         digits=dp.get_precision('Account'),
                                                         compute='_compute_invoice_total_without_acompte',
                                                         store=True, )

    advance_payment_method = fields.Selection([
        ('all', 'Invoice the whole sales order'),
        ('percentage', 'Percentage'),
        ('fixed', 'Fixed price (deposit)'),
        # ('lines', 'Some order lines'),
    ], string='Invoice method source', )

    invoice_attachment = fields.Binary("Pièce jointe de la justification")
    invoice_attachment_filename = fields.Char("Nom du fichier")

    wording_grouping = fields.Char(string="Champ libellé de regroupement")
    function_grouping = fields.Boolean(string='Fonction de regroupeemnt', compute='_compute_function_grouping',
                                       store=True, )

    total_grouping = fields.Float(string='Total grouping', digits=dp.get_precision('Account'),
                                  compute='_compute_total_grouping', store=True, )
    total_remise = fields.Float(string='Total remise', digits=dp.get_precision('Account'),
                                compute='_compute_total_taux_remise', store=True, )
    taux_remise = fields.Float(string='Total remise', digits=dp.get_precision('Account'),
                               compute='_compute_total_taux_remise', store=True, )

    cancel_comment = fields.Text(string='Motif d\'annulation', readonly=True, )

    is_acompte = fields.Boolean(compute='_compute_is_acompte')

    @api.constrains('invoice_address_ids')
    def _constraint_invoice_address_ids(self):
        i = 0
        for invoice_address_id in self.invoice_address_ids:
            i += 1
            _logger.debug(i)
        if i>1:

            raise ValidationError("Attention! Une seule adresse de facturation possible.\n Merci de modifier votre saisie.")


    @api.multi
    @api.depends('invoice_line', 'invoice_line.price_subtotal')
    def _compute_total_grouping(self):
        for obj in self:
            obj.total_grouping = sum(obj.invoice_line.filtered(lambda r: r.function_grouping).mapped('price_subtotal'))

    @api.multi
    @api.depends('invoice_line', 'invoice_line.discount', 'invoice_line.discount_type', 'invoice_line.price_subtotal')
    def _compute_total_taux_remise(self):
        for obj in self:
            invoice_total = obj.amount_untaxed
            total_remise = sum(obj.invoice_line.mapped('total_remise'))
            invoice_total_without_remise = sum(obj.invoice_line.mapped('price_subtotal_without_discount'))
            obj.taux_remise = (1 - (
                invoice_total / invoice_total_without_remise)) * 100 if invoice_total_without_remise else 0
            obj.total_remise = - total_remise

    @api.multi
    @api.depends('invoice_line', 'invoice_line.function_grouping')
    def _compute_function_grouping(self):
        for obj in self:
            obj.function_grouping = True if obj.invoice_line.filtered(lambda r: r.function_grouping) else False

    @api.multi
    @api.depends('invoice_line', 'invoice_line.price_unit', 'invoice_line.price_subtotal')
    def _compute_acompte_recu(self):
        for obj in self:
            obj.acompte_recu = -1 * sum(
                obj.invoice_line.filtered(lambda r: r.price_subtotal < 0).mapped('price_subtotal'))

    @api.multi
    @api.depends('acompte_recu', 'amount_total', 'amount_untaxed')
    def _compute_invoice_total_without_acompte(self):
        for obj in self:
            obj.invoice_total_without_acompte = obj.amount_total + obj.acompte_recu
            obj.invoice_total_untaxed_without_acompte = obj.amount_untaxed + obj.acompte_recu

    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False, payment_term=False, partner_bank_id=False,
                            company_id=False):
        res = super(AccountInvoice, self).onchange_partner_id(type, partner_id, date_invoice, payment_term,
                                                              partner_bank_id, company_id)
        if 'value' in res:
            p = self.env['res.partner'].browse(partner_id or False)
            if partner_id:
                res['value']['partner_vat'] = p.vat
            res['value'].update({
                'invoice_address_ids': False,
                'contact_client_id': False,
            })
        return res

    @api.multi
    def invoice_print_facture(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'gle_accounting_extended.report_facture')

    @api.onchange('invoice_line')
    def on_change_invoice_line(self):

        if self.advance_payment_method and self.type == 'out_invoice' and len(self.invoice_line) != 1:
            raise ValidationError("Vous ne pouvez pas modifier une facture d'acompte.")

    def _compute_is_acompte(self):

        for invoice in self:
            if invoice.advance_payment_method and invoice.type == 'out_invoice':
                invoice.is_acompte = True


class AccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    invoice_state = fields.Selection(related='invoice_id.state', )
    function_grouping = fields.Boolean(string='Fonction de regroupement', )
    price_subtotal_without_discount = fields.Float(string='Sous total sans remise', digits=dp.get_precision('Account'),
                                                   compute='_compute_price_subtotal_without_discount', )  # FIXME Store
    total_remise = fields.Float(string='Total Remise', digits=dp.get_precision('Account'), compute='_compute_price_subtotal_without_discount', )  # FIXME Store

    code_tva_gle = fields.Char("Code Tva Les Echos", compute='_compute_code_tva_gle_invoice', store=True)

    analytic_dimensions = fields.Char("Analytic Dimensions")

    @api.multi
    @api.depends('discount', 'discount_type', 'price_subtotal')
    def _compute_price_subtotal_without_discount(self):
        for obj in self:
            price_subtotal_without_discount = obj.price_subtotal
            if obj.discount:
                if obj.discount_type == 'absolut':
                    price_subtotal_without_discount = obj.price_subtotal + obj.discount
                else:

                    if obj.discount != 100:
                        price_subtotal_without_discount = obj.price_subtotal / (1 - (obj.discount / 100.))
                    else:
                        price_subtotal_without_discount = obj.price_subtotal

            obj.price_subtotal_without_discount = price_subtotal_without_discount
            obj.total_remise = price_subtotal_without_discount - obj.price_subtotal if price_subtotal_without_discount else 0


    @api.depends('invoice_line_tax_id')
    def _compute_code_tva_gle_invoice(self):
        """"""
        i = 0
        for line in self:
            for tax_single in line.invoice_line_tax_id:
                i += 1
                xsearch_code_tva_gle = self.env['account.tax'].search([('id', '=', tax_single.id)])
                xreturn_code_tva_gle = xsearch_code_tva_gle.code_tva_gle
        if i > 1 or i == 0:
            line.code_tva_gle = ''
        else:
            if xreturn_code_tva_gle:
                line.code_tva_gle = xreturn_code_tva_gle
            else:
                line.code_tva_gle = ''

