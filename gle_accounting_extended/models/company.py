# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)

class ResCompany(models.Model):
    _inherit = 'res.company'

    annotation_reglement = fields.Text(string='Annotation règlement',
                                       default="""Merci d’envoyer votre règlement à l’adresse suivante : « Les Echos Solutions, Service Comptabilité Clients, 16 rue du Quatre Septembre 75112 Paris Cedex 2""", )
    annotation_penalite_retard = fields.Text(string='Annotation pénalités de retard',
                                             default="""Taux d’intérêt de la BCE majoré de 10 points de pourcentage et une indémnité pour frais de recouvrement d’un montant de 40 euros. Escompte pour paiement anticipé : néant.""")

    @api.multi
    def action_account_config(self):

        view_id = self.env['ir.model.data'].get_object_reference('account', 'view_account_config_settings')
        _logger.debug(view_id)
        return {
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'account.config.settings',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
