# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import Warning as UserError
from openerp import workflow

import logging

_logger = logging.getLogger(__name__)


class SaleOrderLineMakeInvoice(models.TransientModel):
    _inherit = 'sale.order.line.make.invoice'

    wording_grouping = fields.Char(string='Libellé acompte', size=64, )

    @api.model
    def _prepare_invoice(self, order, lines):
        res = super(SaleOrderLineMakeInvoice, self)._prepare_invoice(order, lines)
        res = order._update_invoice_header(res)
        res['advance_payment_method'] = 'lines'

        search_payment_term_id = self.env['account.payment.term'].search([('name', '=', 'A réception de facture')]).id
        res['payment_term'] = search_payment_term_id
        return res

    def make_invoices(self, cr, uid, ids, context=None):
        """
             To make invoices.

             @param self: The object pointer.
             @param cr: A database cursor
             @param uid: ID of the user currently logged in
             @param ids: the ID or list of IDs
             @param context: A standard dictionary

             @return: A dictionary which of fields with values.

        """
        if context is None: context = {}
        ctx = dict(context, function_grouping=True)
        res = False
        invoices = {}
        wizard = self.browse(cr, uid, ids[0], context=context)

        # TODO: merge with sale.py/make_invoice
        def make_invoice(order, lines):
            """
                 To make invoices.

                 @param order:
                 @param lines:

                 @return:

            """
            inv = self._prepare_invoice(cr, uid, order, lines)
            _logger.debug(inv)
            inv['wording_grouping'] = wizard.wording_grouping
            inv_id = self.pool.get('account.invoice').create(cr, uid, inv)
            return inv_id

        sales_order_line_obj = self.pool.get('sale.order.line')
        sales_order_obj = self.pool.get('sale.order')



        xline_total = 0
        for line in sales_order_line_obj.browse(cr, uid, context.get('active_ids', []),context=context):
            xline_total += 1
            xorder_id = line.order_id

        xtotal_linesnotinvoiced = self.pool.get('sale.order.line').search_count(cr, uid,[('order_id', '=', xorder_id.id),('invoiced', '=', False)],context=context)
        xline_selected = 0
        for line in sales_order_line_obj.browse(cr, uid, context.get('active_ids', []), context=context):
            if (not line.invoiced) and (line.state not in ('draft', 'cancel')):
                xline_selected += 1
        if xtotal_linesnotinvoiced == xline_selected:
            raise UserError(_('Vous ne pouvez pas faire une facture d\'accompte de toutes les lignes du bon de commande.'))



        for line in sales_order_line_obj.browse(cr, uid, context.get('active_ids', []), context=context):
            if (not line.invoiced) and (line.state not in ('draft', 'cancel')):
                if not line.order_id in invoices:
                    invoices[line.order_id] = []
                line_id = sales_order_line_obj.invoice_line_create(cr, uid, [line.id], context=ctx)
                for lid in line_id:
                    invoices[line.order_id].append(lid)
        for order, il in invoices.items():
            res = make_invoice(order, il)
            cr.execute('INSERT INTO sale_order_invoice_rel \
                    (order_id,invoice_id) values (%s,%s)', (order.id, res))
            sales_order_obj.invalidate_cache(cr, uid, ['invoice_ids'], [order.id], context=context)
            flag = True
            sales_order_obj.message_post(cr, uid, [order.id], body=_("Invoice created"), context=context)
            data_sale = sales_order_obj.browse(cr, uid, order.id, context=context)
            for line in data_sale.order_line:
                if not line.invoiced and line.state != 'cancel':
                    flag = False
                    break
            if flag:
                line.order_id.write({'state': 'progress'})
                workflow.trg_validate(uid, 'sale.order', order.id, 'all_lines', cr)

        if not invoices:
            raise osv.except_osv(_('Warning!'), _(
                'Invoice cannot be created for this Sales Order Line due to one of the following reasons:\n1.The state of this sales order line is either "draft" or "cancel"!\n2.The Sales Order Line is Invoiced!'))
        if context.get('open_invoices', False):
            return self.open_invoices(cr, uid, ids, res, context=context)
        return {'type': 'ir.actions.act_window_close'}
