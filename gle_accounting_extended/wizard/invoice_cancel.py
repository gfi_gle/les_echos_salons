# -*- coding: utf-8 -*-

from openerp import models, fields, api, _


class AccountInvoiceCancel(models.TransientModel):
    _name = 'account.invoice.cancel'
    _description = 'Cancel invoice'

    invoice_id = fields.Many2one('account.invoice', string='Invoice',
                                 default=lambda self: self.env.context.get('active_id', False), )
    comment = fields.Text(string='Commentaire', )

    @api.multi
    def action_continue(self):
        self.ensure_one()
        origins = [] if not self.invoice_id.origin else self.invoice_id.origin.strip().split(',')
        purchase_order_lines = self.env['purchase.order.line'].search(
            [('order_id.name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)]).filtered(
            lambda r: r.salon_id in self.invoice_id.invoice_line.mapped('salon_id'))
        sale_orders = self.env['sale.order'].search(
            [('name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)])
        devis = self.env['gle.account.refund'].search(
            [('invoice_new_id', '=', self.invoice_id.id), ('move_id', '!=', False), ('move_id.extourne', '=', False)])
        if self.invoice_id.type == 'out_refund' and devis:
            self.invoice_id.extourne_aae(devis)
        if self.invoice_id.type == 'out_invoice' and sale_orders:
            self.invoice_id.extourne_fae(sale_orders)
        if self.invoice_id.type == 'in_invoice' and purchase_order_lines:
            self.invoice_id.extourne_fnp(purchase_order_lines)
        # self.invoice_id.signal_workflow('invoice_cancel')
        self.invoice_id.cancel_comment = self.comment
        self.invoice_id.state = 'cancel'
        self.invoice_id.tsc_action_cancel()