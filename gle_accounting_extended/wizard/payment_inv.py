# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp.exceptions import Warning as UserError
import logging

_logger = logging.getLogger(__name__)


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = 'sale.advance.payment.inv'

    product_id = fields.Many2one('product.product', string='Article d\'acompte',
                                 default=lambda self: self.env['product.product'].search([('for_acompte', '=', True)],
                                                                                         limit=1), )

    @api.multi
    def create_invoices(self):
        self.ensure_one()
        self = self.with_context(advance_payment_method=self.advance_payment_method)
        sales = self.env['sale.order'].browse(self.env.context.get('active_ids', []))

        amount_total = sum([sale.amount_total for sale in sales])
        amount_total_ht = sum([sale.amount_untaxed for sale in sales])

        if not sales.partner_id.partner_info_id.code or not sales.partner_id.operational_client_account_id:
            raise UserError(_('Merci de configurer un matricule et un compte comptable.'))

        if self.advance_payment_method == 'fixed':
            if self.amount >= amount_total_ht:
                raise UserError(_('Le montant de l\'acompte est erroné,\n il doit etre infèrieur au montant hors taxe de la facture.'))
        if self.advance_payment_method == 'percentage':
            if self.amount == 100:
                raise UserError(_('L\'acompte ne peut pas etre de 100%.'))
        if self.advance_payment_method == 'lines':

            for sale in sales:
                sale_lines_count = self.env['sale.order.line'].search_count([('order_id', '=', sale.id)])
                sale_lines_count_verif_deja_facturer = self.env['sale.order.line'].search_count([('order_id', '=', sale.id),('invoiced', '=', False)])

                if sale_lines_count == 1:
                    raise UserError(_('Le bon de commande ne dispose que d\'une ligne, \nvous ne pouvez pas faire une facture d\'accompte via ce menu,\n merci d\'utiliser la méthode par pourcentage et/ou par prix fixe.'))
                if sale_lines_count_verif_deja_facturer == 1:
                    raise UserError(_('Il ne reste plus qu\'une seule ligne facturable dans ce bon de commande, \nvous ne pouvez pas faire une facture d\'accompte via ce menu,\n merci d\'utiliser la méthode par pourcentage et/ou par prix fixe.'))

        res = super(SaleAdvancePaymentInv, self).create_invoices()

        return res

    @api.multi
    def _prepare_advance_invoice_vals(self):

        res = super(SaleAdvancePaymentInv, self)._prepare_advance_invoice_vals()

        for i, (sale_id, data) in enumerate(res):
            sale = self.env['sale.order'].browse(sale_id)
            res[i][1].update(sale._update_invoice_header(res[i][1]))
            payment_term = self.env.ref('gle_accounting_extended.payment_term_immediate_a_reception')
            res[i][1].update({
                'date_invoice': fields.Date.context_today(self),
                'payment_term': payment_term and payment_term.id or False,
                'advance_payment_method': self.advance_payment_method,
            })
        return res

    def onchange_method(self, cr, uid, ids, advance_payment_method, product_id, context=None):
        return {}
