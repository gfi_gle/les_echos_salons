# -*- coding: utf-8 -*-


from openerp import models, fields, _
from openerp.report import report_sxw
import re
import logging

_logger = logging.getLogger(__name__)

class AccountInvoiceParser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(AccountInvoiceParser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'inline_address': self._inline_address,
            'inline_address_end': self._inline_address_end,
            'partner_company': self._partner_company,
            'partner_invoices': self._partner_invoices,
            'tax_rate': self._tax_rate,
            'tax_rate_from_name': self._tax_rate_from_name,
            'payment_term': self._payment_term,
            'count_diff_tva': self._count_diff_tva,
            'req_grouped_lines': self._req_grouped_lines,
            'req_grouped_lines_tva_name': self._req_grouped_lines_tva_name,
        })

    def _count_diff_tva(self):
        xid = str(self.id)
        _logger.debug("----------------------------------- ID invoice")
        _logger.debug(xid)
        return xid

    def _req_grouped_lines(self,account_invoice):
        # xreq = 'SELECT t.tax_id, SUM(a.price_subtotal), SUM(a.price_unit) FROM account_invoice_line a, account_invoice_line_tax t WHERE a.invoice_id = {} AND a.id = t.invoice_line_id AND a.function_grouping = TRUE GROUP BY t.tax_id;'
        xreq = 'SELECT SUM(a.price_subtotal), SUM(a.price_unit), SUM(a.price_unit*a.quantity), t.tax_id FROM account_invoice_line a, account_invoice_line_tax t WHERE a.invoice_id = {} AND a.id = t.invoice_line_id AND a.function_grouping = TRUE GROUP BY t.tax_id;'
        self.cr.execute(xreq.format(account_invoice.id))
        xreturn_req = self.cr.fetchall()
        res = []
        for xreturn in xreturn_req:
             res.append(xreturn)
        return res

    def _req_grouped_lines_tva_name(self, tva_id):
        """ """
        xreq = 'SELECT amount FROM account_tax WHERE id = {};'
        self.cr.execute(xreq.format(tva_id))
        xreturn_req = self.cr.fetchall()
        res = []
        for xreturn in xreturn_req:
            for xtva in xreturn:
                res.append(xtva)
        return res


    def _inline_address(self, partner):
        if not partner:
            return ''
        tab = [partner.street, partner.street2, partner.street3, partner.country_id and partner.country_id.name or '',
               partner.state_id and partner.state_id.name or '',]
        return ', '.join([x for x in tab if x])

    def _inline_address_end(self, partner):
        if not partner:
            return ''
        tab = [partner.zip, partner.city or '']
        return ', '.join([x for x in tab if x])

    def _partner_company(self, partner):
        return partner.parent_id if partner.parent_id else partner

    def _partner_invoices(self, partner):
        return partner.mapped('child_ids').filtered(lambda r: r.type == 'invoice')

    def _tax_rate(self, tax_line):
        percentages = ["%s %s" % (int(x.amount * 100), '%') for x in tax_line if x.type == 'percent']
        fixed = [x.amount for x in tax_line if x.type == 'fixed']
        nones = ['-' for x in tax_line if x.type == 'none']
        nones = nones and nones[0] or []
        rates = percentages + fixed + nones
        return ', '.join(rates)

    def _tax_rate_from_name(self, name):
        print name
        tab = re.findall(r"[-+]?\d*\,\d+|\d+", name)
        tab[0] += '%'
        return tab[0].replace('%', ' %') if tab else name

    def _payment_term(self, o):
        if not o.payment_term or not o.date_invoice:
            return []
        res = []
        elements = o.payment_term.compute(o.amount_total, o.date_invoice)

        elements = elements and elements[0] or elements

        if not elements:
            for due_date, amount in elements:
                res.append(_('Montant %s %s avant %s') % (
                amount, o.currency_id.symbol, fields.Date.from_string(due_date).strftime('%d/%m/%Y')))
        return res






class ReportAccountInvoiceParser(models.AbstractModel):
    _name = 'report.gle_accounting_extended.report_facture'
    _inherit = 'report.abstract_report'
    _template = 'gle_accounting_extended.report_facture'
    _wrapped_report_class = AccountInvoiceParser
