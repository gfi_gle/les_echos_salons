# -*- coding: utf-8 -*-
{
    "name": 'GLE Accounting Extended',
    "version": "8.0.0.1.0",
    "depends": [
        'gle_accounting',
        'gle_report',
        'gle_numerotation',
        'gfi_payment_term_extension',
        'account',
        'purchase',
    ],
    'license': 'AGPL-3',
    'update_xml': [],
    'data': [
        'data/payment_term.xml',
        'wizard/invoice_cancel.xml',
        'wizard/make_invoices.xml',
        'wizard/payment_inv.xml',
        'views/view.xml',
        'views/company.xml',
        'views/account_invoice.xml',
        'views/purchase_order_line.xml',
        'views/account_tax.xml',
        'report/report_facture_table.xml',
        'report/report_facture_draft.xml',
        'report/report_facture_proforma.xml',
        'report/report_facture_acompte.xml',
        'report/report_facture_avoir.xml',
        'report/report_facture.xml',
        'report/report.xml',
        'security/make_invisible.xml'
    ],

    "author": "GFI",
    "webiste": "http://gfi.world",
    "installable": True,
}

