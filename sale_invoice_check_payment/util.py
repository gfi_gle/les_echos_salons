

def extract(obj):
    if isinstance(obj, list):
        obj = obj[0]
    return obj


def extract_field_value_from_write_dictionary(obj, vals, field):

    if field in vals.keys():
        result = vals[field]
    elif obj is not None:
        result = getattr(obj, field)
    else:
        result = False

    return result


def extract_fields_values_from_write_dictionary(obj, vals, fields):

    fields_dict = {}

    for field in fields:
        fields_dict[field] = (
            extract_field_value_from_write_dictionary(obj, vals, field)
        )

    return fields_dict
