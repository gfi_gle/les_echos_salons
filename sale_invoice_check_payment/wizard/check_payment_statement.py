# -*- encoding: utf-8 -*-

from openerp import api
from openerp import fields
from openerp import models
from openerp import exceptions
from openerp import _

import openerp.addons.decimal_precision as dp

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

from datetime import datetime


class CheckPaymentStatement(models.TransientModel):
    """Gather check trackers in batches.
    """

    _name = 'sale.invoice_check_tracker.payment_statement'
    _description = 'Check payment statement'

    @api.multi
    def get_check_trackers(self):

        date = datetime.strptime(
            self.date, DEFAULT_SERVER_DATE_FORMAT)
        batch_date = datetime.strftime(date, '%Y-%m-%d 23:59:59')

        return self.env['sale.invoice_check_tracker'].search(
            [
                ('batch_id', '=', False),
                ('state', '=', 'validated'),
                ('date', '<=', batch_date),
            ]
        )

    payment_method_id = fields.Many2one(
        comodel_name='payment.mode',
        string='Payment method',
        help=(
            'The payment method on which account will be registered the '
            'payment statement.'
        ),
        required=True,
    )

    date = fields.Date(
        string='Date',
        help=(
            'Check date, up to which all non-registered checks will be '
            'registered on the payment statement.'
        ),
        required=True,
    )

    @api.multi
    def generate(self):

        self.ensure_one()

        name_format = _(u'CheckBatch-%Y%m%d%H%M%S')

        check_tracker_ids = self.get_check_trackers().ids

        batch_id = self.env['sale.invoice_check_tracker.batch'].create({
            'number': datetime.now().strftime(name_format.encode('utf-8')),
            'check_tracker_ids': [(6, 0, check_tracker_ids)],
            'payment_method_id': self.payment_method_id.id,
            'date': self.date,
        })

        return {
            'type': 'ir.actions.act_window',
            'name': _('Check payment statements'),
            'res_model': 'sale.invoice_check_tracker.batch',
            'res_id': batch_id.id,
            'view_mode': 'form',
            'view_type': 'form',
        }
