import time

from datetime import datetime, date, timedelta

import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid, randomString

from openerp import fields
from openerp import exceptions

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_account(self):
        """Create accounting account for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        parent_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(parent_account)

        self.memory.account, = self.createAndTest(
            'account.account',
            [{
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            }],
        )

    def test_0001_create_partners_and_dates(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'customer': True,
                    'name': genUuid(),
                }
                for i in range(3)
            ],
        )

        self.memory.tomorrow = date.today() + timedelta(days=1)

#         self.memory.tomorrow = \
#             datetime.strftime(tomorrow, DEFAULT_SERVER_DATE_FORMAT)

    def test_0002_create_payment_methods(self):
        """Create payment methods and store them for further use.
        """

        # Find accounting journals.
        payment_journal = self.env['account.journal'].search(
            [('type', '=', 'bank')], limit=1,
        )
        self.assertTrue(payment_journal)

        self.memory.payment_methods = self.createAndTest(
            'crm.payment.mode',
            [
                {
                    'check_tracked': True,
                    'name': genUuid(),
                    'check_tracked_payment_journal_id': payment_journal.id,
                    'technical_name': genUuid(),
                },
                {
                    'check_tracked': False,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                },
            ],
        )

        self.env['ir.property'].create({
            'fields_id': self.env.ref(
                'sale_invoice_check_payment.'
                'field_sale_invoice_check_tracker_'
                'check_tracker_payment_method_id'
            ).id,
            'name': 'check_tracker_payment_method_id',
            'type': 'many2one',
            'value_reference': 'crm.payment.mode,%d' % (
                self.memory.payment_methods[0].id
            ),
        })

        code = genUuid()

        self.createAndTest(
            'res.partner.bank.type',
            [
                {
                    'name': genUuid(),
                    'code': code,
                }
            ]
        )

        bank = self.createAndTest(
            'res.bank',
            [
                {
                    'name': genUuid(),
                    'bic': randomString(11),
                }
            ]
        )[0]

        self.memory.bank_account = self.createAndTest(
            'res.partner.bank',
            [
                {
                    'state': code,
                    'acc_number': genUuid(),
                    'bank': bank.id,
                    'bank_name': genUuid(),
                    'bank_bic': randomString(11),
                    'partner_id': self.memory.partners[2].id,
                }
            ]
        )[0]

        self.memory.payment_mode = self.createAndTest(
            'payment.mode',
            [
                {
                    'name': genUuid(),
                    'bank_id': self.memory.bank_account.id,
                    'journal': payment_journal.id,
                }
            ]
        )[0]

    def test_0003_create_products(self):
        """Create products for use in further tests.
        """

        # Create in 2 steps to avoid field propagation issues between products
        # and their templates.
        product_templates = self.createAndTest(
            'product.template',
            [
                {
                    'name': genUuid(),
                    'type': 'service',
                },
            ],
        )
        self.memory.products = self.createAndTest(
            'product.product',
            [
                {'product_tmpl_id': product_template.id}
                for product_template in product_templates
            ],
        )

    def test_0010_create_sales_orders_and_validate_invoices(self):
        """Create sales orders and store them for further use; validate
        resulting invoices in the process.
        """

        def buildSalesOrder():
            return {
                'order_policy': 'prepaid',  # To generate an invoice.
                'partner_id': self.memory.partners[0].id,
                'payment_method_id': self.memory.payment_methods[0].id,
            }

        # Create the sales orders.
        self.memory.sales_orders = self.createAndTest(
            'sale.order',
            [
                buildSalesOrder()
                for i in range(4)
            ] + [
                {
                    'order_policy': 'prepaid',  # To generate an invoice.
                    'partner_id': self.memory.partners[0].id,
                    'payment_method_id': self.memory.payment_methods[1].id,
                },
                {
                    'order_policy': 'prepaid',  # To generate an invoice.
                    'partner_id': self.memory.partners[1].id,
                    'payment_method_id': self.memory.payment_methods[0].id,
                },
                buildSalesOrder(),
            ]
        )

        # Add lines into the sales orders.
        for sales_order in self.memory.sales_orders:
            self.createAndTest(
                'sale.order.line',
                [
                    {
                        'name': genUuid(),
                        'order_id': sales_order.id,
                        'price_unit': 100.0,
                        'product_uom_qty': 1.0,
                        'product_id': self.memory.products[0].id,
                    },
                ],
            )

        self.memory.invoices = []
        self.memory.refunds = []

        for i, sales_order in enumerate(self.memory.sales_orders):
            # Validate sales orders.
            self.assertEqual(sales_order.state, 'draft')
            sales_order.signal_workflow('order_confirm')
            self.assertEqual(sales_order.state, 'progress')

            # Validate invoices.
            invoice = sales_order.invoice_ids
            self.assertTrue(invoice)
            self.assertEqual(invoice.state, 'draft')

            if i < 6:
                invoice.signal_workflow('invoice_open')
                self.assertEqual(invoice.state, 'open')

            if i in [2, 3]:

                wizard = (
                    self.env['account.invoice.refund'].
                    with_context(active_ids=[invoice.id]).
                    create({
                        'description': genUuid(),
                        'filter_refund': 'refund',
                    })
                )

                refund = self.env['account.invoice'].search(
                    wizard.invoice_refund()['domain']
                )[0]

                self.assertTrue(refund)
                self.assertEqual(refund.state, 'draft')

                # Write the sales payment method of refunds.
                refund.sales_payment_method_id = \
                    self.memory.payment_methods[0].id

                refund.signal_workflow('invoice_open')
                self.assertEqual(refund.state, 'open')
                self.memory.refunds.append(refund)
            else:
                self.memory.invoices.append(invoice)

    def test_0020_validation_errors_on_check_trackers_creation(self):
        """Tests validation errors on check tracker creation.
        """

        invoice_below_gap = self.memory.invoices[0]
        invoice_above_gap = self.memory.invoices[1]
        invalid_invoice = self.memory.invoices[2]
        other_client_invoice = self.memory.invoices[3]
        draft_invoice = self.memory.invoices[4]

        # No validation error must occur, if a non tracked payment method is
        # specified.
        self.createAndTest(
            'sale.invoice_check_tracker',
            [
                {  # tracker_below_gap
                    'invoice_type': 'out_invoice',
                    'check_number': genUuid(),
                    'bank_name': genUuid(),
                    'check_holder': genUuid(),
                    'date': fields.date.today(),
                    'tracker_line_ids': [
                        (0, 0, {
                            'invoice_id': invalid_invoice.id,
                            'amount_retained': 100.0,
                            # < default allowed gap (50.0)
                        })
                    ],
                },
            ],
            custom_testers={
                'tracker_line_ids': invoiceTester,
                'date': dateTester,
            },
        )

        # A validation error must be casted, if the invoice is not of the type
        # of invoice, which has been selected.
        with self.assertRaises(exceptions.ValidationError):
            self.createAndTest(
                'sale.invoice_check_tracker',
                [
                    {  # tracker_below_gap
                        'invoice_type': 'out_refund',
                        'check_number': genUuid(),
                        'bank_name': genUuid(),
                        'check_holder': genUuid(),
                        'date': fields.date.today(),
                        'tracker_line_ids': [
                            (0, 0, {
                                'invoice_id': invoice_above_gap.id,
                                'amount_retained': 100.0,
                                # < default allowed gap (50.0)
                            })
                        ],
                    },
                ],
                custom_testers={
                    'tracker_line_ids': invoiceTester,
                    'date': dateTester,
                },
            )

        # A validation error must be casted, if the invoices are not of the
        # same client.
        with self.assertRaises(exceptions.ValidationError):
            self.createAndTest(
                'sale.invoice_check_tracker',
                [
                    {  # tracker_below_gap
                        'invoice_type': 'out_invoice',
                        'check_number': genUuid(),
                        'bank_name': genUuid(),
                        'check_holder': genUuid(),
                        'date': fields.date.today(),
                        'tracker_line_ids': [
                            (0, 0, {
                                'invoice_id': invoice_below_gap.id,
                                'amount_retained': 100.0,
                                # < default allowed gap (50.0)
                            }),
                            (0, 0, {
                                'invoice_id': other_client_invoice.id,
                                'amount_retained': 100.0,
                                # < default allowed gap (50.0)
                            }),
                        ],
                    },
                ],
                custom_testers={
                    'tracker_line_ids': invoiceTester,
                    'date': dateTester,
                },
            )

        # A validation error must be casted, if the invoice is not of the
        # client, who has been specified.
        with self.assertRaises(exceptions.ValidationError):
            self.createAndTest(
                'sale.invoice_check_tracker',
                [
                    {  # tracker_below_gap
                        'client_id': self.memory.partners[0].id,
                        'invoice_type': 'out_invoice',
                        'check_number': genUuid(),
                        'bank_name': genUuid(),
                        'check_holder': genUuid(),
                        'date': fields.date.today(),
                        'tracker_line_ids': [
                            (0, 0, {
                                'invoice_id': other_client_invoice.id,
                                'amount_retained': 100.0,
                                # < default allowed gap (50.0)
                            }),
                        ],
                    },
                ],
                custom_testers={
                    'tracker_line_ids': invoiceTester,
                    'date': dateTester,
                },
            )

        # A validation error must be casted, if the invoice is in draft state.
        with self.assertRaises(exceptions.ValidationError):
            self.createAndTest(
                'sale.invoice_check_tracker',
                [
                    {  # tracker_below_gap
                        'client_id': self.memory.partners[0].id,
                        'invoice_type': 'out_invoice',
                        'check_number': genUuid(),
                        'bank_name': genUuid(),
                        'check_holder': genUuid(),
                        'date': fields.date.today(),
                        'tracker_line_ids': [
                            (0, 0, {
                                'invoice_id': draft_invoice.id,
                                'amount_retained': 100.0,
                                # < default allowed gap (50.0)
                            }),
                        ],
                    },
                ],
                custom_testers={
                    'tracker_line_ids': invoiceTester,
                    'date': dateTester,
                },
            )

    def test_0021_create_sales_check_trackers(self):
        """Create a sales check tracker.
        """

        invoice_below_gap = self.memory.invoices[0]
        invoice_above_gap = self.memory.invoices[1]

        refund_below_gap = self.memory.refunds[0]
        refund_above_gap = self.memory.refunds[1]

        (
            self.memory.tracker_below_gap,
            self.memory.tracker_above_gap,
            self.memory.tracker_refund_below_gap,
            self.memory.tracker_refund_above_gap,
        ) = self.createAndTest(
            'sale.invoice_check_tracker',
            [
                {  # tracker_below_gap
                    'invoice_type': 'out_invoice',
                    'check_number': genUuid(),
                    'bank_name': genUuid(),
                    'check_holder': genUuid(),
                    'date': fields.date.today(),
                    'tracker_line_ids': [
                        (0, 0, {
                            'invoice_id': invoice_below_gap.id,
                            'amount_retained': 90.0,
                            # < default allowed gap (50.0)
                        })
                    ],
                },
                {  # tracker_above_gap
                    'invoice_type': 'out_invoice',
                    'check_number': genUuid(),
                    'bank_name': genUuid(),
                    'check_holder': genUuid(),
                    'date': fields.date.today(),
                    'tracker_line_ids': [
                        (0, 0, {
                            'invoice_id': invoice_above_gap.id,
                            'amount_retained': 40.0,
                            # > default allowed gap (50.0)
                        })
                    ],
                },
                {  # tracker_below_gap
                    'invoice_type': 'out_refund',
                    'check_number': genUuid(),
                    'bank_name': genUuid(),
                    'check_holder': genUuid(),
                    'date': fields.date.today(),
                    'tracker_line_ids': [
                        (0, 0, {
                            'invoice_id': refund_below_gap.id,
                            'amount_retained': 70.0,
                            # < default allowed gap (50.0)
                        })
                    ],
                },
                {  # tracker_above_gap
                    'invoice_type': 'out_refund',
                    'check_number': genUuid(),
                    'bank_name': genUuid(),
                    'check_holder': genUuid(),
                    'date': self.memory.tomorrow,
                    'tracker_line_ids': [
                        (0, 0, {
                            'invoice_id': refund_above_gap.id,
                            'amount_retained': 40.0,
                            # > default allowed gap (50.0)
                        })
                    ],
                },
            ],
            custom_testers={
                'tracker_line_ids': invoiceTester,
                'date': dateTester,
            },
        )

        # Ensure a client has been deduced.
        client = self.memory.partners[0]
        self.assertEqual(self.memory.tracker_below_gap.client_id.id, client.id)
        self.assertEqual(self.memory.tracker_above_gap.client_id.id, client.id)
        self.assertEqual(
            self.memory.tracker_refund_below_gap.client_id.id, client.id
        )
        self.assertEqual(
            self.memory.tracker_refund_above_gap.client_id.id, client.id
        )

    def test_0022_validate_check_trackers(self):
        """Validate check trackers and ensure the effects on accounting
        invoices are as expected.
        """

        # The trackers below the allowed gap should directly become validated.
        for label in ['', 'refund_']:
            tracker = getattr(self.memory, 'tracker_%sbelow_gap' % label)
            self.assertEqual(tracker.state, 'draft')
            tracker.signal_workflow('validate')
            self.assertEqual(tracker.state, 'validated')

            # The trackers above the allowed gap should need 2 validations.
            tracker = getattr(self.memory, 'tracker_%sabove_gap' % label)
            self.assertEqual(tracker.state, 'draft')
            tracker.signal_workflow('validate')
            self.assertEqual(tracker.state, 'amount_gap_validation')
            tracker.tracker_line_ids.amount_retained = 100.0
            # < default allowed gap (50.0)
            tracker.signal_workflow('validate')
            self.assertEqual(tracker.state, 'validated')

        for i, invoice in enumerate(self.memory.invoices):
            if i == 1:
                self.assertEqual(invoice.state, 'paid')
            elif i < 4:
                self.assertEqual(invoice.state, 'open')
            else:
                self.assertEqual(invoice.state, 'draft')

        for i, refund in enumerate(self.memory.refunds):
            if i == 1:
                self.assertEqual(refund.state, 'paid')
            else:
                self.assertEqual(refund.state, 'open')

        # Sales orders should have noticed that and should now be "done".
        for i, sales_order in enumerate(self.memory.sales_orders):
            if i == 1:
                self.assertEqual(sales_order.state, 'done')

    def test_0033_print_payment_statement(self):
        """Print the payment statement, gathering the generated check trackers.
        """

        wizard = self.createAndTest(
            'sale.invoice_check_tracker.payment_statement',
            [
                {
                    'payment_method_id': self.memory.payment_mode.id,
                    'date': fields.date.today(),
                }
            ],
            custom_testers={
                'date': dateTester,
            },
        )[0]

        batch_id = wizard.generate()['res_id']

        batch = self.env['sale.invoice_check_tracker.batch'].browse(batch_id)

        self.assertEqual(batch.name, self.env.user.company_id.name)
        self.assertEqual(batch.bank_id.id, self.memory.bank_account.id)

        self.assertEqual(
            batch.check_tracker_ids.ids,
            [
                self.memory.tracker_below_gap.id,
                self.memory.tracker_above_gap.id,
                self.memory.tracker_refund_below_gap.id,
            ]
        )

        self.assertEqual(batch.total_amount, 120.0)


def invoiceTester(test_instance, asked_value, recorded_value):
    test_instance.assertEqual(
        recorded_value.invoice_id.id, asked_value[0][2]['invoice_id']
    )
    test_instance.assertEqual(
        recorded_value.amount_retained,
        asked_value[0][2]['amount_retained']
    )


def dateTester(test_instance, asked_value, recorded_value):
    test_instance.assertEqual(
        recorded_value,
        datetime.strftime(asked_value, DEFAULT_SERVER_DATE_FORMAT)
    )
