# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class CaCommision(models.Model):
    """ Class representing the model ca_commision """

    _name = 'gle_adv.ca_commision'

    commercial_id = fields.Many2one(comodel_name='res.users', string="Commercial")
    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")

    percentage = fields.Float(string="Pourcentage")

    # amount for sale.order
    amount = fields.Float(string="Montant")

    amount_refund = fields.Float(string="Montant")

    commision_type = fields.Selection([('percentage', "Poucentage"), ('absolut', "Absolue")], string="Type de commissionnement")

    def update_amount_by_percentage(self, ca_commissionnable):

        new_amount = ca_commissionnable * (self.percentage / 100)
        self.write({'amount': new_amount})

        return new_amount



