# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    """ Model inherit res.users class """

    _inherit = 'res.users'

    is_commercial_echo_solution = fields.Boolean(string="Commercial les echos solutions", default=False)
    name_gle = fields.Char('name_gle', compute='_compute_name_gle', store=True)
    user_metier = fields.Selection([
        ('adv', 'ADV'),
        ('compta', 'Comptabilité'),
        ('dir', 'Direction'),])

    salon_ids = fields.Many2many(comodel_name='gle_salon.salon',
                                 relation='gle_salon_salon_res_users_rel',
                                 column1='res_users_id',
                                 column2='gle_salon_salon_id')

    @api.model
    def create(self, vals):
        """ Override method create to use method check_unique_commercial_echos_solution
            @:except ValidationError
            @return res.users
        """
        try:
            self.check_unique_commercial_echos_solution(vals)

        except ValidationError:
            raise

        new_obj = super(ResUsers, self).create(vals)

        return new_obj

    @api.multi
    def write(self, vals):
        """ Override method write to use method check_unique_commercial_echos_solution """

        try:
            self.check_unique_commercial_echos_solution(vals)

        except ValidationError:
            raise

        return super(ResUsers, self).write(vals)

    @api.depends('lastname', 'firstname')
    def _compute_name_gle(self):

        for res_user in self:
            res_user.name_gle = "{0} {1}".format(res_user.firstname, res_user.lastname)

    def get_commercial_echos_solutions(self):
        """ Method return the commercial echos solution selected
            @:return res.users
        """

        res_users_record_set = self.env['res.users'].search([('is_commercial_echo_solution', '=', True)])
        res_users_admin = self.env['res.users'].search([('id', '=', 1)])

        if res_users_record_set:
            return res_users_record_set
        else:
            # raise ValidationError("Aucun utilisateur est désigné comme commercial les Echos solution.\n")
            return res_users_admin


    @api.multi
    def check_unique_commercial_echos_solution(self, vals):
        """
            Method to check if a commercial les echos solution is already selected
            If query find a res.users which is commercial echo solution return False else True
            @:except ValidationError
        """

        if vals.get('is_commercial_echo_solution'):
            res_users_record_set = self.env['res.users'].search([('is_commercial_echo_solution', '=', True)])

            if res_users_record_set and not(res_users_record_set.id == self.id):
                raise ValidationError("Un utilisateur est déjà désigné comme commercial Les Echos Solution.")
