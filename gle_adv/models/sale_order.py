# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    """ Class inherit sale.order to add fields and create methods """

    _inherit = 'sale.order'
    _description = "Inherit sale.order to add fields"

    """ Init method """
    def _init_salon_id(self):
        if self._context.get('salon_id'):
            return self._context.get('salon_id')
        else:
            return self.env['gle_salon.salon']

    def _init_stand_id(self):
        if self._context.get('stand_id'):
            return self._context.get('stand_id')
        else:
            return self.env['gle_salon.stand']

    def _init_price_list(self):

        price_list_record = self.env['product.pricelist'].search([('id', '=', 1)])

        return price_list_record

    """ Attributes fields"""
    salon_id = fields.Many2one('gle_salon.salon', string="Salon", default=_init_salon_id)
    stand_id = fields.Many2one('gle_salon.stand', string="Stand", default=_init_stand_id)
    xstart = fields.Char("")
    stand_id_display = fields.Many2one(comodel_name='gle_salon.stand', string="Stand", compute='compute_on_change_stand')

    sale_order_stand_area = fields.Float(related='stand_id.area', store=True)

    sale_order_stand_statut = fields.Selection(related='stand_id.state')

    sale_order_stand_village = fields.Char(related='stand_id.village_id.name', store=True)

    sale_order_stand_communication = fields.Boolean(related='stand_id.display_communication')
    sale_order_stand_type_ids = fields.Many2one(related='stand_id.stand_type_ids')

    contact_client_id = fields.Many2one('res.partner', string="Contact client", help="Nom et prénom")
    contact_business_id = fields.Many2one('res.users', string="Contact les echos Business", help="Nom et prénom")
    wording_grouping = fields.Char(string="Champ libellé de regroupement")
    analytic_metier_id = fields.Many2one('gle_adv.analytic_metier', string="Analytique métier")

    partner_id = fields.Many2one('res.partner', 'Customer')

    optionned_stand = fields.Boolean(default = False, compute='_init_optionned_stand')

    amount_ca_commissionable = fields.Float(compute='_compute_amount_ca_commissionable', string="CA commissionnable")
    amount_ca_no_commissionable = fields.Float(compute='_compute_amount_ca_no_commissionable', string="CA non commissionnable")

    sale_order_annotation_ids = fields.Many2many(comodel_name='gle_adv.sale_order_annotation',
                                                 relation='gle_adv_sale_order_annotation_relation',
                                                 column1='sale_order_id',
                                                 column2='annotation_id', string="Annotation pied de page",
                                                 default = lambda self : self.init_annotation_ids())

    ca_commision_ids = fields.Many2many(comodel_name='gle_adv.ca_commision',
                                        relation='gle_adv_ca_commision_sale_order_relation',
                                        column1='sale_order_id',
                                        column2='ca_commision_id',
                                        default = lambda self : self._default_ca_commision(),
                                        string="Répartition du CA commissionnable")

    commision_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', "Absolue")],
                                      string="Type de commissionnement")

    function_grouping = fields.Boolean(string='function grouping bool', compute='_compute_function_grouping', inverse='_inverse_function_grouping',
                                       store=True, )

    discount_type_invoice = fields.Selection([('line', "Remise / Ligne"), ('total', "Remise global")], string="Type d'affichage")

    commercial_echo_solution_id = fields.Many2one(comodel_name='res.users', string="Commercial les echos solutions", default= lambda self : self._get_echo_commercial_solution())

    """ Override fields """
    pricelist_id = fields.Many2one(comodel_name='product.pricelist', string="Pricelist", required=True, readonly=True,
                                   states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                   help="Pricelist for current sales order.", default = lambda self : self._init_price_list())

    order_line = fields.One2many('sale.order.line', 'order_id', 'Order Lines', readonly=False, copy=True)



    state = fields.Selection([
        ('draft', 'Draft Quotation'),
        ('valid', 'Validé Commercial'),
        ('valid_d', 'Validé Directeur'),
        ('sent', 'Quotation Sent'),
        ('passage_sans_suite', 'Passage sans suite'),
        ('cancel', 'Cancelled'),
        ('waiting_date', 'Waiting Schedule'),
        ('progress', 'Sales Order'),
        ('manual', 'Sale to Invoice'),
        ('shipping_except', 'Shipping Exception'),
        ('invoice_except', 'Invoice Exception'),
        ('done', 'Done'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
                  \nThe exception status is automatically set when a cancel operation occurs \
                  in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
                   but waiting for the scheduler to run on the order date.", select=True)

    discount_rate = fields.Float(compute='compute_discount_rate', string="Taux de remise", store=True)

    user_id = fields.Many2one('res.users', string="Créateur",
                               states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, select=True,
                               track_visibility='onchange')

    @api.onchange('type_facturation')
    def onchange_partner_list(self):
        """"""
        if self.type_facturation == 'intercos':
            self.partner_id = False
            return {
                'domain': {'partner_id': [('intercos', '=', True),('is_company', '=', True),('customer', '=', True)], }}
        else:
            return {
                'domain': {'partner_id': [('intercos', '=', False),('is_company', '=', True),('customer', '=', True)], }}

    @api.multi
    def write(self, vals):
        """ Override to check if user can modify """
        user_id = self.env['res.users'].browse(self.env.uid)
        if user_id.has_group('gle_user_rules.adv_group') or user_id.has_group('base.group_system'):
            """"""
        else:
            if self.state == 'progress' and self.discount_rate>10:
                raise ValidationError("Vous ne disposez pas de droits suffisants pour modifier\nce devis/bdc qui à nécessité une validation directeur.\n Seul l'ADV peut réaliser la modification.")
        return super(SaleOrder, self).write(vals)

    @api.model
    def create(self, vals):

        new_obj = super(SaleOrder, self).create(vals)
        self._associated_sale_order_id_to_ca_commision(new_obj)
        self._clean_table_ca_commision()

        return new_obj

    @api.multi
    @api.depends('order_line.function_grouping')
    def _compute_function_grouping(self):
        for obj in self:
            for line in obj.order_line:
                if line.function_grouping:
                    obj.function_grouping = line.function_grouping
                    break

    @api.one
    def _inverse_function_grouping(self):
        _logger.debug("inverse")

    @api.multi
    def validate_quotation(self):

        if len(self.order_line) == 0 and self.state == 'draft':
            raise ValidationError("Vous devez au selectioner au moins un article pour créer un devis.")

        if self.salon_id.pdf_file_cgv:
            self.state = 'valid'
        else:
            raise ValidationError(
                "Le salon selectionné dans le devis n'a pas les CGV attachées.\n Le devis ne peut donc pas etre validé.")

    @api.multi
    def validate_quotation_director(self):
        if self.salon_id.pdf_file_cgv:

            if not self.salon_id.dir_salon:
                raise ValidationError(
                    "Le devis ne peut pas etre validé.\nAucun directeur du salon n'est renseigné dans la fiche du salon.")

            if self.salon_id.dir_salon.id != self.env.user.id \
                    and self.salon_id.id not in list(self.env.user.salon_ids._ids):
                raise ValidationError(
                    "Vous n'êtes pas autorisé à valider pour ce salon.")

            self.state = 'progress'

        else:
            raise ValidationError("Le salon selectionné dans le devis n'a pas les CGV attachées.\n Le devis ne peut donc pas etre validé.")

    @api.depends('order_line')
    def compute_discount_rate(self):

        total_no_discount = 0
        discount_rate = 0

        for sale_order in self:
            for sale_order_line in sale_order.order_line:
                total_no_discount += (sale_order_line.price_unit * sale_order_line.product_uom_qty)

            if total_no_discount != 0:
                total_sale_order_ht = sale_order.amount_untaxed
                discount_rate = round(100 - (total_sale_order_ht * 100) / total_no_discount, 2)

            sale_order.discount_rate = discount_rate

    @api.onchange('salon_id')
    def empty_orderlines(self):
        """empty lines"""
        self.order_line = [(5,)]

    @api.onchange('type_facturation')
    def empty_orderlines(self):
        """empty lines"""
        self.order_line = [(5,)]

    @api.depends('stand_id')
    def compute_on_change_stand(self):

        self.stand_id_display = self.stand_id

    @api.multi
    def unlink(self):
        """ Unlink method override from sale.order model. Method delete sale_order_id in status res.partner and delete status if no other record is found for this res.partner """

        query_delete = 'DELETE FROM gle_crm_status_partner_sale_order_rel WHERE sale_order_id = {};'

        status_partner_obj = self.env['gle_crm.status_partner_models']

        for sale_order in self:
            result_recordset = status_partner_obj.search([('partner_id', '=', sale_order.partner_id.id), ('salon_id', '=', sale_order.salon_id.id)])

            length_sale_order_ids = len(result_recordset.sale_order_id)
            length_crm_lead_ids = len(result_recordset.crm_lead_id)
            length_account_invoice = len(result_recordset.account_invoice_id)

            xcheck_name_quotation = str(sale_order.name)
            account_invoice_recordset = self.env['account.invoice'].search([('reference', '=', xcheck_name_quotation)])

            if account_invoice_recordset:
                if account_invoice_recordset.internal_number:
                    xfetchinvoice_name = str(account_invoice_recordset.internal_number)
                    raise ValidationError("Ce devis a fait l'objet d'une facture et ne peut etre supprimé.\nNuméro de facture(s) concernée(s): "+xfetchinvoice_name)

            else:
                if length_sale_order_ids > 1:
                    self.env.cr.execute(query_delete.format(sale_order.id))
                elif length_sale_order_ids == 1 and length_crm_lead_ids == 0 and length_account_invoice == 0:
                    status_partner_obj.search([('id', '=', result_recordset.id)]).unlink()

                super(SaleOrder, sale_order).unlink()

    @api.multi
    def action_pss(self):
        """ Extends the action_pss from gle_accounting\models\sale_order.py -- action_pss for sale orders """

        for sale_order in self:
            xcheck_name_quotation = str(sale_order.name)

            account_invoice_recordset = self.env['account.invoice'].search([('reference', '=', xcheck_name_quotation)])

            if account_invoice_recordset:
                if account_invoice_recordset.internal_number:
                    xfetchinvoice_name = account_invoice_recordset.internal_number
                    raise ValidationError("Ce devis a fait l'objet d'une facture et ne peut pas etre passé sans suite.\nNuméro de facture(s) concernée(s): " + xfetchinvoice_name)
            else:
                super(SaleOrder, sale_order).action_pss()

    def search_status_partner(self, new_obj):
        """ Method to search status of res.partner associated to this sale.order """

        query_insert = 'INSERT INTO gle_crm_status_partner_sale_order_rel (status_partner_id, sale_order_id) VALUES ({}, {});'

        if new_obj.partner_id.id:
            status_partner_obj = self.env['gle_crm.status_partner_models']
            result_recordset = status_partner_obj.search([('partner_id', '=', new_obj.partner_id.id), ('salon_id', '=', new_obj.salon_id.id)])

            if not result_recordset:
                vals = {
                    'salon_id': new_obj.salon_id.id,
                    'partner_id': new_obj.partner_id.id,
                    'partner_type': 'prospect'
                }

                new_status_obj = status_partner_obj.create(vals)
                self.env.cr.execute(query_insert.format(new_status_obj.id, new_obj.id))

            elif result_recordset:
                self.env.cr.execute(query_insert.format(result_recordset.id, new_obj.id))

    @api.multi
    def option_stand(self):
        """ Method triggered by button on sale_order_view, method return wizard to inform required fields for option state """

        if self.stand_id.id:
            stand_id = self.stand_id.id
            sale_order_id = self.id
            view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'salon_stand_option_view_wizard')

            if self.partner_id.id:
                partner_id = self.partner_id.id

            return {
                'name': "Veuillez remplire les champs suivant pour le passage en option du stand.",
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_salon.stand_option_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {'stand_id': stand_id, 'sale_order_id': sale_order_id, 'default_partner_id': partner_id}
            }

    @api.multi
    def option_to_void(self):
        """ Method triggered by button on sale_order_view, method change stand state to 'non attribue' """

        self.stand_id.action_libre_stand()
        self.optionned_stand = False

    @api.multi
    def action_create_invoice_aee(self):
        """ Method triggered by button on sale.order view to load wizard to create gle.account.refund from sale.order """

        if not self.check_amount_account_refund():
            raise ValidationError("Vous ne pouvez pas générer plus de devis AAE.")

        sale_order_id = self.id

        partner_id = None
        if self.partner_id.id:
            partner_id = self.partner_id.id

        view_id = self.env['ir.model.data'].get_object_reference('gle_adv', 'gle_account_refund_wizard_form_view')

        return {
            'name': "Création Devis AAE",
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_adv.gle_account_refund_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_sale_order_id': sale_order_id, 'default_partner_id': partner_id, 'default_salon_id': self.salon_id.id}
        }

    def check_amount_account_refund(self):
        """ Method to check if all account.refund are not upper than invoices invoiced """

        account_invoice_obj = self.env['account.invoice']

        invoice_invoiced_ids = account_invoice_obj.search([('state', 'in', ['open', 'paid']), '|', ('reference', '=', self.name), ('origin', '=', self.name), ('type', '=', 'out_invoice')])
        invoice_refund_record = account_invoice_obj.search([('state', 'in', ['open', 'paid']), '|', ('reference', '=', self.name), ('origin', '=', invoice_invoiced_ids.mapped('internal_number')), ('type', '=', 'out_refund')])

        if invoice_refund_record:

            amount_refund = sum(account_refund.amount_untaxed for account_refund in invoice_refund_record)
            amount_invoiced = abs(sum(account_invoice.amount_untaxed for account_invoice in invoice_invoiced_ids))

            if abs(amount_refund) >= abs(amount_invoiced):

                return False

        return True

    @api.multi
    def cancel_commercial_validation(self):

        self.state = 'draft'

    @api.depends('stand_id')
    def _init_optionned_stand(self):

        for sale_order in self:
            if sale_order.stand_id.state == 'option':
                sale_order.optionned_stand = True
            else:
                sale_order.optionned_stand = False

    def init_annotation_ids(self):

        annotation_record_set = self.env['gle_adv.sale_order_annotation'].search([('is_default', '=', 'true')])

        return annotation_record_set

    def _compute_amount_ca_commissionable(self):
        """ Method to compute value of amount_ca_commissionable """

        ca_commissionnable = 0

        for sale_order in self:
            for sale_order_line in sale_order.order_line:
                if sale_order_line.product_id.commision_state == 'commissionnable':
                    ca_commissionnable += sale_order_line.price_subtotal

            sale_order.update_ca_commision_ids(ca_commissionnable)
            sale_order.amount_ca_commissionable = ca_commissionnable

    def _compute_amount_ca_no_commissionable(self):
        """ Method to compute value of amount_ca_no_commissionable """

        ca_no_commissionnable = 0

        for sale_order in self:
            for sale_order_line in sale_order.order_line:
                if sale_order_line.product_id.commision_state == 'noncommissionnable' or sale_order_line.product_id.commision_state == 'commissionnable_a':
                    ca_no_commissionnable += sale_order_line.price_subtotal

            sale_order.update_ca_no_commissionable(ca_no_commissionnable)
            sale_order.amount_ca_no_commissionable = ca_no_commissionnable

    def update_ca_commision_ids(self, ca_commissionnable):
        """"""

        for ca_commision in self.ca_commision_ids:
            if not ca_commision.commercial_id.is_commercial_echo_solution:
                new_amount = ca_commision.update_amount_by_percentage(ca_commissionnable)

    def update_ca_no_commissionable(self, ca_no_commissionnable):

        for ca_commision in self.ca_commision_ids:
            if ca_commision.commercial_id.is_commercial_echo_solution:
                ca_commision.write({'amount': ca_no_commissionnable})


    @api.multi
    def action_add_commercial_ca_commision_wizard(self):
        """"""

        if not self.commision_type:
            raise ValidationError("Veuillez selectionner un type de commisionnement")

        length_ca_commision_ids = len([ca_commision for ca_commision in self.ca_commision_ids if not ca_commision.commercial_id.is_commercial_echo_solution])
        ca_ids = [ca_commision.id for ca_commision in self.ca_commision_ids if not ca_commision.commercial_id.is_commercial_echo_solution]

        context = {
            'ca_ids': ca_ids,
            'default_sale_order_id': self.id,
            'default_amount_ca_commissionable': self.amount_ca_commissionable,
            'default_commision_type': self.commision_type
        }

        if length_ca_commision_ids == 0 or length_ca_commision_ids == 1:
            return self.launch_ca_commision_wizard(view_id='add_ca_commision_wizard_form_view',
                                                   res_model='gle_adv.add_ca_commsion_wizard', context=context)
        elif length_ca_commision_ids == 2:
            context.update({'commercial_ids': self.get_commercial_ids()})

            return self.launch_ca_commision_wizard(view_id='add_three_ca_commision_wizard_form_view',
                                                   res_model='gle_adv.add_three_ca_commision_wizard', context=context)
        elif length_ca_commision_ids == 3:
            raise ValidationError("Trois commerciaux maximum par CA commissionnable")

    @api.multi
    def delete_ca_commision_wizard(self):

        if len(self.ca_commision_ids) == 1:
            raise ValidationError("Il faut au moin un commercial pour la répartition du commissionement")
        elif not self.commision_type:
            raise ValidationError("Veuillez selectionner un type de commisionnement")

        ca_ids = [ca_commision.id for ca_commision in self.ca_commision_ids]
        context = {
            'ca_ids': ca_ids,
            'default_sale_order_id': self.id,
            'default_commision_type': self.commision_type}

        return self.launch_ca_commision_wizard(view_id='delete_ca_commision_wizard_form_view', res_model='gle_adv.delete_ca_commision_wizard', context=context)

    def launch_ca_commision_wizard(self, view_id, res_model,context={}):
        """"""

        view_id = self.env['ir.model.data'].get_object_reference('gle_adv', view_id)

        return {
            'name': _("Commisionnement CA Devis"),
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': res_model,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context
        }

    def _default_ca_commision(self):
        """"""

        if not self._context.get('stage_type', False) or self._context.get('active_model', False) == 'crm.make.sale':

            user_id = self._context.get('uid')
            commercial_echo_solution = self._get_echo_commercial_solution()

            vals = {
                'commision_type': 'percentage',
                'commercial_id': user_id,
                'percentage': 100,
                'amount': 0
            }

            vals_echo_solution = {
                'commision_type': 'percentage',
                'commercial_id': commercial_echo_solution.id,
                'percentage': 100,
                'amount': 0
            }

            created_solution_ca = self.env['gle_adv.ca_commision'].create(vals_echo_solution)
            created_ca = self.env['gle_adv.ca_commision'].create(vals)

            list = [created_ca.id, created_solution_ca.id]

            return [(6,0,list)]

    def _associated_sale_order_id_to_ca_commision(self, new_obj):
        """"""

        ca_commision_obj = self.env['gle_adv.ca_commision']

        for ca_commision in new_obj.ca_commision_ids:
            record_set = ca_commision_obj.search([('id', '=', ca_commision.id)])

            if record_set.sale_order_id.id is False:
                vals = {
                    'sale_order_id': new_obj.id,
                    'amount': new_obj.amount_ca_commissionable
                }

                record_set.update(vals)

    def _clean_table_ca_commision(self):
        """"""

        ca_commision_obj = self.env['gle_adv.ca_commision']
        ids_to_delete = []

        for ca_commision in ca_commision_obj.search([]):
            if ca_commision.sale_order_id.id is False:
                ids_to_delete.append(ca_commision.id)

        ca_commision_obj.browse(ids_to_delete).unlink()

    def get_commercial_ids(self):
        """"""

        commercial_list_ids = [ca_commision.commercial_id.id for ca_commision in self.ca_commision_ids if not ca_commision.commercial_id.is_commercial_echo_solution]

        return commercial_list_ids

    def action_quotation_send_line(self, cr, uid, ids, context=None):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'gle_adv', 'email_template_edi_sale_line')[1]

        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form_is_company')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def action_quotation_send_total(self, cr, uid, ids, context=None):
        '''
            This function opens a window to compose an email, with the edi sale template message loaded by default
        '''

        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'gle_adv', 'email_template_edi_sale_total')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form_is_company')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def _get_echo_commercial_solution(self):

        try:
            commercial_record_set = self.env['res.users'].get_commercial_echos_solutions()

        except ValidationError:
            raise

        return commercial_record_set

class AnalyticMetier(models.Model):
    """ Class representing model Analytic Metier associted to a Quote """

    _name = 'gle_adv.analytic_metier'

    name = fields.Char(string="Libellé analytique métier")
