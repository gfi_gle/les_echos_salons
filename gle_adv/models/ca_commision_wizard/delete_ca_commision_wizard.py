# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class DeleteCaCommissionWizard(models.TransientModel):
    """"""

    _name = 'gle_adv.delete_ca_commision_wizard'

    commision_type = fields.Selection([('percentage', "Poucentage"), ('absolut', "Absolue")], string="Type de commissionnement")

    commercial_list_ids = fields.Many2many(comodel_name='res.users',
                                           relation='gle_adv_delete_commercial_ids_res_users_relation',
                                           column1='wizard_id',
                                           column2='comemrcial_id', default = lambda self : self._init_list_commercial_ids())

    commercial_to_delete = fields.Many2many(comodel_name='res.users',
                                              relation='gle_adv_delete_ca_commision_res_users_relation',
                                              column1='wizard_id',
                                              column2='commercial_id',
                                              string="Commerciaux à supprimer")

    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")

    def _init_list_commercial_ids(self):
        """"""

        ca_commision_list_ids = self._context.get('ca_ids', False)

        ca_commision_obj = self.env['gle_adv.ca_commision']
        res_users_obj = self.env['res.users']

        ca_commision_recordset = ca_commision_obj.search([('id', 'in', ca_commision_list_ids)])

        list_commercial_ids = [ca_commision.commercial_id.id for ca_commision in ca_commision_recordset if not ca_commision.commercial_id.is_commercial_echo_solution]


        result_recordset = res_users_obj.search([('id', 'in', list_commercial_ids)])

        return result_recordset

    @api.multi
    def delete_action_ca_commision(self):
        """"""

        ca_commision_obj = self.env['gle_adv.ca_commision']


        sale_order = self._context.get('default_sale_order_id')

        length_ca_to_delete = len(self.commercial_to_delete)
        length_rest_commercial = len(self.commercial_list_ids) - length_ca_to_delete

        list_ids = [commercial.id for commercial in self.commercial_to_delete]

        if length_rest_commercial == 1:

            ca_ids_to_delete = ca_commision_obj.search([('sale_order_id', '=', sale_order), ('commercial_id', 'in', list_ids)])
            ca_list_ids_to_delete = [ca_commision.id for ca_commision in ca_ids_to_delete]
            ca_commision_obj.browse(ca_list_ids_to_delete).unlink()

            self.update_ca_commision()

        elif length_rest_commercial < 1:
            raise ValidationError("Il faut au moin un commercial")

        elif length_rest_commercial == 2:

            ca_ids_to_delete = ca_commision_obj.search([('sale_order_id', '=', sale_order), ('commercial_id', 'in', list_ids)])
            ca_list_ids_to_delete = [ca_commision.id for ca_commision in ca_ids_to_delete]
            ca_commision_obj.browse(ca_list_ids_to_delete).unlink()

            view_id = self.env['ir.model.data'].get_object_reference('gle_adv', 'equilize_ca_commision_wizard_form_view')

            ca_ids = ca_commision_obj.search([('sale_order_id', '=', sale_order)])

            commercial_list_ids = [ca_commision.commercial_id.id for ca_commision in ca_ids if not ca_commision.commercial_id.is_commercial_echo_solution]
            amount_ca_commissionable = self.env['sale.order'].search([('id', '=', sale_order)]).amount_ca_commissionable

            context = {
                'default_sale_order_id': sale_order,
                'default_commercial_one_id': commercial_list_ids[0],
                'default_commercial_two_id': commercial_list_ids[1],
                'default_commision_type': self.commision_type,
                'default_amount_ca_commissionable': amount_ca_commissionable
            }

            return {
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_adv.equilize_ca_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': context
            }

    def update_ca_commision(self):

        ca_commision_obj = self.env['gle_adv.ca_commision']

        ca_record_set = ca_commision_obj.search([('sale_order_id', '=', self.sale_order_id.id)])

        new_amount = self.sale_order_id.amount_ca_commissionable

        vals = {
            'percentage': 100,
            'amount': new_amount
        }

        ca_record_set.write(vals)


