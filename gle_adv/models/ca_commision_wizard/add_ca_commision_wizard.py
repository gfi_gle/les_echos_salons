# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class AddCaCommisionWizard(models.TransientModel):
    """"""

    _name = 'gle_adv.add_ca_commsion_wizard'

    commercial_list_ids = fields.Many2many(comodel_name='res.users',
                                           relation='gle_adv_add_one_commercial_ids_res_users_relation',
                                           column1='wizard_id',
                                           column2='commercial_id',
                                           default=lambda self: self._init_list_commercial_ids())

    commercial_id = fields.Many2one(comodel_name='res.users', string="Commercial")
    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")

    commision_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', "Absolue")], string="Type de commissionnement")

    percentage = fields.Float(string="Pourcentage")
    amount = fields.Float(string="Montant")

    amount_ca_commissionable = fields.Float(string="Montant commissionnable")

    @api.onchange('amount')
    def _on_change_amount(self):

        if not self._check_amount():

            self.amount = None
            self.percentage = None

            res = {'warning': {
                'title': "Attention",
                'message': "Le montant est supérieur au CA commissionable."
            }}

            return res

        elif self._check_amount() and self.commision_type == 'absolut':

            self.percentage = self._compute_percentage()

    @api.onchange('percentage')
    def _on_change_percentage(self):

        if not self._check_percentage():

            self.percentage = None
            self.amount = None

            res = {'warning': {
                'title': "Attention",
                'message': "Le poucentage doit être comprit entre 0 et 100."
            }}

            return res

        elif self._check_percentage() and self.commision_type == 'percentage':

            self.amount = self._compute_amount()

    def _on_change_type_commision(self):

        self.amount = None
        self.percentage = None

    def _compute_percentage(self):
        result = (self.amount / self.amount_ca_commissionable) * 100

        return result

    def _compute_amount(self):
        result = self.amount_ca_commissionable * (self.percentage / 100)

        return result

    def _check_amount(self):
        if self.amount > self.amount_ca_commissionable:
            return False

        return True

    def _check_percentage(self):
        if self.percentage > 100 or self.percentage < 0:
            return False

        return True

    @api.multi
    def save_ca_commision(self):
        """"""

        sql_query_insert = 'INSERT INTO gle_adv_ca_commision_sale_order_relation (sale_order_id, ca_commision_id) VALUES ({},{});'

        if self._check_percentage() and self._check_amount():
            ca_commision_obj = self.env['gle_adv.ca_commision']
            vals = {}

            if self.commision_type == 'percentage':
                vals = {
                    'commercial_id': self.commercial_id.id,
                    'sale_order_id': self.sale_order_id.id,
                    'commision_type': self.commision_type,
                    'percentage': self.percentage,
                    'amount': self._compute_amount()
                }

            elif self.commision_type == 'absolut':
                vals = {
                    'commercial_id': self.commercial_id.id,
                    'sale_order_id': self.sale_order_id.id,
                    'commision_type': self.commision_type,
                    'percentage': self._compute_percentage(),
                    'amount': self.amount
                }

            new_obj = ca_commision_obj.create(vals)

            if new_obj:
                self.env.cr.execute(sql_query_insert.format(self.sale_order_id.id, new_obj.id))
                self.update_existing_ca_commision(new_obj=new_obj)


    def update_existing_ca_commision(self, new_obj):
        """"""

        ca_commision_obj = self.env['gle_adv.ca_commision']

        record_set = ca_commision_obj.search([('sale_order_id', '=', self.sale_order_id.id), ('id', '!=', new_obj.id)])

        new_percentage = 100 - new_obj.percentage
        new_amount = self.amount_ca_commissionable - new_obj.amount

        vals = {
            'percentage': new_percentage,
            'amount': new_amount
        }

        for ca_commission in record_set:
            if not ca_commission.commercial_id.is_commercial_echo_solution:
                ca_commission.write(vals)

    def _init_list_commercial_ids(self):
        """"""

        ca_commision_list_ids = self._context.get('ca_ids', False)

        ca_commision_obj = self.env['gle_adv.ca_commision']
        res_users_obj = self.env['res.users']
        commercial_echo_solution = res_users_obj.get_commercial_echos_solutions()

        ca_commision_recordset = ca_commision_obj.search([('id', 'in', ca_commision_list_ids)])

        list_commercial_ids = [ca_commision.commercial_id.id for ca_commision in ca_commision_recordset]
        list_commercial_ids.append(commercial_echo_solution.id)

        result_recordset = res_users_obj.search([('id', 'in', list_commercial_ids)])

        return result_recordset