# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class EquilizeCaWizard(models.TransientModel):
    """"""

    _name = 'gle_adv.equilize_ca_wizard'

    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")
    amount_ca_commissionable = fields.Float(string="Montant commissionnable")
    commision_type = fields.Selection([('percentage', "Poucentage"), ('absolut', "Absolue")], string="Type de commissionnement")

    commercial_one_id = fields.Many2one(comodel_name='res.users', string="Commercial un")
    percentage_one = fields.Float(string="Pourcentage")
    amount_one = fields.Float(string="Montant")

    commercial_two_id = fields.Many2one(comodel_name='res.users', string="Commercial deux")
    percentage_two = fields.Float(string="Pourcentage")
    amount_two = fields.Float(string="Montant")

    @api.multi
    def equilize_action_ca_commision_percentage(self):
        """"""

        total_percentage = self.percentage_one + self.percentage_two

        if not self.check_percentage(total_percentage):
            raise ValidationError("Le pourcentage doit être égale à 100 %")

        ca_commision_ids = self.env['gle_adv.ca_commision'].search([('sale_order_id', '=', self.sale_order_id.id)])
        amouts_result = self.compute_amounts()

        for ca_commision in ca_commision_ids:
            if ca_commision.commercial_id.id == self.commercial_one_id.id and not ca_commision.commercial_id.is_commercial_echo_solution:
                ca_commision.write({
                    'amount': amouts_result.get('amount_one'),
                    'percentage': self.percentage_one
                })
            if ca_commision.commercial_id.id == self.commercial_two_id.id and not ca_commision.commercial_id.is_commercial_echo_solution:
                ca_commision.write({
                    'amount': amouts_result.get('amount_two'),
                    'percentage': self.percentage_two
                })

    @api.multi
    def equilize_action_ca_commision_absolut(self):
        """"""

        total_amount = self.amount_one + self.amount_two

        if not self.check_amount(total_amount):
            raise ValidationError("Le montant total doit être égale au CA commissionable")

        ca_commision_ids = self.env['gle_adv.ca_commision'].search([('sale_order_id', '=', self.sale_order_id.id)])
        percentages_result = self.compute_percentage()

        for ca_commision in ca_commision_ids:
            if ca_commision.commercial_id.id == self.commercial_one_id.id and not ca_commision.commercial_id.is_commercial_echo_solution:
                ca_commision.write({
                    'amount': self.amount_one,
                    'percentage': percentages_result.get('percentage_one')
                })
            if ca_commision.commercial_id.id == self.commercial_two_id.id and not ca_commision.commercial_id.is_commercial_echo_solution:
                ca_commision.write({
                    'amount': self.amount_two,
                    'percentage': percentages_result.get('percentage_two')
                })

    def compute_amounts(self):
        result = {
            'amount_one': self.amount_ca_commissionable * (self.percentage_one / 100),
            'amount_two': self.amount_ca_commissionable * (self.percentage_two / 100)
        }

        return result

    def compute_percentage(self):
        result = {
            'percentage_one': (self.amount_one / self.amount_ca_commissionable) * 100,
            'percentage_two': (self.amount_two / self.amount_ca_commissionable) * 100,
        }

        return result

    def check_percentage(self, total):

        if total != 100:
            return False

        return True

    def check_amount(self, total):

        if total != self.amount_ca_commissionable:
            return False

        return True


