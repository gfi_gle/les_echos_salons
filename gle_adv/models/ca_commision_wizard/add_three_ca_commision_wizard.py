# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class AddThreeCaCommisionWizard(models.TransientModel):
    """"""

    _name = 'gle_adv.add_three_ca_commision_wizard'

    commision_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', "Absolue")],
                                      string="Type de commissionnement")

    amount_ca_commissionable = fields.Float(string="Montant commissionnable")

    commercial_one_id = fields.Many2one(comodel_name='res.users', string="Commercial un", default = lambda self : self._init_commercial_one())
    percentage_one = fields.Float(string="Pourcentage", default = lambda self : self._init_percentage_one())
    amount_one = fields.Float(string="Montant", default = lambda self : self._init_amount_one())

    commercial_two_id = fields.Many2one(comodel_name='res.users', string="Commercial deux", default = lambda self : self._init_commercial_two())
    percentage_two = fields.Float(string="Pourcentage", default = lambda self : self._init_percentage_two())
    amount_two = fields.Float(string="Montant", default = lambda self : self._init_amount_two())

    commercial_three_id = fields.Many2one(comodel_name='res.users', string="Commercial trois")
    percentage_three = fields.Float(string="Pourcentage")
    amount_three = fields.Float(string="Montant")

    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")

    commercial_list_ids = fields.Many2many(comodel_name='res.users',
                                           relation='gle_adv_add_three_commercial_ids_res_users_relation',
                                           column1='wizard_id',
                                           column2='commercial_id',
                                           default=lambda self: self._init_list_commercial_ids())

    def _init_commercial_one(self):
        list_ids = self._context.get('commercial_ids')

        return self.env['res.users'].search([('id', '=', list_ids[0])])

    def _init_commercial_two(self):
        list_ids = self._context.get('commercial_ids')

        return self.env['res.users'].search([('id', '=', list_ids[1])])

    def _init_percentage_one(self):
        ca_commision = self.get_ca_commision(self._init_commercial_one())
        return ca_commision.percentage

    def _init_percentage_two(self):
        ca_commision = self.get_ca_commision(self._init_commercial_two())
        return ca_commision.percentage

    def _init_amount_one(self):
        ca_commision = self.get_ca_commision(self._init_commercial_one())

        return ca_commision.amount

    def _init_amount_two(self):
        ca_commision = self.get_ca_commision(self._init_commercial_two())

        return ca_commision.amount

    def get_ca_commision(self, commercial):

        sale_order_id = self._context.get('default_sale_order_id')
        record_set = self.env['gle_adv.ca_commision'].search([('commercial_id', '=', commercial.id), ('sale_order_id', '=', sale_order_id)])

        return record_set

    def get_ca_commision_ids(self):

        sale_order_id = self._context.get('default_sale_order_id')
        record_set = self.env['gle_adv.ca_commision'].search([('sale_order_id', '=', sale_order_id)])

        return record_set

    @api.multi
    def save_action_ca_commision_percentage(self):

        total_percentage = self.percentage_one + self.percentage_two + self.percentage_three
        sql_query_insert = 'INSERT INTO gle_adv_ca_commision_sale_order_relation (sale_order_id, ca_commision_id) VALUES ({},{});'

        if not self.check_percentage(total_percentage):
            raise ValidationError("Le total des pourcentage doit être égale à 100 %")

        amounts_result = self.compute_amounts()

        for ca_commision in self.get_ca_commision_ids():
            if ca_commision.commercial_id.id == self.commercial_one_id.id:
                ca_commision.write({
                    'amount': amounts_result.get('amount_one'),
                    'percentage': self.percentage_one
                })
            if ca_commision.commercial_id.id == self.commercial_two_id.id:
                ca_commision.write({
                    'amount': amounts_result.get('amount_two'),
                    'percentage': self.percentage_two
                })

        vals = {
            'commercial_id': self.commercial_three_id.id,
            'amount': amounts_result.get('amount_three'),
            'percentage': self.percentage_three
        }

        ca_commision_created = self.env['gle_adv.ca_commision'].create(vals)

        if ca_commision_created:
            self.env.cr.execute(sql_query_insert.format(self.sale_order_id.id, ca_commision_created.id))

    @api.multi
    def save_action_ca_commision_absolut(self):
        """"""

        total_amount = self.amount_one + self.amount_two + self.amount_three
        sql_query_insert = 'INSERT INTO gle_adv_ca_commision_sale_order_relation (sale_order_id, ca_commision_id) VALUES ({},{});'

        if not self.check_amount(total_amount):
            raise ValidationError("Le montant total doit être égale au CA commissionable")

        percentages_result = self.compute_percentage()

        for ca_commision in self.get_ca_commision_ids():
            if ca_commision.commercial_id.id == self.commercial_one_id.id:
                ca_commision.write({
                    'amount': self.amount_one,
                    'percentage': percentages_result.get('percentage_one')
                })
            if ca_commision.commercial_id.id == self.commercial_two_id.id:
                ca_commision.write({
                    'amount': self.amount_two,
                    'percentage': percentages_result.get('percentage_two')
                })

        vals = {
            'commercial_id': self.commercial_three_id.id,
            'amount': self.amount_three,
            'percentage': percentages_result.get('percentage_three')
        }

        ca_commision_created = self.env['gle_adv.ca_commision'].create(vals)

        if ca_commision_created:
            self.env.cr.execute(sql_query_insert.format(self.sale_order_id.id, ca_commision_created.id))

    def check_percentage(self, total):

        if total != 100:
            return False

        return True

    def check_amount(self, total):

        if str(total) == str(self.amount_ca_commissionable):
            return True

        return False

    def compute_amounts(self):

        result = {
            'amount_one': self.amount_ca_commissionable * (self.percentage_one / 100),
            'amount_two': self.amount_ca_commissionable * (self.percentage_two / 100),
            'amount_three': self.amount_ca_commissionable * (self.percentage_three / 100)
        }

        return result

    def compute_percentage(self):

        result = {
            'percentage_one': (self.amount_one / self.amount_ca_commissionable) * 100,
            'percentage_two': (self.amount_two / self.amount_ca_commissionable) * 100,
            'percentage_three': (self.amount_three / self.amount_ca_commissionable) * 100,
        }

        return result

    def _init_list_commercial_ids(self):
        """"""

        ca_commision_list_ids = self._context.get('ca_ids', False)

        ca_commision_obj = self.env['gle_adv.ca_commision']
        res_users_obj = self.env['res.users']
        commercial_echo_solution = res_users_obj.get_commercial_echos_solutions()

        ca_commision_recordset = ca_commision_obj.search([('id', 'in', ca_commision_list_ids)])

        list_commercial_ids = [ca_commision.commercial_id.id for ca_commision in ca_commision_recordset]
        list_commercial_ids.append(commercial_echo_solution.id)

        result_recordset = res_users_obj.search([('id', 'in', list_commercial_ids)])

        return result_recordset


