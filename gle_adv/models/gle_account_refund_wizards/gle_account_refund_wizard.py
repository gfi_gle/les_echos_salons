# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

MAGIC_COLUMNS = ('id', 'create_uid', 'create_date', 'write_uid', 'write_date')


class GleAccountRefundWizard(models.TransientModel):

    _name = 'gle_adv.gle_account_refund_wizard'

    sale_order_id = fields.Many2one(comodel_name='sale.order', string="Devis")
    partner_id = fields.Many2one(comodel_name='res.partner', string="Société")
    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Projet/Produit")
    account_invoice_id = fields.Many2one(comodel_name='account.invoice', string="Facture")

    type_avoir = fields.Selection(
        [('total', 'Avoir total pour annulation'), ('partiel', 'Avoir partiel'), ('regularisation', 'Régularisation')],
        string="Type d\'avoir")

    description = fields.Text(string="Motif d'avoir")

    @api.onchange('salon_id', 'partner_id', 'sale_order_id')
    def on_change_salon_partner(self):
        """"""

        invoice_obj = self.env['account.invoice']

        sql_query = self.prepare_search_query(invoice_refunded_ids=self.get_refund_ids())
        invoice_records = invoice_obj.search(sql_query)

        list_invoice_ids = [invoice.id for invoice in invoice_records]

        return {'domain': {'account_invoice_id': [('id', 'in', list_invoice_ids)]}}

    def prepare_search_query(self, invoice_refunded_ids=None):
        """"""

        sql_select_query = [('type', '=', 'out_invoice'), ('state', 'in', ['open', 'paid'])]

        if self.sale_order_id.name:
            str_name = '{}'.format(self.sale_order_id.name)
            sql_select_query.append(('origin', '=', str_name))
        if self.partner_id.id:
            sql_select_query.append(('partner_id', '=', self.partner_id.id))
        if self.salon_id.id:
            sql_select_query.append(('salon_id', '=', self.salon_id.id))
        if invoice_refunded_ids:
            sql_select_query.append(('id', 'not in', invoice_refunded_ids.ids))

        return sql_select_query

    def get_refund_ids(self):

        invoice_obj = self.env['account.invoice']
        str_ref = '{}'.format(self.sale_order_id.name)

        refund_ids = invoice_obj.search([('state', 'in', ['open', 'paid']), ('reference', '=', str_ref), ('type', '=', 'out_refund')])
        invoice_ids = invoice_obj.search([('type', '=', 'out_invoice'), ('internal_number', 'in', refund_ids.mapped('origin'))])

        return invoice_ids

    @api.model
    def prepare_account_refund_lines(self, lines):
        """"""

        result = []
        for line in lines:
            values = {}
            for name, field in lines._fields.iteritems():
                if name in MAGIC_COLUMNS:
                    continue
                elif field.type == 'many2one':
                    values[name] = line[name].id
                elif (field.type not in ['many2many', 'one2many']) and not (name == 'quantity'):
                    values[name] = line[name]
                elif name == 'quantity':
                    values[name] = - line[name]
                elif name == 'invoice_line_tax_id':
                    values[name] = [(6, 0, line[name].ids)]

            result.append((0, 0, values))

        return result

    @api.multi
    def action_create_invoice_aae(self):

        gle_account_refund_obj = self.env['gle.account.refund']

        if self.type_avoir == 'regularisation':
            state = 'open'

        else:
            state = 'draft'

        vals = {
            'partner_id': self.partner_id.id,
            'salon_id': self.salon_id.id,
            'type_avoir': self.type_avoir,
            'description': self.description,
            'invoice_origin_id': self.account_invoice_id.id,
            'type': 'client',
            'refund_line_ids': self.prepare_account_refund_lines(self.account_invoice_id.invoice_line),
            'ca_commision_ids': [(4, self.sale_order_id.ca_commision_ids.ids)],
            'amount_ca_commissionable': self.sale_order_id.amount_ca_commissionable,
            'amount_ca_no_commissionable': self.sale_order_id.amount_ca_no_commissionable,
            'commercial_echo_solution': self.sale_order_id.commercial_echo_solution_id.id,
            'partner_invoice_id': self.sale_order_id.partner_invoice_id.id,
            'type_facturation': self.sale_order_id.type_facturation,
            'stand_id': self.sale_order_id.stand_id.id or None,
            'state': state,
            'contact_client_id': self.sale_order_id.contact_client_id.id,
            'contact_business_id': self.sale_order_id.contact_business_id.id,
            'sale_order_origin_id': self.sale_order_id.id,
            'discount_type_invoice': self.sale_order_id.discount_type_invoice,
            'gle_section_id': self.account_invoice_id.gle_section_id.id
        }

        account_refund_created = gle_account_refund_obj.create(vals)

        view_id = self.env['ir.model.data'].get_object_reference('gle_accounting', 'view_gle_account_refund_form')

        return {
            'name': "Création Devis AAE",
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle.account.refund',
            'type': 'ir.actions.act_window',
            'context': {},
            'res_id': account_refund_created.id
        }
