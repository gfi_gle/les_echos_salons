# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandWizard(models.TransientModel):
    """ Transient model representing Stand, attributes are required fields to change state of Stand to 'ferme' """

    _name = 'gle_adv.stand_wizard'

    def _default_partner(self):
        return self._context.get('partner_id')

    def _default_stand(self):
        return self.env['gle_salon.stand'].browse(self._context.get('stand_id'))

    def _default_stand_client(self):
        return self._default_stand().client_id

    def _default_stand_type_ids(self):
        return self._default_stand().stand_type_ids

    def _default_stand_enseigne(self):
        return self._default_stand().enseigne

    def _default_stand_state_ware(self):
        return self._default_stand().state_ware

    def _default_stand_contact_id(self):
        return self._default_stand().contact_id

    def _default_stand_commercial_id(self):
        return self._default_stand().commercial_id

    partner_id = fields.Many2one('res.partner', default=_default_partner)
    stand_id = fields.Many2one('gle_salon.stand', string="Stand", default=_default_stand, help="Stand")
    client_id = fields.Many2one('res.partner', string="Contact opérationnel", index=True,default=_default_stand_client, help="Client")
    stand_type_ids = fields.Many2many('gle_salon.stand_type', string="Type de stand", index=True,default=_default_stand_type_ids, help="Type de stand")
    enseigne = fields.Many2many('res.partner', string="Enseigne", index=True, default=_default_stand_enseigne, help="Enseigne")
    state_ware = fields.Selection([('oui', 'oui'), ('non', 'non')], string="Etat: échange marchandise", default=_default_stand_state_ware, help="Etat de la marchandise")
    contact_id = fields.Many2one('res.partner', string="Commercial client", index=True, default=_default_stand_contact_id, help="Contact Commercial chez le client")
    commercial_id = fields.Many2one('res.users', string="Commercial les Echos", index=True, default=_default_stand_commercial_id, help="Contact Commercial")

    @api.multi
    def action_write_stand(self):
        """ Action write to save attributes of transient model in Stand model """

        for stand in self.stand_id:
            stand.client_id = self.client_id
            stand.stand_type_ids = self.stand_type_ids
            stand.enseigne = self.enseigne
            stand.contact_id = self.contact_id
            stand.commercial_id = self.commercial_id
            stand.state_ware = self.state_ware

        return {}