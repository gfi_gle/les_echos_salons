# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandFermeWizard(models.TransientModel):
    """ Transient model representing Stand, attributes are required fields to change state of Stand to 'ferme' """

    _name = 'gle_adv.stand_ferme_wizard'

    def _default_stand_client(self):
        return self.stand_id.client_id

    def _default_stand_type_ids(self):
        return self.stand_id.stand_type_ids

    def _default_stand_enseigne(self):
        return self.stand_id.enseigne

    def _default_stand_state_ware(self):
        return self.stand_id.state_ware

    partner_id = fields.Many2one('res.partner')
    stand_id = fields.Many2one('gle_salon.stand', string="Stand", help="Stand")
    client_id = fields.Many2one('res.partner', string="Contact opérationnel", index=True,default=_default_stand_client, help="Client")
    enseigne = fields.Many2many('res.partner', string="Enseigne", index=True, default=_default_stand_enseigne, help="Enseigne")
    state_ware = fields.Selection([('oui', 'oui'), ('non', 'non')], string="Etat: échange marchandise",default=_default_stand_state_ware, help="Etat de la marchandise")
    stand_type_ids = fields.Many2one(comodel_name='gle_salon.stand_type', string="Type de stand", index=True, default=_default_stand_type_ids, help="Type de stand")

    @api.multi
    def action_write_stand(self):
        """ Action write to save attributes of transient model in Stand model and change sale.order state to 'ferme' """

        if self.stand_id.id:
            if self._context.get('sale_order'):
                sale_order_obj = self.env['sale.order'].search([('id', '=', self._context.get('sale_order'))])
                sale_order_obj.write({'state':'manual'})

            vals = {
                'client_id': self.client_id.id,
                'enseigne': self.enseigne,
                'stand_type_ids': self.stand_type_ids.id,
                'state_ware': self.state_ware,
                'state': 'ferme'
            }

            self.env['gle_salon.stand'].search([('id', '=', self.stand_id.id)]).write(vals)

            sale_order_obj.action_button_confirm()


