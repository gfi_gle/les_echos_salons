# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandOptionWizard(models.TransientModel):
    """ Transient model representing Stand, attributes are required fields to change state of Stand to 'option' """

    _name = 'gle_adv.stand_option_wizard'

    society_id = fields.Many2one('res.partner', string="Client/Prospect", index=True)

    """ Donneur d'ordre"""
    do_ids = fields.Many2many('res.partner', string="Donneur d'ordre", index=True)

    """ Commercial les Echos """
    commercial_id = fields.Many2one('res.users', string="Commercial les Echos", index=True, help="Contact Commercial")

    """ Contact commercial client """
    contact_id = fields.Many2one('res.partner', string="Contact commercial client", index=True)

    display_communication = fields.Boolean(index=True, default=False, string="Affichage communication")

    @api.multi
    def action_write_option_stand(self):
        """ Action write to save attributes of transient model in Stand model and change sale.order state to 'option' """

        if self._context.get('stand_id'):
            stand_id = self._context.get('stand_id')

        _logger.debug('-------------->TEST')
        if self._context.get('sale_order_id'):
            _logger.debug('-------------->TEST 01')
            _logger.debug(self._context.get('sale_order_id'))
            sale_order_obj = self.env['sale.order'].search([('id', '=', self._context.get('sale_order_id'))])
            self._context.get(sale_order_obj)
            sale_order_obj.write({'state':'option'})

        vals = {
            'society_id': self.society_id.id,
            'do_ids': self.do_ids,
            'commercial_id': self.commercial_id.id,
            'display_communication': self.display_communication,
            'state': 'option'
        }

        self.env['gle_salon.stand'].search([('id', '=', stand_id)]).write(vals)

        sale_order_obj.action_confirm_to_option()


