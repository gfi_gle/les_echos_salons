# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
from openerp.addons.gle_numerotation.models.ComputeNumerotation import ComputeNumerotation
import logging

_logger = logging.getLogger(__name__)


class GleAccountRefundExtend(models.Model):
    _inherit = "gle.account.refund"

    type_avoir = fields.Selection([('total','Avoir total pour annulation'),('partiel','Avoir partiel'),('regularisation','Régularisation')], string="Type d\'avoir")

    refund_line_ids = fields.One2many('gle.account.refund.line', 'refund_id',
                                      string='Lignes', readonly=False,
                                      states={'draft': [('readonly', False)]}, copy=True)

    amount_untaxed = fields.Float(string="Montant hors-taxe", compute='_compute_amount_untaxed', store=True)

    amount_taxe_total = fields.Float(string="Taxes", compute='_compute_amount_taxe_total', store=True)

    amount_taxe_total_stored = fields.Float(string="Taxes", compute='_compute_amount_taxe_total', store=True)

    amount_total = fields.Float(string="Total", compute='_compute_amount_total', store=True)

    amount_total_stored = fields.Float(string="Total", compute='_compute_amount_total', store=True)

    absolut_amount_untaxed = fields.Float(compute='_compute_absolut_amount_untaxed', store=True)

    amount_ca_commissionable = fields.Float(string="CA commissionnable", compute='compute_amount_ca_commissionnable')

    amount_ca_no_commissionable = fields.Float(string="CA non commissionnable", compute='_compute_amount_ca_no_commissionable')

    ca_commision_ids = fields.Many2many(comodel_name='gle_adv.ca_commision',
                                        relation='gle_adv_ca_commision_account_refund_relation',
                                        column1='gle_account_refund_id',
                                        column2='ca_commision_id',
                                        string="Répartition du CA commissionnable")

    commercial_echo_solution = fields.Many2one(comodel_name='res.users', string="Commercial les Echos solutions")

    partner_invoice_id = fields.Many2one(comodel_name='res.partner', string="Adresse de facturation")

    stand_id = fields.Many2one(comodel_name='gle_salon.stand', string="Stand")

    internal_number = fields.Char(string="Numéro interne")

    contact_client_id = fields.Many2one(comodel_name='res.partner', string="Contact client")

    contact_business_id = fields.Many2one(comodel_name='res.users', string="Contact les Echos Business")

    sale_order_origin_id = fields.Many2one(comodel_name='sale.order')

    discount_type_invoice = fields.Selection([('line', "Remise / Ligne"), ('total', "Remise global")], string="Type d'affichage")

    gle_section_id = fields.Many2one("gle.section", string="Section")

    state = fields.Selection([
        ('cancel', 'Sans suite'),
        ('draft', 'Brouillon'),
        ('a_facturer', "A facturer"),
        ('wait_cg', "Attente validation CG"),
        ('open', "Ouvert"),
        ('done', 'Terminé'),
    ], 'Status', default='draft')

    ref_customer_field = fields.Char(string="Référence/Description")

    @api.model
    def create(self, vals):

        new_obj = super(GleAccountRefundExtend, self).create(vals)
        new_obj.write(vals={}, type='create', new_refund=new_obj)

        return new_obj

    @api.multi
    def write(self, vals, type=None, new_refund=None):

        if new_refund:
            if new_refund.state == 'open':

                computar = ComputeNumerotation(env=self.env)
                vals.update({'name': computar.compute_sale_order_aae_name(order=new_refund, type_creation='create')})

        res = super(GleAccountRefundExtend, self).write(vals)

        return res

    @api.multi
    def unlink(self):
        """ Override unlink method to check if state is Draft """

        for gle_account_refund in self:
            if gle_account_refund.state in ['confirmed', 'cancel', 'done']:
                raise ValidationError("Vous ne pouvez supprimer un Devis AEE qui a un état {}.".format(gle_account_refund.state))

        super(GleAccountRefundExtend, self).unlink()

    @api.depends('refund_line_ids', 'refund_line_ids.price_unit', 'refund_line_ids.quantity', 'refund_line_ids.price_subtotal')
    def _compute_amount_untaxed(self):
        for refund in self:
            amount = 0
            for refund_lines in refund.refund_line_ids:
                 amount += refund_lines.price_subtotal

            refund.amount_untaxed = amount

    @api.depends('amount_untaxed')
    def _compute_amount_taxe_total(self):
        for refund in self:
            refund.amount_taxe_total = sum(line.refund_line_tax_amount for line in refund.refund_line_ids)
            refund.amount_taxe_total_stored = sum(line.refund_line_tax_amount for line in refund.refund_line_ids)

    @api.depends('refund_line_ids', 'refund_line_ids.price_unit', 'refund_line_ids.quantity',
                 'refund_line_ids.price_subtotal')
    def _compute_amount_total(self):
        for refund in self:

            xamount_total = (refund.amount_taxe_total + refund.amount_untaxed)

            refund.amount_total = xamount_total
            refund.amount_total_stored = xamount_total


    @api.onchange('invoice_origin_id')
    def _onchange_invoice(self):
        self.partner_id = self.invoice_origin_id.partner_id
        self.name = self.invoice_origin_id.name

        if self.invoice_origin_id.salon_id.id:
            self.salon_id = self.invoice_origin_id.salon_id

        self.type_facturation = self.invoice_origin_id.type_facturation

        refund_lines = []
        invoices_lines = self.invoice_origin_id.invoice_line.filtered(
            lambda line: line.product_id.type in ('consu', 'product', 'service')
        )

        for invoice_line in invoices_lines:
            line = {
                'name': invoice_line.name,
                'uos_id': invoice_line.uos_id,
                'invoice_line_id': invoice_line.id,
                'product_id': invoice_line.product_id.id,
                'quantity': invoice_line.quantity,
                'account_id': invoice_line.account_id,
                'price_unit': invoice_line.price_unit,
                'origin_quantity': invoice_line.quantity,
                'origin_price_unit': invoice_line.price_unit,
                'price_subtotal': invoice_line.price_subtotal,
                'discount': invoice_line.discount,
                'discount_type': invoice_line.discount_type,
            }

            refund_lines.append((0, 0, line))

        value = self._convert_to_cache(
            {'refund_line_ids': refund_lines}, validate=False)
        self.update(value)

    @api.onchange('salon_id')
    def _on_change_salon_id(self):
        """ On change method to change domain in terms of invoice_origin_id.salon_id """

        account_invoice_obj = self.env['account.invoice']
        list_ids = []

        if self.salon_id.id:
            record_set_account = account_invoice_obj.search([('salon_id', '=', self.salon_id.id)])
            list_ids = [account.id for account in record_set_account]

        else:
            record_set_account = account_invoice_obj.search([])
            list_ids = [account.id for account in record_set_account]

        if self.salon_id.id != self.invoice_origin_id.salon_id.id:
            self.invoice_origin_id = None

        return {'domain': {'invoice_origin_id': [('id', 'in', list_ids)]}}

    def _prepare_refund(self):

        values = super(GleAccountRefundExtend, self)._prepare_refund()

        contact_business_ids_list = [self.contact_business_id.id]
        invoice_address_list = [self.sale_order_origin_id.partner_invoice_id.id]

        values_to_update = {
            'invoice_address_ids': [(6, 0, invoice_address_list)],
            'contact_business_ids': [(6, 0, contact_business_ids_list)],
            'contact_client_id': self.contact_client_id.id,
            'partner_vat': self.partner_id.vat,
            'stand_id': self.stand_id.id or None,
            'ca_commision_ids': [(4, self.ca_commision_ids.ids)],
            'gle_section_id': self.gle_section_id.id,
        }

        values.update(values_to_update)

        return values

    @api.depends('refund_line_ids')
    def compute_amount_ca_commissionnable(self):
        """ Method to compute value of amount_ca_commissionable """

        ca_commissionnable = 0

        for gle_account_refund in self:
            for gle_account_refund_line in gle_account_refund.refund_line_ids:
                if gle_account_refund_line.product_id.commision_state == 'commissionnable':
                    ca_commissionnable += gle_account_refund_line.price_subtotal

            gle_account_refund.update_ca_commission_ids(ca_commissionnable)
            gle_account_refund.amount_ca_commissionable = ca_commissionnable

    @api.depends('refund_line_ids')
    def _compute_amount_ca_no_commissionable(self):
        """ Method to compute value of amount_ca_no_commissionable """

        ca_no_commissionnable = 0

        for gle_account_refund in self:
            for gle_account_refund_line in gle_account_refund.refund_line_ids:
                if gle_account_refund_line.product_id.commision_state == 'noncommissionnable' or gle_account_refund_line.product_id.commision_state == 'commissionnable_a':
                    ca_no_commissionnable += gle_account_refund_line.price_subtotal

            gle_account_refund.amount_ca_no_commissionable = ca_no_commissionnable
            gle_account_refund.update_ca_no_commission_ids(amount=ca_no_commissionnable)

    def update_ca_no_commission_ids(self, amount):
        """ Update no commisionnable id with new amount """

        for commission_id in self.ca_commision_ids:
            if commission_id.commercial_id.is_commercial_echo_solution:
                result = self.compute_values_ca_commission(percentage=commission_id.percentage, amount=amount)
                commission_id.write({'amount_refund': result})

    def update_ca_commission_ids(self, amount):
        """ Update commisionnable id with new amount """

        for commission_id in self.ca_commision_ids:
            if not commission_id.commercial_id.is_commercial_echo_solution:
                result = self.compute_values_ca_commission(percentage=commission_id.percentage, amount=amount)
                commission_id.write({'amount_refund': result})

    def compute_values_ca_commission(self, percentage, amount):

        new_amount = (amount * (percentage or 0.0 / 100)) / 100

        return new_amount

    def check_amount(self):
        """ Check amount already refunded before generate new account.refund """

        account_invoice_obj = self.env['account.invoice']

        origin_str = self.invoice_origin_id.internal_number
        account_invoice_record_set = account_invoice_obj.search([('state', 'in', ['draft', 'open', 'paid']), ('origin', '=', origin_str), ('type', '=', 'out_refund')])

        calculate_amount_refund = sum(out_refund.amount_untaxed for out_refund in account_invoice_record_set)

        if abs(round(calculate_amount_refund)) >= abs(round(self.invoice_origin_id.amount_untaxed)):

            return False

        return True

    def _compute_absolut_amount_untaxed(self):
        self.absolut_amount_untaxed = abs(self.amount_untaxed)

    def get_user_profile(self):

        u_id = self._context.get('uid')
        result = {}

        if u_id:
            user_id = self.env['res.users'].search([('id', '=', u_id)])

            if user_id:
                result.update({
                    'code_sect': user_id.section_ids.mapped('code_sect'),
                    'profile_code': user_id.profile_type_id.code
                })

        return result

    # action button

    @api.multi
    def action_mark_to_invoice(self):
        self.state = 'a_facturer'

    @api.multi
    def refund_director_co_confirmed(self):

        if self.check_amount():
            result = self.get_user_profile()

            if ('SDE600' in result.get('code_sect') and result.get('profile_code') == 'CS') \
                    or (self.salon_id.dir_salon.id == self.env.user.id) \
                    or (self.salon_id.id in list(self.env.user.salon_ids._ids)):

                if abs(self.amount_untaxed) < 10000:
                    self.refund_confirmed()
                elif abs(self.amount_untaxed) > 10000:
                    self.state = 'wait_cg'

            else:
                raise ValidationError("Vous n'avez pas l'abilitation pour confirmer les Devis AAE.")
        else:
            raise ValidationError("L'avoir associé à cette facture est déjà confirmé.")

    @api.multi
    def refund_cg_confirmed(self):

        if self.check_amount():
            self.refund_confirmed()
        else:
            raise ValidationError("L'avoir associé à cette facture est déjà confirmé.")

    @api.multi
    def refus_refund(self):
        self.state = 'draft'

    @api.multi
    def refund_create(self):

        if self.check_amount():

            values = self._prepare_refund()

            if not self.invoice_new_id:
                new_refund = self.env['account.invoice'].create(values)
                self.update({'state': 'done', 'invoice_new_id': new_refund.id})
                new_refund.button_reset_taxes()  # appel fonction de account.invoice natif qui recalcul les taxes
                view_id = self.env['ir.model.data'].get_object_reference('account', 'invoice_form')

                return {
                    'view_mode': 'form',
                    'view_id': view_id[1],
                    'view_type': 'form',
                    'res_model': 'account.invoice',
                    'type': 'ir.actions.act_window',
                    'res_id': new_refund.id
                }
        else:
            raise ValidationError("L'avoir associé à cette facture est déjà confirmé.")

        return True


class GleAccountRefundLine(models.Model):
    _inherit = "gle.account.refund.line"

    discount_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', 'Absolue')], string="Type de remise")

    invoice_line_tax_id = fields.Many2many(comodel_name='account.tax', relation='gle_account_refund_invoice_line_tax', column1='invoice_line_id', column2='tax_id',
                                           string='Taxes', domain=[('parent_id', '=', False), '|', ('active', '=', False), ('active', '=', True)])

    refund_line_tax_amount = fields.Float(string="Taxes", compute="_compute_line_tax_amount")

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price(self):
        """ Method override from account.invoice.line and test new discount type """

        tax_id = self.get_taxe_id()
        price = self._calc_base_price(tax_id=tax_id)

        taxes = self.invoice_line_tax_id.compute_all(price, self.quantity, product=self.product_id,
                                                     partner=self.invoice_id.partner_id)
        self.price_subtotal = taxes['total']

        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)

    def get_taxe_id(self):
        """ Return the value of the first tax associated to line """

        for taxe in self.invoice_line_tax_id:
            return taxe

    @api.multi
    def _calc_base_price(self, tax_id):

        price_unit = self.price_unit

        if self.discount_type:
            if not tax_id.price_include:
                if self.discount_type == 'percentage':
                    price_unit = self.price_unit * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit = self.price_unit * (
                        1 - (self.calc_percentage_with_amount(price_unit=self.price_unit) or 0.0) / 100)

            elif tax_id.price_include:

                result = tax_id.compute_all(
                    price_unit=self.price_unit,
                    quantity=abs(self.quantity),
                    product=self.product_id,
                    partner=self.partner_id)

                price_unit_without_tax = result['taxes'][0].get('price_unit')

                if self.discount_type == 'percentage':
                    price_unit_with_discount = price_unit_without_tax * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':

                    price_unit_with_discount = (price_unit_without_tax * (
                        1 - (self.calc_percentage_with_amount(price_unit=price_unit_without_tax) or 0.0) / 100))

                price_unit = price_unit_with_discount / (100 / (100 + tax_id.amount * 100))

        return price_unit

    def calc_percentage_with_amount(self, price_unit):

        amount = price_unit * abs(self.quantity)
        return (self.discount / amount) * 100

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_line_tax_amount(self):

        for line_amount_tax in self:
            tax_id = line_amount_tax.get_taxe_id()
            price_with_discount = self._calc_base_price(tax_id)

            result = tax_id.compute_all(
                price_unit=price_with_discount,
                quantity=abs(line_amount_tax.quantity),
                product=line_amount_tax.product_id,
                partner=line_amount_tax.partner_id)

            line_amount_tax.refund_line_tax_amount = - result['taxes'][0].get('amount', 0.0)
