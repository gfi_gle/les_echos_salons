# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class SaleOrderAnnotation(models.Model):
    """ Class representing model annotation on sale_order footer """

    _name = 'gle_adv.sale_order_annotation'

    name = fields.Char(string="Libellé de l'annotation")

    annotation_text = fields.Text(string="Corps de l'annotation")

    is_default = fields.Boolean(default=False,  string="Annotation par défaut")


    @api.onchange('is_default')
    def onchange_is_default(self):
        xtest = self.env['gle_adv.sale_order_annotation'].search([('is_default', '=', True)])

        if xtest:
            self.is_default = False
            res = {'warning': {
                'title': "Attention",
                'message': "Il éxiste déja une annotation par defaut."
            }}

            return res

