# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

class SaleOrderLine(models.Model):
    """ Inherit from Sale.order.line Model to add Attributes and Methods """

    _inherit = 'sale.order.line'


    change_wording_article = fields.Char('Changement libellé article')
    salon_id = fields.Many2one('gle_salon.salon',compute='_compute_salon_2', string='salon', store=True)
    intercos_line = fields.Boolean( string='Intercos', store=True, compute='_compute_intercos')

    product_id = fields.Many2one('product.product', 'Product', domain=[('sale_ok', '=', True),('article_state_adv', '=', 'valid')],
                                 change_default=True,
                                 readonly=True, states={'draft': [('readonly', False)],'valid': [('readonly', False)],'valid_d': [('readonly', False)]}, ondelete='restrict')

    function_grouping = fields.Boolean(string="Fonction de regroupement", default=False)

    name = fields.Text('Description', required=True, readonly=False, states={'empty': [('readonly', True)]})
    order_line = fields.One2many('sale.order.line', 'order_id', 'Order Lines', readonly=False, states={'empty': [('readonly', True)]}, copy=True)

    code_tva_gle = fields.Char("Code Tva Les Echos", compute='_compute_code_tva_gle', store=True)







    @api.depends('product_id')
    def _compute_salon_2(self):
        for record in self:
            related_salon = record.order_id.salon_id
            record.salon_id = related_salon

    @api.depends('product_id')
    def _compute_intercos(self):
        for record in self:
            if record.order_id.type_facturation == 'intercos':
                record.intercos_line = True
            else:
                record.intercos_line = False

    @api.onchange('partner_id')
    def _change_partner_id(self):
        self.order_partner_id = self.partner_id



    @api.multi
    @api.constrains('product_uom_qty')
    def constrains_product_uom_qty(self):
        for line in self:
            if line.product_uom_qty == 0 or line.product_uom_qty < 0:
                raise ValidationError("Les quantités dans les lignes ne peuvent pas être à 0,\n merci de corriger.")


    @api.depends('tax_id')
    def _compute_code_tva_gle(self):
        """"""
        i = 0
        for line in self:
            for tax_single in line.tax_id:
                i += 1
                xsearch_code_tva_gle = self.env['account.tax'].search([('id', '=', tax_single.id)])
                if xsearch_code_tva_gle:
                    xreturn_code_tva_gle = xsearch_code_tva_gle.code_tva_gle
        if i>1 or i==0:
            line.code_tva_gle = ''
        else:
            if xreturn_code_tva_gle:
                line.code_tva_gle = xreturn_code_tva_gle
            else:
                line.code_tva_gle = ''



