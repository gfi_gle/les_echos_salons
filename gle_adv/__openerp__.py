# -*- coding: utf-8 -*-
{
    'name': "gle_adv",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'gle_salon','sale_stock','product','gfi_amount_discount'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/sale_config.xml',
        'views/template.xml',
        'views/gle_account_refund_extend.xml',
        'views/config_analytique_metier_view.xml',
        'views/sale_order_view.xml',
        'views/email_template.xml',
        'views/menu.xml',
        'views/invoice.xml',
        'views/sale_order_annotation_views.xml',
        'views/ca_commision/ca_commision_views.xml',
        'views/ca_commision_wizard/add_ca_commision_wizard_views.xml',
        'views/ca_commision_wizard/add_three_ca_commision_wizard_views.xml',
        'views/ca_commision_wizard/delete_ca_commision_wizard_views.xml',
        'views/ca_commision_wizard/equilize_ca_wizard_views.xml',
        'views/gle_account_refund_wizard_views/gle_account_refund_wizard_view.xml',
        'views/res_users_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [

    ],

}