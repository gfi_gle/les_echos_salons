.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

========
gle_adv
========

This module is used to extends the sale.order and sale.order.line model

Functionnality
===============

#. It extends the existing model :
- sale.order
- sale.order.line


Bug Tracker
============

Incident tracking is done with the GFI Jira platform.

Credits
=======

Contributors
------------

* alexandre.matis@gfi.fr
* william.musy@gfi.fr

Maintainer
----------

.. image:: image url (http://www.gfi....image.png)
   :alt:  Odoo by GFI
   :target: http://http://www.gfi.world/

This module is maintained by GFI.
