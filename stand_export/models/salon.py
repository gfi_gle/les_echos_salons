# -*- coding: utf-8 -*-
import codecs
import datetime
from pytz import timezone
from openerp.addons.account_export.util.unicode_csv import (
    UnicodeWriter,
)
from openerp import models
from openerp import api
from openerp import fields
from openerp import exceptions
from openerp import _
from openerp.addons.account_export.util.timezone import (
    system_timezone_datetime
)
import base64
import datetime
from openerp.osv import osv
from openerp.addons.web import http
from openerp.exceptions import except_orm, Warning
import openerp.addons.web.http as oeweb
from openerp import models, fields, api, _
import openerp
from operator import attrgetter

try:
    import xlwt
except ImportError:
    xlwt = None
import re
from cStringIO import StringIO


def from_data(fields, rows):
    workbook = xlwt.Workbook()
    worksheet = workbook.add_sheet('Sheet 1')

    for i, fieldname in enumerate(fields):
        worksheet.write(0, i, fieldname)
        worksheet.col(i).width = 8000  # around 220 pixels

    base_style = xlwt.easyxf('align: wrap yes')
    date_style = xlwt.easyxf('align: wrap yes', num_format_str='YYYY-MM-DD')
    datetime_style = xlwt.easyxf('align: wrap yes', num_format_str='YYYY-MM-DD HH:mm:SS')

    for row_index, row in enumerate(rows):
        for cell_index, cell_value in enumerate(row):
            cell_style = base_style
            if isinstance(cell_value, basestring):
                cell_value = re.sub("\r", " ", cell_value)
            elif isinstance(cell_value, datetime.datetime):
                cell_style = datetime_style
            elif isinstance(cell_value, datetime.date):
                cell_style = date_style
            worksheet.write(row_index + 1, cell_index, cell_value, cell_style)

    fp = StringIO()
    workbook.save(fp)
    fp.seek(0)
    data = fp.read()
    fp.close()
    return data


class Salon(models.Model):
    _name = 'stand'
    _inherit = 'gle_salon.salon'

    @api.model
    def export_stand_all(self, salon):
        """Export all stand stand.
          """
        mail_to = []

        if salon.user_mail_to_ids:
            for user in salon.user_mail_to_ids:
                mail_to.append(user.email)

        now = fields.Datetime.now()
        last_export = self.env['stand.export'].search([('salon_id', '=', salon.id)], limit=1, order='date desc')
        last_date = last_export.date if last_export else False
        template = self.env.ref('stand_export.salon_email_template')
        values = template.generate_email(template.id, res_id=salon.id)
        # import wdb
        # wdb.set_trace()
        # values = {
        #     'subject': 'OLD List Exposants %s' % salon.name.name,
        #     'body': 'Cher partenaire </br>Vous trouverez ci-joint la liste des exposants ... ',
        #     'body_html':
        #         u"""PJ: List Exposant Les Echos Solutions(fichier Excel)
        #         Cher partenaire
        #         Vous trouverez ci-joint la liste des exposants du %s à la date
        #
        #
        #         - Fichier 1 - Stands passés en ferme depuis le dernier envoi
        #         - Fichier 2 - Stands en ferme dont un attribut au moins a été modifié depuis le dernier envoi.
        #         - Fichier 3 - ensemble des stands en ferme a date.
        #         - Fichier 4 - ensemble des stands en option dont l\'affichage communication est activé a date
        #
        #         Cordialement
        #
        #         Service de Production
        #
        #         Les Echossolution
        #
        #         16 rue du Quatre-septembre
        #         75002 PARIS Cedex 02
        #
        #
        #         """ % salon.name.name
        #     ,
        #     'email_from': 'Les echos solutions',
        #     'email_to': ",".join(mail_to),
        # }

        mail_create = self.env['mail.mail'].create(values)
        attachments = []
        attachment_fermer = self.generate_attachment_fermer(mail_create, last_date, salon)
        for attachment in attachment_fermer:
            if attachment:
                attachments.append(attachment.id)
        mail_create.write({'attachment_ids': [(6, 0, attachments)], })
        mail_create.send()
        self.env['stand.export'].create({'date': now, 'salon_id': salon.id})

    def generate_attachment_data(self, stand_fermer):
        header = [u'N stand', u'Type stand', u'Surface', u'Angle(s)', u'Village', u'Niveau', u'Raison sociale',
                  u'Enseigne*', u'Adresse1', u'Adresse2', u'Adresse3', u'cp', u'Ville', u'Pays',
                  u'Civilite contact stand',
                  u'Prenom contact stand', u'Nom famille contact stand', u'Tel**', u'Portable**', u'Mail',
                  u'Commercial']
        data = []
        data_st = []
        for st in stand_fermer:
            data_st.append(str(st.name) or "")
            data_st.append(st.stand_type_ids.name or "")
            data_st.append(st.area or "")
            data_st.append(st.number_corners.name or "")
            data_st.append(st.village_id.name or "")
            data_st.append(st.stand_level_number or "")
            data_st.append(st.partner_id.name or "")
            enseignes = []
            if st.enseigne_ids:
                for enseigne in st.enseigne_ids:
                    enseignes.append(enseigne.name)
            data_st.append(', '.join(enseignes) or "")
            data_st.append(st.partner_id.street or "")
            data_st.append(st.partner_id.street2 or "")
            data_st.append(st.partner_id.street3 or "")
            data_st.append(st.partner_id.zip or "")
            data_st.append(st.partner_id.city or "")
            data_st.append(st.partner_id.country_id.name or "")

            if st.contact_operational_id.id:
                contact = st.contact_operational_id
                data_st.append(contact.civility or "")
                data_st.append(contact.first_name or "")
                data_st.append(contact.lastname or "")
                data_st.append(contact.phone or "")
                data_st.append(contact.mobile or "")
                data_st.append(contact.email or "")
            data_st.append(st.commercial_echo_id.name or "")
            data.append(data_st)
            data_st = []
        return from_data(header, data)

    def generate_attachment_fermer(self, mail_create, last_date, salon_id):
        display_communication_visible = [('state', '=', 'option'), ('display_communication_visible', '=', 1),
                                         ('salon_id', '=', salon_id.id)]
        stand_fermer = [('state', '=', 'ferme'), ('salon_id', '=', salon_id.id)]
        stand_last_write_fermer = [('write_date', '>', last_date), ('state', '=', 'ferme'),
                                   ('salon_id', '=', salon_id.id)]
        stand_late_fermer = [('date_ferme', '>', last_date), ('state', '=', 'ferme'), ('salon_id', '=', salon_id.id)]
        stand_obj = self.env['gle_salon.stand']
        stand_display_communication_visible = stand_obj.search(display_communication_visible)
        stand_fermer = stand_obj.search(stand_fermer)
        stand_last_fermer = stand_obj.search(stand_late_fermer)
        stand_last_write_fermer = stand_obj.search(stand_last_write_fermer)
        if not stand_late_fermer and not stand_last_write_fermer and not display_communication_visible:
            return False
        attachment_display_communication_visible_data = self.generate_attachment_data(
            stand_display_communication_visible)
        attachment_fermer_data = self.generate_attachment_data(stand_fermer)
        attachment_last_fermer_data = self.generate_attachment_data(stand_last_fermer)
        attachment_last_write_fermer_data = self.generate_attachment_data(stand_last_write_fermer)

        attachment_display_communication_visible = {
            'name': 'Fichier 4- %s/%s/%s.xls' % (salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'datas': base64.b64encode(attachment_display_communication_visible_data),
            'datas_fname': 'Fichier 4- %s/%s/%s.xls' % (
            salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'res_model': mail_create._name,
            'res_id': mail_create.id,
        }

        attachment_fermer = {
            'name': 'Fichier 3- %s/%s/%s.xls' % (salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'datas': base64.b64encode(attachment_fermer_data),
            'datas_fname': 'Fichier 3- %s/%s/%s.xls' % (
            salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'res_model': mail_create._name,
            'res_id': mail_create.id,
        }

        attachment_last_write_fermer = {
            'name': 'Fichier 2- %s/%s/%s.xls' % (salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'datas': base64.b64encode(attachment_last_write_fermer_data),
            'datas_fname': 'Fichier 2- %s/%s/%s.xls' % (
            salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'res_model': mail_create._name,
            'res_id': mail_create.id,
        }

        attachment_last_fermer = {
            'name': 'Fichier 1- %s/%s/%s.xls' % (salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'datas': base64.b64encode(attachment_last_fermer_data),
            'datas_fname': 'Fichier 1- %s/%s/%s.xls' % (
            salon_id.name.name, salon_id.millesime.name, datetime.datetime.now()),
            'res_model': mail_create._name,
            'res_id': mail_create.id,
        }

        attachment_obj = self.env['ir.attachment']

        return [attachment_obj.create(attachment_display_communication_visible),
                attachment_obj.create(attachment_fermer), attachment_obj.create(attachment_last_write_fermer),
                attachment_obj.create(attachment_last_fermer)]

    @api.model
    def export_all_salon(self):
        today_fun = lambda self: fields.Date.today()
        today = today_fun(self)
        salon_all = self.env['gle_salon.salon'].search([])
        for salon in salon_all:
            if not salon.date_debut_mail:
                continue
            if salon.date_debut_mail and salon.date_fin_mail:
                if salon.date_debut_mail and salon.date_debut_mail > today:
                    continue
                if salon.date_fin_mail and salon.date_fin_mail < today:
                    continue
            self.export_stand_all(salon)
