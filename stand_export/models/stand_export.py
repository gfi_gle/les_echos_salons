from openerp import models, fields, api, fields, _
import datetime


class StandExport(models.Model):
    _name = 'stand.export'

    date = fields.Datetime('now', readonly=True)
    salon_id = fields.Many2one(comodel_name='gle_salon.salon')
