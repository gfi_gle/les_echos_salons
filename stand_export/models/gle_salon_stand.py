from openerp import models, fields, api, _
import datetime


class GleSalonStand(models.Model):
    _name = 'gle.salon.stand'
    _inherit = 'gle_salon.stand'

    date_ferme = fields.Datetime('date ferme', readonly=True)
