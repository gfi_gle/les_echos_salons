# -*- coding: utf-8 -*-
##############################################################################
#
#    Account Export, for OpenERP
#    Copyright (C) 2017 GFI (http://gfi.world)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Stand Export",
    "version": '8.0.1.0',
    "author": "GFI",
    "category": 'Salon',
    "description": """
Stand Export
==============

Exports of stand.

Configuration
-------------

No specific configuration.
    """,
    'website': 'http://gfi.world',
    'init_xml': [],
    "depends": [
        'mail',
        'contacts',
        'gle_salon',
    ],
    "data": [
        'data/ir_cron.xml',
        'views/mail_template.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
