.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

========
gle_crm
========

This module is used to extend crm model

Functionnality
===============

#. It extends the existing model :
- crm.lead 
- res.partner
- crm.make.sale 
 
#. It add a new Transient data model :
- reafect_mass_comemrcial_allocation_transient

The Transient model represent the mass reafectation for the view

Bug Tracker
============

Incident tracking is done with the GFI Jira platform.

Credits
=======

Contributors
------------

* alexandre.matis@gfi.fr
* william.musy@gfi.fr

Maintainer
----------

.. image:: image url (http://www.gfi....image.png)
   :alt:  Odoo by GFI
   :target: http://http://www.gfi.world/

This module is maintained by GFI.
