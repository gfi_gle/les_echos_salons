$.fn.waitUntilExistssecurity    = function (handler, shouldRunHandlerOnce, isChild) {
    var found       = 'found';
    var $this       = $(this.selector);
    var $elements   = $this.not(function () { return $(this).data(found); }).each(handler).data(found, true);
    if (!isChild)
    {
        (window.waitUntilExistssecurity_Intervals = window.waitUntilExistssecurity_Intervals || {})[this.selector] =
            window.setInterval(function () { $this.waitUntilExistssecurity(handler, shouldRunHandlerOnce, true); }, 100)
        ;
    }
    else if (shouldRunHandlerOnce && $elements.length)
    {
    }
    return $this;
}

$('.button_click').waitUntilExistssecurity(function(){
    setTimeout(function(){
        $(".button_click").click(function(){
            setTimeout(function(){
                    $(".close").trigger("click");
                  }, 600);
        });
    }, 100);
});