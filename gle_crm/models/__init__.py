# -*- coding: utf-8 -*-

from . import partner
from . import user
from . import crm_lead
from . import reafect_mass_commercial_allocation
from . import commercial_allocation_partner
from . import commercial_allocation_crm
from . import line_product
from . import line_article
from . import status_partner
