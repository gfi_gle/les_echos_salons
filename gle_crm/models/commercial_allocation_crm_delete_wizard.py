# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class CommercialAllocationCrmDeleteWizard(models.TransientModel):
    """"""

    _name = 'gle_crm.commercial_allocation_crm_delete_wizard'

    def _init_commercial_list_crm(self):
        """"""

        commercial_allocation_id = self._context.get('commercial_allocation_id', False)
        list_commercial_already_ids = []

        if commercial_allocation_id:
            list_commercial_already_ids = commercial_allocation_id[0][2]

        res_users_obj = self.env['res.users']
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']

        allocation_ids = commercial_allocation_obj.search([('id', 'in', list_commercial_already_ids)])

        lst_allocation = []
        for allocation in allocation_ids:
            lst_allocation.append(allocation.commercial_id.id)

        result_recordset = res_users_obj.search([('id', 'in', lst_allocation)])

        list_commercial_ids = []

        for id_commercial in result_recordset:
            list_commercial_ids.append(id_commercial.id)

        return list_commercial_ids

    def _init_crm_lead_id(self):
        return self._context.get('crm_lead_id')

    commercial_ids = fields.Many2many('res.users', string="Commercials")
    commercial_list_crm = fields.Many2many('res.users', default=_init_commercial_list_crm)
    crm_lead_id = fields.Many2one('crm.lead', string="Société", default=_init_crm_lead_id)

    @api.multi
    def action_delete_commercial_allocation(self):
        """"""

        ids_unlink = []
        crm_lead_id = self.crm_lead_id.id

        for commercial in self.commercial_ids:
            ids_unlink.append(commercial.id)

        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']

        commercial_allocation_obj.search([('commercial_id', 'in', ids_unlink), ('crm_lead_id', '=', self.crm_lead_id.id)]).unlink()

        for id in ids_unlink:
            sql_query = 'DELETE FROM crm_lead_gle_crm_commercial_allocation_crm_rel WHERE crm_lead_id = {} AND gle_crm_commercial_allocation_crm_id = {}'.format(crm_lead_id, id)
            self.env.cr.execute(sql_query)

        crm_lead_obj = self.env['crm.lead'].search([('id', '=', crm_lead_id)])

        record_set = commercial_allocation_obj.search([('crm_lead_id', '=', crm_lead_id)])
        length_record_set = len(record_set)

        if length_record_set == 1:
            for commercial_allocation in crm_lead_obj.commercial_allocation_id:
                commercial_allocation.write({'percentage': 100})
        elif length_record_set == 2:

            view_id = self.env['ir.model.data'].get_object_reference('gle_crm', 'gle_crm_equilize_percentage_wizard_view')

            return {
                'name': "Attribution commercial pour l'opportunité",
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_crm.equilize_percentage_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {'crm_lead_id': crm_lead_id}
            }
