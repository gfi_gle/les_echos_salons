# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.exceptions import except_orm
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)

class CaLastYear(models.Model):
    """"""

    _name = 'gle_crm.ca_last_year'

    salon_already_exist = fields.Many2many(comodel_name='gle_salon.salon', default= lambda self:self._init_salon_already_exist())

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")
    partner_id = fields.Many2one(comodel_name='res.partner')

    ca_last_year = fields.Float(string="Chiffre d'affaire")

    @api.multi
    def _init_salon_already_exist(self):
        """"""

        ca_obj = self.env['gle_crm.ca_last_year']

        if self._context.get('default_ca_ids'):
            ca_ids = self._context.get('default_ca_ids')
            ca_list_ids = []
            salon_already_exist_ids = []

            for ca_id in ca_ids:
                if ca_id[1]:
                    ca_list_ids.append(ca_id[1])

            for ca_id in ca_ids:
                if ca_id[2]:
                    temp_dict = ca_id[2]
                    salon_already_exist_ids.append(temp_dict.get('salon_id'))

            if ca_list_ids:
                for ca in ca_obj.browse(ca_list_ids):

                    salon_already_exist_ids.append(ca.salon_id.id)

                return salon_already_exist_ids
