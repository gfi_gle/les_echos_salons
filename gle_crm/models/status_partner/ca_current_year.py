# -*- coding: utf-8 -*-
import sys

import datetime
import time

reload(sys)
sys.setdefaultencoding('utf-8')



from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.exceptions import except_orm
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)
state_sale_order = ['valid','valid_d','manual','progress','done']

class CaCurrentYear(models.Model):
    """"""
    _name = 'gle_crm.ca_current_year'

    partner_id = fields.Many2one('res.partner')
    salon_id = fields.Many2one('gle_salon.salon')
    ca_current = fields.Integer()


    @api.multi
    def _get_list_ca(self,partner_id):
        _logger.debug(self.env['gle_crm.ca_current_year'].search([('partner_id', "=", partner_id.id)]))
        return self.env['gle_crm.ca_current_year'].search([('partner_id', "=", partner_id.id)])


    @api.multi
    def _prepare_all_ca(self,partner_id):
        self._prepare_ca_current_year(partner_id)


    @api.multi
    def _prepare_ca_current_year(self,partner_id):
        year_current = datetime.datetime.today().strftime("%Y")
        xmillesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_current)])
        salon_list_current = self.env['gle_salon.salon'].search([('millesime', '=', xmillesime_id.id)])

        for xsalon in salon_list_current:

            xlist_verif_exists = self.env['gle_crm.ca_current_year'].search([('salon_id','=', xsalon.id),('partner_id','=',partner_id.id)])
            xsale_order = self.env['sale.order'].search([('salon_id','=',xsalon.id),('partner_id','=',partner_id.id),('state','in',state_sale_order)])
            # xaccount_invoice = self.env['account.invoice'].search([('salon_id','=',xsalon.id), ('partner_id', '=', partner_id.id),('state', '!=', 'cancel')])

            xcumulht = 0
            # for xaccount in xaccount_invoice:
            #     xcumulht += xaccount.amount_untaxed
            for xaccount in xsale_order:
                xcumulht += xaccount.amount_untaxed

            if xlist_verif_exists:
                # update
                xlist_verif_exists.write({'ca_current':xcumulht})
            else:
                # create
                self.env['gle_crm.ca_current_year'].create({'salon_id':xsalon.id,'ca_current': xcumulht,'partner_id':partner_id.id})


    @api.multi
    def _prepare_ca_last_year(self,partner_id):
        year_before = int(datetime.datetime.today().strftime("%Y"))-1
        xmillesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before)])
        salon_list_current = self.env['gle_salon.salon'].search([('millesime', '=', xmillesime_id.id)])

    @api.multi
    def _prepare_ca_last_year_before(self,partner_id):
        year_before_before = int(datetime.datetime.today().strftime("%Y")) - 2
        xmillesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before_before)])
        salon_list_current = self.env['gle_salon.salon'].search([('millesime', '=', xmillesime_id.id)])



class CaBeforeYear(models.Model):
    """"""
    _name = 'gle_crm.ca_before_year'

    partner_id = fields.Many2one('res.partner')
    salon_id = fields.Many2one('gle_salon.salon')
    ca_before = fields.Integer()


    @api.multi
    def _get_list_ca(self,partner_id):
        return self.env['gle_crm.ca_before_year'].search([('partner_id', "=", partner_id.id)])


    @api.multi
    def _prepare_all_ca(self,partner_id):
        self._prepare_ca_before_year(partner_id)

    @api.multi
    def _prepare_ca_before_year(self,partner_id):
        year_before = int(datetime.datetime.today().strftime("%Y"))-1

        xmillesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before)])
        salon_list_before = self.env['gle_salon.salon'].search([('millesime', '=', xmillesime_id.id)])

        for xsalon in salon_list_before:

            xlist_verif_exists = self.env['gle_crm.ca_before_year'].search([('salon_id','=', xsalon.id),('partner_id','=',partner_id.id)])
            xaccount_invoice = self.env['account.invoice'].search([('salon_id','=',xsalon.id), ('partner_id', '=', partner_id.id),('state', '!=', 'cancel')])

            xcumulht = 0
            for xaccount in xaccount_invoice:
                xcumulht += xaccount.amount_untaxed

            if xlist_verif_exists:
                # update
                xlist_verif_exists.write({'ca_before':xcumulht})
            else:
                # create
                self.env['gle_crm.ca_before_year'].create({'salon_id':xsalon.id,'ca_before': xcumulht,'partner_id':partner_id.id})

class CaBeforeBeforeYear(models.Model):
    """"""
    _name = 'gle_crm.ca_before_before_year'

    partner_id = fields.Many2one('res.partner')
    salon_id = fields.Many2one('gle_salon.salon')
    ca_before_before = fields.Integer()


    @api.multi
    def _get_list_ca(self,partner_id):
        return self.env['gle_crm.ca_before_before_year'].search([('partner_id', "=", partner_id.id)])


    @api.multi
    def _prepare_all_ca(self,partner_id):
        self._prepare_ca_before_before_year(partner_id)

    @api.multi
    def _prepare_ca_before_before_year(self,partner_id):
        year_before = int(datetime.datetime.today().strftime("%Y"))-2

        xmillesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before)])
        salon_list_before = self.env['gle_salon.salon'].search([('millesime', '=', xmillesime_id.id)])

        for xsalon in salon_list_before:

            xlist_verif_exists = self.env['gle_crm.ca_before_before_year'].search([('salon_id','=', xsalon.id),('partner_id','=',partner_id.id)])
            xaccount_invoice = self.env['account.invoice'].search([('salon_id','=',xsalon.id), ('partner_id', '=', partner_id.id),('state', '!=', 'cancel')])

            xcumulht = 0
            for xaccount in xaccount_invoice:
                xcumulht += xaccount.amount_untaxed

            if xlist_verif_exists:
                # update
                xlist_verif_exists.write({'ca_before_before':xcumulht})
            else:
                # create
                self.env['gle_crm.ca_before_before_year'].create({'salon_id':xsalon.id,'ca_before_before': xcumulht,'partner_id':partner_id.id})