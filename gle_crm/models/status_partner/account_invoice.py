# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    """ Model extend from account.invoice model to override methods """

    _inherit = 'account.invoice'

    @api.multi
    def invoice_validate(self):

        for inv in self:
            xtest_error=0
            xgenerator_error = "La facture ne peut pas etre validé, information(s) manquante(s) dans la société:"

            xsiret_test = inv.partner_id.number_siret

            if xtest_error>0:
                raise ValidationError(xgenerator_error)

            if not inv.intercos:
                # CYCLE NORMAL D'ODOO
                super(AccountInvoice, self).invoice_validate()

            inv.state = 'open'

    @api.model
    def create(self, vals):
        """ Override create method to change status res.partner if invoice is created """

        account_invoice_created = super(AccountInvoice, self).create(vals)
        self.search_status_partner(new_obj=account_invoice_created)

        return account_invoice_created

    @api.multi
    def unlink(self):
        """ Override unlink method to delete status partner object. """

        query_delete = 'DELETE FROM gle_crm_status_partner_account_invoice_rel WHERE account_invoice_id = {};'

        status_partner_obj = self.env['gle_crm.status_partner_models']

        for account_invoice in self:
            result_recordset = status_partner_obj.search(
                [('partner_id', '=', account_invoice.partner_id.id), ('salon_id', '=', account_invoice.salon_id.id)])

            length_sale_order_ids = len(result_recordset.sale_order_id)
            length_crm_lead_ids = len(result_recordset.crm_lead_id)
            length_account_invoice = len(result_recordset.account_invoice_id)

            if length_account_invoice > 1:
                for account_invoice_rec in result_recordset.account_invoice_id:
                    self.env.cr.execute(query_delete.format(account_invoice_rec.id))
            elif length_account_invoice == 1 and length_crm_lead_ids == 0 and length_sale_order_ids == 0:
                status_partner_obj.search([('id', '=', result_recordset.id)]).unlink()

            super(AccountInvoice, account_invoice).unlink()

    def search_status_partner(self, new_obj):
        """
            Method to change status of res.partner if record exist.
            If record of status object not exist create one.
        """

        query_insert = 'INSERT INTO gle_crm_status_partner_account_invoice_rel (status_partner_id, account_invoice_id) VALUES ({}, {});'

        if new_obj.partner_id.id:
            status_partner_obj = self.env['gle_crm.status_partner_models']
            result_recordset = status_partner_obj.search([('partner_id', '=', new_obj.partner_id.id), ('salon_id', '=', new_obj.salon_id.id)])

            if not result_recordset:
                vals = {
                    'salon_id': new_obj.salon_id.id,
                    'partner_id': new_obj.partner_id.id,
                    'partner_type': 'client'
                }

                new_status_obj = status_partner_obj.create(vals)
                self.env.cr.execute(query_insert.format(new_status_obj.id, new_obj.id))

            elif result_recordset:

                result_recordset.write({'partner_type': 'client'})
                self.env.cr.execute(query_insert.format(result_recordset.id, new_obj.id))
