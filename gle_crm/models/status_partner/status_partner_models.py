# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.exceptions import except_orm
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)

class StatusPartnerModels(models.Model):
    """"""

    _name = 'gle_crm.status_partner_models'

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")

    partner_id = fields.Many2one(comodel_name='res.partner', string="Société")
    partner_type = fields.Selection([('prospect', "Prospect"), ('client', "Client")], string="Status")

    crm_lead_id = fields.Many2many(comodel_name='crm.lead', relation='gle_crm_status_partner_crm_lead_rel', column1='status_partner_id', column2='crm_lead_id', string="Piste / Opportunité")
    sale_order_id = fields.Many2many(comodel_name='sale.order', relation='gle_crm_status_partner_sale_order_rel', column1='status_partner_id', column2='sale_order_id', string="Devis")
    account_invoice_id = fields.Many2many(comodel_name='account.invoice', relation='gle_crm_status_partner_account_invoice_rel', column1='status_partner_id', column2='account_invoice_id', string="Facture")

