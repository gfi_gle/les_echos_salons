# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging


_logger = logging.getLogger(__name__)

class CrmLead(models.Model):
    """ Class inherit from crm.lead to add fields and methods """
    _inherit = 'crm.lead'

    # Default order used for reporting views
    _order = 'propose_area_square_meter desc'



    propose_area_square_meter = fields.Float(string="Nombre de M² proposés")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")
    village_id = fields.Many2one('gle_salon.village', string="Village")

    wich_stand_1 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°1")
    wich_stand_2 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°2")
    wich_stand_3 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°3")

    commercial_allocation_id = fields.Many2many('gle_crm.commercial_allocation_crm', string="Attribution commercial", default= lambda self: self._default_current_user_commercial_allocation())

    note_credit_safe_ids = fields.Many2many('gle_crm.note_credit_safe', compute="_compute_note")

    # Attribute is True when probability is equal or greater than 80%
    partner_required = fields.Boolean(default=False)

    wich_product_list = fields.Many2many('product.product', string="Liste de produits souhaités")

    square_meter_last_year = fields.Float(string="M²", compute="_square_meter_last_year", store=True)

    stand_last_year = fields.Char(string="Stand", compute="_stand_last_year")
    type_stand_last_year = fields.Char(string="Stand", compute="_type_stand_last_year")
    village_last_year = fields.Char(string="Village", compute="_village_last_year")

    @api.model
    def create(self, vals):
        """"""

        new_id = super(CrmLead, self).create(vals)

        self._associated_crm_lead_id_to_commercial_allocation(new_id)
        self._clean_table_commercial_allocation_crm()

        return new_id

    @api.multi
    def action_add_commercial_allocation_crm_wizard(self):
        """ Method triggered by button on crm_lead_view, and display a wizard form view to delete comemrcial attribution """

        commercial_allocation_crm_obj = self.env['gle_crm.commercial_allocation_crm']
        commercial_allocation_ids_length = 0

        for commercial_allocation in self.commercial_allocation_id:
            commercial_allocation_ids_length += 1

        if commercial_allocation_ids_length == 1:
            view_id = 'commercial_allocation_crm_wizard_view'
            res_model = 'gle_crm.commercial_allocation_crm_wizard'

            return self.launch_commercial_allocation_crm_wizard(view_id, res_model)

        elif commercial_allocation_ids_length ==2:
            view_id = 'commercial_allocation_three_add_wizard_view'
            res_model = 'gle_crm.commercial_allocation_three_add_wizard'

            return self.launch_commercial_allocation_crm_wizard(view_id, res_model)

        elif commercial_allocation_ids_length >= 3:
            raise ValidationError("Le nombre maximum d'allocation commercial est de trois.")

    @api.multi
    def action_delete_commercial_allocation_crm_wizard(self):
        """ Method triggered by button on crm_lead_view, and display a wizard form view to delete comemrcial attribution """

        view_id = 'commercial_allocation_crm_delete_wizard_view'
        res_model = 'gle_crm.commercial_allocation_crm_delete_wizard'

        return self.launch_commercial_allocation_crm_wizard(view_id, res_model)

    def _default_current_user_commercial_allocation(self):
        """"""

        user_id = self._context.get('uid')
        vals = {'commercial_id': user_id, 'percentage': 100}
        commercial_allocation_created = self.env['gle_crm.commercial_allocation_crm'].create(vals)

        return commercial_allocation_created

    def _clean_table_commercial_allocation_crm(self):
        """"""

        recorset = self.env['gle_crm.commercial_allocation_crm'].search([])

        ids_to_delete = []
        for commercial_allocation in recorset:
            if commercial_allocation.crm_lead_id.id is False:
                ids_to_delete.append(commercial_allocation.id)

        self.env['gle_crm.commercial_allocation_crm'].search([('id', 'in', ids_to_delete)]).unlink()

    def _associated_crm_lead_id_to_commercial_allocation(self, new_id):
        """"""

        commercial_allocation_crm_obj = self.env['gle_crm.commercial_allocation_crm']

        for commercial_allocation in new_id.commercial_allocation_id:
            result = commercial_allocation_crm_obj.search([('id', '=', commercial_allocation.id)])

            if result.crm_lead_id.id is False:
                vals = {
                    'crm_lead_id': new_id
                }

                result.update(vals)

    def launch_commercial_allocation_crm_wizard(self, view_id, res_model):
        """"""

        crm_lead_id = self.id
        commercial_allocation_ids = []

        for commercial_allocation in self.commercial_allocation_id:
            commercial_allocation_ids.append(commercial_allocation.id)

        view_id = self.env['ir.model.data'].get_object_reference('gle_crm', view_id)

        return {
            'name': _("Attribution commercial pour l'opportunité"),
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': res_model,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'crm_lead_id': crm_lead_id, 'commercial_ids': commercial_allocation_ids}
        }

    @api.constrains('probability')
    def _constrain_probability(self):
        """ Method constrains probability check if number Siret is defined and Note credit safe active if probability is greater than 80% """

        if self.probability >= 80:

            if not self.check_number_siret():
                raise ValidationError("La probabilité est de 80% ou plus, veuillez renseigner le numéro de Siret")

            if not self.check_active_note():
                raise ValidationError(
                    "La probabilité est de 80% ou plus, veuillez renseigner une note de credit safe active")

    @api.onchange('probability')
    def _change_probability(self):
        """ Check if the probability is greater than 80% and check if Note Safe Credit is not empty """

        if self.probability >= 80:
            self.partner_required = True
        else:
            self.partner_required = False

    @api.onchange('partner_id')
    def _on_changepartner_id(self):
        """ On change partner_id inform field email_from and phone  """

        self.email_from = self.partner_id.email
        self.phone = self.partner_id.phone

    @api.depends('partner_id')
    def _compute_note(self):
        """ Method to compute note_credit_safe_ids, search Note Safe Credit associated to partner_id """

        related_recordset = self.env['gle_crm.note_credit_safe'].search([('partner_id', '=', int(self.partner_id))])
        self.note_credit_safe_ids = related_recordset

    @api.depends('partner_id')
    def _square_meter_last_year(self):
        """ Method to compute square meter sold for a company and add it in the reporting of the opportunities """

        for crm_lead in self:

            current_millesime = crm_lead.salon_id.millesime
            current_place_id = crm_lead.salon_id.place.id
            year_before_millesime = int(current_millesime) - 1

            salon_before_obj = self.env['gle_salon.salon'].search([('millesime', '=', year_before_millesime),('name', '=', crm_lead.salon_id.name.name),('place', '=', current_place_id)])
            sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)),('salon_id', '=', int(salon_before_obj.id)),('state', '=', 'done')])
            sum_areas_before = 0

            for sale_order in sale_order_before:
                sum_areas_before += sale_order.stand_id.area

                crm_lead.square_meter_last_year = sum_areas_before

    @api.depends('partner_id')
    def _stand_last_year(self):
        """ Method to compute stand sold for a company and add it in the reporting of the opportunities """
        for crm_lead in self:
            current_millesime = crm_lead.salon_id.millesime
            current_place_id = crm_lead.salon_id.place.id
            year_before_millesime = int(current_millesime) - 1

            salon_before_obj = self.env['gle_salon.salon'].search(
                [('millesime', '=', year_before_millesime), ('name', '=', crm_lead.salon_id.name.name),
                 ('place', '=', current_place_id)])
            sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(salon_before_obj.id)), ('state', '=', 'done')])
            compile_stand_before = ""

            for sale_order in sale_order_before:
                if sale_order.stand_id.name:
                    compile_stand_before = compile_stand_before + '/' + sale_order.stand_id.name
            crm_lead.stand_last_year = compile_stand_before

    @api.depends('partner_id')
    def _type_stand_last_year(self):
        """ Method to compute stand type sold for a company and add it in the reporting of the opportunities """
        for crm_lead in self:
           sale_order_ids = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)),('salon_id', '=', int(crm_lead.salon_id))])
           for sale_order in sale_order_ids:
              crm_lead.type_stand_last_year = sale_order.stand_id.name

    @api.depends('partner_id')
    def _village_last_year(self):
      """ Method to compute village sold for a company and add it in the reporting of the opportunities """
      for crm_lead in self:
          sale_order_ids = self.env['sale.order'].search(
              [('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(crm_lead.salon_id))])
          for sale_order in sale_order_ids:
              crm_lead.village_last_year = sale_order.stand_id.village_id.name


    def check_active_note(self):
        """ Check if a Note credit safe is active for the associated society, return True if Note credit safe is active """

        result = False

        for note in self.partner_id.note_credit_safe_id:
            if note.state == 'active':
                result = True

        return result

    def check_number_siret(self):
        """ Check if a Number Siret is defined for the associated society, return True if Note credit safe is active """

        result = False

        if self.partner_id.number_siret:
            result = True

        return result

    @api.onchange('partner_id')
    def on_change_partner(self):
        """ Auto-complete Address information when partner_id is change """

        self.partner_name = self.partner_id.name
        self.street = self.partner_id.street
        self.street2 = self.partner_id.street2
        self.street3 = self.partner_id.street3
        self.zip = self.partner_id.zip
        self.city = self.partner_id.city
        self.state_id = self.partner_id.state_id


class CrmLeadReportingPositionnement(models.Model):
    """ Class inherit from crm.lead to add fields and methods """
    _inherit = 'crm.lead'

    # Default order used for reporting positionning views
    _order = 'wich_stand_1 desc'