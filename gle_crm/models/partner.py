# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import osv, models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.exceptions import except_orm
import warnings
import datetime
import time
import logging


_logger = logging.getLogger(__name__)


class TypeCompany(models.Model):
    """ Class representing the model Type Company """

    _name = 'gle_crm.type_company'
    name = fields.Char(string="Type de société")

    @api.multi
    def unlink(self):
        """ Override unlink method, method check not to delete annonceur used as default in company """
        for line in self:
            if line.name == 'Annonceur':
                raise except_orm('Warning', 'Le type de société Annonceur ne peut pas etre supprimé!')
        return super(TypeCompany, self).unlink()


    @api.onchange('name')
    def on_change_type_societe(self):
        """ On_change method on type_societe name. It must stay unique. """

        record_set = self.env['gle_crm.type_company'].search([('name', '=', self.name)])

        if record_set:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Ce type de société est déja disponible."))
            }}

            return res


class Partner(models.Model):
    """ Class inherit from res.partner and is common for Model Society and Contact """

    _inherit = 'res.partner'

    _defaults = {
        'country_id': 76,
    }


    """ Common fields """
    street3 = fields.Char(string="Rue 3...")
    attachment = fields.Binary("Pièce jointe")
    attachment_filename = fields.Char("Nom du fichier")
    """ Society fields """
    number_siret = fields.Char(string="N°SIRET / CODE SIREN")
    number_insee = fields.Char(string="CODE INSEE")
    type_company_id = fields.Many2one('gle_crm.type_company', string="Type de société", default= lambda self:self.type_company_init())
    note_credit_safe_id = fields.One2many('gle_crm.note_credit_safe', 'partner_id', string="Note credit safe")

    """ Secteur """
    sector_lvl_one_id = fields.Many2many(comodel_name= "gle_crm.sector", relation='gle_crm_sector_one', column1="sector_one_id", column2="partner_id", string="Secteur 1", domain=[('sector_level', '=', '1')])
    sector_lvl_two_id = fields.Many2many(comodel_name= "gle_crm.sector", relation='gle_crm_sector_two', column1="sector_two_id", column2="partner_id", string="Secteur 2", domain=[('sector_level', '=', '2')])
    sector_lvl_three_id = fields.Many2many(comodel_name="gle_crm.sector", relation='gle_crm_sector_three', column1="sector_three_id", column2="partner_id",string="Secteur 3", domain=[('sector_level', '=', '3')])

    centers_of_interest_ids = fields.Many2many('gle_crm.centers_of_interest', string="Centres d'intérêt")
    commercial_allocation_ids = fields.Many2many(comodel_name='res.users', relation='gle_crm_res_users_relation', column1='partner_id', column2='res_user_id', default= lambda self: self._default_current_user_commercial_allocation())
    number_tva = fields.Char(string="Numéro de TVA")
    client_code_compta = fields.Char(string="Code client (Compta)")
    """ Contact fields """
    first_name = fields.Char(string="Prénom")
    title_echos = fields.Char(string="Titre")

    civility = fields.Selection([('mlle', "Mademoiselle"),
                                 ('mme', "Madame"),
                                 ('mr', "Monsieur"),
                                 ('dr', "Docteur"),
                                 ('pr', "Professeur"),
                                 ('me', "Maître")], string="Civilité")

    date_departue = fields.Date(string="Date de départ")
    account_twitter = fields.Char(string="Compte Twitter")
    account_linkedin = fields.Char(string="Compte Linkedin")
    account_facebook = fields.Char(string="Compte Facebook")
    # type_contact = fields.Selection([('decideur', "Décideur"), ('supporter', "Supporter"), ('do', "Donneur d'ordre"), ('cop', "Contact Operationnel du DO"), ('com', "Commercial")], string="Type de contact")
    type_contact = fields.Many2one('gle_crm.type_contact', string="Type de contact")
    key_words_ids = fields.Many2many('gle_crm.key_words', string="Mots clés")

    status_partner_id = fields.One2many(comodel_name='gle_crm.status_partner_models', inverse_name='partner_id', string="Status")
    ca_last_year_ids = fields.One2many(comodel_name='gle_crm.ca_last_year', inverse_name='partner_id', string="Chiffre d'affaire")

    property_product_pricelist = fields.Many2one(
        comodel_name='product.pricelist',
        domain=[('type', '=', 'sale')],
        string="Sale Pricelist",
        help="This pricelist will be used, instead of the default one, for sales to the current partner")

    property_product_pricelist_purchase = fields.Many2one(comodel_name='product.pricelist',
                                                          domain=[('type','=','purchase')],
                                                          string="Purchase Pricelist",
                                                          help="This pricelist will be used, instead of the default one, for purchases from the current partner")

    """Override fields"""
    company_type = fields.Selection([('company', 'Company'), ('person', 'Enseigne')])

    """ MODIFICATION 06/12 suite incident _move """
    gle_function = fields.Many2one(comodel_name='gle_crm.function', string="Fonction")


    state_id = fields.Many2one(comodel_name='res.country.state', compute='compute_zip', string="Région")

    property_account_receivable = fields.Many2one(
        comodel_name='account.account',
        string="Account Receivable",
        domain="[('type', '=', 'receivable')]",
        help="This account will be used instead of the default one as the receivable account for the current partner",
        required=False)

    property_account_payable = fields.Many2one(
        comodel_name='account.account',
        string="Account Payable",
        domain="[('type', '=', 'payable')]",
        help="This account will be used instead of the default one as the payable account for the current partner",
        required=False)

    concat_partner_name = fields.Char(string="Contact(s)", compute='_compute_concat_contact_name', store=True)

    contact_opportunity_count = fields.Char(string="opp_count", compute="_compute_opp_count")
    contact_phone_count = fields.Char(string="phone_count", compute="_compute_phone_count")
    contact_meeting_count = fields.Char(string="meeting_count", compute="_compute_meeting_count")

    ca_current_year_ids = fields.One2many(comodel_name='gle_crm.ca_current_year',inverse_name='partner_id',
                                          string="Ca current year",
                                          compute="_default_current_ca_current_year_relation")

    ca_before_year_ids = fields.One2many(comodel_name='gle_crm.ca_before_year', inverse_name='partner_id',
                                         string="Ca before year",
                                         compute="_default_before_ca_before_year_relation")

    ca_before_before_year_ids = fields.One2many(comodel_name='gle_crm.ca_before_before_year', inverse_name='partner_id',
                                                string="Ca before before year",
                                                compute="_default_before_ca_before_before_year_relation")

    label_year_now = fields.Integer(default=(int(datetime.datetime.today().strftime("%Y"))))
    label_year_before = fields.Integer(default=(int(datetime.datetime.today().strftime("%Y"))-1))
    label_year_before_before = fields.Integer(default=(int(datetime.datetime.today().strftime("%Y"))-2))

    name_gle = fields.Char(string="Name")

    is_customer_gle = fields.Boolean(string="customer")
    is_supplier_gle = fields.Boolean(string="supplier")
    first_value_supplier_gle = fields.Boolean(string="supplier")
    first_value_customer_gle = fields.Boolean(string="customer")


    # default by onchange on property_product_pricelist
    @api.multi
    @api.onchange('property_product_pricelist')
    def onchange_property_product_pricelist(self):
        if not self.property_product_pricelist:
            property_product_pricelist_search_onchange = self.env['product.pricelist'].search([("type", '=', 'sale')], limit=1)
            self.property_product_pricelist = property_product_pricelist_search_onchange


    @api.multi
    @api.onchange('is_operational_supplier')
    def onchange_is_operational_supplier_gle(self):
        if not self.first_value_supplier_gle:
            self.is_supplier_gle = self.is_operational_supplier
        else:
            self.first_value_supplier_gle = False

        if not self.is_operational_supplier and not self.is_operational_client and not self.partner_info_id:
            self.partner_info_required = False




    @api.multi
    @api.onchange('is_operational_client')
    def onchange_is_operational_client_gle(self):

        if not self.first_value_customer_gle:
            self.is_customer_gle = self.is_operational_client
        else:
            self.first_value_customer_gle = False


        if not self.is_operational_supplier and not self.is_operational_client and not self.partner_info_id:
            self.partner_info_required = False




    # default by onchange on property_product_pricelist_purchase
    @api.multi
    @api.onchange('property_product_pricelist_purchase')
    def onchange_property_product_pricelist_purchase(self):
        if not self.property_product_pricelist_purchase:
            property_product_pricelist_purchase_search = self.env['product.pricelist'].search([("type", '=', 'purchase')], limit=1)
            self.property_product_pricelist_purchase = property_product_pricelist_purchase_search


    @api.multi
    @api.onchange('name_gle')
    def onchange_name_gle(self):
        for partner in self:
            if partner.name_gle:
                partner.display_name = partner.name_gle
                partner.lastname = partner.name_gle.upper()
                partner.name_gle = partner.name_gle.upper()

    # def force_compute_concat_partner_name(self):
    #     _logger.debug("------------------------------passe ici force")
    #     xall_partner = self.env['res.partner'].search([])
    #     if xall_partner:
    #         for partner in xall_partner:
    #             _logger.debug("------------------------------passe ici force 2")
    #             concat_name = str()
    #             contact_name_list = [contact.name for contact in partner.child_ids if contact.type == 'contact']
    #
    #             for contact_name in contact_name_list:
    #                 concat_name += "{0} \n".format(contact_name)
    #
    #             partner.concat_partner_name = concat_name

    @api.depends('child_ids')
    def _compute_concat_contact_name(self):

        for partner in self:

            concat_name = str()
            contact_name_list = [contact.name for contact in partner.child_ids if contact.type == 'contact']

            for contact_name in contact_name_list:
                concat_name += "{0} \n".format(contact_name)

            partner.concat_partner_name = concat_name

    @api.depends("name")
    def _default_current_ca_current_year_relation(self):
        for res_partner in self:
            if res_partner.is_company == True:
                self.env['gle_crm.ca_current_year']._prepare_all_ca(res_partner)
                list = self.env['gle_crm.ca_current_year']._get_list_ca(res_partner)
                res_partner.ca_current_year_ids = list

    @api.depends("name")
    def _default_before_ca_before_year_relation(self):

        for res_partner in self:
            if res_partner.is_company == True:
                self.env['gle_crm.ca_before_year']._prepare_all_ca(res_partner)
                list = self.env['gle_crm.ca_before_year']._get_list_ca(res_partner)
                res_partner.ca_before_year_ids = list

    @api.depends("name")
    def _default_before_ca_before_before_year_relation(self):

        for res_partner in self:
            if res_partner.is_company == True:
                self.env['gle_crm.ca_before_before_year']._prepare_all_ca(res_partner)
                list = self.env['gle_crm.ca_before_before_year']._get_list_ca(res_partner)
                res_partner.ca_before_before_year_ids = list

    @api.multi
    def detail_year_now(self):
        """
        Method to open tree sale order
        """

        view_id = self.env['ir.model.data'].get_object_reference('sale', 'view_order_tree')
        res = {
            'type': 'ir.actions.act_window',
            'name': _('Devis validés / facturés'),
            'res_model': 'sale.order',
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': view_id[1],
            'domain': [('partner_id', '=', self.id),('state', '!=', 'draft'),('date_order', '>=', time.strftime('%Y-01-01 00:00:00')),('date_order', '<=', time.strftime('%Y-12-31 23:59:59'))]
        }

        return res

    @api.multi
    def detail_year_before(self):
        """
        Method to open tree sale order
        """
        label_year_before = (int(datetime.datetime.today().strftime("%Y")) - 1)
        time_start = str(label_year_before)+'-01-01 00:00:00'
        time_end = str(label_year_before)+'-12-31 23:59:59'

        view_id = self.env['ir.model.data'].get_object_reference('account', 'invoice_tree')
        res = {
            'type': 'ir.actions.act_window',
            'name': _('Factures N-1'),
            'res_model': 'account.invoice',
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': view_id[1],
            'domain': [('partner_id', '=', self.id),('state', '!=', 'draft'),('write_date', '>=', time.strftime(time_start)),('write_date', '<=', time.strftime(time_end))]
        }

        return res

    @api.multi
    def detail_year_before_before(self):
        """
        Method to open tree sale order
        """
        """
                Method to open tree sale order
                """
        label_year_before_before = (int(datetime.datetime.today().strftime("%Y")) - 2)
        time_start = str(label_year_before_before) + '-01-01 00:00:00'
        time_end = str(label_year_before_before) + '-12-31 23:59:59'

        view_id = self.env['ir.model.data'].get_object_reference('account', 'invoice_tree')
        res = {
            'type': 'ir.actions.act_window',
            'name': _('Facturés N-2'),
            'res_model': 'account.invoice',
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': view_id[1],
            'domain': [('partner_id', '=', self.id),('state', '!=', 'draft'),('write_date', '>=', time.strftime(time_start)),
                       ('write_date', '<=', time.strftime(time_end))]
        }

        return res

    @api.depends("first_name")
    def _compute_meeting_count(self):
        for contact_partner in self:
            if contact_partner.id:
                xcurrent_contact_id = contact_partner.id
                sql_query = 'SELECT * FROM calendar_event_res_partner_rel WHERE res_partner_id = {};'
                self.env.cr.execute(sql_query.format(xcurrent_contact_id))
                xcount = self.env.cr.fetchall()

                contact_partner.contact_meeting_count = len(xcount)

    @api.depends("first_name")
    def _compute_opp_count(self):
        for contact_partner in self:
            xcurrent_contact_id = contact_partner.id
        xreqsearch = self.env['crm.lead'].search([('partner_contact_id', '=', xcurrent_contact_id)])
        if xreqsearch:
            xcount=0
            for search_oppor in xreqsearch:
                xcount+=1

            contact_partner.contact_opportunity_count = xcount

    @api.depends("first_name")
    def _compute_phone_count(self):
        for contact_partner in self:
            xcurrent_contact_id = contact_partner.id
        xreqsearch = self.env['crm.phonecall'].search([('partner_id', '=', xcurrent_contact_id)])
        if xreqsearch:
            xcount = 0
            for search_oppor in xreqsearch:
                xcount += 1

            contact_partner.contact_phone_count = xcount

    @api.multi
    def print_historic(self):

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'gle_report.report_crm_historic',
        }

    @api.multi
    @api.onchange('sector_lvl_one_id')
    def onchange_sector_lvl_one_id(self):
        """ Method onchange sector 1 attributes domain to sector 2 """

        if len(self.sector_lvl_one_id) == 0 and len(self.sector_lvl_two_id) == 0:
            _logger.debug("vidage 1")
            list_ids_sector_two_empty = []
            full_two_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '2')])
            for empty in full_two_ids:
                list_ids_sector_two_empty.append(empty.id)

            list_ids_sector_one_empty = []
            full_one_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '1')])
            for empty in full_one_ids:
                list_ids_sector_one_empty.append(empty.id)

            return {'domain': {'sector_lvl_two_id': [('id', 'in', list_ids_sector_two_empty)]},
                    'domain': {'sector_lvl_one_id': [('id', 'in', list_ids_sector_one_empty)]}}
        else:

            list_parent_ids = []
            if len(self.sector_lvl_one_id) > 0:
                for eachsectorone in self.sector_lvl_one_id:
                    parent_ids = self.env['gle_crm.sector'].search([('parent_id', '=', eachsectorone.id),('sector_level', '=', '2')])
                    for parent in parent_ids:
                        list_parent_ids.append(parent.id)
                return {'domain': {'sector_lvl_two_id': [('id', 'in', list_parent_ids)]}}
            else:
                list_ids_sector_two = []
                full_two_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '2')])
                for eachsectortwo in full_two_ids:
                    list_ids_sector_two.append(eachsectortwo.id)

                return {'domain': {'sector_lvl_two_id': [('id', 'in', list_ids_sector_two)]}}

    @api.multi
    @api.onchange('sector_lvl_two_id')
    def onchange_sector_lvl_two_id(self):
        """ Method onchange sector 2 attributes domain to sector 1 """
        if len(self.sector_lvl_one_id) == 0 and len(self.sector_lvl_two_id) == 0 :

            list_ids_sector_two_empty = []
            full_two_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '2')])
            for empty in full_two_ids:
                list_ids_sector_two_empty.append(empty.id)

            list_ids_sector_one_empty = []
            full_one_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '1')])
            for empty in full_one_ids:
                list_ids_sector_one_empty.append(empty.id)

            return {'domain': {'sector_lvl_two_id': [('id', 'in', list_ids_sector_two_empty)]},'domain': {'sector_lvl_one_id': [('id', 'in', list_ids_sector_one_empty)]}}
        else:
            # self.sector_lvl_one_id = False
            if len(self.sector_lvl_one_id) == 0:
                list_parent_one_ids = []
                if len(self.sector_lvl_two_id) > 0:

                    for eachsectortwo in self.sector_lvl_two_id:
                        parent_ids = self.env['gle_crm.sector'].search([('id', '=', eachsectortwo.parent_id.id),('sector_level', '=', '1')])
                        for parent in parent_ids:
                            list_parent_one_ids.append(parent.id)

                        if len(self.sector_lvl_one_id) > 0:
                            for eachsectorone in self.sector_lvl_one_id:
                                list_parent_one_ids.append(eachsectorone.id)

                        vals = []
                        vals.append((6, 0, list_parent_one_ids))
                        self.sector_lvl_one_id = vals

                return {'domain': {'sector_lvl_one_id': [('id', 'in', list_parent_one_ids)]}}
            else:
                list_parent_one_ids = []
                if len(self.sector_lvl_two_id) > 0:
                    for eachsectortwo in self.sector_lvl_two_id:
                        parent_ids = self.env['gle_crm.sector'].search([('id', '=', eachsectortwo.parent_id.id),('sector_level', '=', '1')])
                        for parent in parent_ids:
                            list_parent_one_ids.append(parent.id)

                        if len(self.sector_lvl_one_id) > 0:
                            for eachsectorone in self.sector_lvl_one_id:
                                list_parent_one_ids.append(eachsectorone.id)

                        vals = []
                        vals.append((6, 0, list_parent_one_ids))
                        self.sector_lvl_one_id = vals


                    return {'domain': {'sector_lvl_one_id': [('id', 'in', list_parent_one_ids)]}}
                else:
                    list_ids_sector_one_empty = []
                    full_one_ids = self.env['gle_crm.sector'].search([('sector_level', '=', '1')])
                    for empty in full_one_ids:
                        list_ids_sector_one_empty.append(empty.id)
                    return {'domain': {'sector_lvl_one_id': [('id', 'in', list_ids_sector_one_empty)]}}

    def type_company_init(self):
        record_set = self.env['gle_crm.type_company'].search([('name', '=', 'Annonceur')])
        if record_set:
            return record_set.id
        else:
            return self.env['gle_crm.type_company']

    @api.constrains('number_siret')
    def _constrain_number_siret(self):
        """ Method constrains number_siret check if number Siret is valid """
        if self.number_siret:
            xvalid=True
            if not self.number_siret.isnumeric():
                # if string not all numeric
                xvalid = False
                raise ValidationError("Le SIRET et/ou le SIREN ne peut contenir que des chiffres, merci de corriger.")
            elif len(self.number_siret) != 14 and len(self.number_siret) != 9:
                # if string hasnt 14 or 9 characters (must be numeric)
                xvalid = False
                raise ValidationError("Erreur de saisi, merci de corriger:\n  - Le SIRET doit contenir 14 chiffres.\n - Le SIREN doit contenir 9 chiffres.")

            if xvalid == True:
                xsumverif=0
                xcount=0
                for xsum in self.number_siret:
                    xtestpair=(xcount % 2)

                    if len(self.number_siret) == 14:
                        if xtestpair==0:
                            xtestsum=(int(xsum) * 2)
                            if xtestsum>9:
                                xsum = (int(xsum)*2)-9
                            else:
                                xsum = (int(xsum)*2)

                        xsumverif += int(xsum)
                        xcount += 1

                    if len(self.number_siret) == 9:
                        if xtestpair != 0:
                            xtestsum = (int(xsum) * 2)
                            if xtestsum > 9:
                                xsum = (int(xsum) * 2) - 9
                            else:
                                xsum = (int(xsum) * 2)

                        xsumverif += int(xsum)
                        xcount += 1

                if (xsumverif % 10) != 0:
                    # the modulo of xsumverif must be a multiple of 10 otherwise Siret is false
                    xvalid = False

                    raise ValidationError(str(xsumverif % 10)+"Le numéro de SIRET et/ou SIREN saisi n'est pas valide, merci de corriger.")

    @api.onchange('name')
    def name_upper(self):
        """ Change name of society to Upper case """

        if self.name:
            self.name = self.name.upper()

    def _default_current_user_commercial_allocation(self):
        """"""

        user_id = self._context.get('uid')
        user_obj = self.env['res.users'].search([('id', '=', user_id)])

        return user_obj

    def _get_commercial_ids_to_list(self, commercial_obj_list):
        """"""

        commercial_list_ids = []
        for commercial in commercial_obj_list:
            commercial_list_ids.append(commercial.id)

        return commercial_list_ids

    @api.multi
    def action_add_commercial_allocation(self):
        """"""

        view_id = 'commercial_allocation_partner_add_wizard_view'
        res_model = 'gle_crm.commercial_allocation_partner_add_wizard'

        length_commercial_allocation_ids = len(self.commercial_allocation_ids)

        if length_commercial_allocation_ids == 3:
            raise ValidationError("Il ne peut y avoir plus de trois commerciaux.")
        elif length_commercial_allocation_ids < 3:
            return self.launch_commercial_allocation_partner_wizard(view_id, res_model)

    @api.multi
    def action_delete_commercial_allocation(self):
        """"""

        view_id = 'commercial_allocation_partner_delete_wizard_view'
        res_model = 'gle_crm.commercial_allocation_partner_delete_wizard'

        length_commercial_allocation_ids = len(self.commercial_allocation_ids)

        if length_commercial_allocation_ids < 2:
            raise ValidationError("Il faut au minimum un commercial.")
        elif length_commercial_allocation_ids > 1:
            return self.launch_commercial_allocation_partner_wizard(view_id, res_model)

    def launch_commercial_allocation_partner_wizard(self, view_id, res_model):
        """"""

        partner_id = self.id
        commercial_allocation_ids = []

        for commercial_allocation in self.commercial_allocation_ids:
            commercial_allocation_ids.append(commercial_allocation.id)

        view_id = self.env['ir.model.data'].get_object_reference('gle_crm', view_id)

        return {
            'name': _("Attribution commercial pour l'opportunité"),
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': res_model,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_partner_id': partner_id, 'default_commercial_ids': commercial_allocation_ids}
        }

    @api.depends('zip', 'country_id')
    def compute_zip(self):

        french_id = self.env['res.country'].search([('name', 'ilike', 'France')], limit=1)
        foreign_sate_id = self.env['res.country.state'].search([('name', 'ilike', 'Etranger')], limit=1)

        for res_partner in self:
            if res_partner.zip and res_partner.country_id.id == french_id.id:
                str_zip = res_partner.zip
                code_zip = str_zip[:2]

                result_department = self.env['department'].search([('number_department', '=', code_zip)], limit=1)
                res_partner.state_id = result_department.state_id

            elif res_partner.country_id.id != french_id.id:
                res_partner.state_id = foreign_sate_id.id


class Function(models.Model):
    """ Class representing the model Function """

    _name = 'gle_crm.function'

    name = fields.Char(string="Libellé de la fonction")


class NoteCreditSafe(models.Model):
    """ Class representing the model Note Credit Safe """

    _name = 'gle_crm.note_credit_safe'

    partner_id = fields.Many2one('res.partner')

    name = fields.Char(string="Code credit safe")
    note_credit = fields.Selection([('a', "A"), ('b', "B"), ('c', "C"), ('d', "D"), ('e', "E")], string="Note credit safe")
    note_date = fields.Date(string="Date de la notation")
    note_end = fields.Date(string="Date de fin de la notation")
    free_comment = fields.Text(string="Zone de commentaire libre")
    state = fields.Selection([('draft', "Brouillon"), ('active', "Actif"), ('done', "Archivé")], default='draft')

    active_to_done = fields.Boolean(default=False)

    @api.multi
    def action_active(self):
        """ Check if Note is active and change state of Note credit safe to active """

        note_active = self.env['gle_crm.note_credit_safe'].search([('state', '=', 'active'), ('partner_id', '=', int(self.partner_id[0]))])

        if not note_active:
            self.active_to_done = True
            self.state = 'active'

        else:
            raise ValidationError("Une note de credit active éxiste déja pour cette société")

    @api.multi
    def action_done(self):
        """ Check if note_end is definied and change state of Note credit safe to archive """

        if self.note_end:
            self.state = 'done'
        else:
            raise ValidationError("Veuillez saisir une date de fin de note")


class Sector(models.Model):
    """ Class representing the model Sector associated to a Society """

    _name = 'gle_crm.sector'

    name = fields.Char(string="Libellé du secteur")
    sector_level = fields.Selection([('1', "Niveau 1"), ('2', "Niveau 2"), ('3', "Niveau 3")], string="Secteur de niveau")
    parent_id = fields.Many2one('gle_crm.sector', string="Secteur parent")

    sector_n_lower_one = fields.Integer()

    @api.onchange('sector_level')
    def _on_change_sector_level(self):
        """"""

        result = int(self.sector_level) - 1
        self.sector_n_lower_one = result

class TypeContact(models.Model):
    """ Class representing the model Sector associated to a Society """

    _name = 'gle_crm.type_contact'
    name = fields.Char(string="Libellé du type de contact")


    @api.onchange('name')
    def on_change_type_contact(self):
        """ On_change method on type_contact. A type_contact stored must be unique. """

        record_set = self.env['gle_crm.type_contact'].search([('name', '=', self.name)])

        if record_set:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Ce type de contact est déja disponible."))
            }}

            return res

class KeyWords(models.Model):
    """ Class representing the model Key Words associated to Contact """

    _name = 'gle_crm.key_words'

    name = fields.Char(string="Mots clés")


class CentersOfInterest(models.Model):
    """ Class representing Center of Interest associated to a society """

    _name = 'gle_crm.centers_of_interest'

    name = fields.Char(string="Libellé du centre d'intêret")













