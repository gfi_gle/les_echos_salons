# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class CommercialAllocationThreeAddWizard(models.TransientModel):
    """"""

    _name = 'gle_crm.commercial_allocation_three_add_wizard'

    def _init_list_commercial_allocation_list(self):
        """
            Method to init commercial_allocation_list to exclude commercial already present in commercial_allocation_id.
            Method search list of commercial ids present in commercial_allocation_id and get ids of commercial, then search in res_users table
            all ids not in commercial list to populate a list.
        """

        commercial_allocation_id = self._context.get('commercial_allocation_id', False)
        list_commercial_already_ids = []

        if commercial_allocation_id:
            list_commercial_already_ids = commercial_allocation_id[0][2]
            commercial_allocation_obj_list = self.env['gle_crm.commercial_allocation_crm'].search([('id', 'in', list_commercial_already_ids)])

            return commercial_allocation_obj_list

    def _init_commercial_list(self):

        commercial_list_ids = []
        commercial_allocation_list_ids = self._init_list_commercial_allocation_list()

        for commercial_allocation in commercial_allocation_list_ids:
            commercial_list_ids.append(commercial_allocation.commercial_id.id)

        commercial_list = self.env['res.users'].search([('id', 'in', commercial_list_ids)])

        return commercial_list

    def _init_commercial_one(self):
        commercial_ids = self._get_commercial_ids_to_list(self._init_commercial_list())

        return commercial_ids[0]

    def _init_commercial_two(self):
        commercial_ids = self._get_commercial_ids_to_list(self._init_commercial_list())

        return commercial_ids[1]

    def _init_commercial_one_percentage(self):
        commercial_id = self.env['gle_crm.commercial_allocation_crm'].search([('commercial_id', '=', self._init_commercial_one()), ('crm_lead_id', '=', self._context.get('crm_lead_id'))])

        return commercial_id.percentage

    def _init_commercial_two_percentage(self):
        commercial_id = self.env['gle_crm.commercial_allocation_crm'].search([('commercial_id', '=', self._init_commercial_two()), ('crm_lead_id', '=', self._context.get('crm_lead_id'))])

        return commercial_id.percentage

    commercial_allocation_list = fields.Many2many(comodel_name='gle_crm.commercial_allocation_crm', relation="commercial_allocation_list_three_add",column1='commercial_allocation_id', column2='wizard_three_id' ,default= lambda self: self._init_list_commercial_allocation_list())
    commercial_list = fields.Many2many('res.users',default= lambda self: self._init_commercial_list())

    commercial_one_id = fields.Many2one('res.users', string="Commercial 1", default=lambda self: self._init_commercial_one())
    percentage_one = fields.Float(string="Pourcentage commercial 1", default=lambda self: self._init_commercial_one_percentage())

    commercial_two_id = fields.Many2one('res.users', string="Commercial 2", default=lambda self: self._init_commercial_two())
    percentage_two = fields.Float(string="Pourcentage commercial 2", default=lambda self: self._init_commercial_two_percentage())

    commercial_three_id = fields.Many2one('res.users', string="Commercial 2")
    percentage_three = fields.Float(string="Pourcentage commercial 2")

    def _get_commercial_ids_to_list(self, commercial_obj_list):
        """"""

        commercial_list_ids = []
        for commercial in commercial_obj_list:
            commercial_list_ids.append(commercial.id)

        return commercial_list_ids

    @api.multi
    def action_save_commercial_allocation(self):
        """"""
        commercial_allocation_ids = self._context.get('commercial_allocation_id')
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']

        crm_lead_id = self._context.get('crm_lead_id')
        crm_lead_obj = self.env['crm.lead'].search([('id', '=', crm_lead_id)])
        crm_lead_obj = self.env["crm.lead"].search([("id","=",crm_lead_id)])

        sum_percentage = self.percentage_one + self.percentage_two + self.percentage_three

        if sum_percentage == 100:
            for commercial_allocation in commercial_allocation_obj.browse(commercial_allocation_ids[0][2]):
                if commercial_allocation.commercial_id.id == self.commercial_one_id.id:
                    result = crm_lead_obj.calculate_percentage_ponderation(amount=crm_lead_obj.planned_revenue, percentage=self.percentage_one)
                    commercial_allocation.write({'percentage': self.percentage_one, 'ponderation': result})
                if commercial_allocation.commercial_id.id == self.commercial_two_id.id:
                    result = crm_lead_obj.calculate_percentage_ponderation(amount=crm_lead_obj.planned_revenue,percentage=self.percentage_two)
                    commercial_allocation.write({'percentage': self.percentage_two, 'ponderation': result})

            result = crm_lead_obj.calculate_percentage_ponderation(amount=crm_lead_obj.planned_revenue,percentage=self.percentage_three)
            vals = {'commercial_id': self.commercial_three_id.id, 'crm_lead_id': crm_lead_id, 'percentage': self.percentage_three, 'ponderation': result}
            commercial_allocation_created = self.env['gle_crm.commercial_allocation_crm'].create(vals)

            self.env.cr.execute('INSERT INTO crm_lead_gle_crm_commercial_allocation_crm_rel (crm_lead_id,gle_crm_commercial_allocation_crm_id) VALUES ({},{});'.format(crm_lead_id, commercial_allocation_created.id))
            crm_lead_obj.write_concat_commercial_allocation_id_char()
        else:
            raise ValidationError("La somme des pourcentages doit être égale à 100%")