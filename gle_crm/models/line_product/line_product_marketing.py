# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.osv import expression
from openerp.tools.translate import _
from operator import itemgetter
from openerp import SUPERUSER_ID
import logging

_logger = logging.getLogger(__name__)

class LineProductMarketing(models.Model):
    """"""

    _name = 'gle_crm.line_product_marketing'

    salon_id = fields.Many2one(comodel_name='gle_salon.salon')
    crm_lead_id = fields.Many2one(comodel_name='crm.lead')
    product_marketing_id = fields.Many2one(comodel_name='gle_products.product_marketing', string="Produit Marketing / Communication")

    wich_qty = fields.Integer(string="Quantité souhaitée")

    def name_get(self):
        """"""
        
        super(LineProductMarketing, self).name_get()

        res = []
        for line_product in self:

            name = ''
            name += line_product.crm_lead_id.name or ''
            name += '-'
            name += line_product.product_marketing_id.name or ''
            res.append((line_product.id, name))

        return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('crm_lead_id.name', '=ilike', name + '%'), ('product_marketing_id.name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&'] + domain
        line_product_marketing = self.search(domain + args, limit=limit)
        return line_product_marketing.name_get()