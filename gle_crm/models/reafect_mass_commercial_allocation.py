# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class ReafectMassCommercialAllocation(models.TransientModel):
    """ Transient model representing the reafectation mass for the view """

    _name = 'gle_crm.reafect_mass_comemrcial_allocation_transient'

    """ Ancient commercial reafect by the new """
    commercial_one = fields.Many2one('res.users', string="Ancien commercial")
    """ New commercial to reafect in place of ancient """
    commercial_two = fields.Many2one('res.users', string="Nouveau commercial")

    @api.multi
    def reafectation_mass_commercial_allocation(self):
        """
            Method action button to reafect commercial allocation.
            commercial_one is old
            commercial_two is new
            Reaffect commercial_id for crm.lead, res.partner and sale.order
        """

        """ Reafectation for crm.lead Piste """
        crm_lead_obj = self.env['crm.lead']

        crm_lead_obj.search([('type', '=', 'lead'), ('user_id', '=', self.commercial_one.id)]).write({'user_id': self.commercial_two.id})

        """ Reafectation for crm.lead Opportunity """

        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']
        record_set_commercial_one = commercial_allocation_obj.search([('commercial_id', '=', self.commercial_one.id)])

        crm_lead_update_ids = []

        for attribution_commercial_one in record_set_commercial_one:

            record_set_crm_lead = crm_lead_obj.search([('id', '=', attribution_commercial_one.crm_lead_id.id)])
            commercial_one_percentage = attribution_commercial_one.percentage

            for crm_lead in record_set_crm_lead:
                if crm_lead.probability < 100:
                    crm_lead_update_ids.append((crm_lead.id, commercial_one_percentage, crm_lead.planned_revenue))

        for update_ids in crm_lead_update_ids:

            result_commercial_allocation = commercial_allocation_obj.search([('crm_lead_id', '=', int(update_ids[0])), ('commercial_id', '=', self.commercial_one.id)])
            result_commercial_allocation_two = commercial_allocation_obj.search([('crm_lead_id', '=', int(update_ids[0])), ('commercial_id', '=', self.commercial_two.id)])

            if result_commercial_allocation_two:

                total_percentage = update_ids[1] + result_commercial_allocation_two.percentage
                result_commercial_allocation_two.unlink()

                result_ponderation = crm_lead_obj.calculate_percentage_ponderation(amount=update_ids[2], percentage=total_percentage)

                result_commercial_allocation.write({'commercial_id': self.commercial_two.id, 'percentage': total_percentage, 'crm_lead_id':update_ids[0], 'ponderation': result_ponderation})
            else:
                result_commercial_allocation.write({'commercial_id': self.commercial_two.id})


        """ Reafectation for res.partner """
        # search in relation table all records where res_user_id = commercial_one.id
        sql_query_select_commercial_one_partner = 'SELECT * FROM gle_crm_res_users_relation  WHERE res_user_id = {};'
        # search in relation table all records where res_user_id = commercial_two.id
        sql_query_select_commercial_two_and_partner = 'SELECT * FROM gle_crm_res_users_relation  WHERE res_user_id = {} AND partner_id = {};'
        # delete record where res_user_id = commercial_two.id
        sql_query_delete_relation = 'DELETE FROM gle_crm_res_users_relation  WHERE res_user_id = {} AND partner_id = {};'

        self.env.cr.execute(sql_query_select_commercial_one_partner.format(self.commercial_one.id))
        record_set_commercial_one_allocation = self.env.cr.fetchall()

        for result in record_set_commercial_one_allocation:
            partner_id = result[0]
            self.env.cr.execute(sql_query_select_commercial_two_and_partner.format(self.commercial_two.id, partner_id))
            record_set_commercial_two_allocation = self.env.cr.fetchall()

            # if record found delete these
            if record_set_commercial_two_allocation:
                self.env.cr.execute(sql_query_delete_relation.format(self.commercial_two.id, partner_id))

        # update relation table with new commercial id
        sql_query_reafecation_commercial_partner = 'UPDATE gle_crm_res_users_relation SET res_user_id = {} WHERE res_user_id = {};'
        self.env.cr.execute(sql_query_reafecation_commercial_partner.format(self.commercial_two.id, self.commercial_one.id))

        """ Reafectation for sale.order """
        sale_order_obj = self.env['sale.order']
        sale_order_record_set = sale_order_obj.search([('contact_business_id', '=', self.commercial_one.id), ('state', 'not in', ['cancel', 'invoice_except','done', 'waiting_date','progress', 'manual','shipping_except'])])

        if sale_order_record_set:
            sale_order_record_set.write({'contact_business_id': self.commercial_two.id})
