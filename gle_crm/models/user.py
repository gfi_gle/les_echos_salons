# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.exceptions import except_orm
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)


class ResUsergle(models.Model):
    """ Class representing the model Type Company """
    _inherit = 'res.users'

    @api.multi
    def unlink(self):
        """ Override unlink method, to disable possibility to unlink users """
        for line in self:
           raise except_orm('Warning', 'Fonctionnalité désactivé, vous ne pouvez pas supprimer d\'utilisateurs')
        return super(ResUsergle, self).unlink()



