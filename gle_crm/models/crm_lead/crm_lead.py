# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from operator import itemgetter
from openerp import SUPERUSER_ID
import logging
_logger = logging.getLogger(__name__)


CRM_LEAD_FIELDS_TO_MERGE = ['name',
    'partner_id',
    'campaign_id',
    'company_id',
    'country_id',
    'section_id',
    'state_id',
    'stage_id',
    'medium_id',
    'source_id',
    'user_id',
    'title',
    'city',
    'contact_name',
    'description',
    'email',
    'fax',
    'mobile',
    'partner_name',
    'phone',
    'probability',
    'planned_revenue',
    'street',
    'street2',
                            'street3',
    'zip',
    'create_date',
    'date_action_last',
    'date_action_next',
    'email_from',
    'email_cc',
    'partner_name']

class CrmLead(models.Model):
    """ Class inherit from crm.lead to add fields and methods """
    _inherit = 'crm.lead'

    # Default order used for reporting views
    _order = 'propose_area_square_meter desc'
    # _group_by_full = {'stage_id': _read_group_stage_ids}

    # Fields for piste
    partner_contact_id = fields.Many2one('res.partner', string="Contact")

    """ MODIFICATION 06/12 suite incident _move """
    function = fields.Many2one(related='partner_contact_id.gle_function' ,string="Fonction")

    # Fields for Opportunity
    propose_area_square_meter = fields.Float(string="Nombre de M² proposés")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")
    village_id = fields.Many2one('gle_salon.village', string="Village")

    ponderation = fields.Float(string="Pondération")

    wich_stand_1 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°1")
    wich_stand_2 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°2")
    wich_stand_3 = fields.Many2one('gle_salon.stand', string="Stand souhaité N°3")

    commercial_allocation_id = fields.Many2many('gle_crm.commercial_allocation_crm', string="Attribution commercial", default= lambda self: self._default_current_user_commercial_allocation())
    commercial_allocation_id_char = fields.Char(string="Commercial allocation", default= lambda self: self._default_current_user_commercial_allocation_name())


    note_credit_safe_ids = fields.Many2many('gle_crm.note_credit_safe', compute="_compute_note")

    # Attribute is True when probability is equal or greater than 80%
    partner_required = fields.Boolean(default=False)

    wich_product_list = fields.One2many(comodel_name='gle_crm.line_articles', inverse_name='crm_lead_id', string="Liste d\'articles souhaités")

    square_meter_last_year = fields.Float(string="M²", compute="_square_meter_last_year", store=True)

    stand_last_year = fields.Char(string="Stand", compute="_stand_last_year", store=True)
    type_stand_last_year = fields.Char(string="Stand", compute="_type_stand_last_year", store=True)
    village_last_year = fields.Char(string="Village", compute="_village_last_year", store=True)

    salon_concat = fields.Char(string="salon_concat")

    line_product_marketing_id = fields.One2many(comodel_name='gle_crm.line_product_marketing', inverse_name='crm_lead_id', string="Liste de produits Marketing / Communication souhaités")
    line_product_workshop_id = fields.One2many(comodel_name='gle_crm.line_product_workshop', inverse_name='crm_lead_id', string="Liste de produits Conférences / Ateliers souhaités")

    def write_concat_commercial_allocation_id_char(self):
        xsearch = self.env["gle_crm.commercial_allocation_crm"].search([("crm_lead_id","=",self.id)])
        xnamelist = ""
        for com in xsearch:
            xnamelist += com.commercial_id.name + ";"
        self.write({"commercial_allocation_id_char":xnamelist})


    @api.model
    def create(self, vals):
        """"""

        new_id = super(CrmLead, self).create(vals)

        self._associated_crm_lead_id_to_commercial_allocation(new_id)
        self._clean_table_commercial_allocation_crm()

        self.search_status_partner(new_obj=new_id)

        return new_id

    @api.multi
    def unlink(self):
        """"""

        query_delete = 'DELETE FROM gle_crm_status_partner_crm_lead_rel WHERE crm_lead_id = {};'

        status_partner_obj = self.env['gle_crm.status_partner_models']

        for crm_lead in self:
            result_recordset = status_partner_obj.search([('partner_id', '=', crm_lead.partner_id.id), ('salon_id', '=', crm_lead.salon_id.id)])

            length_crm_lead_ids = len(result_recordset.crm_lead_id)
            length_sale_order_ids = len(result_recordset.sale_order_id)
            length_account_invoice = len(result_recordset.account_invoice_id)

            if length_crm_lead_ids > 1:
                self.env.cr.execute(query_delete.format(crm_lead.id))
            elif length_crm_lead_ids == 1 and length_sale_order_ids == 0 and length_account_invoice == 0:
                status_partner_obj.search([('id', '=', result_recordset.id)]).unlink()

            super(CrmLead, crm_lead).unlink()


    @api.multi
    def action_add_commercial_allocation_crm_wizard(self):
        """ Method triggered by button on crm_lead_view, and display a wizard form view to delete comemrcial attribution """

        commercial_allocation_crm_obj = self.env['gle_crm.commercial_allocation_crm']
        commercial_allocation_ids_length = 0

        for commercial_allocation in self.commercial_allocation_id:
            commercial_allocation_ids_length += 1

        if commercial_allocation_ids_length == 1:
            view_id = 'commercial_allocation_crm_wizard_view'
            res_model = 'gle_crm.commercial_allocation_crm_wizard'

            return self.launch_commercial_allocation_crm_wizard(view_id, res_model)

        elif commercial_allocation_ids_length ==2:
            view_id = 'commercial_allocation_three_add_wizard_view'
            res_model = 'gle_crm.commercial_allocation_three_add_wizard'

            return self.launch_commercial_allocation_crm_wizard(view_id, res_model)

        elif commercial_allocation_ids_length >= 3:
            raise ValidationError("Le nombre maximum d'allocation commercial est de trois.")

    @api.multi
    def action_delete_commercial_allocation_crm_wizard(self):
        """ Method triggered by button on crm_lead_view, and display a wizard form view to delete comemrcial attribution """

        if len(self.commercial_allocation_id)>1:
            view_id = 'commercial_allocation_crm_delete_wizard_view'
            res_model = 'gle_crm.commercial_allocation_crm_delete_wizard'
            return self.launch_commercial_allocation_crm_wizard(view_id, res_model)
        else:
            raise ValidationError("Ce menu n'est pas accessible, une allocation commercial est obligatoire.")

    def _default_current_user_commercial_allocation(self):
        """"""

        user_id = self._context.get('uid')
        vals = {'commercial_id': user_id, 'percentage': 100}
        commercial_allocation_created = self.env['gle_crm.commercial_allocation_crm'].create(vals)

        return commercial_allocation_created

    def _default_current_user_commercial_allocation_name(self):
        user_id = self._context.get('uid')
        user_name = self.env["res.users"].search([("id","=",user_id)])

        return user_name.name

    def _clean_table_commercial_allocation_crm(self):
        """"""

        recorset = self.env['gle_crm.commercial_allocation_crm'].search([])

        ids_to_delete = []
        for commercial_allocation in recorset:
            if commercial_allocation.crm_lead_id.id is False:
                ids_to_delete.append(commercial_allocation.id)

        self.env['gle_crm.commercial_allocation_crm'].search([('id', 'in', ids_to_delete)]).unlink()

    def _associated_crm_lead_id_to_commercial_allocation(self, new_id):
        """"""

        commercial_allocation_crm_obj = self.env['gle_crm.commercial_allocation_crm']
        result_ponderation = self.calculate_percentage_ponderation(amount=new_id.planned_revenue, percentage=100)

        for commercial_allocation in new_id.commercial_allocation_id:
            result = commercial_allocation_crm_obj.search([('id', '=', commercial_allocation.id)])

            if result.crm_lead_id.id is False:
                vals = {
                    'crm_lead_id': new_id,
                    'ponderation': result_ponderation
                }

                result.update(vals)

    def launch_commercial_allocation_crm_wizard(self, view_id, res_model):
        """"""

        crm_lead_id = self.id
        commercial_allocation_ids = []

        for commercial_allocation in self.commercial_allocation_id:
            commercial_allocation_ids.append(commercial_allocation.id)

        view_id = self.env['ir.model.data'].get_object_reference('gle_crm', view_id)

        return {
            'name': _("Attribution commercial pour l'opportunité"),
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': res_model,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'crm_lead_id': crm_lead_id, 'commercial_ids': commercial_allocation_ids}
        }

    @api.constrains('probability')
    def _constrain_probability(self):
        """ Method constrains probability check if number Siret is defined and Note credit safe active if probability is greater than 80% """
        self._on_change_probability()
        if self.probability >= 80:
            _logger.debug("80%")

            # if not self.check_number_siret():
            #     raise ValidationError("La probabilité est de 80% ou plus, veuillez renseigner le numéro de Siret")

            # if not self.check_active_note():
            #     raise ValidationError(
            #         "La probabilité est de 80% ou plus, veuillez renseigner une note de credit safe active")


    @api.constrains('planned_revenue')
    def _constrains_planned_revenue(self):

        if self.planned_revenue == 0:
            raise ValidationError("Le revenu estimé est obligatoire et ne peut pas etre '0'. Merci de corriger.")

    @api.constrains('probability')
    def _constrains_probability(self):

        if self.probability == 0:
            self.probability = 0.000001
        if self.probability == 0:
            raise ValidationError("Le % de chance de réalisation estimé est obligatoire et ne peut pas etre '0'. Merci de corriger.")

    @api.onchange('probability')
    def _change_probability(self):
        """ Check if the probability is greater than 80% and check if Note Safe Credit is not empty """

        if self.probability >= 80:
            self.partner_required = True
        else:
            self.partner_required = True

    @api.onchange('stage_id')
    def onchange_stage_id_gle(self):
        if self.stage_id == 1:
            self.probability = 1

    @api.onchange('partner_id')
    def _on_changepartner_id(self):
        """ On change partner_id inform field email_from and phone  """

        self.email_from = self.partner_id.email
        self.phone = self.partner_id.phone

    @api.onchange('planned_revenue')
    def _on_change_planned_revenue(self):
        self._compute_ponderation()

    @api.onchange('probability')
    def _on_change_probability(self):
        self._compute_ponderation()

    @api.multi
    def _compute_ponderation(self):
        for crm_lead in self:
            xminus = (crm_lead.planned_revenue*crm_lead.probability)/100
            crm_lead.ponderation = xminus

    @api.depends('partner_id')
    def _compute_note(self):
        """ Method to compute note_credit_safe_ids, search Note Safe Credit associated to partner_id """

        related_recordset = self.env['gle_crm.note_credit_safe'].search([('partner_id', '=', int(self.partner_id))])
        self.note_credit_safe_ids = related_recordset

    @api.depends('partner_id')
    def _square_meter_last_year(self):
        """ Method to compute last year square meter reserved in status ferme for a company and add it in the reporting of the opportunities """
        for crm_lead in self:
            if crm_lead.salon_id.id:
                current_millesime = crm_lead.salon_id.millesime.name
                current_place_id = crm_lead.salon_id.place.id
                year_before_millesime = int(current_millesime) - 1
                year_before_millesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before_millesime)])
                salon_before_obj = self.env['gle_salon.salon'].search([('millesime', '=', year_before_millesime_id.id),('name', '=', crm_lead.salon_id.name.id)])
                stand_ferme_before = self.env['gle_salon.stand'].search([('salon_id', "=", salon_before_obj.id),('state','=','ferme'),('partner_id','=',crm_lead.partner_id.id)])

                sum_areas_before = 0
                for ferme_stand in stand_ferme_before:
                    sum_areas_before += ferme_stand.area

                crm_lead.square_meter_last_year = sum_areas_before

                # calculate on sale order done last year
                # sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)),('salon_id', '=', int(salon_before_obj.id)),('state', '=', 'done')])
                # sum_areas_before = 0
                # for sale_order in sale_order_before:
                #     sum_areas_before += sale_order.stand_id.area
                #     crm_lead.square_meter_last_year = sum_areas_before


    @api.depends('partner_id')
    def _stand_last_year(self):
        """ Method to compute stand sold for a company and add it in the reporting of the opportunities """
        for crm_lead in self:
            if crm_lead.salon_id.id:
                current_millesime = crm_lead.salon_id.millesime.name
                current_place_id = crm_lead.salon_id.place.id
                year_before_millesime = int(current_millesime) - 1
                year_before_millesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before_millesime)])
                salon_before_obj = self.env['gle_salon.salon'].search([('millesime', '=', year_before_millesime_id.id), ('name', '=', crm_lead.salon_id.name.id)])

                stand_ferme_before = self.env['gle_salon.stand'].search([('salon_id', "=", salon_before_obj.id), ('state', '=', 'ferme'),('partner_id', '=', crm_lead.partner_id.id)])

                # sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(salon_before_obj.id)), ('state', '=', 'done')])
                # sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(salon_before_obj.id))])

                compile_stand_before = ""
                for ferme_stand in stand_ferme_before:
                    if ferme_stand.name:
                        compile_stand_before = compile_stand_before + ';' + ferme_stand.name
                crm_lead.stand_last_year = compile_stand_before

                # for sale_order in sale_order_before:
                #     if sale_order.stand_id.name:
                #         compile_stand_before = compile_stand_before + '/' + sale_order.stand_id.name
                # crm_lead.stand_last_year = compile_stand_before

    @api.depends('partner_id')
    def _type_stand_last_year(self):
        """ Method to compute stand type sold for a company and add it in the reporting of the opportunities """
        for crm_lead in self:
           sale_order_ids = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)),('salon_id', '=', int(crm_lead.salon_id))])
           for sale_order in sale_order_ids:
              crm_lead.type_stand_last_year = sale_order.stand_id.name

    @api.depends('partner_id')
    def _village_last_year(self):
      """ Method to compute village sold for a company and add it in the reporting of the opportunities """
      for crm_lead in self:
          if crm_lead.salon_id.id:
              # sale_order_ids = self.env['sale.order'].search(
              #     [('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(crm_lead.salon_id))])

              # for sale_order in sale_order_ids:
              #     crm_lead.village_last_year = sale_order.stand_id.village_id.name
              current_millesime = crm_lead.salon_id.millesime.name
              current_place_id = crm_lead.salon_id.place.id
              year_before_millesime = int(current_millesime) - 1
              year_before_millesime_id = self.env['gle_salon.millesime'].search([('name', '=', year_before_millesime)])
              salon_before_obj = self.env['gle_salon.salon'].search(
                  [('millesime', '=', year_before_millesime_id.id), ('name', '=', crm_lead.salon_id.name.id)])

              stand_ferme_before = self.env['gle_salon.stand'].search(
                  [('salon_id', "=", salon_before_obj.id), ('state', '=', 'ferme'),
                   ('partner_id', '=', crm_lead.partner_id.id)])

              # sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(salon_before_obj.id)), ('state', '=', 'done')])
              # sale_order_before = self.env['sale.order'].search([('partner_id', '=', int(crm_lead.partner_id)), ('salon_id', '=', int(salon_before_obj.id))])

              compile_village_before = ""
              for ferme_stand in stand_ferme_before:
                  if ferme_stand.name:
                      compile_village_before = compile_village_before + ';' + ferme_stand.village_id.name
              crm_lead.village_last_year = compile_village_before

    @api.onchange('salon_id')
    def _salon_concat(self):
        self.salon_concat = '{}{}{}'.format(self.salon_id.name.name, ' - ', self.salon_id.millesime.name)

        if len(self.wich_product_list) > 0 or len(self.line_product_marketing_id) > 0 or len(self.line_product_workshop_id) > 0:

            self.wich_product_list = [(5,)]

            self.line_product_workshop_id = [(5,)]
            self.line_product_marketing_id = [(5,)]

    def check_active_note(self):
        """ Check if a Note credit safe is active for the associated society, return True if Note credit safe is active """

        result = False

        for note in self.partner_id.note_credit_safe_id:
            if note.state == 'active':
                result = True

        return result

    def check_number_siret(self):
        """ Check if a Number Siret is defined for the associated society, return True if Note credit safe is active """

        result = False

        if self.partner_id.number_siret:
            result = True

        return result

    @api.onchange('partner_id')
    def on_change_partner(self):
        """ Auto-complete Address information when partner_id is change """

        self.partner_name = self.partner_id.name
        self.street = self.partner_id.street
        self.street2 = self.partner_id.street2
        self.street3 = self.partner_id.street3
        self.street3 = self.partner_id.street3
        self.zip = self.partner_id.zip
        self.city = self.partner_id.city
        self.state_id = self.partner_id.state_id

    def convert_opportunity(self, cr, uid, ids, partner_id, user_ids=False, section_id=False,salon_id=False , planned_revenue=False, context=None):
        """ Override method convert_opportunity from crm.lead to add salon_id in parameters and add in vals dictionary to write salon_id in crm.lead """

        customer = False
        if partner_id:
            partner = self.pool.get('res.partner')
            customer = partner.browse(cr, uid, partner_id, context=context)
        for lead in self.browse(cr, uid, ids, context=context):
            # TDE: was if lead.state in ('done', 'cancel'):
            if lead.probability == 100 or (lead.probability == 0 and lead.stage_id.fold):
                continue
            vals = self._convert_opportunity_data(cr, uid, lead, customer, section_id, context=context)
            if salon_id:
                vals.update({'salon_id': salon_id.id})

            if planned_revenue:
                vals.update({'planned_revenue':planned_revenue})
            self.write(cr, uid, [lead.id], vals, context=context)

        if user_ids or section_id:
            self.allocate_salesman(cr, uid, ids, user_ids, section_id, context=context)

        return True

    def merge_opportunity(self, cr, uid, ids, user_id=False, section_id=False, salon_id=False, context=None):
        """"""

        if context is None:
            context = {}

        if len(ids) <= 1:
            raise osv.except_osv(_('Warning!'), _('Please select more than one element (lead or opportunity) from the list view.'))

        opportunities = self.browse(cr, uid, ids, context=context)
        sequenced_opps = []
        # Sorting the leads/opps according to the confidence level of its stage, which relates to the probability of winning it
        # The confidence level increases with the stage sequence, except when the stage probability is 0.0 (Lost cases)
        # An Opportunity always has higher confidence level than a lead, unless its stage probability is 0.0
        for opportunity in opportunities:
            sequence = -1
            if opportunity.stage_id and not opportunity.stage_id.fold:
                sequence = opportunity.stage_id.sequence
            sequenced_opps.append(((int(sequence != -1 and opportunity.type == 'opportunity'), sequence, -opportunity.id), opportunity))

        sequenced_opps.sort(reverse=True)
        opportunities = map(itemgetter(1), sequenced_opps)
        ids = [opportunity.id for opportunity in opportunities]
        highest = opportunities[0]
        opportunities_rest = opportunities[1:]

        tail_opportunities = opportunities_rest

        fields = list(CRM_LEAD_FIELDS_TO_MERGE)
        merged_data = self._merge_data(cr, uid, ids, highest, fields, context=context)

        if user_id:
            merged_data['user_id'] = user_id
        if section_id:
            merged_data['section_id'] = section_id
        if salon_id:
            merged_data['salon_id'] = salon_id.id
        # Merge notifications about loss of information
        opportunities = [highest]
        opportunities.extend(opportunities_rest)

        self.merge_dependences(cr, uid, highest.id, tail_opportunities, context=context)

        # Check if the stage is in the stages of the sales team. If not, assign the stage with the lowest sequence
        if merged_data.get('section_id'):
            section_stage_ids = self.pool.get('crm.case.stage').search(cr, uid, [
                ('section_ids', 'in', merged_data['section_id']), ('type', 'in', [merged_data.get('type'), 'both'])
            ], order='sequence', context=context)
            if merged_data.get('stage_id') not in section_stage_ids:
                merged_data['stage_id'] = section_stage_ids and section_stage_ids[0] or False
        # Write merged data into first opportunity
        self.write(cr, uid, [highest.id], merged_data, context=context)
        # Delete tail opportunities
        # We use the SUPERUSER to avoid access rights issues because as the user had the rights to see the records it should be safe to do so
        self.unlink(cr, SUPERUSER_ID, [x.id for x in tail_opportunities], context=context)

        return highest.id

    @api.onchange('planned_revenue')
    def on_change_planned_revenue(self):
        """"""

        amount = self.planned_revenue
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']

        for commercial_allocation in self.commercial_allocation_id:
            result = self.calculate_percentage_ponderation(amount=amount, percentage=commercial_allocation.percentage)
            vals = {
                'ponderation': result
            }

            record_result = commercial_allocation_obj.search([('id', '=', commercial_allocation.id)])

            record_result.write(vals)

    def calculate_percentage_ponderation(self, amount, percentage):
        """"""

        percentage_calculate = percentage/100
        result = amount * percentage_calculate

        return result

    def search_status_partner(self, new_obj):
        """"""

        query_insert = 'INSERT INTO gle_crm_status_partner_crm_lead_rel (status_partner_id, crm_lead_id) VALUES ({}, {});'

        if new_obj.partner_id.id:
            status_partner_obj = self.env['gle_crm.status_partner_models']
            result_recordset = status_partner_obj.search([('partner_id', '=', new_obj.partner_id.id), ('salon_id', '=', new_obj.salon_id.id)])

            if not result_recordset:
                vals = {
                    'salon_id': new_obj.salon_id.id,
                    'partner_id': new_obj.partner_id.id,
                    'partner_type': 'prospect'
                }

                new_status_obj = status_partner_obj.create(vals)
                self.env.cr.execute(query_insert.format(new_status_obj.id, new_obj.id))

            elif result_recordset:

                self.env.cr.execute(query_insert.format(result_recordset.id, new_obj.id))


class CrmLeadReportingPositionnement(models.Model):
    """ Class inherit from crm.lead to add fields and methods """
    _inherit = 'crm.lead'

    # Default order used for reporting positionning views
    _order = 'wich_stand_1 desc'


class CrmLeadReportingPrevisional(models.Model):
    _name = 'gle_crm.crmleadreportingprevisional'


    def _init_crm_lead_full(self):
        xquery_lead = []
        xquery_lead.append(('propose_area_square_meter', '>=', 1))
        ids = self.env['crm.lead'].sudo().search(xquery_lead)
        return [(6,0,list(ids._ids))]


    def _init_reservation_full(self):
        _logger.debug(self.env['gle_reservation_salon.reservation_salon'].search([]))
        return self.env['gle_reservation_salon.reservation_salon'].search([])


    def _init_crm_lead_full_comercial_allocation(self):
        for line in self:
            xcrmleadid = self.id
            self.env['crm.lead'].search([])


    crm_lead_ids = fields.Many2many('crm.lead', string="CrmLeads", index=True, default=lambda self: self._init_crm_lead_full())


    crm_lead_onemany_ids = fields.One2many('crm.lead', 'salon_id', string="CrmLeads", index=True, default=lambda self: self._init_crm_lead_full())



    gle_reservation_onemany_ids = fields.One2many('gle_reservation_salon.reservation_salon', 'salon_id', string="Reservation", index=True, default=lambda self: self._init_reservation_full())

    gle_reservation_manymany_ids = fields.Many2many(comodel_name='gle_reservation_salon.reservation_salon', relation='gle_reservation_salon_reservation_salon_reporting',
                                     column1='crmleadreportingprevisional_id', column2='reservation_id', string="Reservation", default=lambda self: self._init_manymany_reservation_ids())



    salon_id_filter = fields.Many2one('gle_salon.salon', string="Salon")
    salon_id_filter_position = fields.Many2one('gle_salon.salon', string="Salon")
    salon_id_filter_span = fields.Many2one('gle_salon.salon', string="Salon")





    @api.multi
    def _init_manymany_reservation_ids(self):
        """"""
        return self.env['gle_reservation_salon.reservation_salon'].search([])



    @api.onchange('salon_id_filter')
    def _onchange_all_toto_filter_crm(self):
        self.testaction()
        for record in self:
            record._query_toto_construct_crm()

    @api.onchange('salon_id_filter_position')
    def _onchange_all_filter_reservation(self):
        self.testaction()
        for record in self:
            record._query_construct_reservation()

    def _query_toto_construct_crm(self):
        xquery_lead = []
        for record in self:
            if record.salon_id_filter.id != False:
                xquery_lead.append(('salon_id', '=', record.salon_id_filter.id))
                xquery_lead.append(('propose_area_square_meter', '>=', 1))
            if record.salon_id_filter.id == False:
                xquery_lead.append(('propose_area_square_meter', '>=', 1))

            # record.crm_lead_ids = record.env['crm.lead'].search(xquery_lead)
            record.crm_lead_onemany_ids = record.env['crm.lead'].sudo().search(xquery_lead)


            # return True
            # self.testaction

    def _query_construct_reservation(self):
        xquery_reservation = []
        for record in self:
            if record.salon_id_filter_position.id != False:
                xquery_reservation.append(('salon_id', '=', record.salon_id_filter_position.id))

            record.gle_reservation_onemany_ids = record.env['gle_reservation_salon.reservation_salon'].sudo().search(xquery_reservation)

            record.gle_reservation_manymany_ids = record.env['gle_reservation_salon.reservation_salon'].sudo().search(xquery_reservation)


    @api.multi
    def testaction(self):
        return True


