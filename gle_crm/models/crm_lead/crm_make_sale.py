# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from openerp.addons.gle_numerotation.models.ComputeNumerotation import ComputeNumerotation

import logging

_logger = logging.getLogger(__name__)


class CrmMakeSale(models.Model):
    """ Class ihnerit from crm.make.sale """

    _inherit = 'crm.make.sale'

    def get_crm_lead_obj(self):
        return self.env['crm.lead'].search([('id', '=', self._context.get('active_id'))])

    @api.multi
    def _init_salon_id(self):
        """ Init value salon_id for the crm.make.sale """

        crm_lead_object = self.env['crm.lead'].search([('id', '=', self._context.get('active_id'))])
        return crm_lead_object.salon_id

    @api.multi
    def _init_wich_list(self):
        """"""

        crm_lead_object = self.env['crm.lead'].search([('id', '=', self._context.get('active_id'))])
        line_article_obj = self.env['gle_crm.line_articles'].search([('crm_lead_id', '=', crm_lead_object.id)])

        return line_article_obj

    @api.multi
    def _init_commercial_allocation_ids(self):
        crm_lead = self.get_crm_lead_obj()

        commercial_allocation_ids = self.env['gle_crm.commercial_allocation_crm'].search([('crm_lead_id', '=', crm_lead.id)])

        return commercial_allocation_ids

    stand_id = fields.Many2one('gle_salon.stand', string="Stand")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon", default=_init_salon_id)
    wich_product_list = fields.Many2many(comodel_name='gle_crm.line_articles', string="Liste d\'articles souhaités", default=_init_wich_list)

    commercial_allocation_ids = fields.Many2many(comodel_name='gle_crm.commercial_allocation_crm',
                                                 relation='gle_crm_commercial_allocation_make_sale_relation',
                                                 column1='crm_make_sale_id',
                                                 column2='commercial_allocation_id',
                                                 default=_init_commercial_allocation_ids)

    def create_ca_commision(self, cr, uid, ids, new_sale_order, context=None):
        """"""

        ca_commision_obj = self.pool.get('gle_adv.ca_commision')
        # sql_query_insert = 'INSERT INTO gle_adv_ca_commision_sale_order_relation (sale_order_id, ca_commision_id) VALUES ({},{});'

        created_list_ids = []

        for obj in self.browse(cr, uid, ids, context=context):
            for commercial_allocation in obj.commercial_allocation_ids:

                vals = {
                    'commercial_id': commercial_allocation.commercial_id.id,
                    'sale_order_id': new_sale_order.id,
                    'percentage': commercial_allocation.percentage,
                    'amount': 0
                }

                ca_created_id = ca_commision_obj.create(cr, uid, vals, context=context)

                created_list_ids.append(ca_created_id)
                #
        return created_list_ids
                # if ca_created:
                #     cr.execute(sql_query_insert.format(new_sale_order.id, ca_created))

    def create_ca_echo_solution(self, cr, uid, ids, new_sale_order, context=None):

        commercial_echo_solution = self.pool.get('res.users').search(cr, uid, [('is_commercial_echo_solution', '=', True)])

        ca_commision_obj = self.pool.get('gle_adv.ca_commision')

        if commercial_echo_solution:
            vals = {
                'commercial_id': commercial_echo_solution[0],
                'sale_order_id': new_sale_order.id,
                'percentage': 100,
                'amount': 0
            }

            new_obj = ca_commision_obj.create(cr, uid, vals)

            return new_obj

    def makeOrder(self, cr, uid, ids, context=None):
        """ Override method makeOrder from crm.make.sale to add fields 'stand_id', 'salon_id' in vals {} and save this attributes in sale_order """

        context = dict(context or {})
        context.pop('default_state', False)

        case_obj = self.pool.get('crm.lead')
        sale_obj = self.pool.get('sale.order')
        partner_obj = self.pool.get('res.partner')
        data = context and context.get('active_ids', []) or []

        for make in self.browse(cr, uid, ids, context=context):

            partner = make.partner_id
            partner_addr = partner_obj.address_get(cr, uid, [partner.id],
                                                   ['default', 'invoice', 'delivery', 'contact'])
            pricelist = partner.property_product_pricelist.id
            fpos = partner.property_account_position and partner.property_account_position.id or False
            payment_term = partner.property_payment_term and partner.property_payment_term.id or False
            new_ids = []

            for case in case_obj.browse(cr, uid, data, context=context):

                partner_contact_id = False
                if case.partner_contact_id:
                    partner_contact_id = case.partner_contact_id.id
                if not partner and case.partner_id:
                    partner = case.partner_id
                    fpos = partner.property_account_position and partner.property_account_position.id or False
                    payment_term = partner.property_payment_term and partner.property_payment_term.id or False
                    partner_addr = partner_obj.address_get(cr, uid, [partner.id],
                                                           ['default', 'invoice', 'delivery', 'contact'])
                    pricelist = partner.property_product_pricelist.id

                if False in partner_addr.values():
                    raise osv.except_osv(_('Insufficient Data!'), _('No address(es) defined for this customer.'))

                vals = {
                    'stand_id': make.stand_id.id,
                    'salon_id': make.salon_id.id,
                    'origin': _('Opportunity: %s') % str(case.id),
                    'section_id': case.section_id and case.section_id.id or False,
                    'categ_ids': [(6, 0, [categ_id.id for categ_id in case.categ_ids])],
                    'partner_id': partner.id,
                    'pricelist_id': pricelist,
                    'partner_invoice_id': partner_addr['invoice'],
                    'partner_shipping_id': partner_addr['delivery'],
                    'date_order': fields.datetime.now(),
                    'fiscal_position': fpos,
                    'payment_term': payment_term,
                    'note': sale_obj.get_salenote(cr, uid, [case.id], partner.id, context=context),
                    'contact_client_id': partner_contact_id,
                }
                if partner.id:
                    vals['user_id'] = partner.user_id and partner.user_id.id or uid

                new_id = sale_obj.create(cr, uid, vals, context=context)

                sale_order = sale_obj.browse(cr, uid, new_id, context=context)

                sale_order.write({
                    'name': sale_order.compute_name()
                })

                if new_id:

                    if not isinstance(make.wich_product_list, (tuple, list)):
                        [make.wich_product_list]
                    for line_article in make.wich_product_list:
                        vals_order_line = {
                            'order_id': new_id,
                            'product_id': line_article.product_product_id.id,
                            'product_uom_qty': line_article.wich_qty
                        }

                        self.pool.get('sale.order.line').create(cr, uid, vals_order_line)

                        #create ca_commision

                    list_ids = self.create_ca_commision(cr, uid, ids, new_sale_order=sale_order, context=context)
                    new_echo_solution = self.create_ca_echo_solution(cr, uid, ids, new_sale_order=sale_order, context=context)

                    list_ids.append(new_echo_solution)

                    sale_order.write({'ca_commision_ids': [(6, 0, list_ids)]})

                case_obj.write(cr, uid, [case.id], {'ref': 'sale.order,%s' % new_id})
                new_ids.append(new_id)
                message = _("Opportunity has been <b>converted</b> to the quotation <em>%s</em>.") % (sale_order.name)
                case.message_post(body=message)
            if make.close:
                case_obj.case_mark_won(cr, uid, data, context=context)
            if not new_ids:
                return {'type': 'ir.actions.act_window_close'}
            if len(new_ids) <= 1:
                value = {
                    'domain': str([('id', 'in', new_ids)]),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'sale.order',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'name': _('Quotation'),
                    'res_id': new_ids and new_ids[0],
                }
            else:
                value = {
                    'domain': str([('id', 'in', new_ids)]),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'sale.order',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'name': _('Quotation'),
                    'res_id': new_ids
                }
            return value

