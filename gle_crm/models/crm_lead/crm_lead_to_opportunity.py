# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging


_logger = logging.getLogger(__name__)

class CrmLeadToOpportunity(models.TransientModel):

    _inherit = 'crm.lead2opportunity.partner'


    salon_id = fields.Many2one('gle_salon.salon', string="Salon")
    planned_revenue = fields.Float(string="Potentiel CA")

    def action_apply(self, cr, uid, ids, context=None):
        """
        Convert lead to opportunity or merge lead and opportunity and open
        the freshly created opportunity view.
        """
        if context is None:
            context = {}

        lead_obj = self.pool['crm.lead']
        partner_obj = self.pool['res.partner']

        for wizard_crm_to_opportunity in self.browse(cr, uid, ids):
            salon_id = wizard_crm_to_opportunity.salon_id

        w = self.browse(cr, uid, ids, context=context)[0]
        opp_ids = [o.id for o in w.opportunity_ids]
        vals = {
            'section_id': w.section_id.id,
        }
        if w.partner_id:
            vals['partner_id'] = w.partner_id.id
        if w.name == 'merge':
            lead_id = lead_obj.merge_opportunity(cr, uid, opp_ids, salon_id=salon_id, context=context)
            lead_ids = [lead_id]
            lead = lead_obj.read(cr, uid, lead_id, ['type', 'user_id'], context=context)
            if lead['type'] == "lead":
                context = dict(context, active_ids=lead_ids)
                vals.update({'lead_ids': lead_ids, 'user_ids': [w.user_id.id]})
                self._convert_opportunity(cr, uid, ids, vals, context=context)
            elif not context.get('no_force_assignation') or not lead['user_id']:
                vals.update({'user_id': w.user_id.id})
                lead_obj.write(cr, uid, lead_id, vals, context=context)
        else:
            lead_ids = context.get('active_ids', [])
            vals.update({'lead_ids': lead_ids, 'user_ids': [w.user_id.id]})
            self._convert_opportunity(cr, uid, ids, vals, context=context)
            for lead in lead_obj.browse(cr, uid, lead_ids, context=context):
                if lead.partner_id and lead.partner_id.user_id != lead.user_id:
                    partner_obj.write(cr, uid, [lead.partner_id.id], {'user_id': lead.user_id.id}, context=context)

        return self.pool.get('crm.lead').redirect_opportunity_view(cr, uid, lead_ids[0], context=context)

    def _convert_opportunity(self, cr, uid, ids, vals, context=None):
        """ Override method convert_opportunity from crm.lead2opportunity.partner to add salon_id when method  convert_opportunity() is used """

        if context is None:
            context = {}
        lead = self.pool.get('crm.lead')
        res = False
        lead_ids = vals.get('lead_ids', [])
        team_id = vals.get('section_id', False)
        partner_id = vals.get('partner_id')

        for wizard_crm_to_opportunity in self.browse(cr, uid, ids):
            salon_id = wizard_crm_to_opportunity.salon_id
            planned_revenue = wizard_crm_to_opportunity.planned_revenue

        data = self.browse(cr, uid, ids, context=context)[0]
        leads = lead.browse(cr, uid, lead_ids, context=context)
        for lead_id in leads:
            partner_id = self._create_partner(cr, uid, lead_id.id, data.action, partner_id or lead_id.partner_id.id, context=context)
            res = lead.convert_opportunity(cr, uid, [lead_id.id], partner_id, [], False, salon_id=salon_id, planned_revenue=planned_revenue, context=context)
        user_ids = vals.get('user_ids', False)
        if context.get('no_force_assignation'):
            leads_to_allocate = [lead_id.id for lead_id in leads if not lead_id.user_id]
        else:
            leads_to_allocate = lead_ids
        if user_ids:
            lead.allocate_salesman(cr, uid, leads_to_allocate, user_ids, team_id=team_id, context=context)
        return res

