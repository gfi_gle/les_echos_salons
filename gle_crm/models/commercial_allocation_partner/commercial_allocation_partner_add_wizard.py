# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class CommercialAllocationPartnerAddWizard(models.TransientModel):
    """"""

    _name = 'gle_crm.commercial_allocation_partner_add_wizard'

    commercial_ids = fields.Many2many(comodel_name='res.users', string="Commercials", relation='commercial_allocation_partner_add_wizard',column1='id', column2='partner_id')
    commercial_list_ids = fields.Many2many(comodel_name='res.users', string="Commercials", relation='commercial_allocation_partner__list_to_add_wizard',column1='id', column2='partner_id')
    partner_id = fields.Many2one('res.partner', string="Société")

    @api.multi
    def action_save_commercial_allocation(self):
        """"""

        slq_query = 'INSERT INTO gle_crm_res_users_relation (partner_id,res_user_id) VALUES ({},{});'
        partner_id = self.partner_id.id

        length_commercial_list = len(self.commercial_list_ids)
        length_commercial_ids = len(self.commercial_ids)
        total = length_commercial_list + length_commercial_ids

        if total > 3:
            raise ValidationError("Il ne peut y avoir plus de trois commerciaux pour une société ou pour un contact.")

        for commercial in self.commercial_list_ids:
            self.env.cr.execute(slq_query.format(partner_id, commercial.id))
