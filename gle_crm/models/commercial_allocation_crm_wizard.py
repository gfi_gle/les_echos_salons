# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class CommercialAllocationCrmWizard(models.TransientModel):
    """ Transient model representing the wizard commercial_allocation_crm """

    _name = 'gle_crm.commercial_allocation_crm_wizard'

    def _init_crm_lead_id(self):
        return self._context.get('crm_lead_id')

    def _init_list_commercial_partner(self):
        """
            Method to init commercial_allocation_list to exclude commercial already present in commercial_allocation_id.
            Method search list of commercial ids present in commercial_allocation_id and get ids of commercial, then search in res_users table
            all ids not in commercial list to populate a list.
        """

        commercial_allocation_id = self._context.get('commercial_allocation_id', False)
        list_commercial_already_ids = []

        if commercial_allocation_id:
            list_commercial_already_ids = commercial_allocation_id[0][2]

        res_users_obj = self.env['res.users']
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']

        allocation_ids = commercial_allocation_obj.search([('id', 'in', list_commercial_already_ids)])

        lst_allocation = []
        for allocation in allocation_ids:
            lst_allocation.append(allocation.commercial_id.id)

        result_recordset = res_users_obj.search([('id', 'not in', lst_allocation)])

        list_commercial_ids = []

        for id_commercial in result_recordset:
            list_commercial_ids.append(id_commercial.id)

        return list_commercial_ids

    commercial_id = fields.Many2one('res.users', string="Commercial")
    crm_lead_id = fields.Many2one('crm.lead', string="Société", default=_init_crm_lead_id)
    percentage = fields.Float(string="Pourcentage")

    commercial_allocation_list = fields.Many2many('res.users', default=_init_list_commercial_partner)

    @api.onchange('percentage')
    def on_change_percentage(self):

        if self.percentage > 100:
            self.percentage = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage inférieur à 100."
            }}

            return res

        elif self.percentage < 0:
            self.percentage = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage supérieur à 0."
            }}

            return res

    @api.multi
    def action_write_commercial_allocation_crm(self):
        """ Action triggered by button on wizard view. This method save commercial_allocation_crm and in the relation table crm_lead_gle_crm_commercial_allocation_crm_rel"""

        commercial_allocation_ids = self._context.get('commercial_allocation_id')
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']
        crm_lead_id = None

        if self.crm_lead_id.id:
            crm_lead_id = self.crm_lead_id.id

        crm_lead_obj = self.env['crm.lead'].search([['id', '=', crm_lead_id]])

        lst_commercial_allocation_obj = []
        if commercial_allocation_ids:
            lst_commercial_allocation_obj = commercial_allocation_obj.search([('id', 'in', commercial_allocation_ids[0][2])])

        result_bool = self.calcul_new_total_percentage(lst_commercial_allocation_obj)

        if result_bool:
            crm_lead_obj.write({'total_percentage_allocation': 100})

            vals = {'commercial_id': self.commercial_id.id, 'crm_lead_id': crm_lead_id, 'percentage': self.percentage}
            commercial_allocation_created = self.env['gle_crm.commercial_allocation_crm'].create(vals)

            self.env.cr.execute('INSERT INTO crm_lead_gle_crm_commercial_allocation_crm_rel (crm_lead_id,gle_crm_commercial_allocation_crm_id) VALUES ({},{});'.format(self.crm_lead_id.id, commercial_allocation_created.id))
        else:
            raise ValidationError("La somme des pourcentages n'est pas équilibré")

    def calcul_new_total_percentage(self, lst_attribution_commercial):
        """"""
        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']
        length_lst_attribution = len(lst_attribution_commercial)

        _logger.debug(length_lst_attribution)

        if length_lst_attribution == 1:
            percentage = 0
            for commercial_allocation in lst_attribution_commercial:
                percentage += commercial_allocation.percentage

            new_percentage = percentage - self.percentage
            _logger.debug(new_percentage)
            result_absolut = abs(new_percentage + self.percentage)
            _logger.debug(new_percentage)
            if result_absolut == 100:
                commercial_allocation_obj.browse(commercial_allocation.id).write({'percentage': new_percentage})
                return True
        elif length_lst_attribution == 2:
            return True
        return False




