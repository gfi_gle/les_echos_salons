# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class EquilizePercentageWizard(models.TransientModel):
    """"""

    _name = 'gle_crm.equilize_percentage_wizard'

    def _init_list_commercial_allocation_list(self):

        commercial_allocation_obj = self.env['gle_crm.commercial_allocation_crm']
        crm_lead_id = self._context.get('crm_lead_id')

        record_set = commercial_allocation_obj.search([('crm_lead_id', '=', crm_lead_id)])
        return record_set

    def _init_commercial_one(self):

        commercial_ids = self._get_commercial_ids_to_list(self._init_list_commercial_allocation_list())
        return commercial_ids[0]

    def _init_commercial_two(self):

        commercial_ids = self._get_commercial_ids_to_list(self._init_list_commercial_allocation_list())
        return commercial_ids[1]

    commercial_allocation_list = fields.Many2many(comodel_name='gle_crm.commercial_allocation_crm', relation="commercial_allocation_list_equilize_add",column1='commercial_allocation_id', column2='wizard_three_id' ,default= lambda self: self._init_list_commercial_allocation_list())

    commercial_one_id = fields.Many2one('res.users', string="Commercial 1", default=lambda self: self._init_commercial_one())
    percentage_one = fields.Float(string="Pourcentage commercial 1")

    commercial_two_id = fields.Many2one('res.users', string="Commercial 2", default=lambda self: self._init_commercial_two())
    percentage_two = fields.Float(string="Pourcentage commercial 2")

    @api.multi
    def action_save_commercial_allocation(self):
        """"""

        crm_lead_id = self._context.get('crm_lead_id')
        sum_percentage = self.percentage_one + self.percentage_two

        if sum_percentage == 100:
            commercial_allocation_ids = self.env['gle_crm.commercial_allocation_crm'].search([('crm_lead_id', '=', crm_lead_id)])

            for commercial_allocation in commercial_allocation_ids:
                if commercial_allocation.commercial_id.id == self.commercial_one_id.id:
                    commercial_allocation.write({'percentage': self.percentage_one})
                if commercial_allocation.commercial_id.id == self.commercial_two_id.id:
                    commercial_allocation.write({'percentage': self.percentage_two})
        else:
            raise ValidationError("La somme des pourcentages doit être égale à 100%")

    def _get_commercial_ids_to_list(self, commercial_obj_list):
        """"""

        comemrcial_list_ids = []
        for commercial_allocation in commercial_obj_list:
            comemrcial_list_ids.append(commercial_allocation.commercial_id.id)

        return comemrcial_list_ids

    @api.onchange('percentage_one')
    def on_change_percentage_one(self):

        if self.percentage_one > 100:
            self.percentage_one = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage inférieur à 100."
            }}

            return res

        elif self.percentage_one < 0:
            self.percentage_one = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage supérieur à 0."
            }}

            return res

    @api.onchange('percentage_two')
    def on_change_percentage_two(self):

        if self.percentage_two > 100:
            self.percentage_two = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage inférieur à 100."
            }}

            return res

        elif self.percentage_two < 0:
            self.percentage_two = None

            res = {'warning': {
                'title': "Attention",
                'message': "Veuillez saisir un pourcentage supérieur à 0."
            }}

            return res