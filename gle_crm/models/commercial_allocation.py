# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class CommercialAllocationCrm(models.Model):
    """ Class representing the Commercial(res.users) allocated to a CRM """

    _name = 'gle_crm.commercial_allocation_crm'

    name = fields.Char(string="Name")

    commercial_id = fields.Many2one('res.users' ,string="Commercial")
    crm_lead_id = fields.Many2one('crm.lead', string="Opportunité")

    percentage = fields.Float(string="Pourcentage")

    def check_percentage_commercial_allocation(self, list_commercial_allocation=None, wizard_percentage=None):
        """"""

        length_commercial_allocation = len(list_commercial_allocation)

        if length_commercial_allocation > 0:
            sum_percentage = 0
            for commercial_allocation in list_commercial_allocation:
                sum_percentage += commercial_allocation.percentage

            if wizard_percentage:
                sum_percentage += wizard_percentage
            _logger.debug(sum_percentage)
            if sum_percentage > 100:
                raise ValidationError(
                    "La somme des pourcentage de l'allocation commercial doit être inférieur ou égale à 100%")

            if sum_percentage < 100:
                raise ValidationError(
                    "La somme des pourcentage de l'allocation commercial doit être égale à 100%")