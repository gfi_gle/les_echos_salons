# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp.addons.crm import crm
from openerp.osv import fields, osv
from openerp import tools

class commercial_allocation_reports(osv.Model):
    """"""

    _name = 'commercial_allocation_reports'
    _description = "Sales Orders Statistics"

    _auto = False
    _rec_name = 'partner_name'

    _columns = {
        'ponderation': fields.float(string="Chiffre d'affaire"),
        'partner_name': fields.char(string="Nom du commercial"),
        'crm_lead_name': fields.char(string="Opportunité"),
        'percentage': fields.float(string="Pourcentage"),
        'produit': fields.char(string="Produit"),
        'projet': fields.char(string="Projet"),
        'planned_revenue': fields.float(string="Revenue espéré"),
        'create_date': fields.datetime('Date de création')
    }

    def init(self, cr):
        """"""

        tools.drop_view_if_exists(cr, 'commercial_allocation_reports')
        cr.execute("""
            CREATE OR REPLACE VIEW commercial_allocation_reports AS (
                SELECT min(c.id) as id,
                    c.ponderation AS ponderation,
                    p.display_name AS partner_name,
                    l.name AS crm_lead_name,
                    c.percentage AS percentage,
                    sp.name AS produit,
                    m.name AS projet,
                    l.planned_revenue AS planned_revenue,
                    c.create_date as create_date
                FROM
                    gle_crm_commercial_allocation_crm c,
                    res_users u,
                    crm_lead l,
                    res_partner p,
                    gle_salon_salon s,
                    gle_salon_produit sp,
                    gle_salon_millesime m
                WHERE
                    c.crm_lead_id = l.id
                AND c.commercial_id = u.id
                AND u.partner_id = p.id
                AND l.salon_id = s.id
                AND s.name = sp.id
                AND s.millesime = m.id
                AND l.type = 'opportunity'
                GROUP BY
                    p.display_name,
                    c.ponderation,
                    l.name,
                    c.percentage,
                    sp.name,
                    m.name,
                    l.planned_revenue,
                    c.create_date
            )""")