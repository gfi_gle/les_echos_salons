# -*- coding: utf-8 -*-
{
    'name': "gle_crm",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'crm','sale_crm', 'gle_adv','gle_reservation_salon','gle_salon','gle_report','partner_firstname', 'product'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/config_type_contact_view.xml',
        'views/config_rp_view.xml',
        'views/society_contact_view.xml',
        'views/crm_lead/crm_lead_view.xml',
        'views/note_credit_safe_view.xml',
        'views/commercial_allocation_crm/commercial_allocation_crm.xml',
        'views/crm_lead/crm_piste_view.xml',
        'views/reaffect_mass_commercial_allocation.xml',
        'views/convert_opportunity_to_quote.xml',
        'views/templates.xml',

        'views/commercial_allocation_crm/commercial_allocation_crm_wizard.xml',
        'views/commercial_allocation_crm/commercial_allocation_three_add_wizard.xml',
        'views/commercial_allocation_crm/commercial_allocation_crm_delete_wizard.xml',
        'views/commercial_allocation_crm/equilize_percentage_wizard.xml',
        'views/commercial_allocation_partner/commercial_allocation_partner_add_wizard.xml',
        'views/commercial_allocation_partner/commercial_allocation_partner_delete_wizard.xml',

        'views/line_product_views/line_product_marketing_view.xml',
        'views/line_product_views/line_product_workshop_view.xml',
        'views/line_article_views/line_article_view.xml',

        'views/status_partner_view/ca_last_year_view.xml',

        'views/config_sector_view.xml',
        'views/config_centers_of_interest_view.xml',
        'views/crm_lead/crm_lead_to_opportunity_view.xml',
        'views/key_words_view.xml',
        'views/function_view.xml',
        'views/type_company_view.xml',
        'views/position_follow_view.xml', # reporting
        'views/position_follow_view_evol.xml', # reporting
        'views/previsional_opportunities.xml', # reporting
        'views/position_list_view.xml', # reporting
        'static/src/js/widget_radio.js',

        'views/menus.xml',

        'report/commercial_allocation_report_view.xml',
        #
        'data/res_users_data.xml',
        # 'data/company_data.xml',
        # 'data/crm_lead_data.xml',
        'data/state_data.xml',
    ],
    'qweb': ['static/src/xml/widget_radio.xml'],
    # only loaded in demonstration mode
    'demo': [
        'demo/company_demo_data.xml',
        'demo/res_users_demo_data.xml',
        # 'demo/crm_lead_demo_data.xml',
    ],
    # 'update_xml': ['sql/crm.sql'],
}