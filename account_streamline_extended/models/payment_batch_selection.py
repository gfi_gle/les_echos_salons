# -*- coding: utf-8 -*-
from collections import defaultdict
from openerp import models, fields, api, _, exceptions
from openerp.addons.account_streamline.payment_batch_selection import PaymentBatchSelection

import logging

_logger = logging.getLogger(__name__)

PAYMENT_MODE_SELECTION = [
    ('supplier_invoices', "Factures et avoirs fournisseurs"),
    ('client_refunds', "Avoirs client"),
    ('both', "Les deux"),
]

DEBIT_PAYMENT_MODE_SELECTION = [
    ('client_invoices', _('Client invoices')),
    ('supplier_refunds', _('Supplier refunds')),
    ('both', _('Both')),
]


class PaymentBatchSelection(models.TransientModel):

    _inherit = 'account.payment_batch.selection'

    def get_mode_selection(self):
        context = self.env.context
        option = 'payment'

        if 'default_option' in context.keys():
            option = context['default_option']

        if option == 'payment':
            selection = self.translate_tuple_list(PAYMENT_MODE_SELECTION)
        elif option == 'debit_payment':
            selection = self.translate_tuple_list(DEBIT_PAYMENT_MODE_SELECTION)

        return selection

    mode = fields.Selection(
        selection=get_mode_selection,
        string='Mode',
        help='How to filter accounting entries.',
        required=True,
    )

    @api.multi
    def generate_batch(self):

        self.ensure_one()

        entry_search_domain = [  # noqa
            '|',
            ('reconcile_id', '=', False),
            ('reconcile_partial_id', '!=', False),
        ]

        if self.mode == 'supplier_invoices':
            entry_search_domain += [
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', 'in', ['purchase', 'purchase_refund']),
            ]

        elif self.mode == 'client_refunds':
            entry_search_domain += [
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale_refund'),
            ]

        elif self.mode == 'both' and self.option == 'payment':
            entry_search_domain += [  # noqa
                '|',
                '&',
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', 'in', ['purchase', 'purchase_refund']),
                '&',
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale_refund'),
            ]

        elif self.mode == 'client_invoices':
            entry_search_domain += [
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale'),
            ]

        elif self.mode == 'supplier_refunds':
            entry_search_domain += [
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', '=', 'purchase_refund'),
            ]

        elif self.mode == 'both' and self.option == 'debit_payment':
            entry_search_domain += [  # noqa
                '|',
                '&',
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale'),
                '&',
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', '=', 'purchase_refund'),
            ]

        entry_search_domain += [
            ('state', '=', 'valid'),
            ('move_id.state', '=', 'posted'),
            ('date_maturity', '<=', self.date_maturity),
            ('is_not_payment_batchable', '=', False),
            ('payment_batch_id', '=', False),
            ('is_in_voucher', '=', False),
        ]

        # Hook for other modules, in order to filter accounting document lines
        # on payment methods, which are associated to debit payments.
        if self.option == 'debit_payment':
            entry_search_domain = (
                self.with_context(self.env.context).sale_payment_method_filter(
                    entry_search_domain
                )
            )
            entry_search_domain = (
                self.with_context(self.env.context).partner_filter(
                    entry_search_domain
                )
            )
        # import wdb
        # wdb.set_trace()
        lines = self.env['account.move.line'].search(entry_search_domain)

        if not lines:
            raise exceptions.Warning('No lines found.')

        # Split lines by partner and by preferred bank account.
        batch_lines = defaultdict(dict)

        # Prepare a cache to avoid too many partner bank lookups.
        partner_bank_cache = {}

        for line in lines:
            partner = line.partner_id
            bank_id = self.get_preferred_bank_account_id(
                partner, line, partner_bank_cache,
            )
            subdict = batch_lines[(partner.id, bank_id)]
            subdict['bank_id'] = bank_id
            subdict['partner_id'] = partner.id
            subdict.setdefault('line_ids', []).append(line.id)

        # Create a payment batch
        pb = self.env['account.payment_batch'].create({
            'journal_id': self.journal_id.id,
            'line_ids': [
                (0, 0, {
                    'partner_id': line_data['partner_id'],
                    'line_ids': [
                        (0, 0, {'move_line_id': ml})
                        for ml in line_data['line_ids']
                        ],
                    'bank_id': line_data['bank_id'],
                })
                for line_data in batch_lines.itervalues()
                ],
            'date_maturity': self.date_maturity,
            'option': self.option,
        })

        lines.write({'payment_batch_id': pb.id})

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment_batch',
            'name': _("Payment Batch"),
            'view_mode': 'form,tree',
            'view_type': 'form',
            'res_id': pb.id,
        }
