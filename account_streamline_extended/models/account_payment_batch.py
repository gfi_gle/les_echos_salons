# -*- coding: utf-8 -*-

from openerp import models, fields, api, _, exceptions

import logging

_logger = logging.getLogger(__name__)


class AccountPaymentBatch(models.Model):

    _inherit = 'account.payment_batch'

    state = fields.Selection(
        selection=[
            ('draft', "Brouillon"),
            ('dir_compta', "Soumis Dir Compta"),
            ('paid', "Payé"),
        ],
        string='State',
        track_visibility='onchange',
    )

    @api.multi
    def action_submit_dir_compta(self):
        self.state = 'dir_compta'

    @api.multi
    def action_refus(self):
        self.state = 'draft'

    def _get_name_move_bank(self):

        account_voucher_obj = self.env['account.voucher']
        get_journal_bnk_by_code = self.env['account.journal']._get_journal_bnk_by_code

        encais_cb_les_id = get_journal_bnk_by_code('5BKEC')
        encais_cheque_les_id = get_journal_bnk_by_code('5BKEH')
        encais_virm_etra_id = get_journal_bnk_by_code('5BKEVE')
        encais_virm_fran_id = get_journal_bnk_by_code('5BKEVF')
        decais_cb_les_id = get_journal_bnk_by_code('5BKDC')
        decais_cheque_les_id = get_journal_bnk_by_code('5BKDH')
        decais_virm_id = get_journal_bnk_by_code('5BKDV')

        pay_encais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cb_les_id)])
        pay_encais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cheque_les_id)])
        pay_encais_virm_etra_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_etra_id)])
        pay_encais_virm_fran_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_fran_id)])

        pay_decais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cb_les_id)])
        pay_decais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cheque_les_id)])
        pay_decais_virm_ids = account_voucher_obj.search([('journal_id', '=', decais_virm_id)])

        bank_invoice_count = sum(
            [len(pay_encais_cb_les_ids), len(pay_encais_cheque_les_ids), len(pay_encais_virm_etra_ids),
             len(pay_encais_virm_fran_ids), len(pay_decais_cb_les_ids), len(pay_decais_cheque_les_ids),
             len(pay_decais_virm_ids)])

        bank_invoice_count += 1

        chrono_projet_str = "000"

        if bank_invoice_count == 0:
            bank_invoice_str = "0001"
        else:
            bank_invoice_str = "0000{0}".format(bank_invoice_count)

        return "B{0}{1}".format(chrono_projet_str[-3:], bank_invoice_str[-4:])

    @api.multi
    def wkf_paid(self):

        self.generate_vouchers()

        # Validate the vouchers - Use the admin account as this might have
        # various effects on invoices / etc. Not a security issue as payment
        # batch access rights have already been checked.
        self.voucher_ids.sudo().signal_workflow('proforma_voucher')

        self.write({
            'validate_date': fields.Datetime.now(),
            'validate_user_id': self.env.uid,
            'state': 'paid',
        })
        for voucher in self.voucher_ids:
            if voucher.move_id.state == 'draft':
                voucher.move_id.button_validate()
        return True

    @api.multi
    def generate_vouchers(self):
        voucher_obj = self.env['account.voucher']
        avl_obj = self.env['account.voucher.line']
        voucher_ids = []

        for line in self.line_ids:
            partner = line.partner_id
            journal = self.journal_id
            move_lines = line.line_ids.mapped('move_line_id')

            if not move_lines:
                continue

            type_payment = False
            payment_method = False
            date_origin_document = False

            if journal.code == '5BKDV':
                type_payment = 'VIR'
            elif journal.code == '5BKDH':
                type_payment = 'CH'

            if type_payment:
                payment_method = self.env['crm.payment.mode'].search(
                    [('technical_name', '=', type_payment)], limit=1,
                )

            if self.option == 'receipt':
                date_origin_document = self.validate_date and self.validate_date or fields.Datetime.now()

            vals = {
                'payment_batch_line_id': line.id,
                'partner_id': partner.id,
                'journal_id': journal.id,
                'payment_option': 'without_writeoff',
                'partner_bank_id': line.bank_id.id,
                'amount': line.subtotal,
                'type': 'payment' if self.option == 'payment' else 'receipt',
                # Payment batches are for payment, if the option is set on
                # 'payment'. Otherwise, if the option is set on 'debit
                # payment', these are for receipts.

                # Define "pre_line" to ensure the voucher is aware of the
                # lines we are going to add; otherwise it doesn't show all
                # of them.
                'number': self._get_name_move_bank(),
                'reference': self._get_name_move_bank(),
                'name': self._get_name_move_bank(),
                'pre_line': True,
                'date_origin_document': date_origin_document

            }

            if payment_method:
                vals.update({'sales_payment_method_id': payment_method.id})

            account = (
                journal.default_credit_account_id or
                journal.default_debit_account_id
            )
            if not account:
                raise exceptions.Warning(
                    _('Error!'),
                    msg_define_dc_on_journal % journal.name
                )

            vals['account_id'] = account.id

            # Hook to let other addons customize the voucher creation.
            self.voucher_creation_hook(line, vals)

            voucher = voucher_obj.create(vals)

            voucher_ids.append(voucher.id)

            # For payments, the generated vouchers must be in debit. and for
            # debit payments, in credit.
            # For payments, the generated vouchers are supplier payments in
            # debit. Credit lines for suppliers must be in debit in the
            # vouchers.
            # For debit payments, the generated vouchers are customer payments
            # in credit. Debit lines for customers must be in credit in the
            # vouchers.

            # now that we have a voucher id we'll add our lines to it
            for ml in move_lines:
                avl = avl_obj.create({
                    'name': ml.name,
                    'voucher_id': voucher.id,

                    # Voucher lines must use the same account as the move
                    # lines, in order to be able to reconcile them with the
                    # move lines created during the validation of the voucher.
                    'account_id': ml.account_id.id,
                    'type': 'dr' if ml.credit else 'cr',
                    'move_line_id': ml.id,
                })

                # Those values are not in the create for some unknown reasons.
                # Maybe we should check if they really need to be written after
                #  the creation process
                avl.write({
                    'reconcile': True,
                    'amount': avl.amount_unreconciled,
                })
                # Mark the lines as batched
                ml.payment_batch_id = self.id

        # Store the vouchers in the batch
        self.voucher_ids = [(6, 0, voucher_ids)]
