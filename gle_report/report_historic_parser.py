from openerp.report import report_sxw
from openerp import api, models
import logging

_logger = logging.getLogger(__name__)

class report_historic_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(report_historic_parser, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'parser_current_year':self.parser_current_year,
            'parser_last_year':self.parser_last_year,
            'parser_last_year_before': self.parser_last_year_before
        })

    # N
    def parser_current_year(self,partner_id):
        parse_current_year_ids = self.pool['gle_crm.ca_current_year'].search(self.cr,self.uid,[('partner_id','=',partner_id.id)])
        parse_current_year_list = []
        for parse in self.pool['gle_crm.ca_current_year'].browse(self.cr,self.uid,parse_current_year_ids):
            parse_current_year_list.append(parse)
        return parse_current_year_list

    # N-1
    def parser_last_year(self,partner_id):
        parse_last_year_ids = self.pool['gle_crm.ca_before_year'].search(self.cr,self.uid,[('partner_id','=',partner_id.id)])
        parse_last_year_list = []
        for parse in self.pool['gle_crm.ca_before_year'].browse(self.cr,self.uid,parse_last_year_ids):
            parse_last_year_list.append(parse)
        return parse_last_year_list

    # N-2
    def parser_last_year_before(self,partner_id):
        parse_last_year_ids = self.pool['gle_crm.ca_before_before_year'].search(self.cr,self.uid,[('partner_id','=',partner_id.id)])
        parse_last_year_list = []
        for parse in self.pool['gle_crm.ca_before_before_year'].browse(self.cr,self.uid,parse_last_year_ids):
            parse_last_year_list.append(parse)
        return parse_last_year_list


class report_link_parser_historic(models.AbstractModel):
    _name = 'report.gle_report.report_crm_historic'
    _inherit = 'report.abstract_report'
    _template = 'gle_report.report_crm_historic'
    _wrapped_report_class = report_historic_parser