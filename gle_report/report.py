# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 Auguria (<http://www.auguria.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models
import logging
from openerp import api, _



_logger = logging.getLogger(__name__)

class saleOrdergle(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def print_quotation(self):
        # self.filtered(lambda s: s.state == 'draft').write({'state': 'sent'})
        return self.env['report'].get_action(self, 'gle_report.report_devis')


class purchaseOrdergle(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def print_quotation(self):
        # self.write({'state': "sent"})
        return self.env['report'].get_action(self, 'gle_report.report_glepurchasequotationprint')

