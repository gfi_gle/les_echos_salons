# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'gle_report',
    'version': '1.0',
    'website': '',
    'category': 'report',
    'sequence': 14,
    'summary': '',
    'author': 'GFI',
    'depends': ['base','product', 'report','sale','stock','purchase'],
    'description': """

    """,
    'data': [
        'report/layouts.xml',
        'report/report.xml',
        'report/report_devis.xml',
        'report/report_historic.xml',
        'report/report_suivi_charge.xml',

        'report/template_tva_footer.xml',
        'report/template_tva_footer_refund.xml',
        'report/template_tva_footer_purchase.xml',
        'report/template_line_by_line.xml',
        'report/template_line_by_line_avoir.xml',
        'report/template_global.xml',
        'report/template_global_avoir.xml',

        'report/report_devis_total.xml',
        'report/report_devis_avoir.xml',
        'report/report_facture.xml',
        'report/report_duplicata.xml',

        'report/report_glepurchaseorder.xml',
        'report/report_glepurchasequotation.xml',
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
