from openerp.report import report_sxw
from openerp import api, models
import logging

_logger = logging.getLogger(__name__)

class report_devis_parser(report_sxw.rml_parse):
    """"""

    def __init__(self, cr, uid, name, context):
        super(report_devis_parser, self).__init__(cr, uid, name, context=context)

        self.localcontext.update({
            'calculate_discount_regrouping': self.calculate_discount_regrouping,
            'calculate_subtotal_regrouping': self.calculate_subtotal_regrouping,
            'calculate_subtotal_discount': self.calculate_subtotal_discount
        })

    def calculate_discount_regrouping(self, sale_order):

        result = 0
        for sale_order_line in sale_order.order_line:
            if sale_order_line.function_grouping:
                result += self.calculate_subtotal_discount_grouping(sale_order_line)

        return result

    def calculate_subtotal_discount_grouping(self, line):

        result = (line.price_unit * line.product_uom_qty) - (line.price_subtotal)

        return result

    def calculate_subtotal_discount(self, sale_order):

        result = 0
        for sale_order_line in sale_order.order_line:
            if sale_order_line.discount > 0:
                result += (sale_order_line.price_unit * sale_order_line.product_uom_qty) - (sale_order_line.price_subtotal)
        return result

    def calculate_subtotal_regrouping(self, sale_order):

        result = 0
        for sale_order_line in sale_order.order_line:
            if sale_order_line.function_grouping:
                result += sale_order_line.price_subtotal

        if result == 0:
            return None

        return result


class report_link_parser(models.AbstractModel):
    _name = 'report.gle_report.report_devis'
    _inherit = 'report.abstract_report'
    _template = 'gle_report.report_devis'
    _wrapped_report_class = report_devis_parser