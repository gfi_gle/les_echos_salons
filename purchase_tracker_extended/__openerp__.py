# -*- coding: utf-8 -*-

{
    'name': 'Purchase Tracker Extended',
    'version': '8.0.1.0.0',
    'category': 'Purchases',
    'sequence': 75,
    'summary': '',
    'author': 'GFI Informatique, (GFI Informatique)',
    'website': 'http://gfi.world',
    'images': [],
    'description': """
Purchase Tracker Extended
=========================
Extends purchase Tracker
        """,
    'depends': [
        'purchase_tracker',
        'gle_accounting',
        'purchase_streamline_extended',
        'hr_streamline',
    ],
    'data': [
        'views/account_invoice_view.xml',
        'views/purchase_order.xml',
        'views/purchase_tracker_tracker.xml',
        'workflow/purchase_tracker.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
