# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'

    def get_po_fields(self):

        res = {
            'po_employee': self.employee_id.name,
            'po_department': self.department_id.name,
            'po_description': self.description,
            'po_tracker_type': self.tracker_type,
            'po_status': self.status
        }

        return res
