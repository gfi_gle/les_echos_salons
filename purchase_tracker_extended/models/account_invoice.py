# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)

MAGIC_COLUMNS = ('id', 'create_uid', 'create_date', 'write_uid', 'write_date')


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    tracker_status = fields.Selection(selection_add=[('passage_sans_suite', 'Passage sans suite')])
    invoice_id = fields.Many2one(comodel_name='account.invoice', string="Facture")

    @api.multi
    def _compute_section_id(self):

        section_obj = self.env['gle.section']

        section_list = []

        if self.invoice_line:
            for account_invoice_line in self.invoice_line:
                section_list.append(account_invoice_line.gle_section_id.id)

            if len(set(section_list)) <= 1:
                section_id = section_obj.search([('id', '=', section_list[0])])
            else:
                section_id = section_obj.search([('name', '=', 'ADV')])

            self.write({
                'accounting_section_id': section_id.id,
            })

    @api.multi
    def action_cancel(self):
        for inv in self:
            super(AccountInvoice, self).action_cancel()
            if inv.tracker_id and inv.tracker_id.state == 'approved':
                inv.tracker_id.signal_workflow('reset')

    @api.multi
    def invoice_validate(self):
        for inv in self:
            super(AccountInvoice, self).invoice_validate()
            if inv.tracker_id and inv.tracker_id.state == 'draft':
                inv.tracker_id.signal_workflow('approve')
