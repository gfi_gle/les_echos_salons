# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import Warning as UserError
from openerp.osv import osv
from openerp.addons.purchase_tracker.model.purchase_order import ENUM_STATUS, ENUM_TRACKER_TYPE
from openerp.addons.purchase_tracker.model.invoicegenerator import (
    WithPOInvoiceBuilder, BaseInvoiceBuilder
)
from openerp import SUPERUSER_ID
import logging
import datetime

_logger = logging.getLogger(__name__)

ENUM_STATUS = [
    ('draft', 'Draft'),
    ('open', 'Open'),
    ('partial', 'Partially invoiced'),
    ('total', 'Totally invoiced'),
    ('close', 'Closed manually'),
]


def _get_po_lines_data(self):
    lines = list()
    for order_line in self.tracker.purchase_order_ids.mapped('order_line'):
        lines.append(self._prepare_invoice_line(
            order_line))

    for refund_line in self.tracker.refund_to_receive_ids.mapped('refund_line'):
        lines.append(self._prepare_invoice_line(
            refund_line))

    return lines


def _get_invoice_jrnl_id(self):
    """Get the id of the invoice journal,
       Override method to change attribute property_purchase_invoice_journal to use property_purchase_invoice_journal_gle
       Because property_purchase_invoice_journal is a property field and is not overridable
    """
    journal_prop = self.tracker.property_purchase_invoice_journal_gle
    if not journal_prop:
        raise osv.except_osv(
            _(u"Error"),
            _(u"\"property_purchase_invoice_journal\" property missing.")
        )
    return journal_prop.id


def _generate(self):
    """Creates the invoice and returns it.
            """
    self.invoice = self._create_invoice()
    self._create_lines()
    self.invoice._compute_section_id()
    return self.invoice


WithPOInvoiceBuilder._get_po_lines_data = _get_po_lines_data
BaseInvoiceBuilder._get_invoice_jrnl_id = _get_invoice_jrnl_id
BaseInvoiceBuilder.generate = _generate


class PurchaseTrackerTracker(models.Model):
    _inherit = 'purchase_tracker.tracker'

    state = fields.Selection(selection_add=[('passage_sans_suite', 'Passage Sans suite')])
    purchase_order_ids = fields.Many2many(
        comodel_name='purchase.order',
        relation='purchase_tracker_tracker_order_rel',
        column1='tracker_id',
        column2='order_id',
        string='Purchase Orders', )

    refund_to_receive_ids = fields.Many2many(
        comodel_name='gle.refund.to.receive',
        relation='purchase_tracker_tracker_refund_rel',
        column1='tracker_id',
        column2='refund_id',
        string="Bon de commande AAR",
    )

    order_type = fields.Selection(selection=[('po', "Bon de commande"), ('aar', "Bon de commande AAR")], string="Type de bon de commande")

    po_employee = fields.Char(comodel_name='', compute='_onchange_purchase_order_ids', string="Émetteur")
    po_department = fields.Char(compute='_onchange_purchase_order_ids', string="BU")
    po_status = fields.Selection(selection=ENUM_STATUS, compute='_onchange_purchase_order_ids', string="Statut du BdC")
    po_description = fields.Char(compute='_onchange_purchase_order_ids', string="Description")
    po_amount_total = fields.Float(compute='_onchange_purchase_order_ids', string="Montant BdC")
    po_tracker_type = fields.Selection(selection=ENUM_TRACKER_TYPE, compute='_onchange_purchase_order_ids', string="Type de Tracker")

    property_purchase_invoice_journal_gle = fields.Many2one(comodel_name='account.journal', compute='_compute_invoice_journal', store=True)

    @api.onchange('invoice_type')
    @api.depends('invoice_type')
    def _compute_invoice_journal(self):

        journal_obj = self.env['account.journal']

        for purchase_tracker in self:
            journal_id = journal_obj

            if purchase_tracker.invoice_type:
                if purchase_tracker.invoice_type == 'in_invoice':
                    journal_id = journal_obj.search([('code', '=', '5ACFF')])
                else:
                    journal_id = journal_obj.search([('code', '=', '5ACAF')])

            purchase_tracker.property_purchase_invoice_journal_gle = journal_id

    @api.onchange('has_purchase_order')
    def _onchange_has_purchase_order(self):
        if self.has_purchase_order and not self.partner_id:
            self.has_purchase_order = False
            self.order_type = False
            return {
                'warning': {
                    'title': 'Avertissement',
                    'message': _('Veuillez sélectionner un partenaire avant de rattacher les bons de commandes'),
                }
            }

        if not self.has_purchase_order:
            self.order_type = False
            self.purchase_order_ids = [(5, )]

    @api.onchange('order_type')
    def _onchange_order_type(self):
        self.purchase_order_ids = [(5,)]

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id and self.partner_id not in self.purchase_order_ids.mapped('partner_id'):
            self.purchase_order_ids = False
        self._refresh_date_due()

    @api.onchange('invoice_date')
    def _onchange_invoice_date(self):
        self._refresh_date_due()

    @api.one
    @api.constrains('invoice_date')
    def _check_invoice_date(self):
        if self.invoice_date > fields.Date.today():
            raise UserError(
                _('Impossible de mettre la date postérieure à la date du jour dans le champ "Date de facture".'))

    @api.onchange('purchase_order_ids', 'refund_to_receive_ids')
    @api.depends('purchase_order_ids', 'refund_to_receive_ids')
    def _onchange_purchase_order_ids(self):
        for tracker in self:
            res = {}
            if tracker.purchase_order_ids:
                for po in tracker.purchase_order_ids:
                    res = po.get_po_fields()
                res['po_amount_total'] = tracker.get_po_amount_total(order_ids=tracker.purchase_order_ids)

            if tracker.refund_to_receive_ids:
                for po_aar in tracker.refund_to_receive_ids:
                    res = po_aar.get_po_refund_fields()
                res['po_amount_total'] = tracker.get_po_amount_total(order_ids=tracker.refund_to_receive_ids)

            tracker.update(res)

        return self._onchange_purchase_orders_change_po_status()

    def get_po_amount_total(self, order_ids):
        """ Return sum of all purchase_order_ids """

        return sum(po.amount_total for po in order_ids)

    @api.onchange('po_change_status')
    def _onchange_po_change_status(self):
        return self._onchange_purchase_orders_change_po_status()

    @api.multi
    def _refresh_date_due(self):
        payment_term = self.partner_id.operational_supplier_payment_terms_id
        if payment_term and self.invoice_date:
            lines = payment_term.line_ids
            payment_term_due_date = payment_term.compute(self.po_amount_total, self.invoice_date)
            due_date = payment_term_due_date and payment_term_due_date[0] and payment_term_due_date[0][0] and \
                       payment_term_due_date[0][0][0] or False
            if due_date:
                self.due_date = due_date
            elif lines:
                days = lines[0].days
                invoice_date = datetime.datetime.strptime(self.invoice_date, "%Y-%m-%d")
                due_date = invoice_date + datetime.timedelta(days=days)
                self.due_date = due_date.strftime("%Y-%m-%d")
        else:
            self.due_date = self.invoice_date

    def onchange_partner_invoice_date(
            self, cr, uid, ids,
            invoice_date, partner_id,
            context=None):
        res = dict()
        return dict(value=res)

    @api.multi
    def _onchange_purchase_orders_change_po_status(self):

        for tracker in self:
            if len(tracker.purchase_order_ids) > 1 and tracker.po_change_status == 'partial':
                tracker.po_change_status = False
                return dict(
                    warning=dict(title='Avertissement', message='Il faudrait changer le statut de(s) bon(s) de commande en total'))

    def _get_lines_data(self):
        """Implement to handle multiple purchase orders and subscription
        specifics.
        """

        return [
            self._make_invoice_line(order_line)
            for order_line in self.tracker.purchase_order_ids.mapped('order_line')
            ]

    @api.multi
    def _forbid_write_approved(self, vals):
        """Override to allow updates when going back to draft.
        """
        if vals and vals.get('state', False) == 'draft':
            return True
        if self.state == 'passage_sans_suite' and 'state' not in vals:
            raise UserError(_('Impossible de modifier un tracker qui a le statut "passage sans suite"'))
        return super(PurchaseTrackerTracker, self)._forbid_write_approved(vals)

    def _get_partner(self, cr, uid, ids, vals, context):

        if vals.get('purchase_order_ids', []) and len(vals.get('purchase_order_ids', [])[0][2]) > 0:
            purchase_order_ids = vals.get('purchase_order_ids', [])
            purchase_order_ids = purchase_order_ids and purchase_order_ids[0] and purchase_order_ids[0][2] or []

            po_osv = self.pool.get('purchase.order')
            po = po_osv.browse(cr, uid, purchase_order_ids, context)

        if vals.get('refund_to_receive_ids', []) and len(vals.get('refund_to_receive_ids', [])[0][2]) > 0:
            refund_to_receive_ids = vals.get('refund_to_receive_ids', [])
            refund_to_receive_ids = refund_to_receive_ids and refund_to_receive_ids[0] and refund_to_receive_ids[0][2] or []

            po_refund_osv = self.pool.get('gle.refund.to.receive')
            po = po_refund_osv.browse(cr, uid, refund_to_receive_ids, context)

        return po and po[0].partner_id or False

    def _change_po_status(self, cr, uid, ids, vals, context):
        return True

    @api.multi
    def wkf_draft(self):
        self.state = 'draft'

    @api.multi
    def call_approve_wkf(self):
        for tracker in self:
            if not tracker.partner_id.partner_info_id.code \
                    or not (tracker.partner_id.operational_supplier_account_id or tracker.partner_id.author_account_id):
                raise UserError(_('Merci de configurer un matricule et un compte comptable.'))

            state = tracker.state
            tracker.signal_workflow('approve')
            tracker.invoice_id._modifier = 'no'
            if tracker.state == state:
                raise UserError(_('Vous n\'avez pas les droits suffisants pour valider ce tracker'))

    def wkf_approved(self, cr, uid, ids, context=None):
        context = context or {}
        purchase_obj = self.pool['purchase.order']
        refund_obj = self.pool['gle.refund.to.receive']

        for tracker in self.browse(cr, uid, ids, context=context):
            self._notify_state_changed(cr, uid, tracker)

            if not tracker.delivery_only:
                self._check_and_invoice(cr, uid, tracker, context)

            if tracker.purchase_order_ids:
                purchase_obj.write(cr, uid, tracker.purchase_order_ids.ids, {
                    'status': tracker.po_change_status,
                }, context)

            if tracker.refund_to_receive_ids:
                refund_obj.write(cr, uid, tracker.refund_to_receive_ids.ids, {
                    'status': tracker.po_change_status,
                }, context)

            self.write(cr, uid, [tracker.id], {
                'date_validation': fields.Date.today(),
                'date_approve': fields.Date.today(),
                'state': 'approved',
            })

    @api.multi
    def wkf_passage_sans_suite(self):
        if self.purchase_order_id and self.purchase_order_id.state != 'passage_sans_suite':
            self.purchase_order_id.action_pss()
        self.state = 'passage_sans_suite'
