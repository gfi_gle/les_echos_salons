# -*- coding: utf-8 -*-
{
    'name': "gle_salon",

    'summary': """Exhibition Management""",

    'description': """
        Exhibition Management
    """,

    'author': "Gfi",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    'update_xml' : ['security/make_invisible.xml'],
    # always loaded
    'data': [
        'views/template_layout.xml',
        'views/views_place.xml',
        'views/views_salon.xml',
        'views/stand_views/stand_view_production.xml',
        'views/stand_views/stand_view_commercial.xml',
        'views/stand_views/stand_view_production_form_tree.xml',
        'views/views_village.xml',
        'views/menus.xml',
        'views/views_millesime.xml',
        'views/views_angle.xml',
        'views/views_type_stand.xml',
        'views/stand_wizard_view/stand_option_view_wizard.xml',
        'views/stand_wizard_view/stand_ferme_view_wizard.xml',
        'views/stand_wizard_view/stand_redecoupage_view_wizard.xml',
        'views/menu_enseigne.xml',

    ],
    'css': ['static/src/css/salon.css'],
    'qweb': ['static/src/xml/stand_quickadd.xml'],

}