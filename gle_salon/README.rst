.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=========
gle_salon
=========

This module add different data model to the existing model

Functionnality
===============

#. You can access to this new data with the new top menu 'Salon'
#. You can create 'Salon', 'Village' and 'Stand'


Configuration
=============

You can configure and manage your :
 - Project Name
 - Places
 - Stand Type

Bug Tracker
============

Incident tracking is done with the GFI Jira platform.

Credits
=======

Contributors
------------

* alexandre.matis@gfi.fr
* william.musy@gfi.fr

Maintainer
----------

.. image:: image url (http://www.gfi....image.png)
   :alt:  Odoo by GFI
   :target: http://http://www.gfi.world/

This module is maintained by GFI.