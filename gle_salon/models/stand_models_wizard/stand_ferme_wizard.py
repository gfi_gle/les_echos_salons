# -*- coding: utf-8 -*-

from openerp import models, fields, api
from datetime import datetime
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandFermeWizard(models.TransientModel):
    """ Transient model representing Stand, attributes are required fields to change state of Stand to 'ferme' """

    _name = 'gle_salon.stand_ferme_wizard'

    def get_stand_obj(self):
        stand_id = self._context.get('default_stand_id')
        return self.env['gle_salon.stand'].search([('id', '=', stand_id)])

    def _default_stand_client(self):

        stand_obj = self.get_stand_obj()
        return stand_obj.contact_operational_id

    def _default_stand_type_ids(self):
        stand_obj = self.get_stand_obj()
        return stand_obj.stand_type_ids

    def _default_stand_enseigne(self):
        stand_obj = self.get_stand_obj()
        return stand_obj.enseigne_ids

    def _default_stand_state_ware(self):
        stand_obj = self.get_stand_obj()
        return stand_obj.state_ware

    partner_id = fields.Many2one('res.partner')
    stand_id = fields.Many2one('gle_salon.stand', string="Stand", help="Stand")
    contact_operational_id = fields.Many2one('res.partner', string="Contact opérationnel", index=True,default=_default_stand_client, help="Client")
    enseigne_ids = fields.Many2many('gle_salon.enseigne', string="Enseigne", index=True, default=_default_stand_enseigne, help="Enseigne")
    state_ware = fields.Selection([('yes', 'Oui'), ('no', 'Non')], string="Etat: échange marchandise",default=_default_stand_state_ware, help="Etat de la marchandise")
    stand_type_ids = fields.Many2one(comodel_name='gle_salon.stand_type',relation='gle_salon_stand_ferme_salon_type', column1='type_id', column2='wizard_id' ,string="Type de stand", index=True, default=_default_stand_type_ids, help="Type de stand")

    @api.multi
    def action_write_stand(self):
        """ Action write to save attributes of transient model in Stand model and change sale.order state to 'ferme' """
        self.stand_id.date_ferme = datetime.now()
        print self.stand_id.date_ferme
        print self.stand_id
        if self.stand_id.id:
            stand_id = self.stand_id.id

        sql_query_enseigne = 'INSERT INTO gle_salon_stand_enseigne (stand_id, enseigne_id) VALUES ({},{});'


        list_enseigne_ids = ""


        stand_obj = self.get_stand_obj()
        if not stand_obj.enseigne_ids:
            for enseigne in self.enseigne_ids:
                list_enseigne_ids += enseigne.name + ";"
                self.env.cr.execute(sql_query_enseigne.format(stand_id, enseigne.id))

        vals = {
            'contact_operational_id': self.contact_operational_id.id,
            'state_ware': self.state_ware,
            'stand_type_ids': self.stand_type_ids.id,
            'state': 'ferme'
        }

        stand_obj = self.env['gle_salon.stand'].search([('id', '=', self.stand_id.id)])
        stand_obj.write(vals)

        stand_obj.create_reservation_salon(state='ferme',list=list_enseigne_ids)


