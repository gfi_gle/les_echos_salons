# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandRedecoupageWizard(models.TransientModel):
    """ Transient model representing Stand redecoupage, attributes are required fields to change redecoupage of Stand """

    _name = 'gle_salon.stand_redecoupage_wizard'

    redistribution_required = fields.Boolean(index=True, default=True, string="Redécoupage demandé")
    """ Date de la demande de redécoupage """
    date_required_redistribution = fields.Datetime('Date de redécoupage', default=fields.Datetime.now)

    """ Commentaire redecoupage """
    comment_required_redistribution = fields.Text(string='Commentaire redécoupage', default=lambda self: self._context.get('comment_required_redistribution'))

    @api.multi
    def action_write_redecoupage_stand(self):
        """ """
        if self._context.get('stand_id'):
            stand_id = self._context.get('stand_id')

        vals = {
            'redistribution_required': self.redistribution_required,
            'date_required_redistribution': self.date_required_redistribution,
            'comment_required_redistribution': self.comment_required_redistribution
        }

        stand_obj = self.env['gle_salon.stand'].search([('id', '=', stand_id)])
        stand_obj.write(vals)








