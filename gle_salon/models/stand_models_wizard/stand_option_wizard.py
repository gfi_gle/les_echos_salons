# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class StandOptionWizard(models.TransientModel):
    """ Transient model representing Stand, attributes are required fields to change state of Stand to 'option' """

    _name = 'gle_salon.stand_option_wizard'

    def get_stand_obj(self):
        stand_id = self._context.get('stand_id')
        return self.env['gle_salon.stand'].search([('id', '=', stand_id)])

    partner_id = fields.Many2one('res.partner', string="Donneur d'ordre", index=True)

    """ Commercial les Echos """
    commercial_echo_id = fields.Many2one('res.users', string="Commercial les Echos", index=True, help="Contact Commercial")

    """ Contact commercial client """
    commercial_partner_id = fields.Many2one('res.partner', string="Contact commercial client", index=True)

    display_communication = fields.Boolean(index=True, default=False, string="Affichage communication")
    display_communication_visible = fields.Selection([('1', "Oui"), ('0', "Non")], string="Affichage communication")

    @api.onchange('display_communication_visible')
    def _display_communication_visible(self):
        if self.display_communication_visible == "1":
            self.display_communication = 1
        else:
            self.display_communication = 0

    @api.multi
    def action_write_option_stand(self):
        """ Action write to save attributes of transient model in Stand model and change sale.order state to 'option' """

        sale_order_id = False

        if self._context.get('stand_id'):
            stand_id = self._context.get('stand_id')

        if self._context.get('sale_order_id'):
            sale_order_id = self._context.get('sale_order_id')

        vals = {
            'partner_id': self.partner_id.id,
            'commercial_echo_id': self.commercial_echo_id.id,
            'commercial_partner_id': self.commercial_partner_id.id,
            'display_communication_visible': self.display_communication_visible,
            'display_communication': self.display_communication,
            'state': 'option'
        }

        stand_obj = self.env['gle_salon.stand'].search([('id', '=', stand_id)])
        stand_obj.write(vals)

        self.env['sale.order'].search([('id', '=', sale_order_id)]).write({'optionned_stand': True})

        stand_obj.create_reservation_salon(state='option')

