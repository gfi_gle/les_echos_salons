# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import date, datetime, timedelta
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class Angle(models.Model):
    """ Class representing model Angle """

    _name = 'gle_salon.angle'
    _description = 'angle'

    name = fields.Char(string="Nombre d'angle")

    @api.onchange('name')
    def on_change_name_angle(self):
        """ On_change method on angle name. A angle stored must be unique. """

        record_set = self.env['gle_salon.angle'].search([('name', '=', self.name)])

        if record_set:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Ce nombre d'angle est déja disponible."))
            }}

            return res
