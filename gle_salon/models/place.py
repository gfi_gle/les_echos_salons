# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import date, datetime, timedelta
import logging

class Place(models.Model):
    """ Class representing the Place model. Place is the spot where is the Salon """

    _name = 'gle_salon.place'
    _description = 'place'

    name = fields.Char(string="Nom du lieu")
    adress = fields.Char(string="Adresse du lieu")
    cp = fields.Integer(string="Code postal", index=True)
    city = fields.Char(string="Ville")
