# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import date, datetime, timedelta
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class Millesime(models.Model):
    """ Class representing model Millesime """

    _name = 'gle_salon.millesime'
    _description = 'millesime'

    name = fields.Char(string="Année projet (millesime)")
    code = fields.Char(string="Code analytique projet")

    @api.onchange('name')
    def on_change_name_millesime(self):
        """ On_change method on millesime name. A millesime must be unique. """

        record_set = self.env['gle_salon.millesime'].search([('name', '=', self.name)])

        if record_set:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Ce projet (millesime) éxiste déja."))
            }}

            return res
