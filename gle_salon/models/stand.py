# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import date, datetime, timedelta, time
from datetime import datetime
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.exceptions import ValidationError
import logging
import os
import time



_logger = logging.getLogger(__name__)

class Stand(models.Model):
    """ Class representing Stand model """

    _name = 'gle_salon.stand'
    _description = 'stand'

    """ Stand number """
    name = fields.Char(string="Numéro du stand")

    village_id = fields.Many2one('gle_salon.village', string="Village", index=True)
    area = fields.Float(string='Surface ', help="Surface en m²", digits=(10,2))
    number_corners = fields.Many2one('gle_salon.angle', string="Nombre d'angle", index=True)

    organisation_area_visible = fields.Selection([('1', "Oui"),('0', "Non")], string="Zone organisateur")


    name_organisation_area = fields.Char(string="Nom de la zone organisateur")

    """ stand au niveau (étage) x du salon """
    stand_level_number = fields.Integer(index=True, string="Niveau")

    """ client/prospect """
    partner_id = fields.Many2one('res.partner', string="Client/Prospect", index=True)

    """ Enseigne """
    enseigne_ids = fields.Many2many('gle_salon.enseigne', relation='gle_salon_stand_enseigne', column1='stand_id', column2='enseigne_id', string="Enseigne", index=True)

    display_communication = fields.Boolean(index=True, default=False, string="Affichage communication")
    display_communication_visible = fields.Selection([('1', "Oui"), ('0', "Non")], string="Affichage communication")

    """ Salon / produit """
    salon_id = fields.Many2one('gle_salon.salon', string="Salon", index=True)

    stand_type_ids = fields.Many2one('gle_salon.stand_type', string="Type de stand", index=True)

    redistribution_required = fields.Boolean(index=True, default=False, string="Redécoupage demandé")
    """ Date de la demande de redécoupage """
    date_required_redistribution = fields.Datetime('Date de redécoupage', default=fields.Datetime.now)

    """ Commentaire redecoupage """
    comment_required_redistribution = fields.Text(string='Commentaire redécoupage')
    """Date fermiture de stand"""
    date_ferme = fields.Datetime('date ferme', readonly=True)

    state = fields.Selection([
        ('organisation', "Organisation"),
        ('libre', "Non attribué"),
        ('option', "Option"),
        ('ferme', "Ferme"),
    ], default='libre')

    """ Contact opérationnel du stand """
    contact_operational_id = fields.Many2one('res.partner', string="Contact stand (contact opérationnel)", index=True)

    """ Commercial les Echos """
    commercial_echo_id = fields.Many2one('res.users', string="Commercial les Echos", index=True)

    """ Commercial Client"""
    commercial_partner_id = fields.Many2one('res.partner', string="Contact Commercial", index=True)
    state_ware = fields.Selection([('yes', 'Oui'), ('no', 'Non')], string="Etat: échange marchandise", )

    reservation_salon_id = fields.Many2one('gle_reservation_salon.reservation_salon')

    @api.constrains('area')
    def _constrains_area(self):
        """ Override write method, method check if surface > 0 """
        if self.area <= 0:
            raise ValidationError('Warning', 'La surface doit etre supèrieur à 0, merci de corriger!')

    @api.onchange('display_communication_visible')
    def _display_communication_visible(self):
        if self.display_communication_visible == "1":
            self.display_communication = 1
        else:
            self.display_communication = 0

    @api.onchange('name','salon_id')
    def _stand_number(self):
        """ On change method to check if the stand number is already exist for a Salon """
        if self.salon_id:
            xsalon_id = int(self.salon_id[0])
            xtest_name_two = self.env['gle_salon.stand'].search([['name', '=', self.name],['salon_id', '=', xsalon_id]]).name

            if xtest_name_two:
                self.name = None
                res = {}
                res = {'warning': {
                   'title': _('Warning'),
                   'message': ("Ce numero de stand existe deja pour ce projet: ") + xtest_name_two + "\n" + ("Veuillez en saisir une autre valeur.")
                }}
                return res

    @api.onchange('salon_id')
    def _associate_village_empty(self):
        """ On change method to empty Village if Salon is changed """
        if self.salon_id:
            self.village_id = None

    @api.onchange('organisation_area_visible')
    def _state_change_organisation_area(self):
        """ On change method to change state workflow to 'organisation' if  organisation_area_visible = True """
        if self.organisation_area_visible == '1':
            self.state = 'organisation'
        else:
            self.state = 'libre'

    @api.multi
    def action_libre_stand(self):
        """ Workflow action change state to 'libre' """

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']
        reservation_obj.search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id), ('stand_id', '=', self.id)]).unlink()

        vals = {
            'partner_id':'',
            'commercial_partner_id':'',
            'display_communication':False,
            'display_communication_visible': '',
            'commercial_echo_id': '',
            'state': 'libre'
        }

        self.write(vals)

    @api.multi
    def action_option_stand(self):
        """ Action workflow to change state of stand to 'option'. If redistribution is required method rise an exception. """

        if self.partner_id and self.commercial_echo_id and self.commercial_partner_id and self.display_communication_visible:

            vals = {
                'partner_id': self.partner_id.id,
                'commercial_echo_id': self.commercial_echo_id.id,
                'commercial_partner_id': self.commercial_partner_id.id,
                'display_communication_visible': self.display_communication_visible,
                'state': 'option',
            }

            self.env['gle_salon.stand'].search([('id', '=', self.id)]).write(vals)

        elif not self.partner_id or not self.commercial_echo_id or not self.commercial_partner_id or self.display_communication_visible:
            raise ValidationError('Erreur',"Ce stand ne peut pas être passé en option, des champs requis ne sont pas saisis:" + "\n" + " - Type de stand" + "\n" + " - Contact opérationnel" + "\n" + " - Enseigne" + "\n" + " - Etat: échange marchandise")

    @api.multi
    def action_ferme_stand(self):
        print 'ici' * 110
        """ Action workflow to change state of stand to 'ferme'. If redistributison is required method rise an exception. """

        if not self.redistribution_required and self.stand_type_ids and self.contact_operational_id and self.enseigne_ids and self.state_ware:
            self.state = 'ferme'
            self.date_ferme = datetime.now()
            print 'date_ferme' * 10
            print '-*-' * 100
            print self.date_ferme

        else:
            if self.redistribution_required:
                raise ValidationError('Erreur', 'Ce stand ne peut pas être passé en option, un redecoupage du stand à été demandé')
            if not self.stand_type_ids or not self.contact_operational_id or not self.enseigne_ids or not self.state_ware:
                raise ValidationError('Erreur',"Ce stand ne peut pas être passé en ferme, des champs requis ne sont pas saisis:" + "\n" + " - Type de stand" + "\n" + " - Contact opérationnel" + "\n" + " - Enseigne" + "\n" + " - Etat: échange marchandise")

    @api.multi
    def button_action_option(self):
        """"""

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']
        reservation_obj.search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id), ('stand_id', '=', self.id)])

        if reservation_obj:
            raise ValidationError("Il y'a déja une option sur ce stand.")

        try:
            self.action_option_stand()
        except:

            if self.id:
                stand_id = self.id

            if self.partner_id.id:
                partner_id = self.partner_id.id

            if self.commercial_echo_id.id:
                commercial_echo_id = self.commercial_echo_id.id

            view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'salon_stand_option_view_wizard')

            return {
                'name': "Veuillez remplire les champs suivant pour le passage en option du stand.",
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_salon.stand_option_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {'stand_id': stand_id}
            }

    @api.multi
    def button_action_redecoupage(self):
        """"""

        if self.id:
            stand_id = self.id

        view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'salon_stand_redecoupage_view_wizard')

        return {
            'name': "Veuillez remplire les champs suivant pour la demande de redécoupage.",
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_salon.stand_redecoupage_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'stand_id': stand_id,
                        'redistribution_required': self.redistribution_required,
                        'date_required_redistribution': self.date_required_redistribution,
                        'comment_required_redistribution': self.comment_required_redistribution
                        }
        }

    @api.multi
    def button_action_ferme(self):
        """"""

        if self.redistribution_required:
            raise ValidationError('Erreur', 'Ce stand ne peut pas être passé en ferme, un redecoupage du stand à été demandé')

        try:
            self.action_ferme_stand()
        except:

            if self.id:
                stand_id = self.id
            if self.partner_id.id:
                partner_id = self.partner_id.id

            view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'salon_stand_wizard_adv_view')

            return {
                'name': "Veuillez remplire les champs suivant pour le passage en ferme du stand.",
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_salon.stand_ferme_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {'default_stand_id': stand_id, 'default_partner_id': partner_id, 'default_commercial_echo_id': self._context.get('uid')}
            }

    @api.multi
    def button_action_ferme_to_option(self):
        """"""

        if self.id:
            stand_id = self.id

        sql_query_type_ids = 'DELETE FROM gle_salon_stand_gle_salon_stand_type_rel WHERE gle_salon_stand_id = {} AND gle_salon_stand_type_id = {};'
        sql_query_enseigne_ids = 'DELETE FROM gle_salon_stand_enseigne WHERE stand_id = {} AND enseigne_id = {};'

        for enseigne_id in self.enseigne_ids:
            self.env.cr.execute(sql_query_enseigne_ids.format(stand_id, enseigne_id.id))

        vals = {
            'contact_operational_id':'',
            'state_ware': '',
            'stand_type_ids': '',
            'state': 'option'
        }

        self.write(vals)

    @api.multi
    def unlink(self):
        """ Override unlink method, method check if stands states are 'libre' and delete them """

        for line in self:
            if line.state != 'libre':
                raise except_orm('Erreur','Seul un stand non attribué peut être supprimé!')

        return super(Stand, self).unlink()

    @api.multi
    def create_reservation_salon(self, state, list=None):
        """"""
        xenseignestring = []
        if state == 'option':
            vals = {
                'salon_id': self.salon_id.id,
                'product_salon_code': self.salon_id.name.code,
                'project_code': self.salon_id.millesime.code,
                'partner_id': self.partner_id.id,
                'stand_id': self.id,
                'commercial_partner_id': self.commercial_partner_id.id,
                'commercial_echo_id': self.commercial_echo_id.id,
                'reservation_type': 'stand',
                'state': state,
                'stand_angle': self.number_corners.name,
                'stand_communication': self.display_communication_visible,
                'village': self.village_id.name,
                'stand_area': self.area
            }
            
            created_reservation_salon = self.env['gle_reservation_salon.reservation_salon'].create(vals)

            if created_reservation_salon:
                reservation_salon_id = created_reservation_salon.id

                self.write({'reservation_salon_id': reservation_salon_id})

        elif state == 'ferme':
            if list:
                xenseignestring = list

            vals = {
                'state_ware': self.state_ware,
                'state': state,
                'stand_type': self.stand_type_ids.name,
                'stand_enseigne': xenseignestring
            }

            self.reservation_salon_id.write(vals)

class Stand_type(models.Model):
    """ Class representing model type de stand  """

    _name = 'gle_salon.stand_type'
    _description = 'stand type'

    name = fields.Char(string="Libellé du type de stand (ex: Animation)")

class Enseigne(models.Model):
    """ Class representing model type de stand """

    _name = 'gle_salon.enseigne'

    name = fields.Char(string="Nom de l'enseigne")


class StandFull(models.Model):
    """ stand_ids """

    _name = 'gle_salon.standfull'

    def _init_stand_full(self):
        return self.env['gle_salon.stand'].search([])

    stand_ids_form_tree = fields.Many2many('gle_salon.stand', string="Stand", index=True, default=lambda self:self._init_stand_full())
    salon_id_filter = fields.Many2one('gle_salon.salon', string="Salon")
    village_id_filter = fields.Many2one('gle_salon.village', string="Village")
    state_id_filter = fields.Selection([
        ('organisation', "Organisation"),
        ('libre', "Non attribué"),
        ('option', "Option"),
        ('ferme', "Ferme"),
    ])
    stand_type_id_filter = fields.Many2one('gle_salon.stand_type', string="Type de stand", index=True)
    partner_id_filter = fields.Many2one('res.partner', string="Client", index=True)

    @api.onchange('salon_id_filter')
    def _onchange_salon_id_filter(self):
        self.village_id_filter = False

    @api.onchange('salon_id_filter','village_id_filter','state_id_filter','stand_type_id_filter','partner_id_filter')
    def _onchange_all_filter(self):
        self._query_construct()

    def _query_construct(self):
        xquery = []
        if self.salon_id_filter.id != False:
            xquery.append(('salon_id', '=', self.salon_id_filter.id))
        if self.village_id_filter.id != False:
            xquery.append(('village_id', '=', self.village_id_filter.id))
        if self.state_id_filter != False:
            xquery.append(('state', '=', self.state_id_filter))
        if self.stand_type_id_filter.id != False:
            xquery.append(('stand_type_ids', '=', self.stand_type_id_filter.id))
        if self.partner_id_filter.id != False:
            xquery.append(('partner_id', '=', self.partner_id_filter.id))

        self.stand_ids_form_tree = self.env['gle_salon.stand'].search(xquery)

    @api.multi
    def action_create_new_stand(self):
       view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'stand_commercial_view_form')

       return {
           'name': "Creation stand",
           'view_mode': 'form',
           'view_id': view_id[1],
           'view_type': 'form',
           'res_model': 'gle_salon.stand',
           'type': 'ir.actions.act_window',
       }











