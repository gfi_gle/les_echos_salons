# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import date, datetime, timedelta
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class Village(models.Model):
    """ Class representing model Village """

    _name = 'gle_salon.village'
    _description = 'village'

    name = fields.Char(string="Nom du village")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon", index=True)
    stand_ids = fields.One2many('gle_salon.stand', 'village_id', string="Stand", index=True)

    @api.onchange('name', 'salon_id')
    def on_change_name_village(self):
        """ On_change method on village attribute. A name is unique for a project. """

        record_set = self.env['gle_salon.village'].search([('name', '=', self.name), ('salon_id', '=', int(self.salon_id))])

        if record_set:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Ce nom de village éxiste déja pour ce projet."))
            }}

            return res

class village_full(models.Model):
    """ Class representing form easy search in village """
    _name = 'gle_salon.villagefull'

    def _init_village_full(self):
        return self.env['gle_salon.village'].search([])

    village_ids_form_tree = fields.Many2many('gle_salon.village', string="Villages", index=True, default=lambda self:self._init_village_full())
    salon_id_filter = fields.Many2one('gle_salon.salon', string="Salon")


    @api.onchange('salon_id_filter')
    def _onchange_all_filter(self):
        self._query_construct()


    def _query_construct(self):
        xquery = []
        if self.salon_id_filter.id != False:
            xquery.append(('salon_id', '=', self.salon_id_filter.id))

        self.village_ids_form_tree = self.env['gle_salon.village'].search(xquery)


    @api.multi
    def action_create_new_village(self):
        view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'village_form')
        return {
            'name': "Creation stand",
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_salon.village',
            'type': 'ir.actions.act_window',
        }