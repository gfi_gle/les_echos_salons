# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from datetime import datetime
import os


from datetime import date, datetime, timedelta
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging
_logger = logging.getLogger(__name__)


class Salon(models.Model):
    """ Class representing the Salon model. Salon is couple project/millesime. """

    _name = 'gle_salon.salon'
    _description = 'salon'

    name = fields.Many2one('gle_salon.produit', string="Intitulé du produit", index=True) #salon produit
    millesime = fields.Many2one('gle_salon.millesime', string='Projet / millesime') # Millesime projet

    place = fields.Many2one('gle_salon.place', string="Lieu du projet", index=True)
    levels_number = fields.Integer(index=True, string="Nombre de niveau") #Nombre de niveau du salon
    village_ids = fields.One2many('gle_salon.village', 'salon_id', string='village ids')

    salon_concat = fields.Char('salon_concat')
    date_debut_mail = fields.Date(string='Date début')
    date_fin_mail = fields.Date(string='Date fin')
    p_zero = fields.Date(string="Date P0")
    p_one = fields.Date(string="Date P1")
    p_two = fields.Date(string="Date P2")
    p_three = fields.Date(string="Date P3")
    p_four = fields.Date(string="Date P4")

    date_realisation = fields.Date(string="Date de réalisation")

    pdf_file_cgv = fields.Many2one(comodel_name='ir.attachment', string="Fichier CGV propre au salon")
    pdf_file_cgv_filename = fields.Char("Nom du fichier")

    dir_salon = fields.Many2one('res.users', string="Directeur du salon", index=True)

    deleg_user_salon_ids = fields.Many2many(comodel_name='res.users',
                                 relation='gle_salon_salon_res_users_rel',
                                 column1='gle_salon_salon_id',
                                 column2='res_users_id',
                                 string="Délégation de validation directeur à d'autre personnes", index=True)

    user_mail_to_ids = fields.Many2many(comodel_name='res.partner', string="Destinataires", index=True, required=True)
    user_mail_from_id = fields.Many2one(comodel_name='res.partner', string="Expéditeur", index=True, required=True)

    @api.model
    def create(self, vals):

        res = super(Salon, self).create(vals)

        self.update_groups_deleg_user_salon_ids()

        return res

    @api.multi
    def write(self, vals):
        res = super(Salon, self).write(vals)

        self.update_groups_deleg_user_salon_ids()

        return res

    def update_groups_deleg_user_salon_ids(self):

        res_group_obj = self.env['res.groups']
        res_users_obj = self.env['res.users']

        group_delegate_id = res_group_obj.search([('name', '=', 'Groupe délégation validation devis')])

        user_delegate_ids = self.deleg_user_salon_ids._ids
        user_not_delegate_ids = res_users_obj.search([('id', 'not in', user_delegate_ids)])

        salon_ids = self.env['gle_salon.salon'].search([('id', '!=', self.id)])
        list_salon_user_ids = []

        for salon in salon_ids:
            list_salon_user_ids += [user.id for user in salon.deleg_user_salon_ids]

        list_salon_user_ids = set(list_salon_user_ids)
        list_salon_user_ids = list(list_salon_user_ids)

        # delete group delegate in users not in self.deleg_user_salon_ids
        for user in user_not_delegate_ids:
            if user.id not in list_salon_user_ids:
                list_groups_by_user = user.mapped('groups_id')._ids

                if group_delegate_id.id in list_groups_by_user:
                    list_groups_by_user = list(list_groups_by_user)
                    list_groups_by_user.remove(group_delegate_id.id)

                    user.write({
                        'groups_id': [(6, 0, list_groups_by_user)]
                    })

        # update group delegate in users in self.deleg_user_salon_ids
        for user in self.deleg_user_salon_ids:
            list_groups_user = user.mapped('groups_id')._ids
            if group_delegate_id.id not in list_groups_user:

                list_groups_user = list(list_groups_user)
                list_groups_user.append(group_delegate_id.id)

                user.write({
                    'groups_id': [(6, 0, list_groups_user)]
                })

    @api.onchange('name','millesime')
    def on_change_name_salon(self):
        """ On_change method on salon name. A salon/millesime must be unique. """

        if self.name.id:
            record_set = self.env['gle_salon.salon'].search([('name', '=', int(self.name)),('millesime', '=', int(self.millesime))])

            if record_set:
                self.millesime = None
                res = {}
                res = {'warning': {
                    'title': _('Warning'),
                    'message': _(("Ce couple Produit/Projet éxiste déja dans la base."))
                }}

                return res
            else:
                self.salon_concat = '{}{}{}'.format(self.name.name, ' - ', self.millesime.name)


    @api.multi
    @api.onchange('start_date')
    def onchange_start_date(self):
        """ On change method on start_date attribute and reload millesime in function of start_date """

        if self.start_date:
            start_date_in_datetime = datetime.strptime(self.start_date, "%Y-%m-%d")
            start_date_in_string = start_date_in_datetime.strftime("%Y %m %d")
            self.millesime = start_date_in_string[:4]

    @api.multi
    def name_get(self):
        """ Concatenate name of produit/salon with projet/millesime. It's more easy to to recognize a Salon. """

        result = super(Salon, self).name_get()

        res = []
        for element in self:
            name = ''
            name += element.name.name or ''
            name += ' - '
            name += element.millesime.name or ''
            res.append((element.id, name))
        return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):

        salon_concat_obj = self.env['gle_salon.salon']
        args = args or []
        recs = self.browse()

        if name:

            salon_concat_ids = salon_concat_obj.search([('salon_concat', 'ilike', name)], limit=limit)

            list_salon_concat_ids = []

            for xsalon_concat in salon_concat_ids:
                list_salon_concat_ids.append(xsalon_concat.id)

            recs = self.search((args + [('id', 'in', list_salon_concat_ids)]),limit=limit)

            if not recs:
                name_list = name.split()
                i=0
                for word in name_list :
                    if word == "-":
                        continue
                    if i == 0:
                        salon_concat_ids = salon_concat_obj.search([('salon_concat', 'ilike', word)], limit=limit)

                        i+=1
                    if i > 0:
                        list_ids = salon_concat_obj.search([('salon_concat', 'ilike', word)], limit=limit)
                        salon_concat_ids = set(salon_concat_ids) & set(list_ids)

                list_ids = []
                for salon in list(salon_concat_ids):
                    list_ids.append(salon.id)

                recs = self.search([('id', 'in', list_ids)])

        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()


class Produit(models.Model):
    """ Class representing the Salon produit model.  """

    _name = 'gle_salon.produit'
    _description = 'produit'

    name = fields.Char(string="Intitulé du produit")
    code = fields.Char(string="Code analytique produit")

    montant_valid_one = fields.Integer(string="Montant validation 2 (> à): ", default=0)
    montant_valid_two = fields.Integer(string="Montant validation 3 (< à): ", default=0)
    montant_valid_three = fields.Integer(string="Montant validation 4 (> à):", default=0)
    montant_valid_four = fields.Integer(string="Montant validation 5 (> à):", default=0)

    @api.onchange('name')
    def _name_produit(self):
        """ On change method to check if the produit already exist in db """

        xtest_name_two = self.env['gle_salon.produit'].search([('name', '=', self.name)]).name

        if xtest_name_two:
            self.name = None
            res = {}
            res = {'warning': {
                'title': _('Warning'),
                'message': ("Un produit ayant cet intitule éxiste déja: ") + xtest_name_two + "\n" + (
                "Veuillez en saisir un autre intitulé.")
            }}
            return res


