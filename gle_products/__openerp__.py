# -*- coding: utf-8 -*-
{
    'name': "gle_products",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','gle_reservation_salon', 'gle_crm'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/product_marketing_views/product_marketing_view.xml',
        'views/product_marketing_views/product_marketing_category_view.xml',
        'views/product_workshop_views/product_workshop_view.xml',
        'views/product_workshop_views/product_workshop_category_view.xml',

        'views/product_wizard_views/product_reservation_option_view_wizard.xml',
        'views/product_wizard_views/product_reservation_ferme_view_wizard.xml',

        'views/menus.xml',

        'data/product_marketing_category_data.xml',
        'data/product_workshop_category_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}