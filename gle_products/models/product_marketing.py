# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)

class ProductMarketing(models.Model):
    """ Class representing products marketing/communication """

    _name = 'gle_products.product_marketing'

    name = fields.Char(string="Libellé du produit")

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Produit/projet")
    product_salon_code = fields.Char(string="Code produit", compute='_compute_code_product_salon_id')
    project_code = fields.Char(string="Code projet", compute='_compute_code_project_salon_id')

    stock_starting = fields.Integer(string="Stock de départ")
    current_stock = fields.Integer(compute='_compute_current_stock_marketing', string="Stock restant")

    category_marketing_id = fields.Many2one(comodel_name='gle_products.product_marketing_category', string="Categorie produit")

    line_product_marketing_id = fields.One2many(comodel_name='gle_crm.line_product_marketing', inverse_name='product_marketing_id')

    @api.onchange('salon_id')
    def _compute_code_project_salon_id(self):

        for product_marketing in self:
            product_marketing.project_code = product_marketing.salon_id.millesime.code

    @api.onchange('salon_id')
    def _compute_code_product_salon_id(self):

        for product_marketing in self:
            product_marketing.product_salon_code = product_marketing.salon_id.name.code

    @api.multi
    def action_launch_products_reservation_option_wizard(self):
        """ Method triggered by button on product_view to load wizard to change state in option """


        view_id = self.env['ir.model.data'].get_object_reference('gle_products', 'products_reservation_option_wizard_view')

        if isinstance(self, ProductMarketing):
            reservation_type = 'marketing'

        return {
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_products.products_option_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_product_marketing_id': self.id,
                        'default_commercial_echo_id': self._context.get('uid'),
                        'default_salon_id': self.salon_id.id,
                        'default_reservation_type': reservation_type,
                        'default_category_marketing_id': self.category_marketing_id.id}
        }

    @api.multi
    def action_launch_products_reservation_ferme_wizard(self):
        """ Method triggered by button on product_view to load wizard to change state in ferme """

        view_id = self.env['ir.model.data'].get_object_reference('gle_products','products_reservation_ferme_wizard_view')

        if isinstance(self, ProductMarketing):
            reservation_type = 'marketing'

        return {
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_products.products_ferme_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_product_marketing_id': self.id,
                        'default_reservation_type': reservation_type,
                        'default_salon_id': self.salon_id.id,
                        'default_category_marketing_id': self.category_marketing_id.id}
        }

    @api.multi
    def _compute_current_stock_marketing(self):
        """ Method to compute the current stock of a product """

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']

        for product_marketing in self:
            record_set = reservation_obj.search([('reservation_type', '=', 'marketing'), ('product_marketing_id', '=', product_marketing.id)])

            total_qty = 0
            for reservation in record_set:
                total_qty += reservation.qty_ordered

            current_stock = product_marketing.stock_starting - total_qty

            product_marketing.current_stock = current_stock

class ProductMarketingCategory(models.Model):
    """ Class representing products category's for marketing / communication """

    _name = 'gle_products.product_marketing_category'

    name = fields.Char(string="Nom de la categorie")

