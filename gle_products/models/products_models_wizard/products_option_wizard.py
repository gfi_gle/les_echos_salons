# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)

class ProductsOptionWizard(models.TransientModel):
    """ Model representing model transient wizard to create a reservation.salon from products views """

    _name = 'gle_products.products_option_wizard'

    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    product_salon_code = fields.Char(compute='_compute_code_product', string="Code produit")
    project_code = fields.Char(compute='_compute_code_project', string="Code projet")

    partner_id = fields.Many2one('res.partner', string="Client (Donneur d'ordre)")
    partner_workshop_ids = fields.Many2many(comodel_name='res.partner',
                                            relation='gle_products_workshop_res_partner_option_reservation_relation',
                                            column1="reservation_id", column2='partner_id',
                                            string="Clients (Donneurs d'ordres)")

    product_marketing_id = fields.Many2one('gle_products.product_marketing', string="Produit Marketing / Communication")
    product_workshop_id = fields.Many2one('gle_products.product_workshop', string="Produit Conférences / Ateliers")

    commercial_partner_id = fields.Many2one('res.partner', string="Contact commercial")
    commercial_echo_id = fields.Many2one('res.users', string="Commercial les Echos")

    commercial_partner_workshop_ids = fields.Many2many(comodel_name='res.partner',
                                                       relation='gle_salon_product_ws_res_partner_option_commercial_rel',
                                                       column1="reservation_id", column2='partner_id',
                                                       string="Contact commercial")

    commercial_echo_workshop_ids = fields.Many2many(comodel_name='res.users',
                                                    relation='gle_salon_product_ws_res_users_option_echos_rel',
                                                    column1="reservation_id", column2='res_users_id',
                                                    string="Commercial les Echos")

    reservation_type = fields.Selection([('stand', 'Stand'), ('marketing', "Marketing / Communication"), ('workshop', "Conférences / Ateliers")], string="Type de produit")
    qty_ordered = fields.Integer(string="Quantité commandé")

    category_workshop_id = fields.Many2one(comodel_name='gle_products.product_workshop_category',
                                            string="Categorie produit")

    category_marketing_id = fields.Many2one(comodel_name='gle_products.product_marketing_category',
                                            string="Categorie produit")

    @api.onchange('salon_id')
    def _compute_code_project(self):
        self.project_code = self.salon_id.millesime.code

    @api.onchange('salon_id')
    def _compute_code_product(self):
        self.product_salon_code = self.salon_id.name.code

    @api.multi
    def action_create_reservation_salon_products(self):
        """"""

        xquery_insert_partner_workshop_ids = "INSERT INTO gle_salon_product_workshop_res_partner_reservation_relation (reservation_id,partner_id) VALUES ({},{});"
        xquery_insert_commercial_partner_workshop_ids = "INSERT INTO gle_salon_product_workshop_res_partner_commercial_rel (reservation_id,partner_id) VALUES ({},{});"
        xquery_insert_commercial_echos_workshop_ids = "INSERT INTO gle_salon_product_workshop_res_users_echos_rel (reservation_id,res_users_id) VALUES ({},{});"

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']

        search_query = self.generate_search_query()

        record_set = reservation_obj.search(search_query)

        if record_set:
            ids_length = len(self.partner_workshop_ids)
            ids_list = [partner.id for partner in self.partner_workshop_ids]

            for reservation in record_set:
                ids_record_list = [partner.id for partner in reservation.partner_workshop_ids]

                if ids_length == len(reservation.partner_workshop_ids):
                    if set(ids_list) == set(ids_record_list):
                        raise ValidationError("Une réservation éxiste déja")

        if not self.check_qty():
            raise ValidationError("Le stock pour ce produit est insuffisant.")
        else:

            vals = {
                'salon_id': self.salon_id.id,
                'product_salon_code': self.product_salon_code,
                'project_code': self.project_code,
                'partner_id': self.partner_id.id,
                'commercial_partner_id': self.commercial_partner_id.id,
                'commercial_echo_id': self.commercial_echo_id.id,
                'reservation_type': self.reservation_type,
                'qty_ordered': self.qty_ordered,
                'state': 'option'
            }

            new_reservation = reservation_obj.create(vals)
            for partner in self.partner_workshop_ids:
                self.env.cr.execute(xquery_insert_partner_workshop_ids.format(new_reservation.id,partner.id))

            for commercial in self.commercial_partner_workshop_ids:
                self.env.cr.execute(xquery_insert_commercial_partner_workshop_ids.format(new_reservation.id, commercial.id))

            for commercial_echos in self.commercial_echo_workshop_ids:
                self.env.cr.execute(xquery_insert_commercial_echos_workshop_ids.format(new_reservation.id, commercial_echos.id))


    def generate_search_query(self):
        """ On change method, if reservation is find set value in reservation_id field """

        query_search = []

        if self.product_marketing_id.id:
            query_search.append(('product_marketing_id', '=', self.product_marketing_id.id))

        if self.product_workshop_id.id:
            query_search.append(('product_workshop_id', '=', self.product_workshop_id.id))

        if self.salon_id.id:
            query_search.append(('salon_id', '=', self.salon_id.id))

        if self.partner_id.id:
            query_search.append(('partner_id', '=', self.partner_id.id))

        if len(self.partner_workshop_ids) > 0:
            list_ids = [partner.id for partner in self.partner_workshop_ids]
            query_search.append(('partner_workshop_ids', 'in', list_ids))

        return query_search

    def check_qty(self):
        """
            Method to check quantity product.
            If current_stock is equal 0 or greater return True, else return False.
        """

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']
        reservation_search_query = []

        if self.reservation_type == 'marketing':

            reservation_search_query.append(('reservation_type', '=', 'marketing'))
            reservation_search_query.append(('product_marketing_id', '=', self.product_marketing_id.id))

        elif self.reservation_type == 'workshop':

            reservation_search_query.append(('reservation_type', '=', 'workshop'))
            reservation_search_query.append(('product_workshop_id', '=', self.product_workshop_id.id))

        reservation_record_set = reservation_obj.search(reservation_search_query)

        total_qty = 0
        for reservation in reservation_record_set:
            total_qty += reservation.qty_ordered

        current_stock = None
        if self.product_marketing_id.id:
            current_stock = self.product_marketing_id.stock_starting - total_qty


        if self.product_workshop_id.id:
            current_stock = self.product_workshop_id.stock_starting - total_qty

        current_stock = current_stock - self.qty_ordered

        if current_stock < 0:
            return False

        return True




