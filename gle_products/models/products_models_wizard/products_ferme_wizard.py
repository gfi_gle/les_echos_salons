# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)

class ProductsFermeWizard(models.TransientModel):
    """ Transient model representing wizard to change state of a product in ferme """

    _name = 'gle_products.products_ferme_wizard'

    product_marketing_id = fields.Many2one('gle_products.product_marketing', string="Produit Marketing / Communication")
    product_workshop_id = fields.Many2one('gle_products.product_workshop', string="Produit Conférences / Ateliers")

    partner_id = fields.Many2one('res.partner', string="Société")
    partner_workshop_ids = fields.Many2many(comodel_name='res.partner',
                                            relation='gle_products_workshop_res_partner_ferme_reservation_relation',
                                            column1="reservation_id", column2='partner_id',
                                            string="Clients (Donneurs d'ordres)")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    main_conference_partner_id = fields.Many2one('res.partner', string="Contact conférence principale")
    conference_partner_ids = fields.Many2many(comodel_name='res.partner',
                                              relation="gle_products_salon_res_partner_conference_wizard_rel",
                                              column1="reservation_id", column2="res_partner_id",
                                              string="Autre(s) Contact(s) Conférence")

    reservation_type = fields.Selection([('stand', 'Stand'), ('marketing', "Marketing / Communication"),
                                         ('workshop', "Conférences / Ateliers")], string="Type de produit")

    state_ware = fields.Selection([('yes', 'Oui'), ('no', 'Non')],
                                  string="Etat (Echange marchandise)")

    reservation_selection = fields.Selection([('0', 'Not Found'), ('1', 'Ferme'), ('2', 'Found')], default='0')

    reservation_id = fields.Many2one(comodel_name='gle_reservation_salon.reservation_salon', string="Reservation")

    @api.multi
    def action_ferme_reservation_salon(self):
        """ Action triggered by button, method write reservation object and change state in ferme """

        sql_qery_insert_conference_partner = 'INSERT INTO gle_reservation_salon_res_partner_conference_rel (reservation_id, res_partner_id) VALUES ({},{});'
        query_search = self.generate_search_query()

        reservation_obj = self.env['gle_reservation_salon.reservation_salon'].search(query_search)

        if reservation_obj:
            if reservation_obj.reservation_type == 'marketing':
                vals = {
                    'state_ware': self.state_ware,
                    'state': 'ferme'
                }

            if reservation_obj.reservation_type == 'workshop':


                vals = {
                    'main_conference_partner_id': self.main_conference_partner_id.id,
                    'state_ware': self.state_ware,
                    'state': 'ferme'
                }

                for partner_id in self.conference_partner_ids:
                    self.env.cr.execute(sql_qery_insert_conference_partner.format(reservation_obj.id, partner_id.id))

            reservation_obj.write(vals)

    def generate_search_query(self):
        """ Method to generate a search query to find reservation """

        query_search = []

        if self.product_marketing_id.id:
            query_search.append(('product_marketing_id', '=', self.product_marketing_id.id))
        if self.product_workshop_id.id:
            query_search.append(('product_workshop_id', '=', self.product_workshop_id.id))
        if self.salon_id.id:
            query_search.append(('salon_id', '=', self.salon_id.id))
        if self.partner_id.id:
            query_search.append(('partner_id', '=', self.partner_id.id))
        if len(self.partner_workshop_ids)>0:
            list_ids = [partner.id for partner in self.partner_workshop_ids]
            query_search.append(('partner_workshop_ids', 'in', list_ids))


        return query_search




