# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import warnings
import datetime
import logging

_logger = logging.getLogger(__name__)

class ProductWorkshop(models.Model):
    """ Class representing products workshop/communication """

    _name = 'gle_products.product_workshop'

    name = fields.Char(string="Libellé du produit")

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Produit/projet")
    product_salon_code = fields.Char(string="Code produit", compute='_compute_code_product_salon_id')
    project_code = fields.Char(string="Code projet", compute='_compute_code_project_salon_id')

    stock_starting = fields.Integer(string="Stock de départ")
    current_stock = fields.Integer(compute='_compute_current_stock_workshop', string="Stock restant")

    category_workshop_id = fields.Many2one(comodel_name='gle_products.product_workshop_category',
                                            string="Categorie de l'article")

    line_product_workshop_id = fields.One2many(comodel_name='gle_crm.line_product_workshop', inverse_name='product_workshop_id')

    @api.depends('salon_id')
    def _compute_code_project_salon_id(self):

        for product_workshop in self:
            product_workshop.project_code = product_workshop.salon_id.millesime.code

    @api.depends('salon_id')
    def _compute_code_product_salon_id(self):

        for product_workshop in self:
            product_workshop.product_salon_code = product_workshop.salon_id.name.code

    @api.multi
    def action_launch_products_reservation_wizard(self):
        """ Method triggered by button on product_view to load wizard to change state in option """

        view_id = self.env['ir.model.data'].get_object_reference('gle_products',
                                                                 'products_reservation_option_wizard_view')
        if isinstance(self, ProductWorkshop):
            reservation_type = 'workshop'

        return {
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_products.products_option_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_product_workshop_id': self.id,
                        'default_commercial_echo_id': self._context.get('uid'),
                        'default_salon_id': self.salon_id.id,
                        'default_reservation_type': reservation_type,
                        'default_category_workshop_id': self.category_workshop_id.id}
        }

    @api.multi
    def action_launch_products_reservation_ferme_wizard(self):
        """ Method triggered by button on product_view to load wizard to change state in ferme """

        view_id = self.env['ir.model.data'].get_object_reference('gle_products', 'products_reservation_ferme_wizard_view')

        if isinstance(self, ProductWorkshop):
            reservation_type = 'workshop'

        return {
            'view_mode': 'form',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_products.products_ferme_wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'default_product_workshop_id': self.id,
                        'default_reservation_type': reservation_type,
                        'default_salon_id': self.salon_id.id,
                        'default_category_workshop_id': self.category_workshop_id.id}
        }

    def _compute_current_stock_workshop(self):
        """ Method to compute the current stock of a product """

        reservation_obj = self.env['gle_reservation_salon.reservation_salon']

        for product_workshop in self:
            record_set = reservation_obj.search([('reservation_type', '=', 'workshop'), ('product_workshop_id', '=', product_workshop.id)])

            total_qty = 0
            for reservation in record_set:
                total_qty += reservation.qty_ordered

            current_stock = product_workshop.stock_starting - total_qty

            product_workshop.current_stock = current_stock

class ProductWorkshopCategory(models.Model):
    """ Class representing products workshop/communication """

    _name = 'gle_products.product_workshop_category'

    name = fields.Char(string="Nom de la categorie")