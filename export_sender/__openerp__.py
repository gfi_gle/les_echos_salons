# -*- coding: utf-8 -*-
{
    'name': "gfi_export_sender",

    'summary': """
        """,

    'description': """
        Send partner and accounting export files to external systems.
    """,

    'author': "GFI Informatique",
    'website': 'http://gfi.world',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '8.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'account', 'sale_invoice_bank_tracker_afb120'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_move_line_batch.xml',
        'views/partner_batch.xml',
        'views/sepa_batch.xml',
        'data/cron_tasks/bank_tracker_receiver.xml',
        'data/cron_tasks/partner_batch_sender.xml',
        'data/cron_tasks/acc_entry_batch_sender.xml',
        'data/cron_tasks/sepa_sender.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
