import base64
import logging
from openerp import api
from openerp import exceptions
from openerp import models
import os
import paramiko
import scp
from tempfile import gettempdir

log = logging.getLogger(__name__)


class SalesInvoiceBankTrackerImport(models.TransientModel):
    """Receive sales bank trackers from a remote server.Post
    """

    _inherit = 'sale.invoice_bank_tracker.import'

    @api.model
    def receive_files(self, remote_server_args, remote_path):
        """Receive files from a remote server and import them as sales bank
        trackers.

        :param remote_server_args: Arguments sent to
        `paramiko's "connect" function`_.
        :type remote_server_args: Dictionary.

        .. _paramiko's "connect" function:
            http://docs.paramiko.org/en/latest/api/client.html
            #paramiko.client.SSHClient.connect

        :param remote_path: Where files reside on the remote server.
        :type remote_path: String.
        """

        # Check remote server arguments.
        if not remote_server_args or not remote_server_args.get('hostname'):
            raise exceptions.Warning(
                'Bank tracker receiver remote server undefined.'
            )
        # Check the remote path.
        if not remote_path:
            remote_path = '/'
        elif remote_path[-1] != '/':
            remote_path += '/'

        # Prepare the local temp path.
        temp_path = gettempdir() or '/tmp/'
        if temp_path[-1] != '/':
            temp_path += '/'

        def logThis(msg, *args):
            log.info('Receiving bank trackers: %s' % msg, *args)

        logThis('Start.')
        try:
            logThis('Init (keys / etc)...')
            ssh_client = paramiko.SSHClient()
            ssh_client.set_log_channel(__name__)
            ssh_client.load_system_host_keys()
            ssh_client.set_missing_host_key_policy(
                paramiko.WarningPolicy(),
            )
            logThis('Connecting to "%s"...', remote_server_args['hostname'])
            ssh_client.connect(**remote_server_args)
            logThis('Connected OK.')
            logThis('Finding files...')
            ssh_stdin, ssh_stdout, ssh_stderr = (
                ssh_client.exec_command('ls %s*.txt' % remote_path)
            )
            file_list = [file_name.strip('\n') for file_name in ssh_stdout]

            if not file_list:
                logThis('Nothing to process.')
                return True
            logThis('Found %d files (%s).', len(file_list),
                    ', '.join(file_list))

            for file_name in file_list:
                local_path = temp_path + os.path.basename(file_name)

                with scp.SCPClient(ssh_client.get_transport()) as scp_client:
                    logThis('Receiving "%s"...', file_name)
                    scp_client.get(file_name, local_path)
                    logThis('Received.')

                logThis('Reading the local file.')
                local_file = open(local_path, 'rb')
                tracker_data = local_file.read()
                local_file.close()
                logThis('Read the local file.')

                logThis('Importing...')
                trackers = self.create({
                    'file': base64.b64encode(tracker_data),
                }).parse_file()
                logThis('Imported; tracker IDs: %s.', trackers.ids)

            logThis('Marking the files as processed...')
            for file_name in file_list:
                ssh_client.exec_command(
                    'mv "%s" "%s_processed"' % (file_name, file_name)
                )
            logThis('Marked the files as processed.')

        except Exception as e:
            logThis('Failed: %s', e.message)
            return

        # Done!
        logThis('Done.')
        return True
