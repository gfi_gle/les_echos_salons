# -*- coding: utf-8 -*-

import base64
import logging
from openerp import api
from openerp import exceptions
from openerp import models
from openerp import fields
import paramiko
from tempfile import NamedTemporaryFile

log = logging.getLogger(__name__)


class SepaBatch(models.Model):
    """Customize SEPA batches for JAL:
    - Send SEPA files to a remote server.
    """

    _inherit = 'account.voucher.sepa_batch'

    sent_to_remote_server = fields.Boolean(
        string='Sent to a remote server',
        help=(
            'Whether the SEPA file generated for this batch has been sent to '
            'a remote server.'
        ),
        readonly=True,
    )

    @api.model
    def send_sepa_files(self, remote_server_args, remote_path):
        """Send SEPA files to a remote server for batches for which the "sent"
        flag has not already been set.

        :param remote_server_args: Arguments sent to
        `paramiko's "connect" function`_.
        :type remote_server_args: Dictionary.

        .. _paramiko's "connect" function:
            http://docs.paramiko.org/en/latest/api/client.html
            #paramiko.client.SSHClient.connect

        :param remote_path: Where to send files on the remote server.
        :type remote_path: String.
        """

        # Check remote server arguments.
        if not remote_server_args or not remote_server_args.get('hostname'):
            raise exceptions.Warning('SEPA remote server undefined.')

        # Check the remote path.
        if not remote_path:
            remote_path = '/'
        elif remote_path[-1] != '/':
            remote_path += '/'

        # Explicit ordering to send files in the right order.

        for batch in self.search([
            ('sent_to_remote_server', '=', False),
        ], order='id ASC'):
            batch.send_sepa_file(remote_server_args, remote_path)

        return True

    @api.multi
    def send_sepa_file(self, remote_server_args, remote_path):
        """Send the SEPA file attached to this batch to a remote server.

        :param remote_server_args: Arguments sent to
        `paramiko's "connect" function`_.
        :type remote_server_args: Dictionary.

        .. _paramiko's "connect" function:
            http://docs.paramiko.org/en/latest/api/client.html
            #paramiko.client.SSHClient.connect

        :param remote_path: Where to send files on the remote server.
        :type remote_path: String.
        """

        self.ensure_one()

        if not remote_server_args:
            raise exceptions.Warning('SEPA remote server undefined.')

        def logThis(msg, *args):
            log.info(
                'Sending SEPA file - "%s" batch: %s' % (self.name, msg), *args
            )

        logThis('Start.')

        # Find attachments linked to this batch.
        attachments = self.env['ir.attachment'].search([
            ('res_id', '=', self.id),
            ('res_model', '=', self._name),
        ], limit=1)
        if not attachments:
            logThis('No attachment; aborting.')
            self.sent_to_remote_server = True
            return

        target_path = remote_path + 'XSCT_%s.xml' % self.name

        with NamedTemporaryFile(suffix='.xml') as temp_file:

            logThis('Writing to the "%s" file...', temp_file.name)
            temp_file.write(base64.b64decode(attachments.datas))
            temp_file.flush()

            try:
                logThis('Init (keys / etc)...')
                ssh_client = paramiko.SSHClient()
                ssh_client.set_log_channel(__name__)
                ssh_client.load_system_host_keys()
                ssh_client.set_missing_host_key_policy(
                    paramiko.WarningPolicy(),
                )
                logThis('Connecting to "%s"...',
                        remote_server_args['hostname'])
                ssh_client.connect(**remote_server_args)
                logThis('Connected.')

                with ssh_client.open_sftp() as sftp_client:
                    logThis('Sending to "%s"...', target_path)
                    sftp_client.put(temp_file.name, target_path)
                    logThis('Sent.')

            except Exception as e:
                logThis('Failed: %s', e.message)
                return

        # Done! Flag the record as such.
        self.sent_to_remote_server = True
        logThis('Done.')
        return True
