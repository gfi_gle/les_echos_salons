# -*- coding: utf-8 -*-


{
    'name': 'Payment Term Extension',
    'version': '8.0.1.0.0',
    'category': 'Accounting & Finance',
    'summary': 'Adds rounding, months and weeks properties on '
               'payment term lines',
    'description': "",
    'author': 'Camptocamp,Odoo Community Association (OCA)',
    'maintainer': 'OCA',
    'website': 'http://www.camptocamp.com/',
    'license': 'AGPL-3',
    'depends': ['account'],
    'data': ['account_view.xml'],
    'test': [],
    'installable': True,
}
