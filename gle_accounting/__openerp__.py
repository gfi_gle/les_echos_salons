# -*- coding: utf-8 -*-
{
    'name': "gle_accounting",

    'summary': """
        Les Echos Accounting module""",

    'description': """
        Module de comptabilité spécifique pour Les Echos    """,

    'author': "GFI",
    'website': "http://www.yourcompany.com",
    'category': 'Accounting & Finance',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'account',
#                 'l10n_fr',
                'sale',
                'purchase',
                'product',
                'gle_salon',
                'account_streamline'
                ],

    # always loaded
    'data': [
        'wizard/step_wizard.xml',
        'wizard/account_invoice_refus.xml',
        'security/ir.model.access.csv',
        'data/ir_cron.xml',
        'data/ir_sequence_type.xml',
        'data/email_template.xml',
        'data/account_journal.xml',
        'views/account_invoice.xml',
        'views/purchase_order.xml',
        'views/sale_order.xml',
        'views/gle_account_refund.xml',
        'views/account_move.xml',
        'views/move_line.xml',
        'views/company_view.xml',
        'views/res_config_view.xml',
        'views/res_partner.xml',
        'views/product.xml',
        'views/refund_to_receive_views.xml',
        'views/account_journal.xml',
        'views/bank_menu_item_view.xml',
        'views/account_period.xml',
    ],
    'demo': [
    ]
}
