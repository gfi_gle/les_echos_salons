# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp import models, fields
import datetime

import logging

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):

    _inherit = "account.move"

    extourne = fields.Boolean("Extourne", default=False)
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    parent_move_id = fields.Many2one(comodel_name='account.move', string="Pièce comptable parente (Provision)")

    @api.multi
    def post(self):

        ret = super(AccountMove, self).post()

        for doc in self:
            for entry in doc.line_id:

                if entry.tag in ['new_pca', 'new_cca'] and entry.account_id.code.startswith('4'):
                    entry.reconciliation_marker = doc.internal_sequence_number

                if entry.tag in ['ex_fae', 'ex_aae', 'ex_aar'] \
                        and entry.account_id.code.startswith('4'):
                    entry.reconciliation_marker = doc.parent_move_id.internal_sequence_number

                # Only care about client & supplier accounts.
                if entry.account_id.type not in ('payable', 'receivable'):
                    continue

                if doc.name.startswith(('F', 'H')):
                    if doc.object_reference and hasattr(doc.object_reference, 'invoice_type') \
                            and doc.object_reference.invoice_type == 'in_refund':
                        refund_id = self.env['account.invoice'].search([('internal_number', '=', doc.name)])
                        invoice_id = refund_id.invoice_id

                        move_id = self.env['account.move'].search([('name', '=', invoice_id.internal_number)])

                        entry.reconciliation_marker = move_id.internal_sequence_number
                    else:
                        entry.reconciliation_marker = doc.internal_sequence_number

                if doc.name.startswith('A'):

                    invoice_id = self.env['account.invoice'].search([('internal_number', '=', doc.object_reference.origin)])
                    move_id = self.env['account.move'].search([('name', '=', invoice_id.internal_number)])

                    entry.reconciliation_marker = move_id.internal_sequence_number

                # test pour savoir si l'account.move est crée manuellement
                if not doc.name.startswith(('A', 'B', 'F', 'H', 'P')):
                    doc.write({'name': self.env['account.invoice'].get_name_provision()})

        return ret

    @api.multi
    def _get_move_extourne(self, name=None, journal_id=None, ref=None, dt=None, salon_id=None):
        # ToDO Gestions des dates
        period_id = False
        if dt:
            period_id = self.env['account.period'].find(dt=dt).id or False
        """
        move._get_move_extourne
        :return:  vals
        """
        # Date
        # Period from date
        self.ensure_one()
        return {
            'name': name,
            'ref': ref or self.ref,
            'journal_id': journal_id or self.journal_id.id,
            'period_id': period_id or self.period_id.id,
            'salon_id': salon_id,
            'line_id': [(0, 0, line._get_move_line_extourne()) for line in self.line_id],
            'date': dt
        }


class AccountMoveLine(models.Model):

    _inherit = "account.move.line"

    @api.multi
    def _get_move_line_extourne(self, modele=None):
        """
        move._get_move_line_extourne
        :return:  vals
        """
        self.ensure_one()
        vals = self.get_line_dict()

        # reconciliation marker
        if vals.get('account_id', False):
            account_id = self.env['account.account'].search([('id', '=', vals.get('account_id'))])
            if account_id and account_id.code.startswith('4'):
                vals.update({
                    'reconciliation_marker': self.move_id.internal_sequence_number,
                })

        vals['credit'], vals['debit'] = vals['debit'], vals['credit']
        vals['projet_id'] = self.projet_id.id
        vals['produit_id'] = self.produit_id.id
        vals['section_id'] = self.section_id.id
        vals['nature_id'] = self.nature_id.id
        vals['code_interco'] = self.code_interco
        vals['date_origin_document'] = self.date_origin_document
        vals['tax_id'] = self.tax_id.id

        return vals

    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id','=',1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now()

    def get_periode(self):
        periode_fiscal = None
        now = str(self.get_validation_date())
        self._cr.execute("SELECT id FROM account_fiscalyear WHERE state like 'draft' AND date_stop >= %s AND date_start <= %s", (now, now))
        fiscal_year = self._cr.fetchall()[0][0]
        periodes = self.env['account.period'].search([('fiscalyear_id.id', '=', fiscal_year)])
        for periode in periodes:
            if datetime.datetime.strptime(periode.date_start, '%Y-%m-%d').date().month == self.get_validation_date().month:
                periode_fiscal = periode.id
                p =periode
        return periode_fiscal

    projet_id = fields.Many2one(comodel_name='gle_salon.millesime', string="Projet")
    produit_id = fields.Many2one(comodel_name='gle_salon.produit', string="Produit")
    section_id = fields.Many2one(comodel_name='gle.section', string="Section")
    code_interco = fields.Char(string="Interco")

    nature_id = fields.Many2one(comodel_name='gle.nature.comptable', string="Nature")

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Projet / Produit")

    tag = fields.Selection(selection=[     ('new_pca', "Comptabilisation PCA"),
                                           ('new_cca', "Comptabilisation CCA"),
                                           ('new_fae', "Comptabilisation FAE"),
                                           ('new_fnp', "Comptabilisation FNP"),
                                           ('ex_pca', "Extourne PCA"),
                                           ('ex_cca', "Extourne CCA"),
                                           ('ex_fae', "Extourne FAE"),
                                           ('ex_fnp', "Extourne FNP"),
                                           ('new_pca_echange', "Comptabilisation PCA ECHANGE"),
                                           ('new_cca_echange', "Comptabilisation CCA ECHANGE"),
                                           ('new_fae_echange', "Comptabilisation FAE ECHANGE"),
                                           ('new_fnp_echange', "Comptabilisation FNP ECHANGE"),
                                           ('ex_pss', "Extourne passage sans suite"),
                                           ('ex_pca_echange', "Extourne PCA ECHANGE"),
                                           ('ex_cca_echange', "Extourne CCA ECHANGE"),
                                           ('ex_fae_echange', "Extourne FAE ECHANGE"),
                                           ('ex_fnp_echange', "Extourne FNP ECHANGE"),
                                           ('new_aae', "Comptabilisation AAE"),
                                           ('new_aar', "Comptabilisation AAR"),
                                           ('ex_aae', "Extourne AAE"),
                                           ('ex_aar', "Extourne AAR"),
                                           ('new_aae_echange', "Comptabilisation AAE ECHANGE"),
                                           ('new_aar_echange', "Comptabilisation AAR ECHANGE"),
                                           ('ex_aae_echange', "Extourne AAE ECHANGE"),
                                           ('ex_aar_echange', "Extourne AAR ECHANGE"),
                                           ],)

    #transforme une ligne en un dictionnaire pour creer un nouvel enregistrement
    def get_line_dict(self):
        return {

            'partner_id': self.partner_id.id,
            'name': self.name.split('\n')[0][:64],
            'debit': self.debit,
            'credit': self.credit,
            'account_id': self.account_id.id,
            'quantity': self.quantity,

        }

    def create_additional_account_move_line(self, dict_line, modele, value):

        if modele in ["pca","fnp"]:
            dict_line.update({'credit': value,
                              'debit': 0})
        if modele in ["cca","fae"]:
            dict_line.update({'credit': 0,
                              'debit': value})
        self.env['account.move.line'].create(dict_line)

    def set_move_line_dict_per_modele(self, modele, vals, invoice=None, order=None, devis=None, value=0, salon=None,
                                      line_order=None):
        if modele in ["aae", "aae_echange"] and devis:
            if isinstance(devis, self.env['gle.account.refund'].__class__):
                vals.update({'credit': value or sum([x.price_subtotal for x in devis.refund_line_ids]),
                             'debit': 0})
        if modele in ["aar","aar_echange"] and devis:
            if isinstance(devis, self.env['gle.refund.to.receive'].__class__):
                if line_order:
                    vals.update({'credit': 0,
                                 'debit': value or sum([x.price_subtotal for x in line_order])})
                else:
                    vals.update({'credit': 0,
                                 'debit': value or sum([x.price_subtotal for x in devis.refund_line])})

        if modele in ["pca", "pca_echange"]:
            if invoice:
                vals.update({'credit': value or sum([x.price_subtotal for x in invoice.invoice_line]),
                             'debit': 0})
            elif order:
                vals.update({'credit': 0,
                             'debit': value or sum([x.price_subtotal for x in order.order_line])})
        if modele in ["fnp","fnp_echange"]:
            if invoice:
                vals.update({'credit': 0,
                             'debit': value or sum([x.price_subtotal for x in invoice.invoice_line])})
            elif order:
                if line_order:
                    vals.update({'credit': value or sum([x.price_subtotal for x in line_order]),
                                 'debit': 0})
                else:
                    vals.update({'credit': value or sum([x.price_subtotal for x in order.order_line]),
                                 'debit': 0})
        if modele in ["cca","cca_echange"]:
            if invoice:
                # Get lines invoices for salon
                lines = invoice.invoice_line.filtered(lambda x: x.salon_id == salon)
                vals.update({'credit': 0,
                             'debit': value or sum([x.price_subtotal for x in lines])})
            elif order:
                vals.update({'credit': value or sum([x.price_subtotal for x in order.order_line]),
                             'debit': 0})
        if modele in ["fae","fae_echange"]:
            if invoice:
                vals.update({'credit': value or sum([x.price_subtotal for x in invoice.invoice_line]),
                             'debit': 0})
            elif order:
                vals.update({'credit': 0,
                             'debit': value or sum([x.price_subtotal for x in order.order_line])})
        return vals

    def get_additional_move_line_dict_from_invoice(self, invoice, modele=None, value=0, salon=None):
        salon = salon
        period = self.env['account.period'].find(dt=invoice.date_comptabilisation)
        if modele == 'aar':
            modele2 = 'fnp'
        elif modele == 'aar_echange':
            modele2 = 'fnp_echange'
        elif modele == 'aae':
            modele2 ='fae'
        elif modele == 'aae_echange':
            modele2= 'fae_echange'
        else:
            modele2 = modele

        vals = {

            'partner_id': invoice.partner_id.id,
            'name': modele,
            'tag': 'new_' + modele,
            'account_id': self.env['account.account'].search([('modele', '=', modele)], limit=1).id,
            'quantity': 1,
        }

        vals = self.set_move_line_dict_per_modele(modele, vals, invoice=invoice, value=value, salon=salon)
        return vals

    def get_values_taxes(self, order, modele):
        if modele == 'aar':
            tag = 'new_aar'
            modele = 'fnp'
        elif modele == 'aar_echange':
            tag = 'new_aar_echange'
            modele = 'fnp_echange'
        elif modele == 'aae':
            tag = 'new_aae'
            modele ='fae'
        elif modele == 'aae_echange':
            tag = 'new_aae_echange'
            modele= 'fae_echange'
        else:
            tag = 'new_'+ modele
        print 'TVA'*10, order.amount_untaxed
        vals =  {
            'journal_id': order.journal_id.id,
            'partner_id': order.partner_id.id,
            'name' : 'TVA %s' % modele.replace('_',' '),
            'tag' :  tag,
            'account_id' : self.env['account.account'].search([('code','=','445870')], limit=1).id,
            'date': str(self.get_validation_date()),
            'debit': 0,
            'credit': order.amount_tax,
            'quantity': 1,
        }
        return vals

    def get_additional_move_line_dict_from_order(self, order, modele, line_order=None):
        if modele == 'aar':
            tag = 'new_aar'
            modele = 'fnp'
        elif modele == 'aar_echange':
            tag = 'new_aar_echange'
            modele = 'fnp_echange'
        elif modele == 'aae':
            tag = 'new_aae'
            modele ='fae'
        elif modele == 'aae_echange':
            tag = 'new_aae_echange'
            modele= 'fae_echange'
        else:
            tag = 'new_'+ modele
        vals =  {

            'partner_id': order.partner_id.id,
            'name' : modele,
            'tag' :  tag,
            'account_id' : self.env['account.account'].search([('modele','=',modele)], limit=1).id,
            'quantity': 1,
        }

        vals = self.set_move_line_dict_per_modele(modele, vals, order=order, line_order=line_order)

        return  vals

    def get_additional_move_line_dict_from_devis(self, devis, modele, line_order=None):
        if modele == 'aar':
            tag = 'new_aar'
            modele2 = 'fnp'
        elif modele == 'aar_echange':
            tag = 'new_aar_echange'
            modele2 = 'fnp_echange'
        elif modele == 'aae':
            tag = 'new_aae'
            modele2 = 'fae'
        elif modele == 'aae_echange':
            tag = 'new_aae_echange'
            modele2= 'fae_echange'
        else:
            modele2 = modele
            tag =  'new_'+ modele2

        vals = {
            'partner_id': devis.partner_id.id,
            'name': modele2,
            'tag': tag,
            'account_id' : self.env['account.account'].search([('modele','=',modele2)], limit=1).id,
            'quantity': 1,
        }

        vals = self.set_move_line_dict_per_modele(modele, vals, invoice=None, order=None, devis=devis, value=0,
                                                  line_order=line_order)
        return vals

    def get_account_move_line_to_cancel(self, tag):
        vals = self.get_line_dict()

        if tag == 'ex_pss' and self.account_id.code.startswith('4'):
            vals.update({
                'reconciliation_marker': self.move_id.internal_sequence_number,
            })

        #PERMUTATION DES VALEURS CREDIT/DEBIT
        vals['credit'],  vals['debit'] = vals['debit'], vals['credit']
        vals['tag'] = tag
        return vals

    def cancel_account_move_line(self, tag):
        vals = self.get_account_move_line_to_cancel(tag)
        vals.update({
            'period_id': self.get_periode(),
        })
        self.create(vals)

    @api.onchange('salon_id')
    def on_change_salon_id(self):
        if self.salon_id:
            self.produit_id = self.salon_id.name
            self.projet_id = self.salon_id.millesime
        else:
            self.produit_id = False
            self.projet_id = False

    def onchange_partner_id(self, cr, uid, ids, move_id, partner_id, account_id=None, debit=0, credit=0, date=False, journal=False, context=None):

        vals = super(AccountMoveLine, self).onchange_partner_id(cr, uid, ids, move_id, partner_id, account_id=None, debit=0, credit=0, date=False, journal=False, context=None)
        partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
        account = self.pool.get('account.account').browse(cr, uid, account_id, context=context)

        code_interco = "9Z99"

        if partner and account and account.code.startswith(('6', '7')):
            if partner.intercos:
                code_interco = partner.code_intercos

            vals.get('value').update({
                'code_interco': code_interco,
            })

            return vals

    def onchange_account_id(self, cr, uid, ids, account_id=False, partner_id=False, context=None):
        vals = super(AccountMoveLine, self).onchange_account_id(cr, uid, ids, account_id=False, partner_id=False, context=None)

        partner = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
        account = self.pool.get('account.account').browse(cr, uid, account_id, context=context)

        code_interco = "9Z99"

        if partner and account and account.code.startswith(('6', '7')):
            if partner.intercos:
                code_interco = partner.code_intercos

            vals.get('value').update({
                'code_interco': code_interco,
            })

            return vals
