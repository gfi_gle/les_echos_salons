# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import datetime 

import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'
    type_facturation = fields.Selection(selection=[ ('normal', 'Classique'),
                                                    ('intercos', 'Intercos'),
                                                    ('r_echange', 'Echange des règlements'),
                                                    ('m_echange', 'Echange des marchandises')],
                                        string="Type de facturation", default="normal")
        
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")
    move_id = fields.Many2one('account.move', string="Pièce Comptable")

    @api.onchange('type_facturation')
    @api.multi
    def onchange_Type_facturation(self):
        if not self.type_facturation:
            return {'domain':{'partner_id':  [('supplier', '=', True)],
                              }}
        elif self.type_facturation == 'intercos':
            if self.partner_id and not self.partner_id.intercos:
                self.partner_id=False
            return {'domain':{'partner_id':  [('intercos', '=', True), ('supplier', '=', True)],
                              }}
        elif self.type_facturation in ['r_echange', 'm_echange']:
            if self.partner_id and (self.partner_id.supplier==False or self.partner_id.customer == False):
                self.partner_id=False
            domain = [('supplier', '=', True), ('customer', '=', True)]
            return {'domain':{'partner_id':  domain,
                              }}
        
    STATE_SELECTION = [
        ('draft', 'Draft PO'),
        ('sent', 'RFQ'),
        ('bid', 'Bid Received'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Purchase Confirmed'),
        ('except_picking', 'Shipping Exception'),
        ('except_invoice', 'Invoice Exception'),
        ('done', 'Done'),
        ('passage_sans_suite','Passage sans suite'),
        ('cancel', 'Cancelled')
    ]
    
    state = fields.Selection(selection=[
                                        ('draft', 'Draft PO'),
                                        ('sent', 'RFQ'),
                                        ('bid', 'Bid Received'),
                                        ('confirmed', 'Waiting Approval'),
                                        ('approved', 'Purchase Confirmed'),
                                        ('except_picking', 'Shipping Exception'),
                                        ('except_invoice', 'Invoice Exception'),
                                        ('done', 'Done'),
                                        ('passage_sans_suite','Passage sans suite'),
                                        ('cancel', 'Cancelled')
                                    ],)
    
    @api.depends('invoice_ids')
    def _compute_total_invoice(self):
        for rec in self:
            rec.nbr_invoice = len(rec.invoice_ids)
    nbr_invoice = fields.Float(string="Nombre factures",compute="_compute_total_invoice",  store=True, select=True)
    

    def _get_def_user(self):
        return self.env['res.users'].browse(self._uid)
    suivi_user_id = fields.Many2one('res.users', "Affaire suivie par", default=_get_def_user)
    contact_fournisseur_id = fields.Many2one('res.partner',string="Contact fournisseur")

    def _prepare_invoice(self, cr, uid, order, line_ids, context=None):
        invoice_vals = super(PurchaseOrder, self)._prepare_invoice(cr, uid, order, line_ids, context=context)
        # Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
        invoice_vals.update({'salon_id' : order.salon_id.id,
                             'type_facturation' : order.type_facturation })
        return invoice_vals

    @api.multi
    def wkf_send_rfq(self):
        result = super(PurchaseOrder, self).wkf_send_rfq()
        ctx = result['context']
        template_id = self.env['ir.model.data'].get_object_reference('gle_accounting', 'gle_facture_fournisseur_email_template')[1]
        ctx.update({'default_template_id':template_id})
        result["context"] = ctx
        return result

    @api.multi
    def copy(self, default=None):
        raise ValidationError(_('Duplication est désactivée'))

    @staticmethod
    def _get_object_reference(purchase_order):

        return "purchase.order,{0}".format(purchase_order.id)
