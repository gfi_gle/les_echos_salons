# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import time 
import datetime
from lxml import etree
import openerp.addons.decimal_precision as dp
from openerp.addons.gle_numerotation.models.ComputeNumerotation import ComputeNumerotation
import logging

_logger = logging.getLogger(__name__)

TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Supplier Invoice
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Supplier Refund
}

MAGIC_COLUMNS = ('id', 'create_uid', 'create_date', 'write_uid', 'write_date')

# mapping invoice type to journal type
TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale_refund',
    'in_refund': 'purchase_refund',
}


class GleAccountRefund(models.Model):
    _name = "gle.account.refund"
        
    @api.model
    def _default_journal(self):
        if self._context.get('type_invoice', 'client') == 'client' : 
            type='sale_refund'
        else:
            type='purchase_refund'
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', '=', type),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)
    
    type_facturation = fields.Selection(selection=[ ('normal', 'Classique'),
                                                    ('intercos', 'Intercos'),
                                                    ('r_echange', 'Echange des règlements'),
                                                    ('m_echange', 'Echange des marchandises')    ],
                                        string="Type de facturation")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed','Confirmed'),
        ('cancel', 'Sans suite'),
        ('done', 'Done'),
    ], 'Status',default = 'draft')
    
    name = fields.Char('Name', default='0',required=True)

    
    type = fields.Selection([
        ('client', 'Client'),
        ('fournisseur', 'Fournisseur'),
    ], 'Type',)
    
    partner_id = fields.Many2one('res.partner', 'Client')
    
    date = fields.Datetime('Date', default=fields.Datetime.now )
    
    invoice_origin_id = fields.Many2one('account.invoice', string="Facture d'origine",
                                 help="Facture d'origine", required=True)
    
    invoice_new_id = fields.Many2one('account.invoice', string='Nouvel avoir',
                                 help='New refund')

    period_id = fields.Many2one('account.period', string='Forcer période')
    journal_id = fields.Many2one('account.journal', string='Journal devis', default=_default_journal, )
    
    salon_id = fields.Many2one('gle_salon.salon', string="Salon", required=True)
    move_id = fields.Many2one('account.move', string='Pièce comptable',
        help="Pièce comptable.")
    
    description = fields.Char('Reason', required=True)

    refund_line_ids = fields.One2many('gle.account.refund.line', 'refund_id',  
                                      string='Lignes', readonly=True, 
                                      states={'draft': [('readonly', False)]}, copy=True)
    
    @api.multi
    @api.depends('invoice_new_id')
    def _compute_refund_count(self):
        if self.invoice_new_id:
            self.refund_count = 1

    refund_count = fields.Integer(string='Orders', compute='_compute_refund_count')

    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(GleAccountRefund, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        
        if self._context.get('type_invoice'):
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//field[@name='invoice_origin_id']"):
                if self._context['type_invoice'] == 'client':
                    node.set('domain', "[('type', '=', 'out_invoice'),('state','=',['open','paid']),('salon_id','!=', None)]")
                else:
                    node.set('domain', "[('type', '=', 'in_invoice'),('state','in',['open','paid']),('salon_id','!=', None)]")
            res['arch'] = etree.tostring(doc)
            
        return res
    
    @api.model
    def _refund_cleanup_lines(self, lines):
        result = []
        for line in lines:
            values = {}
            for name, field in line._fields.iteritems():
                if name in MAGIC_COLUMNS:
                    continue
                elif field.type == 'many2one':
                    values[name] = line[name].id
                elif field.type not in ['many2many', 'one2many']:
                    values[name] = line[name]
                elif name == 'invoice_line_tax_id':
                    values[name] = [(6, 0, line[name].ids)]
            values['gle_account_refund_line_id'] = line.id
            result.append((0, 0, values))
        return result    
    
    @api.model
    def _prepare_refund(self):
        values = {}
        invoice = self.invoice_origin_id
        for field in ['name', 'reference', 'comment', 'date_due', 'partner_id', 'company_id',
                'account_id', 'currency_id', 'payment_term', 'user_id', 'fiscal_position']:
            if invoice._fields[field].type == 'many2one':
                values[field] = invoice[field].id
            else:
                values[field] = invoice[field] or False

        values['invoice_line'] = self._refund_cleanup_lines(self.refund_line_ids)
        
        tax_lines = filter(lambda l: l.manual, invoice.tax_line)
        values['tax_line'] = self._refund_cleanup_lines(tax_lines)

        if self.journal_id:
            journal = self.env['account.journal'].browse(self.journal_id.id)
        elif invoice['type'] == 'in_invoice':
            journal = self.env['account.journal'].search([('type', '=', 'purchase_refund')], limit=1)
        else:
            journal = self.env['account.journal'].search([('type', '=', 'sale_refund')], limit=1)
        values['journal_id'] = journal.id

        values['type'] = TYPE2REFUND[invoice['type']]
        values['date_invoice'] = self.date or fields.Date.context_today(invoice)
        values['state'] = 'draft'
        values['salon_id'] = invoice.salon_id.id
        values['number'] = False
        values['origin'] = invoice.number

        if self.period_id:
            values['period_id'] = self.period_id
        if self.description:
            values['name'] = self.description

        return values         
    
    @api.multi
    def refund_create(self):
        values = self._prepare_refund()
        if not self.invoice_new_id:
            new_refund = self.env['account.invoice'].create(values)
            self.update({'state': 'done', 'invoice_new_id': new_refund.id})
        return True
    
    @api.multi
    def refund_confirmed(self):
        computar = ComputeNumerotation(env=self.env)
        return self.update({'state': 'open', 'name': computar.compute_sale_order_aae_name(order=self, type_creation='account_refund')})
    
    @api.onchange('invoice_origin_id')
    def _onchange_invoice(self):
        self.partner_id = self.invoice_origin_id.partner_id
        self.name = self.invoice_origin_id.name
        self.salon_id = self.invoice_origin_id.salon_id
        self.type_facturation = self.invoice_origin_id.type_facturation
        refund_lines = []
        invoices_lines = self.invoice_origin_id.invoice_line.filtered(
            lambda line: line.product_id.type in ('consu', 'product', 'service')
        )
        for invoice_line in invoices_lines:
            line = {
                'name': invoice_line.name,
                'uos_id': invoice_line.uos_id,
                'invoice_line_id': invoice_line.id,
                'product_id': invoice_line.product_id.id,
                'quantity': invoice_line.quantity,
                'account_id': invoice_line.account_id,
                'price_unit': invoice_line.price_unit,
                'origin_quantity': invoice_line.quantity,
                'origin_price_unit': invoice_line.price_unit,
                'price_subtotal': invoice_line.price_subtotal,
                'discount': invoice_line.discount,
            }
            
            refund_lines.append((0, 0, line))
            
        value = self._convert_to_cache(
            {'refund_line_ids': refund_lines}, validate=False)
        self.update(value)
        
    @api.multi
    def action_refund_access(self):

        view_id = self.env['ir.model.data'].get_object_reference('account', 'invoice_form')

        return {
            'name': _('Refund'),
            'view_id': view_id[1],
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': self.invoice_new_id.id,
        }

    @api.multi
    def action_pss(self):
        self.state = "cancel"

    @staticmethod
    def _get_object_reference(account_refund):

        return "gle.account.refund,{0}".format(account_refund.id)


class GleAccountRefundLine(models.Model):
    _inherit = "account.invoice.line"
    _name = "gle.account.refund.line"
    
    invoice_line_id = fields.Many2one('account.invoice.line',
                                      string='Invoice Line')
    
    refund_id = fields.Many2one('gle.account.refund', string='Refund')
    
    origin_price_unit = fields.Float(string='Origin Unit Price', digits= dp.get_precision('Product Price'))
    
    origin_quantity = fields.Float(string='Origin Quantity', digits= dp.get_precision('Product Unit of Measure'),
        required=True, default=1)