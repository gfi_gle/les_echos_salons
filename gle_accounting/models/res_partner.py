# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _

class ResPartner(models.Model):
    """ add intercos in partners """
    _inherit = 'res.partner'


    intercos = fields.Boolean('Interco', default=False)
    code_intercos = fields.Char('Code interco', size=4)
    lastname = fields.Char("Lastname", required=True)
    is_company = fields.Boolean('Is a Company', help="Check if the contact is a company, otherwise it is a person")
