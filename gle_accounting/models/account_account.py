# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import time 
import datetime

import logging
_logger = logging.getLogger(__name__)
class AccountAccount(models.Model):
    _inherit = "account.account"
    
    modele = fields.Selection(selection=[('pca', "PCA"),
                                           ('cca', "CCA"),
                                           ('fae', "FAE"),
                                           ('fnp', "FNP"),
                                           ('pca_echange', "PCA ECHANGE"),
                                           ('cca_echange', "CCA ECHANGE"),
                                           ('fae_echange', "FAE ECHANGE"),
                                           ('fnp_echange', "FNP ECHANGE")],)
    
    @api.model
    def creation_comptes_comptable_le(self):
        obj_account = self.env['account.account']
        obj_product_cat = self.env['product.category']
        obj_pos_fiscal=self.env['account.fiscal.position']
        obj_section=self.env['gle.section']
        obj_taxes = self.env['account.tax']
        oat = self.env['account.account.type']
        
       
       
#      ~~~~   Comptes comptable Catégories d’acticles
        if len(obj_account.search([['code','=',"701560"]])) == 0:
            obj_account.create(vals={
                        'name': _('VENTES PRODUITS DIVERS FR'),
                        'code': '701560',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','701']]).id
                        })
            
        if len(obj_account.search([['code','=',"706060"]])) == 0:
            obj_account.create(vals={
                        'name': _('VENTE ESPACE PUB REGIE EXTERNE'),
                        'code': '706060',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })

        if len(obj_account.search([['code','=',"706100"]])) == 0:
            obj_account.create(vals={
                        'name': _('PUBLICITE FRANCE'),
                        'code': '706100',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
            
        if len(obj_account.search([['code','=',"706106"]])) == 0:
            obj_account.create(vals={
                        'name': _('PUBLICITE INTERNE'),
                        'code': '706106',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706280"]])) == 0:
            obj_account.create(vals={
                        'name': _('PUB ECHANGES FRANCE'),
                        'code': '706280',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706510"]])) == 0:    
            obj_account.create(vals={
                        'name': _('VISITEURS/ PARTICIP FR'),
                        'code': '706510',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706520"]])) == 0:
            obj_account.create(vals={
                        'name': _('OP SPEC/SPONS/PARTEN FR'),
                        'code': '706520',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706527"]])) == 0:
            obj_account.create(vals={
                        'name': _('OP SPEC/SPONS/PARTEN CEE'),
                        'code': '706527',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706530"]])) == 0:
            obj_account.create(vals={
                        'name': _('CONF & ATELIERS TECHNIQUE'),
                        'code': '706530',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706540"]])) == 0:
            obj_account.create(vals={
                        'name': _('LOCATION STANDS'),
                        'code': '706540',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        if len(obj_account.search([['code','=',"706600"]])) == 0:
            obj_account.create(vals={
                        'name': _('VENTES LOCATIONS FICHIERS'),
                        'code': '706600',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Income']]).id,
                        'parent_id': obj_account.search([['code','=','706000']]).id
                        })
        
#      ~~~~   Comptes comptable Compte de TVA
        
        if len(obj_account.search([['code','=',"445663"]])) == 0:
            obj_account.create(vals={
                        'name': _('TVA à récupérer/débit'),
                        'code': '445663',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Asset']]).id,
                        'parent_id': obj_account.search([['code','=','4456']]).id
                        })
            
        if len(obj_account.search([['code','=',"445665"]])) == 0:   
            obj_account.create(vals={
                        'name': _('TVA à récupérer/encaissement'),
                        'code': '445665',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Asset']]).id,
                        'parent_id': obj_account.search([['code','=','4456']]).id
                        })
            
        if len(obj_account.search([['code','=',"445720"]])) == 0:   
            obj_account.create(vals={
                        'name': _('TVA coll 20% ENC'),
                        'code': '445720',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Liability']]).id,
                        'parent_id': obj_account.search([['code','=','4457']]).id
                        })
            
        if len(obj_account.search([['code','=',"445730"]])) == 0:    
            obj_account.create(vals={
                        'name': _('TVA coll 8,5% Dom ENC'),
                        'code': '445730',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Liability']]).id,
                        'parent_id': obj_account.search([['code','=','4457']]).id
                        })
        
#      ~~~~   Comptes comptable - D’autres comptes comptables
        if len(obj_account.search([['code','=',"487200"]])) == 0:  
            obj_account.create(vals={
                        'name': _("PRODUITS CONSTATES D’AVANCE"),
                        'code': '487200',
                        "modele":"pca",
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Liability']]).id,
                        'parent_id': obj_account.search([['code','=','487000']]).id
                        })
        else:
            obj_account.search([['code',"=","487200"]]).write({"modele":"pca"})
            
        if len(obj_account.search([['code','=',"418500"]])) == 0:      
            obj_account.create(vals={
                        'name': _('FACTURES A ETABLIR CAS ECHANGE'),
                        'code': '418500',
                        "modele":"fae_echange",
                        'type': 'receivable',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Receivable']]).id,
                        'parent_id': obj_account.search([['code','=','418']]).id
                        })
        else:
            obj_account.search([['code',"=","418500"]]).write({"modele":"fae_echange"})
            
        if len(obj_account.search([['code','=',"487500"]])) == 0:      
            obj_account.create(vals={
                        'name': _("PRODUITS CONSTATES D’AVANCE CAS ECHANGE"),
                        'code': '487500',
                        "modele":"pca_echange",
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Liability']]).id,
                        'parent_id': obj_account.search([['code','=','487000']]).id
                        })
        else:
            obj_account.search([['code',"=","487500"]]).write({"modele":"pca_echange"})
        
        if len(obj_account.search([['code','=',"408500"]])) == 0:      
            obj_account.create(vals={
                        'name': _('FACTURES NON PARVENUES CAS ECHANGE'),
                        'code': '408500',
                        "modele":"fnp_echange",
                        'type': 'payable',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Payable']]).id,
                        'parent_id': obj_account.search([['code','=','408']]).id
                        })
        else:
            obj_account.search([['code',"=","408500"]]).write({"modele":"fnp_echange"})
        
        if len(obj_account.search([['code','=',"486500"]])) == 0:      
            obj_account.create(vals={
                        'name': _('CHARGES CONSTATES D’AVANCE CAS ECHANGE'),
                        'code': '486500',
                        "modele":"cca_echange",
                        'type': 'receivable',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Receivable']]).id,
                        'parent_id': obj_account.search([['code','=','486000']]).id
                        })
        else:
            obj_account.search([['code',"=","486500"]]).write({"modele":"cca_echange"})
           
        if len(obj_account.search([['code','=',"408000"]])) == 0:      
            obj_account.create(vals={
                        'name': _('FACTURES NON PARVENUES'),
                        'code': '408000',
                        "modele":"fnp",
                        'type': 'receivable',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Payable']]).id,
                        'parent_id': obj_account.search([['code','=','408']]).id
                        })
        else:
            obj_account.search([['code',"=","408000"]]).write({"modele":"fnp"})
        
        if len(obj_account.search([['code','=',"418000"]])) == 0:    
            obj_account.create(vals={
                        'name': _('FACTURES A ETABLIR'),
                        'code': '418000',
                        "modele":"fae",
                        'type': 'receivable',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Receivable']]).id,
                        'parent_id': obj_account.search([['code','=','418']]).id
                        })
        else:
            obj_account.search([['code',"=","418100"]]).write({"modele":"fae"})

           
            
#         obj_account.search([['code',"=","418100"]]).write({"modele":"fae"})
#         obj_account.search([['code',"=","40800"]]).write({"modele":"fnp"})
        obj_account.search([['code',"=","486000"]]).write({"modele":"cca"})
        
#      ~~~~   Comptes comptable - D’autres comptes comptables
        if len(obj_account.search([['code','=',"623109"]])) == 0:
            obj_account.create(vals={
                        'name': _('ECHG INSERTIONS PRESS'),
                        'code': '623109',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','623100']]).id
                        })
        if len(obj_account.search([['code','=',"623129"]])) == 0:
            obj_account.create(vals={
                        'name': _('ECHG PUB EN LIGNE'),
                        'code': '623129',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','623100']]).id
                        })
        if len(obj_account.search([['code','=',"623359"]])) == 0:    
            obj_account.create(vals={
                        'name': _('ECHG OPERATION PROMO'),
                        'code': '623359',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','623300']]).id
                        })
        if len(obj_account.search([['code','=',"604150"]])) == 0:   
            obj_account.create(vals={
                        'name': _('INSTAL GENE EVENEMENT'),
                        'code': '604150',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','604000']]).id
                        })
        if len(obj_account.search([['code','=',"605700"]])) == 0:    
            obj_account.create(vals={
                        'name': _('AUT. COUTS FAB'),
                        'code': '605700',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','605000']]).id
                        })
        if len(obj_account.search([['code','=',"621300"]])) == 0:    
            obj_account.create(vals={
                        'name': _('PERS SALONS/CONF'),
                        'code': '621300',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','621']]).id
                        })
        if len(obj_account.search([['code','=',"623256"]])) == 0:    
            obj_account.create(vals={
                        'name': _('REFERCEMT SITES INTERNET'),
                        'code': '623256',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','623200']]).id
                        })
        if len(obj_account.search([['code','=',"622306"]])) == 0:   
            obj_account.create(vals={
                        'name': _("COM S/APPORTEUR D'AFFAIRES"),
                        'code': '622306',
                        'type': 'other',
                        'user_type': oat.with_context(lang="en_US").search([['name','=','Expense']]).id,
                        'parent_id': obj_account.search([['code','=','622']]).id
                        })
        
#      ~~~~   Catégories

        if len(obj_product_cat.search([['name','=',"All GLE"]])) == 0:
            obj_product_cat.create(vals={
                        'name':"All GLE"
                })
            
        if len(obj_product_cat.search([['name','=',"Acompte N°1"]])) == 0:
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Acompte N°1",
                        'property_account_income_categ':obj_account.search([['code','=','419100']]).id,
                        'code':1,
                })
            
        if len(obj_product_cat.search([['name','=',"Exposition"]])) == 0:   
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Exposition",
                        'property_account_income_categ':obj_account.search([['code','=','706540']]).id,
                        'code':2,
                })
            
        if len(obj_product_cat.search([['name','=',"Marketing Visiteurs"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Marketing Visiteurs",
                        'property_account_income_categ':obj_account.search([['code','=','708800']]).id,
                        'code':3,
                })
       
        
        if len(obj_product_cat.search([['name','=',"Conférences, Ateliers ET Plénières"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Conférences, Ateliers ET Plénières",
                        'property_account_income_categ':obj_account.search([['code','=','706530']]).id,
                        'code':4,
                })
        
        if len(obj_product_cat.search([['name','=',"Supports de Communication ET Produit Dérivés"]])) == 0:     
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Supports de Communication ET Produit Dérivés",
                        'property_account_income_categ':obj_account.search([['code','=','708800']]).id,
                        'code':5,
                })
        
            
        if len(obj_product_cat.search([['name','=',"Partenariat ET Sponsoring"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Partenariat ET Sponsoring",
                        'property_account_income_categ':obj_account.search([['code','=','706520']]).id,
                        'code':6,
                })
        
        if len(obj_product_cat.search([['name','=',"Electricité"]])) == 0:     
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Electricité",
                        'property_account_income_categ':obj_account.search([['code','=','706540']]).id,
                        'code':7,
                })
        
        if len(obj_product_cat.search([['name','=',"Publicité Web"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Publicité Web",
                        'code':8,
                        'property_account_income_categ':obj_account.search([['code','=','706100']]).id,
                })
        
        
        if len(obj_product_cat.search([['name','=',"Publicité hors Web"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Publicité hors Web",
                        'property_account_income_categ':obj_account.search([['code','=','706100']]).id,
                        'code':9,
                })
     
        if len(obj_product_cat.search([['name','=',"Location de fichiers ET frais techniques"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Location de fichiers ET frais techniques",
                        'property_account_income_categ':obj_account.search([['code','=','706600']]).id,
                        'code':10,
                })
            
         
        if len(obj_product_cat.search([['name','=',"Echanges marchandises"]])) == 0:   
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Echanges marchandises",
                        'property_account_income_categ':obj_account.search([['code','=','706280']]).id,
                        'code':11,
                })
        
        if len(obj_product_cat.search([['name','=',"Entrées visiteurs"]])) == 0:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Entrées visiteurs",
                        'property_account_income_categ':obj_account.search([['code','=','706510']]).id,
                        'code':12,
                })
        intercos_categ = obj_product_cat.search([['name','=',"Intercos"]])
        if not intercos_categ:    
            obj_product_cat.create(vals={
                        'parent_id': obj_product_cat.search([['name','=','All GLE']]).id,
                        'name':"Intercos",
                        'property_account_income_categ':obj_account.search([['code','=','706520']]).id,
                        'code':13,
                        'intercos': True
                })
        else:
            intercos_categ.write({'intercos' : True})
            
        #############################################
        if not self.env['product.template'].search([('name','=','Acompte')]) :
            self.env['product.template'].create({
                        'name': 'Acompte',
                        'for_acompte': True,
                        'type':'service',
                        'sale_ok' : True,
                        'purchase_ok': False,
                        'categ_id': self.env['product.category'].search([['name','=',"Acompte N°1"]]).id,
                        })
        else:
            self.env['product.template'].search([('name','=','Acompte')]).write({"for_acompte":True})

                
        if  not obj_taxes.search([('name','=',"TVA coll 20% ENC")]) :
            obj_taxes.create({
                        'name': 'TVA coll 20% ENC',
                        'description': "N",
                        'type_tax_use': 'sale',
                        'type': 'percent',
                        'amount': 0.2,
                        'sequence': 2,
#                         'parent_id': obj_taxes.search([['name','=','TVA à collecter']]).id,
                        'account_collected_id': obj_account.search([['code','=','445720']]).id,
                        'base_sign':1.0,
                        'tax_sign':1.0,
                        'account_paid_id': obj_account.search([['code','=','445720']]).id,
                        'ref_base_sign':-1.0,
                        'ref_tax_sign':-1.0,
                        })
        else:
            self.env['account.tax'].search([('name','=',"TVA coll 20% ENC")]).write({'description':"N"})
        
            
        if  not obj_taxes.search([('name','=',"TVA coll 8,5% Dom ENC")]) :
            obj_taxes.create({
                        'name': 'TVA coll 8,5% Dom ENC',
                        'description': "2",
                        'type_tax_use': 'sale',
                        'type': 'percent',
                        'amount': 0.085,
                        'sequence': 3,
#                             'parent_id': obj_taxes.search([['name','=','TVA à collecter']]).id,
                        'account_collected_id': obj_account.search([['code','=','445730']]).id,
                        'base_sign':1.0,
                        'tax_sign':1.0,
                        'account_paid_id': obj_account.search([['code','=','445730']]).id,
                        'ref_base_sign':-1.0,
                        'ref_tax_sign':-1.0,
                        })
        else:
            self.env['account.tax'].search([('name','=',"TVA coll 8,5% Dom ENC")]).write({'description':"2"})
        
        if  not obj_taxes.search([('name','=',"TVA à récupérer/débit 20 %")]) :
            obj_taxes.create({
                        'name': 'TVA à récupérer/débit 20 %',
                        'description': "N",
                        'parent_id': obj_taxes.search([['name','=','TVA à collecter']]).id,
                        'type_tax_use': 'purchase',
                        'type': 'percent',
                        'amount':0.2,
                        'sequence': 2,
#                             'parent_id': obj_taxes.search([['name','=','TVA à récupérer']]).id,
                        'account_collected_id': obj_account.search([['code','=','445663']]).id,
                        'base_sign':-1.0,
                        'tax_sign':-1.0,
                        'account_paid_id': obj_account.search([['code','=','445663']]).id,
                        'ref_base_sign':1.0,
                        'ref_tax_sign':1.0,
                        })
        else:
            self.env['account.tax'].search([('name','=',"TVA à récupérer/débit 20 %")]).write({'description':"N"})
      
                  
        if  not obj_taxes.search([('name','=',"TVA à récupérer/débit 8,5 %")]) :
            obj_taxes.create({
                        'name': 'TVA à récupérer/débit 8,5 %',
                        'description': "2",
                        'type_tax_use': 'purchase',
                        'type': 'percent',
                        'amount':0.085,
                        'sequence':3,
#                             'parent_id': obj_taxes.search([['name','=','TVA à récupérer']]).id,
                        'account_collected_id': obj_account.search([['code','=','445663']]).id,
                        'base_sign':-1.0,
                        'tax_sign':-1.0,
                        'account_paid_id': obj_account.search([['code','=','445663']]).id,
                        'ref_base_sign':1.0,
                        'ref_tax_sign':1.0,
                        })
        else:
            self.env['account.tax'].search([('name','=',"TVA à récupérer/débit 8,5 %")]).write({'description':"2"}) 
            
        if  not obj_taxes.search([('name','=',"TVA à récupérer/encaissement 20 %")]) :
            obj_taxes.create({
                        'name': 'TVA à récupérer/encaissement 20 %',
                        'description': "N",
                        'type_tax_use': 'purchase',
                        'type': 'percent',
                        'amount':0.2,
                        'sequence': 4,
#                             'parent_id': obj_taxes.search([['name','=','TVA à récupérer']]).id,
                        'account_collected_id': obj_account.search([['code','=','445665']]).id,
                        'base_sign':-1.0,
                        'tax_sign':-1.0,
                        'account_paid_id': obj_account.search([['code','=','445665']]).id,
                        'ref_base_sign':1.0,
                        'ref_tax_sign':1.0,
                        })
        else:
            self.env['account.tax'].search([('name','=',"TVA à récupérer/encaissement 20 %")]).write({'description':"N"})   
        
        if  not obj_taxes.search([('name','=',"TVA à récupérer/encaissement 8,5 %")]) :
            obj_taxes.create({
                        'name': 'TVA à récupérer/encaissement 8,5 %',
                        'type_tax_use': 'purchase',
                        'description': "2",
                        'type': 'percent',
                        'amount':0.085,
                        'sequence': 5,
#                             'parent_id': obj_taxes.search([('name','=','TVA à récupérer')]).id,
                        'account_collected_id': obj_account.search([['code','=','445665']]).id,
                        'base_sign':-1.0,
                        'tax_sign':-1.0,
                        'account_paid_id': obj_account.search([['code','=','445665']]).id,
                        'ref_base_sign':1.0,
                        'ref_tax_sign':1.0,
                        }) 
        else:
            self.env['account.tax'].search([('name','=',"TVA à récupérer/encaissement 8,5 %")]).write({'description':"2"})

        _logger.debug("--------------------------------------------------------")
        _logger.debug(obj_pos_fiscal.search([('name', 'ilike', "Récupérer TVA sur débit")]))

        if not obj_pos_fiscal.search([('name','ilike',"Récupérer TVA sur débit")]) :
            _logger.debug("-------------------------------------------------------- 1")
            obj_pos_fiscal.create({
                        'name': 'Récupérer TVA sur débit',
                        'tax_ids': [(0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA déductible (achat) 20,0%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA à récupérer/débit 20 %")]).id,
                                          }),
                                    (0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA déductible (achat) 8,5%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA à récupérer/débit 8,5 %")]).id,
                                          })]
                        })

        if not obj_pos_fiscal.search([('name','ilike',"Récupérer TVA sur encaissement")]) :
            _logger.debug("-------------------------------------------------------- 2")
            obj_pos_fiscal.create({
                        'name': 'Récupérer TVA sur encaissement',
                        'tax_ids': [(0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA déductible (achat) 20,0%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA à récupérer/encaissement 20 %")]).id,
                                          }),
                                    (0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA déductible (achat) 8,5%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA à récupérer/encaissement 8,5 %")]).id,
                                          })]
                        })

        if not obj_pos_fiscal.search([('name','ilike',"Collecter TVA sur encaissement")]) :
            _logger.debug("-------------------------------------------------------- 3")
            obj_pos_fiscal.create({
                        'name': 'Collecter TVA sur encaissement',
                        'tax_ids': [(0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA collectée (vente) 20,0%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA coll 20% ENC")]).id,
                                          }),
                                    (0,0,{'tax_src_id':obj_taxes.search([('name','=',"TVA collectée (vente) 8,5%")]).id,
                                          'tax_dest_id':obj_taxes.search([('name','=',"TVA coll 8,5% Dom ENC")]).id,
                                          })]
                        })
        
        gle_types = self.env['gle.type'].search([])
        for type in gle_types:
            if not type.account_id:
                account_id = self.env["account.account"].search([('code','=','607100')])
                if account_id:
                    type.write({"account_id":account_id.id})
                else:
                    print "Le compte 607100 n'existe pas, assurez-vous de bien configurer votre plan comptable français"
        
        gle_prestations = self.env['gle.prestation'].search([])
        for prestation in gle_prestations:
            if not prestation.product_ids:
                product_ids = [(0,0,{
                        'name': prestation.name,
                        'type': "service",
                        'uom_id': self.env.ref('product.product_uom_unit').id ,
                        'property_account_expense' : prestation.type_id.account_id.id,
                        'sale_ok' : False,
                        'purchase_ok' : True,
                        })]
                prestation.write({'product_ids':product_ids})
            
        gle_salon = self.env['gle_salon.produit']
        salon1 = gle_salon.search([('name','=','Salon des Entrepreneurs Paris')])
        salon2 = gle_salon.search([('name','=','Forum National des Associations et Fondations Nantes')])
        salon3 = gle_salon.search([('name','=','Congrès des DAF et des Directeurs juridiques Paris')])
        salon4 = gle_salon.search([('name','=','Salon Handicap, Emploi et Achats Responsables Marseille')])
        salon5 = gle_salon.search([('name','=','Small Business Tech Lyon')])
        
        if salon1:
            salon1.write({"code":"SDEPAR"})
        if salon2:
            salon2.write({"code":"FORNAF"})
        if salon3:
            salon3.write({"code":"CGRDAF"})
        if salon4:
            salon4.write({"code":"SDEHDP"})
        if salon5:
            salon5.write({"code":"SDELYO"})
            
            
            
            
            
