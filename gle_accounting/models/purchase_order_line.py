# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    move_id = fields.Many2one('account.move', string="Piece comptable provision")