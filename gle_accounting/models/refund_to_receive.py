# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.addons.purchase_tracker.model.purchase_order import ENUM_TRACKER_TYPE
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

STATE_SELECTION = [
    ('cancel', 'Sans suite'),
    ('draft', 'Brouillon'),
    ('confirm', "Achat confirmé"),

]

ENUM_STATUS = [
    ('draft', 'Brouillon'),
    ('open', 'Ouvert'),
    ('partial', 'Partiellement Facturé'),
    ('total', 'Totallement facturé'),
    ('close', 'Fermé manuellement'),
]


class RefundToReceive(models.Model):
    """ Model representing quotation AAR """

    _name = 'gle.refund.to.receive'

    name = fields.Char(string="N° commande")
    state = fields.Selection(STATE_SELECTION, string="Statut", default='draft')

    def _default_journal(self):
        domain = [
            ('type', '=', 'purchase'),
            ('company_id', '=', self.env.user.company_id.id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Projet/Produit")
    partner_id = fields.Many2one(comodel_name='res.partner', string="Fournisseur")

    pricelist_id = fields.Many2one(comodel_name='product.pricelist', string="Liste de prix", required=True)

    type_facturation = fields.Selection(selection=[('normal', 'Classique'),
                                                   ('intercos', 'Intercos'),
                                                   ('r_echange', 'Echange des règlements'),
                                                   ('m_echange', 'Echange des marchandises')],
                                        string="Type de facturation", default='normal')

    date_order = fields.Datetime(string="Date de la commande", default=lambda self: fields.Datetime.now())

    refund_line = fields.One2many(comodel_name='gle.refund.to.receive.line', inverse_name='refund_id', string="Lignes de devis")

    amount_untaxed = fields.Float(compute='_amount_all', string="Montant Hors-taxe", store=True)
    amount_tax = fields.Float(compute='_amount_all', string="Taxes", store=True)
    amount_total = fields.Float(compute='_amount_all', string="Total", store=True)
    move_id = fields.Many2one('account.move', string="Pièce Comptable")
    journal_id = fields.Many2one('account.journal', string='Journal',
                                 default=_default_journal)
    period_id = fields.Many2one('account.period', string='Forcer période')

    employee_id = fields.Many2one(comodel_name='hr.employee', string="Emetteur", default=lambda self: self._get_user())
    department_id = fields.Many2one(comodel_name='hr.department', string="Departement de l'émetteur")
    tracker_type = fields.Selection(selection=ENUM_TRACKER_TYPE, string="Type de tracker", default='invoicing')
    status = fields.Selection(selection=ENUM_STATUS, string="Status", readonly=True, default='open')  # state to purchase tracker
    description = fields.Char(size=100, string="Description")
    invoice_new_id = fields.Many2one('account.invoice', string='Nouvel avoir fournisseur',
                                     help='New refund')

    contact_fournisseur_id = fields.Many2one('res.partner', string="Contact fournisseur")
    partner_ref = fields.Char(string="Référence du fournisseur")

    def _get_user(self):
        """ Return current user to populate employee_id field """

        employee_obj = self.env['hr.employee']
        employee_id = employee_obj.search([('user_id', '=', self._context.get('uid'))])

        if employee_id:
            return employee_id

        return employee_obj

    def get_po_refund_fields(self):

        res = {
            'po_employee': self.employee_id.name,
            'po_department': self.department_id.name,
            'po_description': self.description,
            'po_tracker_type': self.tracker_type,
            'po_status': self.status
        }

        return res

    def onchange_pricelist(self, cr, uid, ids, pricelist_id, context=None):
        if not pricelist_id:
            return {}
        return {'value': {'currency_id': self.pool.get('product.pricelist').browse(cr, uid, pricelist_id, context=context).currency_id.id}}

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        partner = self.pool.get('res.partner')
        if not partner_id:
            return {'value': {
                'fiscal_position': False,
                'payment_term_id': False,
                }}

        company_id = context.get('company_id') or self.pool.get('res.users')._get_company(cr, uid, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))

        supplier_address = partner.address_get(cr, uid, [partner_id], ['default'], context=context)
        supplier = partner.browse(cr, uid, partner_id, context=context)
        return {'value': {
            'pricelist_id': supplier.property_product_pricelist_purchase.id,
            'payment_term_id': supplier.property_supplier_payment_term.id or False,
            }}

    @api.multi
    @api.depends('refund_line')
    def _amount_all(self):

        line_obj = self.env['purchase.order.line']
        
        for order in self:
            val = val1 = 0.0
            cur = order.pricelist_id.currency_id

            for line in order.refund_line:
                val1 += line.price_subtotal

                line_price = abs(line_obj._calc_line_base_price(line))
                line_qty = abs(line_obj._calc_line_quantity(line))
                for c in line.taxes_id.compute_all(
                        line_price, line_qty,
                        line.product_id, order.partner_id)['taxes']:

                    val += c.get('amount', 0.0)

            order.amount_tax = -cur.round(val)
            order.amount_untaxed = cur.round(val1)
            order.amount_total = -(abs(cur.round(val)) + abs(cur.round(val1)))

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    @api.multi
    def action_refus(self):
        self.state = 'draft'

    @staticmethod
    def _get_object_reference(refund_to_receive):

        return "gle.refund.to.receive,{0}".format(refund_to_receive.id)

