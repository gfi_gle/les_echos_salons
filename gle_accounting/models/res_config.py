# -*- coding: utf-8 -*-

import time
import datetime
from dateutil.relativedelta import relativedelta

import openerp
from openerp import SUPERUSER_ID
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _

from openerp.osv import fields, osv
from openerp import models, fields, api
from openerp.addons.account.res_config import account_config_settings

import logging
_logger = logging.getLogger(__name__)
# _logger.debug("___________________________ pass ici first")






# def _check_account_gain_gle(self, cr, uid, ids, context=None):
#     _logger.debug("___________________________ pass ici first2")
#     return True
#
#
# def _check_account_loss_gle(self, cr, uid, ids, context=None):
#     return True
#
# _logger.debug("___________________________ pass ici3")
# account_config_settings._check_account_gain = _check_account_gain_gle
# account_config_settings._check_account_loss = _check_account_loss_gle
#



class res_company(models.Model):
    _inherit = 'res.company'

    date_day_test = fields.Date('Date aujourd\'hui')


class account_config_settings_gle(osv.osv_memory):
    _inherit = 'account.config.settings'

    def _check_account_gain(self, cr, uid, ids, context=None):
        _logger.debug("_____________________________________________________HIER")
        return True

    def _check_account_loss(self, cr, uid, ids, context=None):
        return True


    _constraints = [
        (_check_account_gain, 'The company of the gain exchange rate account must be the same than the company selected.',
         ['income_currency_exchange_account_id']),
        (_check_account_loss, 'The company of the loss exchange rate account must be the same than the company selected.',
         ['expense_currency_exchange_account_id']),
    ]

