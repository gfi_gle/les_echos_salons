# -*- coding: utf-8 -*-

from openerp import api
from openerp import models, fields, api
from openerp.exceptions import ValidationError

import logging
_logger = logging.getLogger(__name__)


class environ_account_install(models.Model):
    _inherit = 'account.config.settings'
    
    @api.multi
    def config_gle_init_account(self):
        module_compta_fr = self.env['ir.module.module'].search([['name','=','l10n_fr']])
#         module_compta = self.env['ir.module.module'].search([['name','=','account']])
        
#         module_compta.button_immediate_upgrade()
        
        if module_compta_fr.state == "installed":
            accounts = self.env['account.account'].search([])
            accounts.creation_comptes_comptable_le()
        else:
            raise ValidationError("Attention! Le plan comptable français n'est pas installé.")







        
        