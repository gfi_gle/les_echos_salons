# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
import openerp.addons.decimal_precision as dp
from openerp.exceptions import ValidationError, except_orm
import logging

_logger = logging.getLogger(__name__)


class RefundToReceiveLine(models.Model):
    """ Model representing quotation AAR line """

    _name = 'gle.refund.to.receive.line'
    _inherit = 'purchase.order.line'
    _table = 'gle_refund_to_receive_line'

    @api.model
    def _init_prestation_domain(self):
        """
            Method to init domain associated to attribute prestation_id,
            domain is generate in function of section associated to current user
        """

        user_id = self.env['res.users'].browse([self.env.context.get("uid")])

        if user_id.section_ids and 'ADV' not in user_id.section_ids.mapped('name'):
            return [('section_related_id', 'in', user_id.section_ids.ids)]
        else:
            return []

    move_id = fields.Many2one('account.move', string="Piece comptable provision")
    refund_id = fields.Many2one(comodel_name='gle.refund.to.receive', string="Devis AAR")

    discount_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', 'Absolue')], string="Type de remise")
    discount = fields.Float(string='Discount', digits_compute=dp.get_precision('Discount'))

    price_subtotal = fields.Float(compute='_amount_line', string="Sous-Total", digits_compute=dp.get_precision('Account'))

    salon_id = fields.Many2one(
        comodel_name='gle_salon.salon',
        string="Salon")

    gle_section_id = fields.Many2one(
        comodel_name='gle.section',
        string="Section",
        compute='compute_analytic',
        store=True)

    nature_id = fields.Many2one(
        comodel_name='gle.nature',
        string="Nature",
        compute='compute_analytic',
        store=True)

    type_id = fields.Many2one(
        comodel_name='gle.type',
        string="Type",
        compute='compute_analytic',
        store=True)

    prestation_id = fields.Many2one(comodel_name='gle.prestation',
                                    string="Prestation", )

    invoice_line_tax_id = fields.Many2many(
        comodel_name='account.tax',
        relation='gle_refund_to_receive_line_taxes_rel',
        column1='refund_line_id', column2='tax_id',
        string='Taxes',
        domain=[('parent_id', '=', False), '|', ('active', '=', False),
                ('active', '=', True)])

    order_id = fields.Many2one('purchase.order', 'Order Reference', select=True, required=False, ondelete='cascade')

    @api.depends('prestation_id')
    def compute_analytic(self):

        for refund_line in self:
            refund_line.type_id = refund_line.prestation_id.type_id.id
            refund_line.nature_id = refund_line.type_id.nature_id.id
            refund_line.gle_section_id = refund_line.nature_id.section_id.id
            refund_line.date_planned = datetime.datetime.now()

    @api.onchange('prestation_id')
    def onchange_prestation_id(self):
        domain_dict = {}

        if self.prestation_id and self.salon_id:

            # add domain to product_id field
            domain_dict.update({'domain': {'product_id': [('purchase_ok', '=', True),
                                                          ('property_account_expense', '!=', None),
                                                          ('prestation_id', '=', self.prestation_id.id)]
                                           }})

            self.product_id = self.env['product.product'].search([('purchase_ok', '=', True),
                                                                  ('property_account_expense', '!=', None),
                                                                  ('prestation_id', '=', self.prestation_id.id)])

        return domain_dict

    def empty_attributes_product(self, type_change=None):
        """ Method to empty product's attributes in po line """

        self.date_planned = None
        self.gle_section_id = None
        self.product_id = None
        self.name = None
        self.product_qty = None
        self.price_unit = None
        self.discount_type = None
        self.discount = None

        if type_change == 'salon':
            self.type_id = None
            self.nature_id = None
            self.gle_section_id = None
            self.prestation_id = None

    @api.onchange('product_id')
    def onchange_product_id(self):

        if not self._context.get('default_salon_id', False):
            raise except_orm(_('Pas de salon défini!'), _("Vous devez le définir avant d'ajouter une ligne!"))

        self.name = self.product_id.name
        self.price_unit = -self.product_id.standard_price
        self.taxes_id = self.product_id.supplier_taxes_id
        self.product_qty = -1

    @api.multi
    @api.model
    def _amount_line(self):
        res = {}

        for line in self:
            line_price = abs(line._calc_line_base_price(line))
            line_qty = abs(line._calc_line_quantity(line))
            taxes = line.taxes_id.compute_all(line_price,
                                              line_qty, line.product_id,
                                              line.order_id.partner_id)

            res[line.id] = line.refund_id.pricelist_id.currency_id.round(taxes['total'])

            line.price_subtotal = -res[line.id]

    @api.onchange('product_qty')
    def onchange_product_qty(self):

        if self.product_qty > 0:
            self.product_qty = -self.product_qty

    @api.onchange('price_unit')
    def onchange_price_unit(self):

        if self.price_unit > 0:
            self.price_unit = -self.price_unit

    @api.model
    def _calc_line_base_price(self, line):
        """ Override method to calculate the base price unit in function of discount_type and taxes_id """

        tax_id = line.get_taxes_include()
        return line.calc_base_price(tax_id=tax_id)

    def get_taxes_include(self):
        """ Return the value of the first tax associated to line """

        for taxe in self.taxes_id:
            return taxe

    @api.multi
    def calc_base_price(self, tax_id):

        price_unit = self.price_unit

        if self.discount_type:

            if not tax_id.price_include:
                if self.discount_type == 'percentage':
                    price_unit = self.price_unit * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit = self.price_unit * (
                        1 - (self.calc_percentage_with_amount(price_unit=self.price_unit) or 0.0) / 100)

            elif tax_id.price_include:

                result = tax_id.compute_all(
                    price_unit=self.price_unit,
                    quantity=self.product_qty,
                    product=self.product_id,
                    partner=self.partner_id, )

                price_unit_without_tax = result['taxes'][0].get('price_unit')

                if self.discount_type == 'percentage':
                    price_unit_with_discount = price_unit_without_tax * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit_with_discount = (price_unit_without_tax * (
                        1 - (self.calc_percentage_with_amount(price_unit=price_unit_without_tax) or 0.0) / 100))

                price_unit = price_unit_with_discount / (100 / (100 + tax_id.amount * 100))

        return price_unit

    def calc_percentage_with_amount(self, price_unit):

        amount = price_unit * self.product_qty

        return (self.discount / amount) * 100

    @api.onchange('discount', 'discount_type')
    def _on_change_discount(self):

        self.discount = abs(self.discount)
        if not self.check_amount_absolut_discount():
            self.discount = None
            res = {
                'warning': {
                    'title': _('Warning'),
                    'message': _(("La remise ne peut être supérieure au sous-total (HT)"))
                }
            }

            return res

        if not self.check_percentage_discount():
            self.discount = None
            res = {
                'warning': {
                    'title': _('Warning'),
                    'message': _(("Le pourcentage doit être comprit entre 0 et 100."))
                }
            }

            return res

    def check_amount_absolut_discount(self):

        tax_id = self.get_taxes_include()

        if self.discount_type == 'absolut':
            price_without_tax = (self.price_unit * (100 / ((tax_id.amount * 100) + 100)))

            if self.discount > (price_without_tax * self.product_qty):
                return False

        return True

    def check_percentage_discount(self):

        if self.discount_type == 'percentage':
            if not 0 < self.discount < 100 and self.discount:
                return False

        return True
