# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _

class Product(models.Model):
    _inherit = 'product.template'

    for_acompte = fields.Boolean('For Acompte', default=False)
    
class ProductCategory(models.Model):
    _inherit = "product.category"
    
    intercos = fields.Boolean('Interco', default=False)