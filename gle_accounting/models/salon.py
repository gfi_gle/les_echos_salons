# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
import calendar

import logging


_logger = logging.getLogger(__name__)


premier_jour_du_mois = datetime.datetime.strptime('2017-04-01', '%Y-%m-%d')
dernier_jour_du_mois = datetime.datetime.strptime('2017-04-30', '%Y-%m-%d')


class Salon(models.Model):
    _inherit = 'gle_salon.salon'

    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d')
        else:
            return datetime.datetime.now()

    def verif_dernier_jour_mois_salon(self, date_realisation):
        """
            Method de verification des dates pour FNP / FAE / AAE / AAR
            Si la date_now est supérieur ou égale au dernier jour du mois du salon et dans l'excercice fiscal
            la method génère des provision
        """

        date_now = self.get_validation_date().date()

        if date_realisation and date_now:
            date_realisation = datetime.datetime.strptime(date_realisation, '%Y-%m-%d').date()

            last_day_month_real = date_realisation.replace(day=calendar.monthrange(date_realisation.year, date_realisation.month)[1])
            last_day_month_now = date_now.replace(day=calendar.monthrange(date_now.year, date_now.month)[1])

            if last_day_month_real.year == date_now.year \
                    and date_now.month >= date_realisation.month \
                    and last_day_month_now.day == date_now.day:
                return True
            else:
                return False

    def verif_jour_realisation_salon(self, date_realisation):
        """
            Method de verification des dates pour EX_CCA / EX_PCA
            Si la date_now est supérieur ou égale à la date de réalisation et dans l'excercice fiscal du salon
            génère les extournes
        """

        date_now = self.get_validation_date().date()
        date_realisation = datetime.datetime.strptime(date_realisation, '%Y-%m-%d').date()

        if date_now.year == date_realisation.year and date_now >= date_realisation:
            return True
        else:
            return False

    # After the accounting period
    def verif_apres_cloture(self, date_b):
        if date_b and (self.get_validation_date().year > datetime.datetime.strptime(date_b, '%Y-%m-%d').date().year):
            return True
        else:
            return False

    # retourne un dict pret a etre envoye a create
    def get_move_line_dict(self, order_line, modele):
        if modele == 'aar':
            modele = 'fnp'
            tag = 'new_aar'
        elif modele == 'aar_echange':
            modele = 'fnp_echange'
            tag = 'new_aar_echange'
        elif modele == 'aae':
            modele = 'fae'
            tag = 'new_aae'
        elif modele == 'aae_echange':
            modele = 'fae_echange'
            tag = 'new_aae_echange'
        else:
            tag = "new_" + modele
        vals = {
            'tag': tag,
            'name': order_line.name.split('\n')[0][:64],
            'product_id': order_line.product_id.id,
            'quantity': 1,
        }

        code_interco = u''

        if isinstance(order_line, self.env['sale.order.line'].__class__) \
                or isinstance(order_line, self.env['purchase.order.line'].__class__):
            if order_line.order_id.partner_id.intercos:
                code_interco = order_line.order_id.partner_id.code_intercos
            else:
                code_interco = "9Z99"

        elif isinstance(order_line, self.env['gle.account.refund.line'].__class__) \
                or isinstance(order_line, self.env['gle.refund.to.receive.line'].__class__):
            if order_line.refund_id.partner_id.intercos:
                code_interco = order_line.refund_id.partner_id.code_intercos
            else:
                code_interco = "9Z99"

        if isinstance(order_line, self.env['sale.order.line'].__class__):
            vals.update({'debit': 0,
                         'credit': order_line.price_subtotal,
                         'account_id': order_line.product_id.property_account_income.id if order_line.product_id.property_account_income else order_line.product_id.categ_id.property_account_income_categ.id,
                         'partner_id': order_line.order_id.partner_id.id,

                         # analytic for compte 6 and 7
                         'projet_id': order_line.order_id.salon_id.millesime.id,
                         'produit_id': order_line.order_id.salon_id.name.id,
                         'section_id': order_line.order_id.gle_section_id.id,
                         'nature_id': order_line.product_id.product_tmpl_id.categ_id.nature_comptable_id.id,
                         'code_interco': code_interco,
                         'date_origin_document': self.create_date,

                         })

        elif isinstance(order_line, self.env['purchase.order.line'].__class__):
            vals.update({'credit': 0,
                         'debit': order_line.price_subtotal,
                         'account_id': order_line.type_id.account_id.id,
                         'partner_id': order_line.order_id.partner_id.id,

                         # analytic for compte 6 and 7
                         'projet_id': order_line.salon_id.millesime.id,
                         'produit_id': order_line.salon_id.name.id,
                         'section_id': order_line.gle_section_id.id,
                         'nature_id': order_line.nature_id.nature_comptable_id.id,
                         'code_interco': code_interco,
                         'date_origin_document': self.create_date,

                         })

        elif isinstance(order_line, self.env['gle.account.refund.line'].__class__):
            vals.update({'credit': 0,
                         'debit': order_line.price_subtotal,
                         'account_id': order_line.account_id.id,
                         'partner_id': order_line.refund_id.partner_id.id,

                         # analytic for compte 6 and 7
                         'projet_id': order_line.refund_id.salon_id.millesime.id,
                         'produit_id': order_line.refund_id.salon_id.name.id,
                         'section_id': order_line.refund_id.gle_section_id.id,
                         'nature_id': order_line.product_id.product_tmpl_id.categ_id.nature_comptable_id.id,
                         'code_interco': code_interco,
                         'date_origin_document': self.create_date,

                         })

        # add case refund receive
        elif isinstance(order_line, self.env['gle.refund.to.receive.line'].__class__):
            vals.update({'credit': 0,
                         'debit': order_line.price_subtotal,
                         'account_id': order_line.product_id.property_account_expense.id if order_line.product_id.property_account_expense else order_line.product_id.categ_id.property_account_expense_categ.id,
                         'partner_id': order_line.refund_id.partner_id.id,

                         # analytic for compte 6 and 7
                         'projet_id': order_line.salon_id.millesime.id,
                         'produit_id': order_line.salon_id.name.id,
                         'section_id': order_line.gle_section_id.id,
                         'nature_id': order_line.nature_id.nature_comptable_id.id,
                         'code_interco': code_interco,
                         'date_origin_document': self.create_date,

                         })
        else:
            if order_line.refund_id.type == "client":
                vals.update({'credit': 0,
                             'debit': order_line.price_subtotal,
                             'account_id': order_line.product_id.property_account_expense.id if order_line.product_id.property_account_expense else order_line.product_id.categ_id.property_account_expense_categ.id,
                             'journal_id': order_line.refund_id.journal_id.id,
                             'partner_id': order_line.refund_id.partner_id.id,

                             # analytic for compte 6 and 7
                             'projet_id': order_line.refund_id.salon_id.millesime.id,
                             'produit_id': order_line.refund_id.salon_id.name.id,
                             'section_id': order_line.refund_id.gle_section_id.id,
                             'nature_id': order_line.product_id.product_tmpl_id.categ_id.nature_comptable_id.id,
                             'code_interco': code_interco,
                             'date_origin_document': self.create_date,
                             })
            else:
                vals.update({'credit': order_line.price_subtotal,
                             'debit': 0,
                             'account_id': order_line.product_id.property_account_expense.id if order_line.product_id.property_account_expense else order_line.product_id.categ_id.property_account_expense_categ.id,
                             'journal_id': order_line.refund_id.journal_id.id,
                             'partner_id': order_line.refund_id.partner_id.id,

                             # analytic for compte 6 and 7
                             'projet_id': order_line.salon_id.millesime.id,
                             'produit_id': order_line.salon_id.name.id,
                             'section_id': order_line.gle_section_id.id,
                             'nature_id': order_line.nature_id.nature_comptable_id.id,
                             'code_interco': code_interco,
                             'date_origin_document': self.create_date,
                             })
        return vals

    def get_name_provision(self):
        move_obj = self.env['account.move']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code

        journal_aae_id = get_journal_provision_id_by_code('aae')
        journal_aar_id = get_journal_provision_id_by_code('aar')
        journal_pca_id = get_journal_provision_id_by_code('pca')
        journal_cca_id = get_journal_provision_id_by_code('cca')
        journal_fae_id = get_journal_provision_id_by_code('fae')
        journal_fnp_id = get_journal_provision_id_by_code('fnp')

        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        journal_reprise_aar_id = get_journal_provision_id_by_code('reprise_aar')
        journal_reprise_pca_id = get_journal_provision_id_by_code('reprise_pca')
        journal_reprise_cca_id = get_journal_provision_id_by_code('reprise_cca')
        journal_reprise_fae_id = get_journal_provision_id_by_code('reprise_fae')
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        account_move_obj = self.env['account.move']

        aar_move_ids = move_obj.search([('journal_id', '=', journal_aar_id)])
        reprise_aar_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_aar_id)])

        aae_move_ids = move_obj.search([('journal_id', '=', journal_aae_id)])
        reprise_aae_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_aae_id)])

        pca_move_ids = move_obj.search([('journal_id', '=', journal_pca_id)])
        reprise_pca_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_pca_id)])

        cca_move_ids = move_obj.search([('journal_id', '=', journal_cca_id)])
        reprise_cca_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_cca_id)])

        fae_move_ids = move_obj.search([('journal_id', '=', journal_fae_id)])
        reprise_fae_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_fae_id)])

        fnp_move_ids = move_obj.search([('journal_id', '=', journal_fnp_id)])
        reprise_fnp_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_fnp_id)])

        account_invoice_count = sum(
            [len(aae_move_ids), len(reprise_aae_move_ids),
             len(aar_move_ids), len(reprise_aar_move_ids),
             len(pca_move_ids), len(reprise_pca_move_ids),
             len(cca_move_ids), len(reprise_cca_move_ids),
             len(fae_move_ids), len(reprise_fae_move_ids),
             len(fnp_move_ids), len(reprise_fnp_move_ids)])
        account_invoice_count += 1

        chrono_projet_str = "000"

        if account_invoice_count == 0:
            account_invoice_str = "0001"
        else:
            account_invoice_str = "0000{0}".format(account_invoice_count)

        return "P{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

    @api.model
    def get_salons_no_invoices(self):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_reprise_pca_id = get_journal_provision_id_by_code('reprise_pca')
        journal_reprise_cca_id = get_journal_provision_id_by_code('reprise_cca')
        journal_fnp_id = get_journal_provision_id_by_code('fnp')
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        journal_fae_id = get_journal_provision_id_by_code('fae')
        journal_reprise_fae_id = get_journal_provision_id_by_code('reprise_fae')
        journal_aar_id = get_journal_provision_id_by_code('aar')
        journal_reprise_aar_id = get_journal_provision_id_by_code('reprise_aar')
        journal_aae_id = get_journal_provision_id_by_code('aae')
        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        print "______________CRON"
        account_move_line_obj = self.env['account.move.line']
        salons = self.search([])
        for salon in salons:
            # after accouting period
            date_now = self.get_validation_date()
            last_day_generate_prov = date_now.replace(
                day=calendar.monthrange(year=date_now.year, month=date_now.month)[1])
            period_pro_id = self.env['account.period'].find(dt=last_day_generate_prov).id or False

            if self.verif_apres_cloture(salon.date_realisation):
                ################################################################### FNP PURCHASE ORDER AFTER ACCOUNTING PERIOD ##########################################################
                move_order_app_pss_lines = self.env['purchase.order.line'].search([
                    ('move_id', '!=', False),
                    ('move_id.extourne', '=', False),
                    ('order_id.state', '=', 'passage_sans_suite'),
                    ('salon_id', '=', salon.id)
                ]).filtered(lambda r: r.order_id.type_facturation != 'intercos').mapped('move_id')
                for move in move_order_app_pss_lines:
                    purchase_order_app_line_pss = self.env['purchase.order.line'].search([('move_id', '=', move.id)])[0]
                    pss = purchase_order_app_line_pss.order_id
                    name_pov = self.get_name_provision()
                    date_pro_purchase_order_app_pss = last_day_generate_prov
                    ref_pro_order_pss = 'extourne FNP' + ' ' + pss.name or ''
                    vals = move._get_move_extourne(name=name_pov, journal_id=journal_reprise_fnp_id,
                                                   ref=ref_pro_order_pss,
                                                   salon_id=salon.id, dt=date_pro_purchase_order_app_pss)

                    move_ex_order_line_pss = move.create(vals)
                    move_ex_order_line_pss.extourne = True
                    move.extourne = True
                    move_ex_order_line_pss.button_validate()

                    ################################################################### AAR ORDER AFTER ACCOUNTING PERIOD ##########################################################
                move_aar_app_pss_lines = self.env['gle.refund.to.receive.line'].search(
                    [('move_id', '!=', False),
                     ('move_id.extourne', '=', False),
                     ('refund_id.state', '=', 'cancel'),
                     ('salon_id', '=', salon.id)]).filtered(
                    lambda r: r.refund_id.type_facturation != 'intercos').mapped('move_id')
                for move in move_aar_app_pss_lines:
                    aar_app_line_pss = self.env['gle.refund.to.receive.line'].search([('move_id', '=', move.id)])[0]
                    arr_pss = aar_app_line_pss.refund_id
                    name_pov = self.get_name_provision()
                    date_pro_aar_pss = last_day_generate_prov
                    ref_pro_order_pss = 'extourne AAR' + ' ' + arr_pss.name or ''
                    vals = move._get_move_extourne(name=name_pov, journal_id=journal_reprise_aar_id,
                                                   ref=ref_pro_order_pss,
                                                   salon_id=salon.id, dt=date_pro_aar_pss)

                    move_ex_aar_line_pss = move.create(vals)
                    move_ex_aar_line_pss.extourne = True
                    move.extourne = True
                    move_ex_aar_line_pss.button_validate()

                ############################################### FAE SALE ORDER  PSS######################################################
                sale_orders_pss = self.env['sale.order'].search(
                    [('move_id', '!=', False), ('move_id.extourne', '=', False), ('state', '=', 'passage_sans_suite'),
                     ('salon_id.id', '=', salon.id)])
                for pss in sale_orders_pss:
                    list_move_lines = []
                    data = []
                    for line in pss.move_id.line_id:
                        vals = line.get_account_move_line_to_cancel('ex_pss')

                        # analytic for compte 6 and 7
                        vals['projet_id'] = line.projet_id.id or u''
                        vals['produit_id'] = line.produit_id.id or u''
                        vals['section_id'] = line.section_id.id or u''
                        vals['nature_id'] = line.nature_id.id or u''
                        vals['code_interco'] = line.code_interco or u''
                        vals['date_origin_document'] = line.date_origin_document
                        # vals['tax_id'] = line.tax_id.id or u''

                        list_move_lines.append((0, 0, vals))

                    account_move = self.env['account.move'].create({
                        'journal_id': journal_reprise_fae_id,
                        'period_id': period_pro_id or account_move_line_obj.get_periode(),
                        'salon_id': pss.salon_id.id,
                        'name': self.get_name_provision(),
                        'ref': 'extourne FAE' + ' ' + pss.name,
                        'line_id': list_move_lines,
                        'date': last_day_generate_prov,
                    })
                    account_move.button_validate()
                    pss.move_id.extourne = True
                ############################################### AAE SALE ORDER  PSS######################################################
                aar_orders_pss = self.env['gle.account.refund'].search(
                    [('move_id', '!=', False), ('move_id.extourne', '=', False), ('state', '=', 'cancel'),
                     ('salon_id.id', '=', salon.id)])

                for pss in aar_orders_pss:
                    print pss.name
                    list_move_lines = []
                    data = []
                    for line in pss.move_id.line_id:
                        vals = line.get_account_move_line_to_cancel('ex_pss')

                        # analytic for compte 6 and 7
                        vals['projet_id'] = line.projet_id.id or u''
                        vals['produit_id'] = line.produit_id.id or u''
                        vals['section_id'] = line.section_id.id or u''
                        vals['nature_id'] = line.nature_id.id or u''
                        vals['code_interco'] = line.code_interco or u''
                        vals['date_origin_document'] = line.date_origin_document
                        # vals['tax_id'] = line.tax_id.id or u''

                        list_move_lines.append((0, 0, vals))

                    account_move = self.env['account.move'].create({
                        'journal_id': journal_aae_id,
                        'period_id': period_pro_id or account_move_line_obj.get_periode(),
                        'salon_id': pss.salon_id.id,
                        'name': self.get_name_provision(),
                        'ref': 'extourne AAE' + ' ' + pss.name,
                        'line_id': list_move_lines,
                        'date': last_day_generate_prov,
                    })
                    account_move.button_validate()
                    pss.move_id.extourne = True



            if self.verif_jour_realisation_salon(salon.date_realisation):
                print "EX_CCA/ EX_CCA_ECHANGE/ EX_PCA/ EX_PCA_ECHANGE"
                date_ex_pca_cca = salon.date_realisation
                period_ex_pca_cca = self.env['account.period'].find(
                    dt=date_ex_pca_cca).id or account_move_line_obj.get_periode()
                # invoices = self.env['account.invoice'].search([
                #     ('state', 'in', ['open', 'paid']),
                #     ('salon_id', '=', salon.id),
                #     '|', ('type_facturation', '=', False),
                #     ('type_facturation', '!=', 'intercos'),
                # ])

                invoices = self.env['account.invoice'].search([
                    ('type', 'in', ['out_invoice', 'out_refund']),
                    ('state', 'in', ['open', 'paid']),
                    ('salon_id', '=', salon.id),
                    '|', ('type_facturation', '=', False),
                    ('type_facturation', '!=', 'intercos'),
                ])
                #################################################   CCA    ################################################
                moves_origines_cca = self.env['account.invoice.line'].search([
                    ('invoice_id.type', 'in', ['in_invoice', 'in_refund']),
                    ('invoice_id.state', 'in', ['open', 'paid']),
                    ('salon_id', '=', salon.id),
                    '|', ('invoice_id.type_facturation', '=', False),
                    ('invoice_id.type_facturation', '!=', 'intercos')
                ]).filtered(lambda line: not line.move_prov_origin_id.extourne).mapped('move_prov_origin_id')
                for move in moves_origines_cca:
                    inv_line_id = self.env['account.invoice.line'].search([('move_prov_origin_id', '=', move.id)])[0]
                    _invoice = inv_line_id.invoice_id or ''
                    number = _invoice.number or ''
                    ref = 'extourne ' + 'CCA' + ' ' + number or ''
                    name_pov = _invoice.get_name_provision()
                    journal_provision_id = journal_reprise_cca_id
                    if _invoice.type_facturation in ['r_echange', 'm_echange']:
                        tag = "new_cca_echange"
                        tag_inverse = 'ex_cca_echange'
                    else:
                        tag = "new_cca"
                        tag_inverse = 'ex_cca'
                    date_ex_cca = salon.date_realisation
                    period_ex_cca = self.env['account.period'].find(
                        dt=date_ex_cca).id or account_move_line_obj.get_periode()

                    vals = move._get_move_extourne(name=name_pov, journal_id=journal_provision_id, ref=ref,
                                                   salon_id=salon.id, dt=date_ex_cca)
                    # reconciliation_marker

                    move_ex = move.create(vals)
                    move_ex.extourne = True
                    move.extourne = True
                    move_ex.button_validate()
                    ### PCA ###
                    #################################################   PCA    ################################################
                for inv in invoices:
                    if inv.move_ex_id and not inv.move_ex_id.extourne:
                        # extourne DES ECRITURES COMPTABLES PCA et CCA, ET ANNULATION DES ECRITURES COMPTABLES DANS LES COMPTES PRODUITS
                        list_move_lines = []

                        ref = 'PCA'
                        date_ex_pca = salon.date_realisation
                        period_ex_pca = self.env['account.period'].find(
                            dt=date_ex_pca).id or account_move_line_obj.get_periode()
                        journal_provision_id = journal_reprise_pca_id
                        name_pov = inv.get_name_provision()
                        journal_provision_id = journal_reprise_pca_id
                        if inv.type_facturation in ['r_echange', 'm_echange']:
                            tag = "new_pca_echange"
                            tag_inverse = 'ex_pca_echange'
                        else:
                            tag = "new_pca"
                            tag_inverse = 'ex_pca'
                        vals_move = {
                            'journal_id': journal_provision_id,
                            'period_id': period_ex_pca,
                            'date': date_ex_pca,
                            'salon_id': inv.salon_id.id,
                            'name': name_pov,
                            'ref': 'extourne ' + ref + ' ' + inv.number or ''
                        }
                        move_ex = self.env['account.move'].create(vals_move)
                        for line in inv.move_ex_id.line_id:

                            if line.account_id.code.startswith('44'):
                                continue

                            vals = line.get_account_move_line_to_cancel(tag_inverse)
                            if line.account_id.code.startswith('4'):
                                provision_move_id = self.env['account.move'].search([('parent_move_id', '=', inv.move_id.id)])
                                vals['reconciliation_marker'] = provision_move_id.internal_sequence_number

                            vals['move_id'] = move_ex.id
                            # analytic for compte 6 and 7
                            vals['projet_id'] = line.projet_id.id
                            vals['produit_id'] = line.produit_id.id
                            vals['section_id'] = line.section_id.id
                            vals['nature_id'] = line.nature_id.id
                            vals['code_interco'] = line.code_interco
                            vals['date_origin_document'] = line.date_origin_document
                            # vals['tax_id'] = line.tax_id.id

                            list_move_lines.append((0, 0, vals))
                        move_ex.write({'line_id': list_move_lines, 'extourne': True})
                        inv.move_id.extourne = True
                        inv.move_ex_id.extourne = True
                        if not list_move_lines:
                            move_ex.unlink()
                        else:
                            move_ex.button_validate()

            if self.verif_dernier_jour_mois_salon(salon.date_realisation):
                try:
                    print "FAE/ FAE_ECHANGE/ FNP/ FNP_ECHANGE / AAE/ AAE_ECHANGE/ AAR/ AAR_ECHANGE"

                    purchase_orders_pss = self.env['purchase.order'].search(
                        [('move_id', '!=', False), ('move_id.extourne', '=', False),
                         ('state', '=', 'passage_sans_suite'), ('salon_id.id', '=', salon.id)])


                    purchase_orders_aar = self.env['gle.refund.to.receive'].search(
                        [('move_id', '!=', False), ('state', '=', 'cancel'), ('move_id.extourne', '=', False),
                         ('salon_id.id', '=', salon.id)])

                    date_now = self.get_validation_date()
                    last_day_generate_prov = date_now.replace(day=calendar.monthrange(year=date_now.year, month=date_now.month)[1])
                    period_pro_id = self.env['account.period'].find(dt=last_day_generate_prov).id or False
                    ################################################################### SANS SUITS ##########################################################
                    ################################################################### FNP PURCHASE ORDER ##########################################################
                    move_order_pss_lines = self.env['purchase.order.line'].search([
                        ('move_id', '!=', False),
                        ('move_id.extourne', '=', False),
                        ('order_id.state', '=', 'passage_sans_suite'),
                        ('salon_id', '=', salon.id)
                    ]).filtered(lambda r: r.order_id.type_facturation != 'intercos').mapped('move_id')

                    for move in move_order_pss_lines:
                        purchase_order_line_pss = self.env['purchase.order.line'].search([('move_id', '=', move.id)])[0]
                        pss = purchase_order_line_pss.order_id
                        name_pov = self.get_name_provision()
                        date_pro_purchase_order_pss = last_day_generate_prov
                        ref_pro_order_pss = 'extourne FNP' + ' ' + pss.name or ''
                        vals = move._get_move_extourne(name=name_pov, journal_id=journal_reprise_fnp_id,
                                                       ref=ref_pro_order_pss,
                                                       salon_id=salon.id, dt=date_pro_purchase_order_pss)

                        move_ex_order_line_pss = move.create(vals)
                        move_ex_order_line_pss.extourne = True
                        move.extourne = True
                        move_ex_order_line_pss.button_validate()


                    ################################################################### SANS SUITS ##########################################################
                    ###################################################################### AAR ##############################################################
                    move_aar_pss_lines = self.env['gle.refund.to.receive.line'].search(
                        [('move_id', '!=', False),
                         ('move_id.extourne', '=', False),
                         ('refund_id.state', '=', 'cancel'),
                         ('salon_id', '=', salon.id)]).filtered(
                        lambda r: r.refund_id.type_facturation != 'intercos').mapped('move_id')
                    for move in move_aar_pss_lines:
                        aar_line_pss = self.env['gle.refund.to.receive.line'].search([('move_id', '=', move.id)])[0]
                        arr_pss = aar_line_pss.refund_id
                        name_pov = self.get_name_provision()
                        date_pro_aar_pss = last_day_generate_prov
                        ref_pro_order_pss = 'extourne AAR' + ' ' + arr_pss.name or ''
                        vals = move._get_move_extourne(name=name_pov, journal_id=journal_reprise_aar_id,
                                                       ref=ref_pro_order_pss,
                                                       salon_id=salon.id, dt=date_pro_aar_pss)

                        move_ex_aar_line_pss = move.create(vals)
                        move_ex_aar_line_pss.extourne = True
                        move.extourne = True
                        move_ex_aar_line_pss.button_validate()


                    ################################################################### SANS SUITS ##########################################################
                    ###################################################################### FAE ##############################################################
                    sale_orders_pss = self.env['sale.order'].search(
                        [('move_id', '!=', False), ('move_id.extourne', '=', False),
                         ('state', 'in', ['passage_sans_suite', 'cancel']), ('salon_id.id', '=', salon.id)])
                    # SALE ORDER SANS SUITE
                    for pss in sale_orders_pss:
                        list_move_lines = []
                        data = []
                        for line in pss.move_id.line_id:
                            vals = line.get_account_move_line_to_cancel('ex_pss')
                            # analytic for compte 6 and 7
                            vals['projet_id'] = line.projet_id.id
                            vals['produit_id'] = line.produit_id.id
                            vals['section_id'] = line.section_id.id
                            vals['nature_id'] = line.nature_id.id
                            vals['code_interco'] = line.code_interco
                            vals['date_origin_document'] = line.date_origin_document
                            # vals['tax_id'] = line.tax_id.id

                            list_move_lines.append((0, 0, vals))

                        account_move = self.env['account.move'].create({
                            'journal_id': journal_reprise_fae_id,
                            'period_id': period_pro_id or account_move_line_obj.get_periode(),
                            'salon_id': pss.salon_id.id,
                            'name': self.get_name_provision(),
                            'ref': 'extourne FAE' + ' ' + pss.name,
                            'line_id': list_move_lines,
                            'date': last_day_generate_prov,
                        })
                        account_move.button_validate()
                        pss.move_id.extourne = True

                    sale_orders = self.env['sale.order'].search(
                        [('move_id', '=', False), ('state', 'in', ['valid', 'valid_d', 'progress', 'manual', 'done']),
                         ('salon_id.id', '=', salon.id)])
                    sale_orders = sale_orders.filtered(lambda r: r.type_facturation != 'intercos')

                    purchase_orders = self.env['purchase.order'].search(
                        [('move_id', '=', False),
                         ('state', 'in', ['validated_bid', 'validated', 'confirmed', 'approved', 'done']),
                         ('salon_id.id', '=', salon.id)])
                    purchase_orders = purchase_orders.filtered(lambda r: r.type_facturation != 'intercos')

                    ############################################### AAE SALE ORDER  PSS######################################################
                    aar_orders_pss = self.env['gle.account.refund'].search(
                        [('move_id', '!=', False), ('move_id.extourne', '=', False), ('state', '=', 'cancel'),
                         ('salon_id.id', '=', salon.id)])

                    for pss in aar_orders_pss:
                        print pss.name
                        list_move_lines = []
                        data = []
                        for line in pss.move_id.line_id:
                            vals = line.get_account_move_line_to_cancel('ex_pss')

                            # analytic for compte 6 and 7
                            vals['projet_id'] = line.projet_id.id or u''
                            vals['produit_id'] = line.produit_id.id or u''
                            vals['section_id'] = line.section_id.id or u''
                            vals['nature_id'] = line.nature_id.id or u''
                            vals['code_interco'] = line.code_interco or u''
                            vals['date_origin_document'] = line.date_origin_document
                            # vals['tax_id'] = line.tax_id.id or u''

                            list_move_lines.append((0, 0, vals))
                        account_move = self.env['account.move'].create({
                            'journal_id': journal_aae_id,
                            'period_id': period_pro_id or account_move_line_obj.get_periode(),
                            'salon_id': pss.salon_id.id,
                            'name': self.get_name_provision(),
                            'ref': 'extourne AAE' + ' ' + pss.name,
                            'line_id': list_move_lines,
                            'date': last_day_generate_prov,
                        })
                        account_move.button_validate()
                        pss.move_id.extourne = True
                    ########################################################################################################################################
                    ###################################################################### FNP ##############################################################
                    purchase_orders_lines = self.env['purchase.order.line'].search([
                        ('move_id', '=', False),
                        ('order_id.state', 'in', ['validated_bid', 'validated', 'confirmed', 'approved', 'done']),
                        ('salon_id', '=', salon.id)
                    ])
                    purchase_orders_lines = purchase_orders_lines.filtered(
                        lambda r: r.order_id.type_facturation != 'intercos')
                    purchases_orders = purchase_orders_lines.mapped('order_id')

                    for order in purchases_orders:
                        invoice_id = self.env['account.invoice'].search(
                            [('origin', 'like', order.name), ('state', 'in', ('open', 'paid'))])
                        # import pdb; pdb.set_trace()
                        if not invoice_id:
                            lines_order = purchase_orders_lines.filtered(lambda r: r.order_id == order)
                            if not lines_order:
                                continue
                            account_move = self.env['account.move'].create({
                                'date': last_day_generate_prov,
                                'journal_id': journal_fnp_id,
                                'period_id': period_pro_id or account_move_line_obj.get_periode(),
                                'name': self.get_name_provision(),
                                'ref': 'FNP' + ' ' + order.name,
                                'salon_id': salon.id,
                            })
                            list_move_line_dict = []

                            for po in lines_order:
                                if po.order_id.type_facturation in ['r_echange', 'm_echange']:
                                    modele = "fnp_echange"
                                else:
                                    modele = "fnp"
                                po.write({'move_id': account_move.id})
                                vals2 = self.get_move_line_dict(po, modele)
                                if vals2['debit'] < 0 or vals2['credit'] < 0:
                                    vals2['credit'], vals2['debit'] = abs(vals2['debit']), abs(vals2['credit'])
                                list_move_line_dict.append((0, 0, vals2))
                            vals = account_move_line_obj.get_additional_move_line_dict_from_order(modele="fnp",
                                                                                                  order=order,
                                                                                                  line_order=lines_order)
                            if vals['debit'] < 0 or vals['credit'] < 0:
                                vals['credit'], vals['debit'] = abs(vals['debit']), abs(vals['credit'])
                            list_move_line_dict.append((0, 0, vals))
                            # account_move.update({'salon_id': po.salon_id.id})
                            account_move.write({'line_id': list_move_line_dict})
                            account_move.button_validate()

                    ####### FAE ########
                    for so in sale_orders:

                        if so.amount_total < 0:
                            continue

                        if so.type_facturation in ['r_echange', 'm_echange']:
                            modele = "fae_echange"
                        else:
                            modele = "fae"
                        # RECHERCHE DE FACTURES ASSOCIEES AU BON DE COMMANDE DE VENTE EN COURS
                        invoice_id = self.env['account.invoice'].search(
                            [('origin', 'like', so.name), ('state', 'in', ('open', 'paid'))])
                        # SI PAS DE FACTURES VALIDEES, CREATION D'UNE ECRITURE COMPTABLE DANS LE COMPTE FAE
                        if not invoice_id:
                            # if so.amount_total < 0:
                            #     continue

                            vals = account_move_line_obj.get_additional_move_line_dict_from_order(so, modele)
                            vals['date_origin_document'] = so.create_date

                            if vals['debit'] < 0 or vals['credit'] < 0:
                                vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                            list_move_line_dict = [(0, 0, vals)]

                            for line in so.order_line:
                                vals = self.get_move_line_dict(line, modele)

                                if vals['debit'] < 0 or vals['credit'] < 0:
                                    vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])

                                list_move_line_dict.append((0, 0, vals))

                            account_move = self.env['account.move'].create({
                                'date': last_day_generate_prov,
                                'journal_id': journal_fae_id,
                                'period_id': period_pro_id or account_move_line_obj.get_periode(),
                                'line_id': list_move_line_dict,
                                'salon_id': so.salon_id.id,
                                'name': self.get_name_provision(),
                                'ref': 'FAE' + ' ' + so.name,
                                'object_reference': self.env['sale.order']._get_object_reference(so)
                            })

                            so.write({'move_id': account_move.id})
                            # VALIDER LA PIECE COMPTABLE
                            account_move.button_validate()

                    devis_refund_orders = self.env['gle.account.refund'].search(
                        [('move_id', '=', False), ('state', 'in', ['a_facturer', 'wait_cg', 'open', 'done']),
                         ('salon_id', '=', salon.id)])

                    purchase_orders_aar_lines = self.env['gle.refund.to.receive.line'].search(
                        [('move_id', '=', False), ('refund_id.state', '=', 'confirm'), ('salon_id', '=', salon.id)])
                    purchases_orders_aar = purchase_orders_aar_lines.mapped('refund_id')

                    ####### AAR #######
                    # GLE REFUND TO RECEIVE ( BON DE COMMANDE AAR)
                    for aar in purchases_orders_aar:
                        invoice_id = self.env['account.invoice'].search(
                            [('origin', 'like', aar.name), ('state', 'in', ('open', 'paid', 'cancel'))])

                        if aar.name:
                            name_aar = aar.name
                        else:
                            name_aar = ""
                        modele = "aar"
                        if aar.type_facturation in ['r_echange', 'm_echange']:
                            modele = "aar_echange"
                        lines_order = purchase_orders_aar_lines.filtered(lambda r: r.refund_id == aar)
                        if not lines_order:
                            continue

                        if not invoice_id:
                            vals = {
                                'date': last_day_generate_prov,
                                'journal_id': journal_aar_id,
                                'period_id': period_pro_id or account_move_line_obj.get_periode(),
                                'salon_id': salon.id,
                                'name': self.get_name_provision(),
                                'ref': 'AAR' + ' ' + name_aar,
                            }
                            account_move = self.env['account.move'].create(vals)
                            list_move_line_dict = []
                            for line in lines_order:
                                line.write({'move_id': account_move.id})
                                vals2 = self.get_move_line_dict(line, modele)
                                vals2['credit'] = abs(vals2['credit'])
                                vals2['debit'] = abs(vals2['debit'])
                                list_move_line_dict.append((0, 0, vals2))

                            vals = account_move_line_obj.get_additional_move_line_dict_from_devis(devis=aar,
                                                                                                  modele="aar",
                                                                                                  line_order=lines_order)
                            vals['credit'], vals['debit'] = abs(vals['debit']), abs(vals['credit'])
                            list_move_line_dict.append((0, 0, vals))
                            account_move.write({'line_id': list_move_line_dict})

                            account_move.button_validate()

                    # GLE ACCOUNT REFUND (DEVIS AAE)
                    for dro in devis_refund_orders:

                        invoice_id = self.env['account.invoice'].search(
                            [('origin', 'like', dro.invoice_origin_id.number), ('state', 'in', ('open', 'paid'))])

                        if dro.type == 'client':
                            modele = "aae"
                            if dro.type_facturation in ['r_echange', 'm_echange']:
                                modele = "aae_echange"
                                modele = "aae"

                        elif dro.type == 'fournisseur':
                            modele = "aar"
                            if dro.type_facturation in ['r_echange', 'm_echange']:
                                modele = "aar_echange"
                            else:
                                modele = "aar"
                        if not invoice_id:
                            if dro.amount_total < 0:
                                # print dro.amount
                                vals = account_move_line_obj.get_additional_move_line_dict_from_devis(dro, modele)
                                vals['credit'] = abs(vals['credit'])
                                vals['debit'] = abs(vals['debit'])

                                list_move_line_dict = [(0, 0, vals)]
                                for line in dro.refund_line_ids:
                                    vals2 = self.get_move_line_dict(line, modele)
                                    vals2['debit'], vals2['credit'] = -vals2['credit'], -vals2['debit']
                                    if vals2['credit'] < 0 or vals2['debit'] < 0:
                                        vals2['debit'], vals2['credit'] = abs(vals2['debit']), abs(vals2['credit'])
                                    else:
                                        vals2['credit'], vals2['debit'] = abs(vals2['debit']), abs(vals2['credit'])
                                    list_move_line_dict.append((0, 0, vals2))
                            else:
                                vals = account_move_line_obj.get_additional_move_line_dict_from_devis(dro, modele)
                                # vals['credit'] = abs(vals['debit'])
                                # vals['debit'] = abs(vals['credit'])
                                vals['debit'], vals['credit'] = vals['credit'], vals['debit']
                                list_move_line_dict = [(0, 0, vals)]
                                for line in dro.refund_line_ids:
                                    vals2 = self.get_move_line_dict(line, modele)
                                    vals2['debit'], vals2['credit'] = -vals2['credit'], -vals2['debit']
                                    if vals2['credit'] < 0 or vals2['debit'] < 0:
                                        vals2['debit'], vals2['credit'] = abs(vals2['debit']), abs(vals2['credit'])
                                    else:
                                        vals2['credit'], vals2['debit'] = abs(vals2['debit']), abs(vals2['credit'])
                                    list_move_line_dict.append((0, 0, vals2))

                                # attribute value if partner id interco
                                if dro.partner_id.intercos:
                                    code_interco = so.partner_id.code_intercos
                                else:
                                    code_interco = "9Z99"


                            account_move = self.env['account.move'].create({
                                'date': last_day_generate_prov,
                                'journal_id': journal_aae_id,
                                'period_id': account_move_line_obj.get_periode(),
                                'line_id': list_move_line_dict,
                                'salon_id': dro.salon_id.id,
                                'name': self.get_name_provision(),
                                'ref': 'AAE' + ' ' + dro.invoice_origin_id.number,
                                'object_reference': self.env['gle.account.refund']._get_object_reference(dro)
                            })
                            dro.write({'move_id': account_move.id})

                            # VALIDER LA PIECE COMPTABLE
                            account_move.button_validate()

                except ValueError as e:
                    _logger.debug('----> EXCEPTION')
                    _logger.debug(e)

    def dro_has_problem(self, dro):
        """Check for errors refound"""
        negativ = dro.refund_line_ids.filtered(lambda r: r.price_subtotal < 0)
        print 'has probleme'
        print negativ
        if negativ:
            return True

    def po_has_problem(self, po):
        """Check for errors refound"""
        negativ = po.refund_line_ids.filtered(lambda r: r.price_subtotal < 0)
        print 'PO has probleme'
        print negativ
        if negativ:
            return True
        return False

    def create_move_inv(self, inv):
        account_invoice_tax = self.env['account.invoice.tax']
        account_move = self.env['account.move']
        if not inv.journal_id.sequence_id:
            raise except_orm(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
        if not inv.invoice_line:
            raise except_orm(_('No Invoice Lines!'), _('Please create some invoice lines.'))
        # if inv.move_id:
        #     return

        ctx = dict(self._context, lang=inv.partner_id.lang)

        if not inv.date_invoice:
            inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
        if not inv.date_comptabilisation:
            inv.with_context(ctx).write({'date_comptabilisation': fields.Date.context_today(self)})
        date_invoice = inv.date_comptabilisation

        company_currency = inv.company_id.currency_id
        # create the analytical lines, one move line per invoice line
        iml = inv._get_analytic_lines()
        # check if taxes are all computed
        compute_taxes = account_invoice_tax.compute(inv)
        inv.check_tax_lines(compute_taxes)

        # I disabled the check_total feature
        if self.env['res.users'].has_group('account.group_supplier_inv_check_total'):
            if inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (
                        inv.currency_id.rounding / 2.0):
                raise except_orm(_('Bad Total!'), _(
                    'Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))

        if inv.payment_term:
            total_fixed = total_percent = 0
            for line in inv.payment_term.line_ids:
                if line.value == 'fixed':
                    total_fixed += line.value_amount
                if line.value == 'procent':
                    total_percent += line.value_amount
            total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
            if (total_fixed + total_percent) > 100:
                raise except_orm(_('Error!'), _(
                    "Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))

        # one move line per tax line
        iml += account_invoice_tax.move_line_get(inv.id)

        if inv.type in ('in_invoice', 'in_refund'):
            ref = inv.reference
        else:
            ref = inv.number

        diff_currency = inv.currency_id != company_currency
        # create one move line for the total and possibly adjust the other lines amount
        total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, ref, iml)

        name = inv.name or inv.supplier_invoice_number or '/'

        totlines = []
        if inv.payment_term:
            totlines = inv.with_context(ctx).payment_term.compute(total, date_invoice)[0]

        if totlines:
            res_amount_currency = total_currency
            ctx['date'] = date_invoice
            for i, t in enumerate(totlines):
                if inv.currency_id != company_currency:
                    amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                else:
                    amount_currency = False

                # last line: add the diff
                res_amount_currency -= amount_currency or 0
                if i + 1 == len(totlines):
                    amount_currency += res_amount_currency

                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': t[1],
                    'account_id': inv.account_id.id,
                    'date_maturity': t[0],
                    'amount_currency': diff_currency and amount_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'ref': ref,
                })
        else:

            iml.append({
                'type': 'dest',
                'name': name,
                'price': total,
                'account_id': inv.account_id.id,
                'date_maturity': inv.date_due,
                'amount_currency': diff_currency and total_currency,
                'currency_id': diff_currency and inv.currency_id.id,
                'ref': ref,
            })

        date = date_invoice

        part = self.env['res.partner']._find_accounting_partner(inv.partner_id)

        line = [(0, 0, inv.line_get_convert(l, part.id, date)) for l in iml]
        line = inv.group_lines(iml, line)

        journal = inv.journal_id.with_context(ctx)
        if journal.centralisation:
            raise except_orm(_('User Error!'),
                             _(
                                 'You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))

        line = inv.finalize_invoice_move_lines(line)

        if (
                        inv.type in ('in_invoice', 'in_refund') and
                    inv.supplier_invoice_number
        ):
            # Use the reference of this invoice as the reference of generated account_move_object
            move_ref = inv.supplier_invoice_number
        else:
            move_ref = inv.reference or inv.name

        move_vals = {
            'ref': move_ref,
            'line_id': line,
            'journal_id': journal.id,
            'date': date_invoice,
            'narration': inv.comment,
            'company_id': inv.company_id.id,
            'object_reference': inv._get_object_reference(inv),
        }
        ctx['company_id'] = inv.company_id.id
        period = inv.period_id
        if not period:
            period = period.with_context(ctx).find(date_invoice)[:1]
        if period:
            move_vals['period_id'] = period.id
            for i in line:
                i[2]['period_id'] = period.id

        ctx['invoice'] = inv
        move = account_move.with_context(ctx).create(move_vals)
        if inv.type == 'out_refund' and inv.amount_total < 0:
            print inv.amount_total
            for line in move.line_id:
                line.write(
                    {'debit': line.credit, 'credit': line.debit, 'debit_curr': line.credit, 'credit_curr': line.debit})
        # make the invoice point to that move

        # make the invoice point to that move
        vals = {
            'move_id': move.id,
            'period_id': period.id,
            'move_name': move.name,
        }
        # inv.with_context(ctx).write(vals)
        # Pass invoice in context in method post: used if you want to get the same
        # account move reference when creating the same invoice after a cancelled one:
        inv.internal_number = False
        inv.number = False
        invoice_type = inv._context.get('default_type')
        try:
            inv.get_name_invoice()
            inv.number = inv.get_name_invoice()
            inv.internal_number = inv.get_name_invoice()
        except ValueError:
            inv.internal_number = False
            inv.number = False
        move.post()

        return move
