# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

TYPES_PROVISION = [
    ('pca', 'PCA'),
    ('reprise_pca', 'Reprise PCA'),
    ('cca', 'CCA'),
    ('reprise_cca', 'Reprise CCA'),
    ('fae', 'FAE'),
    ('reprise_fae', 'Reprise FAE'),
    ('fnp', 'FNP'),
    ('reprise_fnp', 'Reprise FNP'),
    ('aae', 'AAE'),
    ('reprise_aae', 'Reprise AAE'),
    ('aar', 'AAR'),
    ('reprise_aar', 'Reprise AAR')
]

TYPES_PROVISION_KEYS = [x[0] for x in TYPES_PROVISION]


class AccountJournal(models.Model):
    """ Model account journal and provision """

    _name = 'account.journal'
    _inherit = 'account.journal'

    type_provision = fields.Selection(TYPES_PROVISION)
    code = fields.Char(size=6, )
    type = fields.Selection(selection_add=[('provision', 'Provision')])

    def init(self, cr):
        SQL = """ALTER TABLE account_journal ALTER COLUMN code TYPE character varying(6)"""
        print SQL
        cr.execute(SQL)

    @api.model
    def _get_journal_provision_id_by_code(self, code):
        code = code.strip().lower()
        if code not in TYPES_PROVISION_KEYS:
            raise ValidationError(_('Code %s not found') % code)
        journal = self.search([('type_provision', '=', code)], limit=1)
        if not journal:
            raise ValidationError(_('No journal found for the code %s not found') % code)
        return journal.id

    @api.model
    def _get_journal_bnk_by_code(self, code):
        journal = self.search([('code', '=', code)], limit=1)
        if not journal:
            raise ValidationError(_('No journal found for the code %s') % code)
        return journal.id
