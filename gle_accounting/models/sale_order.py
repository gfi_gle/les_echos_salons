# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from lxml import etree
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):

    _inherit = 'sale.order'
    type_facturation = fields.Selection(selection=[ ('normal', 'Classique'),
                                                    ('intercos', 'Intercos'),
                                                    ('r_echange', 'Echange des règlements'),
                                                    ('m_echange', 'Echange des marchandises')    ],
                                        string="Type de facturation", default="normal")


    state = fields.Selection([
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('passage_sans_suite', 'Passage sans suite'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
              \nThe exception status is automatically set when a cancel operation occurs \
              in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
               but waiting for the scheduler to run on the order date.", select=True)
               
    def _default_journal(self):
        domain = [
            ('type', '=', 'sale'),
            ('company_id', '=', self.env.user.company_id.id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    move_id = fields.Many2one('account.move', string="Pièce Comptable")
    
    journal_id = fields.Many2one('account.journal', string='Journal',
        default=_default_journal, domain="[('company_id', '=', company_id)]")
    
    @api.multi
    def _compute_total_invoice(self):
        for rec in self:
            rec.nbr_invoice = len(rec.invoice_ids)
    nbr_invoice = fields.Float(string="Nombre factures", compute="_compute_total_invoice")

    @api.multi
    def action_pss(self):
        if self.picking_ids:
            for line_picking in self.picking_ids:
                line_picking.action_cancel()
        if self.move_id:            
            # for line in self.move_id.line_id:
            #     self.move_id.button_cancel()
            #     line.cancel_account_move_line('ex_pss')
            self.state = "passage_sans_suite"
        else:
            self.state = "passage_sans_suite"
        # self.move_id.button_validate()
        return True
    
    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        invoice_vals = super(SaleOrder, self)._prepare_invoice(cr, uid, order, lines, context=context)
        invoice_vals.update({'salon_id' : order.salon_id.id,
                             'type_facturation' : order.type_facturation, })
        return invoice_vals

    @staticmethod
    def _get_object_reference(sale_order):

        return "sale.order,{0}".format(sale_order.id)

