# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic

import datetime
import calendar

from openerp.addons.account_invoice_streamline.models.account_invoice import Invoice  # action_move_create

import logging

_logger = logging.getLogger(__name__)


def action_move_create(self):
    """ Creates invoice related analytics and financial move lines

        Add reference to the invoice on the move
    """

    _logger.info('Invoice validation > Accounting document creation started.')

    account_invoice_tax = self.env['account.invoice.tax']
    account_move = self.env['account.move']

    for inv in self:
        origins = [] if not inv.origin else inv.origin.strip().split(',')
        purchase_order_lines = self.env['purchase.order.line'].search(
            [('order_id.name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)]).filtered(
            lambda r: r.salon_id in inv.invoice_line.mapped('salon_id'))
        devis = self.env['gle.account.refund'].search(
            [('invoice_new_id', '=', inv.id), ('move_id', '!=', False), ('move_id.extourne', '=', False)])
        devis_aar_line = self.env['gle.refund.to.receive.line'].search(
            [('refund_id.name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)])
        sale_orders = self.env['sale.order'].search(
            [('name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)])

        if not inv.journal_id.sequence_id:
            raise except_orm(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
        if not inv.invoice_line:
            raise except_orm(_('No Invoice Lines!'), _('Please create some invoice lines.'))
        if inv.move_id:
            continue

        ctx = dict(self._context, lang=inv.partner_id.lang)

        if not inv.date_invoice:
            inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
        if not inv.date_comptabilisation:
            inv.with_context(ctx).write({'date_comptabilisation': fields.Date.context_today(self)})
        date_invoice = inv.date_comptabilisation

        company_currency = inv.company_id.currency_id
        # create the analytical lines, one move line per invoice line
        iml = inv._get_analytic_lines()
        # check if taxes are all computed
        compute_taxes = account_invoice_tax.compute(inv)
        inv.check_tax_lines(compute_taxes)

        # I disabled the check_total feature
        # if self.env['res.users'].has_group('account.group_supplier_inv_check_total'):
        #     if inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (
        #                 inv.currency_id.rounding / 2.0):
        #         raise except_orm(_('Bad Total!'), _(
        #             'Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))

        if inv.payment_term:
            total_fixed = total_percent = 0
            for line in inv.payment_term.line_ids:
                if line.value == 'fixed':
                    total_fixed += line.value_amount
                if line.value == 'procent':
                    total_percent += line.value_amount
            total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
            if (total_fixed + total_percent) > 100:
                raise except_orm(_('Error!'), _(
                    "Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))

        # one move line per tax line
        iml += account_invoice_tax.move_line_get(inv.id)

        if inv.type in ('in_invoice', 'in_refund'):
            ref = inv.reference
        else:
            ref = inv.number

        diff_currency = inv.currency_id != company_currency
        # create one move line for the total and possibly adjust the other lines amount
        total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, ref, iml)

        name = inv.supplier_invoice_number or '/'
        totlines = []

        if inv.payment_term:
            totlines = inv.with_context(ctx).payment_term.compute(total, date_invoice)[0]

        if totlines:
            res_amount_currency = total_currency
            ctx['date'] = date_invoice
            for i, t in enumerate(totlines):
                if inv.currency_id != company_currency:
                    amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                else:
                    amount_currency = False

                # last line: add the diff
                res_amount_currency -= amount_currency or 0
                if i + 1 == len(totlines):
                    amount_currency += res_amount_currency
                if inv.type == "out_invoice":
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'ref': ref,
                    })
                else:
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'ref': ref,
                    })

        else:

            iml.append({
                'type': 'dest',
                'name': name,
                'price': total,
                'account_id': inv.account_id.id,
                'date_maturity': inv.date_due,
                'amount_currency': diff_currency and total_currency,
                'currency_id': diff_currency and inv.currency_id.id,
                'ref': ref,
            })

        date = date_invoice

        part = self.env['res.partner']._find_accounting_partner(inv.partner_id)

        line = [(0, 0, self.line_get_convert(l, part.id, date)) for l in iml]
        line = inv.group_lines(iml, line)

        journal = inv.journal_id.with_context(ctx)
        if journal.centralisation:
            raise except_orm(_('User Error!'),
                             _(
                                 'You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))

        line = inv.finalize_invoice_move_lines(line)

        if (
                        inv.type in ('in_invoice', 'in_refund') and
                    inv.supplier_invoice_number
        ):
            # Use the reference of this invoice as the reference of generated account_move_object
            move_ref = inv.supplier_invoice_number
        else:
            move_ref = inv.reference or inv.name

        move_vals = {
            'ref': move_ref,
            'line_id': line,
            'salon_id': inv.salon_id.id,
            'journal_id': journal.id,
            'date': date_invoice,
            'narration': inv.comment,
            'company_id': inv.company_id.id,
            'object_reference': self._get_object_reference(inv),
        }
        ctx['company_id'] = inv.company_id.id
        period = inv.period_id
        if not period:
            period = period.with_context(ctx).find(date_invoice)[:1]
        if period:
            move_vals['period_id'] = period.id
            for i in line:
                i[2]['period_id'] = period.id

        ctx['invoice'] = inv
        move = account_move.with_context(ctx).create(move_vals)
        if inv.type == 'out_refund' and inv.amount_total < 0:
            print inv.amount_total
            for line in move.line_id:
                line.write(
                    {'debit': line.credit, 'credit': line.debit, 'debit_curr': line.credit,
                     'credit_curr': line.debit})
        # make the invoice point to that move

        # make the invoice point to that move
        inv.internal_number = False
        inv.number = False

        if inv.type == 'out_invoice':
            inv.number = self.compute_account_invoice_name(inv, 'out_invoice')
            inv.internal_number = self.compute_account_invoice_name(inv, 'out_invoice')

        elif inv.type == 'out_refund':
            inv.number = self.compute_account_invoice_name(inv, 'out_refund')
            inv.internal_number = self.compute_account_invoice_name(inv, 'out_refund')

        if inv.type in ['in_invoice', 'in_refund']:
            inv.number = self.compute_account_invoice_name(inv, 'in_invoice')
            inv.internal_number = self.compute_account_invoice_name(inv, 'in_invoice')

        vals = {
            'move_id': move.id,
            'period_id': period.id,
            'move_name': move.name,
        }
        inv.with_context(ctx).write(vals)
        # Pass invoice in context in method post: used if you want to get the same
        # account move reference when creating the same invoice after a cancelled one:
        move.post()

        if inv.type == 'in_invoice':
            salons = inv.invoice_line.filtered(lambda r: not r.move_prov_origin_id and self.verif_avant_mois_salon(
                r.salon_id.date_realisation)).mapped('salon_id')
            for salon in salons:
                inv.action_move_create_for_cca(salon)
        if inv.type == 'in_refund':
            salons = inv.invoice_line.filtered(lambda r: not r.move_prov_origin_id and self.verif_avant_mois_salon(
                r.salon_id.date_realisation)).mapped('salon_id')
            for salon in salons:
                inv.action_move_create_for_refund_cca(salon)
        # Extourne AAR
        if inv.type == 'in_invoice' and devis_aar_line:
            inv.extourne_aar(devis_aar_line)
        if inv.type == 'in_refund' and devis_aar_line:
            inv.extourne_aar(devis_aar_line)
        if self.verif_avant_mois_salon(inv.salon_id.date_realisation):
            if inv.type == 'out_invoice' and inv.advance_payment_method not in ['fixed', 'percentage']:
                inv.action_move_create_for_pca()
            elif inv.type == 'out_refund':
                inv.action_move_create_for_refund_pca()

        if inv.type == 'out_invoice' and sale_orders:
            inv.extourne_fae(sale_orders)
        if inv.type == 'in_invoice' and purchase_order_lines:
            inv.extourne_fnp(purchase_order_lines)
        if inv.type == 'out_refund' and devis:
            inv.extourne_aae(devis)

    self._log_event()
    _logger.info('Invoice validation > Accounting document creation done.')

    return super(Invoice, self).action_move_create()


Invoice.action_move_create = action_move_create


# kept for backward compatibility
class except_orm(Exception):
    def __init__(self, name, value):
        self.name = name
        self.value = value
        self.args = (name, value)


validation_date = datetime.datetime.now()
avant_mois_salon = datetime.datetime.strptime('2017-03-31', '%Y-%m-%d')
apres_mois_salon = datetime.datetime.strptime('2017-05-1', '%Y-%m-%d')

validation_date = apres_mois_salon


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    gle_account_refund_line_id = fields.Many2one('gle.account.refund.line', string='Refund line', )


class AccountInvoice(models.Model):
    __metaclass__ = MetaAnalytic
    _inherit = "account.invoice"
    _analytic = 'account_invoice'

    _modifier = fields.Char(string='modifier')

    def get_name_invoice(self):

        salon_id = self.salon_id.id

        if self.type in ['out_invoice', 'out_refund']:
            account_invoice_count = self.search_count(
                [('salon_id', '=', salon_id), ('type', 'in', ['out_invoice', 'out_refund'])])
            print account_invoice_count
        elif self.type in ['in_invoice', 'in_refund']:
            account_invoice_count = self.search_count(
                [('salon_id', '=', salon_id), ('type', 'in', ['in_invoice', 'in_refund'])])

        chrono_projet_str = "00{0}".format(str(salon_id))

        if account_invoice_count == 0:
            account_invoice_str = "0001"
        else:
            account_invoice_str = "0000{0}".format(account_invoice_count)

        if self.type == "out_invoice":
            return "F{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

        elif self.type == "out_refund":
            return "A{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

        elif self.type in ['in_invoice', 'in_refund']:
            return "H{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

    def get_name_move_bank(self, inv):

        account_voucher_obj = inv.env['account.voucher']
        get_journal_bnk_by_code = inv.env['account.journal']._get_journal_bnk_by_code

        encais_cb_les_id = get_journal_bnk_by_code('5BKEC')
        encais_cheque_les_id = get_journal_bnk_by_code('5BKEH')
        encais_virm_etra_id = get_journal_bnk_by_code('5BKEVE')
        encais_virm_fran_id = get_journal_bnk_by_code('5BKEVF')
        decais_cb_les_id = get_journal_bnk_by_code('5BKDC')
        decais_cheque_les_id = get_journal_bnk_by_code('5BKDH')
        decais_virm_id = get_journal_bnk_by_code('5BKDV')

        pay_encais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cb_les_id)])
        pay_encais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cheque_les_id)])
        pay_encais_virm_etra_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_etra_id)])
        pay_encais_virm_fran_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_fran_id)])

        pay_decais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cb_les_id)])
        pay_decais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cheque_les_id)])
        pay_decais_virm_ids = account_voucher_obj.search([('journal_id', '=', decais_virm_id)])

        bank_invoice_count = sum(
            [len(pay_encais_cb_les_ids), len(pay_encais_cheque_les_ids), len(pay_encais_virm_etra_ids),
             len(pay_encais_virm_fran_ids), len(pay_decais_cb_les_ids), len(pay_decais_cheque_les_ids),
             len(pay_decais_virm_ids)])

        bank_invoice_count += 1

        chrono_projet_str = "000"

        if bank_invoice_count == 0:
            bank_invoice_str = "0001"
        else:
            bank_invoice_str = "0000{0}".format(bank_invoice_count)

        return "B{0}{1}".format(chrono_projet_str[-3:], bank_invoice_str[-4:])

    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher',
                                                                             'view_vendor_receipt_dialog_form')

        inv = self.browse(cr, uid, ids[0], context=context)

        return {
            'name': _("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'ref': 'tttt',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': self.get_name_move_bank(inv),
                'default_reference': self.get_name_move_bank(inv),
                'default_number': self.get_name_move_bank(inv),
                'number': self.get_name_move_bank(inv),
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice', 'out_refund') and 'receipt' or 'payment'
            }
        }

    def extourne_aae(self, devis):
        account_move_line_obj = self.env['account.move.line']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        print "EX_AAE / EX_AAE_Echange"
        date_pro_aae = self.get_validation_date()
        period_aae = self.env['account.period'].find(dt=date_pro_aae)

        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "ex_aae_echange"
            modele = "aae_echange"
        else:
            tag = "ex_aae"
            modele = "aae"

        aml_from_po = {}
        aml_from_invoice = {}
        value = 0
        if len(devis) > 0:
            for po in devis:

                parent_move_id = self.env['account.move'].search([('object_reference', '=', "gle.account.refund,{0}".format(po.id))])

                move_po = self.env['account.move'].create({
                    'journal_id': journal_reprise_aae_id,
                    'period_id': period_aae.id,
                    'salon_id': devis.salon_id.id,
                    'name': self.get_name_provision(),
                    'ref': 'extourne AAE' + ' ' + self.origin,
                    'parent_move_id': parent_move_id.id,
                    'date': date_pro_aae
                })
                for line in po.move_id.line_id:
                    vals = line.get_account_move_line_to_cancel(tag)
                    vals['move_id'] = move_po.id
                    # analytic for compte 6 and 7
                    vals['projet_id'] = line.projet_id.id
                    vals['produit_id'] = line.produit_id.id
                    vals['section_id'] = line.section_id.id
                    vals['nature_id'] = line.nature_id.id
                    vals['code_interco'] = line.code_interco
                    vals['date_origin_document'] = line.date_origin_document
                    vals['tax_id'] = line.tax_id.id

                    account_move_line_obj.create(vals)
                move_po.button_validate()
                po.move_id.extourne = True

    def extourne_aar(self, devis_aar_line):
        account_move_line_obj = self.env['account.move.line']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_reprise_aar_id = get_journal_provision_id_by_code('reprise_aar')
        print "EX_AAR / EX_AAR_Echange"
        # period = self.env['account.period'].find(dt=self.date_comptabilisation)

        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "ex_aar_echange"
            modele = "aar_echange"
        else:
            tag = "ex_aar"
            modele = "aar"

        aml_from_po = {}
        aml_from_invoice = {}
        value = 0

        ref = 'extourne AAR' + ' ' + self.origin
        moves_origines_aar = devis_aar_line.mapped('move_id')

        for move in moves_origines_aar:
            _devis_aar_line = self.env['gle.refund.to.receive.line'].search([('move_id', '=', move.id)])[0]
            po = _devis_aar_line.refund_id
            name_pro = self.get_name_provision()
            date_pro = self.get_validation_date()
            ref = 'extourne AAR' + ' ' + str(po.name) or ''
            vals = move._get_move_extourne(name=name_pro, journal_id=journal_reprise_aar_id, ref=ref, dt=date_pro,
                                           salon_id=move.salon_id.id)
            move_ex = move.create(vals)
            move_ex.extourne = True
            move.extourne = True
            move_ex.button_validate()

    def extourne_fae(self, devis):
        account_move_line_obj = self.env['account.move.line']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_reprise_fae_id = get_journal_provision_id_by_code('reprise_fae')
        print "EX_FAE / EX_FAE_Echange"
        date_pro_fae = self.get_validation_date()
        period_fae = self.env['account.period'].find(dt=date_pro_fae)

        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "ex_fae_echange"
            modele = "fae_echange"
        else:
            tag = "ex_fae"
            modele = "fae"

        aml_from_po = {}
        aml_from_invoice = {}
        value = 0
        if len(devis) > 0:
            for po in devis:

                parent_move_id = self.env['account.move'].search([('object_reference', '=', "sale.order,{0}".format(po.id))])

                move_po = self.env['account.move'].create({
                    'journal_id': journal_reprise_fae_id,
                    'period_id': period_fae.id,
                    'salon_id': po.salon_id.id,
                    'name': self.get_name_provision(),
                    'ref': 'extourne FAE' + ' ' + self.origin,
                    'parent_move_id': parent_move_id.id,
                    'date': date_pro_fae,
                })

                for line in po.move_id.line_id:
                    vals = line.get_account_move_line_to_cancel(tag)
                    vals['move_id'] = move_po.id
                    # analytic for compte 6 and 7
                    vals['projet_id'] = line.projet_id.id
                    vals['produit_id'] = line.produit_id.id
                    vals['section_id'] = line.section_id.id
                    vals['nature_id'] = line.nature_id.id
                    vals['code_interco'] = line.code_interco
                    vals['date_origin_document'] = line.date_origin_document
                    vals['tax_id'] = line.tax_id.id

                    account_move_line_obj.create(vals)

                move_po.button_validate()
                po.move_id.extourne = True

    def extourne_fnp(self, devis):
        account_move_line_obj = self.env['account.move.line']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        print "EX_FNP / EX_FNP_Echange"
        date_pro_fnp = self.get_validation_date()
        period_fnp = self.env['account.period'].find(dt=date_pro_fnp)

        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "ex_fnp_echange"
            modele = "fnp_echange"
        else:
            tag = "ex_fnp"
            modele = "fnp"

        aml_from_po = {}
        aml_from_invoice = {}
        value = 0
        if len(devis) > 0:

            move_pro_origine = devis.mapped('move_id')

            for move in move_pro_origine:
                po_line = self.env['purchase.order.line'].search([('move_id', '=', move.id)])[0]
                po = po_line.order_id

                move_po = self.env['account.move'].create({
                    'journal_id': journal_reprise_fnp_id,
                    'period_id': period_fnp.id,
                    'salon_id': po.salon_id.id,
                    'name': self.get_name_provision(),
                    'ref': 'extourne FNP' + ' ' + po.name,
                    'date': date_pro_fnp
                })
                for line in move.line_id:
                    vals = line.get_account_move_line_to_cancel(tag)
                    # reconciliation marker
                    if line.account_id.code.startswith('4'):
                        vals['reconciliation_marker'] = po_line.move_id.internal_sequence_number

                    vals['move_id'] = move_po.id
                    # analytic for compte 6 and 7
                    vals['projet_id'] = line.projet_id.id
                    vals['produit_id'] = line.produit_id.id
                    vals['section_id'] = line.section_id.id
                    vals['nature_id'] = line.nature_id.id
                    vals['code_interco'] = line.code_interco
                    vals['date_origin_document'] = line.date_origin_document
                    vals['tax_id'] = line.tax_id.id

                    account_move_line_obj.create(vals)

                move_po.button_validate()
                move_po.extourne = True

    def action_move_create_for_refund_cca(self, salon):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_cca_id = get_journal_provision_id_by_code('cca')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in self.invoice_line]
        date_pro_cca = self.get_validation_date()
        period_cca = self.env['account.period'].find(dt=date_pro_cca)
        ref = 'CCA' + ' ' + self.number
        vals = self.get_move_provision(journal=journal_cca_id, period=period_cca, salon=salon,
                                       name=self.get_name_provision(), reference=ref, date=date_pro_cca)

        move_provision_cca = self.env['account.move'].create(vals)
        lines = self.invoice_line.filtered(lambda x: x.salon_id == salon)
        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "new_cca_echange"
            modele = "cca_echange"
        else:
            tag = "new_cca"
            modele = "cca"

            for line in lines:
                vals = {
                    'partner_id': self.partner_id.id,
                    'move_id': move_provision_cca.id,
                    'name': line.name.split('\n')[0][:64],
                    'account_id': line.account_id.id,
                    'product_id': line.product_id.id,
                    'debit': line.price_subtotal,
                    'debit_curr': line.price_subtotal,
                    'credit_curr': 0,
                    'credit': 0,
                }

                if vals['debit'] < 0 or vals['credit'] < 0:
                    vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                    vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['credit_curr'])
                vals.update(self.get_line_gle_info_for_cca(invoice_line=line, vals=vals))
                account_move_line_obj.create(vals)

            vals = account_move_line_obj.get_additional_move_line_dict_from_invoice(self, modele="cca", salon=salon)

            vals['debit'], vals['credit'] = vals['credit'], vals['debit']
            vals['debit_curr'] = vals['debit']
            vals['credit_curr'] = vals['credit']
            if vals['debit'] < 0 or vals['credit'] < 0:
                vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])
            vals['move_id'] = move_provision_cca.id
            account_move_line_obj.create(vals)
            line.move_prov_origin_id = move_provision_cca
            move_provision_cca.button_validate()

    def get_move_provision(self, journal=None, period=None, salon=None, name=None, reference=None, date=None):
        move = {
            'journal_id': journal,
            'period_id': period.id,
            'salon_id': salon.id,
            'name': name,
            'ref': reference,
            'date': date,
        }
        return move

    def action_move_create_for_cca(self, salon):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_cca_id = get_journal_provision_id_by_code('cca')
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in self.invoice_line]
        date_pro_cca = self.get_validation_date()
        period = self.env['account.period'].find(dt=date_pro_cca)

        ref = 'CCA' + ' ' + self.number
        vals = self.get_move_provision(journal=journal_cca_id, period=period, salon=salon,
                                       name=self.get_name_provision(), reference=ref,
                                       date=date_pro_cca)

        move_provision_cca = self.env['account.move'].create(vals)
        lines = self.invoice_line.filtered(lambda x: x.salon_id == salon)
        if self.type_facturation in ['m_echange', 'r_echange']:
            tag = "new_cca_echange"
            modele = "cca_echange"
        else:
            tag = "new_cca"
            modele = "cca"

        for line in lines:
            vals = {
                'partner_id': self.partner_id.id,
                'move_id': move_provision_cca.id,
                'name': line.name.split('\n')[0][:64],
                'account_id': line.account_id.id,
                'product_id': line.product_id.id,
                'debit': 0,
                'debit_curr': 0,
                'credit_curr': line.price_subtotal,
                'credit': line.price_subtotal,

            }

            if vals['debit'] < 0 or vals['credit'] < 0:
                vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['credit_curr'])
            vals.update(self.get_line_gle_info_for_cca(invoice_line=line, vals=vals))

            account_move_line_obj.create(vals)

        vals = account_move_line_obj.get_additional_move_line_dict_from_invoice(self, modele="cca", salon=salon)

        vals['debit_curr'] = vals['debit']
        vals['credit_curr'] = vals['credit']
        if vals['debit'] < 0 or vals['credit'] < 0:
            vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
            vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])
        vals['move_id'] = move_provision_cca.id

        account_move_line_obj.create(vals)
        line.move_prov_origin_id = move_provision_cca
        move_provision_cca.button_validate()

    def get_line_gle_info_for_cca(self, invoice_line, vals):

        if self.partner_id.intercos:
            code_interco = self.partner_id.code_intercos
        else:
            code_interco = "9Z99"

        vals.update({
            'projet_id': invoice_line.salon_id.millesime.id,
            'produit_id': invoice_line.salon_id.name.id,
            'section_id': invoice_line.gle_section_id.id,
            'nature_id': invoice_line.nature_id.nature_comptable_id.id,
            'code_interco': code_interco,
            'date_origin_document': self.create_date,
        })

        return vals

    def action_move_create_for_refund_pca(self):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_pca_id = get_journal_provision_id_by_code('pca')
        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in self.invoice_line]
        date_pro_pca = self.get_validation_date()
        period_pca = self.env['account.period'].find(dt=date_pro_pca)

        if not self.move_ex_id:
            move = self.env['account.move'].create({
                'journal_id': journal_pca_id,
                'period_id': period_pca.id,
                'salon_id': self.salon_id.id,
                'name': self.get_name_provision(),
                'ref': 'PCA' + ' ' + self.origin,
                'parent_move_id': self.move_id.id,
                'date': date_pro_pca,
            })
            if self.type_facturation in ['m_echange', 'r_echange']:
                tag = "new_pca_echange"
                modele = "pca_echange"
            else:
                tag = "new_pca"
                modele = "pca"

            for line in self.invoice_line:
                vals = {
                    'partner_id': self.partner_id.id,
                    'move_id': move.id,
                    'name': line.name.split('\n')[0][:64],
                    'account_id': line.account_id.id,
                    'product_id': line.product_id.id,
                    'debit': 0,
                    'debit_curr': 0,
                    'credit_curr': line.price_subtotal,
                    'credit': line.price_subtotal,
                }

                if line.account_id.code.startswith('4'):
                    vals.update({
                        'reconciliation_marker': self.move_id.internal_sequence_number,
                    })

                if vals['debit'] < 0 or vals['credit'] < 0:
                    vals['credit'], vals['debit'] = abs(vals['credit']), abs(vals['debit'])
                    vals['credit_curr'], vals['debit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])
                else:
                    vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                    vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])
                print 'line ', line.id, 'credit ', ' ', vals['credit_curr'], vals['credit_curr'], 'debit', vals[
                    'debit_curr'], ' ', vals['debit_curr']

                vals.update(self.get_line_gle_info_for_pca(invoice_line=line, vals=vals))
                account_move_line_obj.create(vals)
            vals = account_move_line_obj.get_additional_move_line_dict_from_invoice(self, modele)
            vals['move_id'] = move.id
            vals['debit_curr'] = vals['debit']
            vals['credit_curr'] = vals['credit']

            if vals['debit'] < 0 or vals['credit'] < 0:
                vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])
            else:
                vals['debit'], vals['credit'] = abs(vals['debit']), abs(vals['credit'])
                vals['debit_curr'], vals['credit_curr'] = abs(vals['debit_curr']), abs(vals['credit_curr'])

            print 'vals ', 'credit ', ' ', vals['credit_curr'], vals['credit_curr'], 'debit', vals['debit_curr'], ' ', \
                vals['debit_curr']

            vals['move_id'] = move.id
            account_move_line_obj.create(vals)
            self.move_ex_id = move
            move.button_validate()

    def get_name_provision(self):
        move_obj = self.env['account.move']
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code

        journal_aae_id = get_journal_provision_id_by_code('aae')
        journal_aar_id = get_journal_provision_id_by_code('aar')
        journal_pca_id = get_journal_provision_id_by_code('pca')
        journal_cca_id = get_journal_provision_id_by_code('cca')
        journal_fae_id = get_journal_provision_id_by_code('fae')
        journal_fnp_id = get_journal_provision_id_by_code('fnp')

        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        journal_reprise_aar_id = get_journal_provision_id_by_code('reprise_aar')
        journal_reprise_pca_id = get_journal_provision_id_by_code('reprise_pca')
        journal_reprise_cca_id = get_journal_provision_id_by_code('reprise_cca')
        journal_reprise_fae_id = get_journal_provision_id_by_code('reprise_fae')
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        account_move_obj = self.env['account.move']

        aar_move_ids = move_obj.search([('journal_id', '=', journal_aar_id)])
        reprise_aar_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_aar_id)])

        aae_move_ids = move_obj.search([('journal_id', '=', journal_aae_id)])
        reprise_aae_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_aae_id)])

        pca_move_ids = move_obj.search([('journal_id', '=', journal_pca_id)])
        reprise_pca_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_pca_id)])

        cca_move_ids = move_obj.search([('journal_id', '=', journal_cca_id)])
        reprise_cca_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_cca_id)])

        fae_move_ids = move_obj.search([('journal_id', '=', journal_fae_id)])
        reprise_fae_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_fae_id)])

        fnp_move_ids = move_obj.search([('journal_id', '=', journal_fnp_id)])
        reprise_fnp_move_ids = move_obj.search(
            [('journal_id', '=', journal_reprise_fnp_id)])

        account_invoice_count = sum(
            [len(aae_move_ids), len(reprise_aae_move_ids), len(reprise_aar_move_ids), len(aar_move_ids),
             len(pca_move_ids), len(reprise_pca_move_ids),
             len(cca_move_ids), len(reprise_cca_move_ids), len(fae_move_ids), len(reprise_fae_move_ids),
             len(fnp_move_ids), len(reprise_fnp_move_ids)])
        account_invoice_count += 1

        chrono_projet_str = "000"

        if account_invoice_count == 0:
            account_invoice_str = "0001"
        else:
            account_invoice_str = "0000{0}".format(account_invoice_count)

        return "P{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

    def action_move_create_for_pca(self):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_pca_id = get_journal_provision_id_by_code('pca')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in self.invoice_line]
        date_pro_pca = self.get_validation_date()
        period_pca = self.env['account.period'].find(dt=date_pro_pca)

        if not self.move_ex_id:
            move = self.env['account.move'].create({
                'journal_id': journal_pca_id,
                'period_id': period_pca.id,
                'date': self.get_validation_date(),
                'salon_id': self.salon_id.id,
                'name': self.get_name_provision(),
                'ref': 'PCA' + ' ' + self.origin,
                'parent_move_id': self.move_id.id
            })
            if self.type_facturation in ['m_echange', 'r_echange']:
                tag = "new_pca_echange"
                modele = "pca_echange"
            else:
                tag = "new_pca"
                modele = "pca"

            for line in self.invoice_line:
                vals = {
                    'partner_id': self.partner_id.id,
                    'move_id': move.id,
                    'name': line.name.split('\n')[0][:64],
                    'account_id': line.account_id.id,
                    'product_id': line.product_id.id,
                    'credit': 0,
                    'credit_curr': 0,
                    'debit_curr': line.price_subtotal,
                    'debit': line.price_subtotal,
                }

                if vals['debit'] < 0 or vals['credit'] < 0:
                    vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                    vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])

                vals.update(self.get_line_gle_info_for_pca(invoice_line=line, vals=vals))
                account_move_line_obj.create(vals)

            vals = account_move_line_obj.get_additional_move_line_dict_from_invoice(self, modele)
            vals['debit_curr'], vals['credit_curr'] = vals['debit'], vals['credit']

            if vals['debit'] < 0 or vals['credit'] < 0:
                vals['debit'], vals['credit'] = abs(vals['credit']), abs(vals['debit'])
                vals['debit_curr'], vals['credit_curr'] = abs(vals['credit_curr']), abs(vals['debit_curr'])

            vals['move_id'] = move.id
            account_move_line_obj.create(vals)
            self.move_ex_id = move
            move.button_validate()

    def get_line_gle_info_for_pca(self, invoice_line, vals):

        if self.partner_id.intercos:
            code_interco = self.partner_id.code_intercos
        else:
            code_interco = "9Z99"

        vals.update({
            'projet_id': invoice_line.invoice_id.salon_id.millesime.id,
            'produit_id': invoice_line.invoice_id.salon_id.name.id,
            'section_id': invoice_line.invoice_id.gle_section_id.id,
            'nature_id': invoice_line.product_id.product_tmpl_id.categ_id.nature_comptable_id.id,
            'code_interco': code_interco,
            'date_origin_document': self.create_date,
        })

        return vals

    def _default_gle_uid_user_metier(self):
        user_obj = self.env['res.users'].browse([self._uid])
        return user_obj.user_metier

    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    gle_uid_user_metier = fields.Selection([
        ('adv', 'ADV'),
        ('compta', 'Comptabilité'),
        ('dir', 'Direction'), ],
        default=lambda self: self._default_gle_uid_user_metier(),
        compute='_compute_gle_uid_user_metier')

    accounting_section_id = fields.Many2one(comodel_name='gle.section', string="Section")

    comment_step1 = fields.Char('Commentaire validation 1')
    comment_step2 = fields.Char('Commentaire validation 2')
    comment_step3 = fields.Char('Commentaire validation 3')
    comment_step4 = fields.Char('Commentaire validation 4')

    move_cca = fields.Many2one('account.move', string="Pièce Comptable cca")
    move_ex_id = fields.Many2one('account.move', string="Pièce Comptable extourne")
    move_ex_pca_id = fields.Many2one('account.move', string="Pièce Comptable pca")

    type_facturation = fields.Selection(selection=[('normal', 'Classique'),
                                                   ('intercos', 'Intercos'),
                                                   ('r_echange', 'Echange des règlements'),
                                                   ('m_echange', 'Echange des marchandises')],
                                        string="Type de facturation", default="normal")

    refused = fields.Boolean('Refused', default=False)
    date_comptabilisation = fields.Date("Date comptabilisation")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('a_facturer', 'A Facturer'),
        ('proforma_a_facturer', 'A Facturer'),
        ('step1', 'Step 1'),
        ('step2', 'Step 2'),
        ('step3', 'Step 3'),
        ('step4', 'Step 4'),
        ('finalstep', 'Final step'),
        ('proforma', 'Pro-forma'),
        ('proforma2', 'Pro-forma'),
        ('open', 'Open'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Pro-forma' when invoice is in Pro-forma status,invoice does not have an invoice number.\n"
             " * The 'Open' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")

    @api.onchange('date_comptabilisation')
    def _onchange_date_comptabilisation(self):
        if self.date_comptabilisation and self.type in ['out_invoice', 'out_refund']:
            self.date_invoice = self.date_comptabilisation

    def _compute_gle_uid_user_metier(self):
        user_obj = self.env['res.users'].browse([self._uid])

        for account_invoice in self:
            account_invoice.gle_uid_user_metier = user_obj.user_metier

    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now()

    def verif_avant_mois_salon(self, date_b):

        if not date_b:
            return False

        date_b = datetime.datetime.strptime(date_b, '%Y-%m-%d').date()
        last_day = date_b.replace(day=calendar.monthrange(date_b.year, date_b.month)[1])
        first_day = last_day.replace(day=1)

        if date_b and (self.get_validation_date() - first_day).days < 0:
            return True
        else:
            return False

    def verif_apres_mois_salon(self, date_b):

        if not date_b:
            return False

        date_b = datetime.datetime.strptime(date_b, '%Y-%m-%d').date()
        last_day = date_b.replace(day=calendar.monthrange(date_b.year, date_b.month)[1])

        if date_b and (self.get_validation_date() - last_day).days > 0:
            return True
        else:
            return False

    def manage_in_invoice(self, inv):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_cca_id = get_journal_provision_id_by_code('cca')
        journal_reprise_fnp_id = get_journal_provision_id_by_code('reprise_fnp')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in inv.invoice_line]

        purchase_orders = self.env['purchase.order'].search(
            [('name', '=', inv.origin), ('move_id', '!=', False), ('move_id.extourne', '=', False)])

    def manage_out_invoice(self, inv):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_pca_id = get_journal_provision_id_by_code('pca')
        journal_reprise_fae_id = get_journal_provision_id_by_code('reprise_fae')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in inv.invoice_line]

    #         acompte = 0
    #         for line in inv.invoice_line:
    #             if line.product_id.for_acompte :
    #                 acompte =  acompte +1

    #         if acompte == 0:
    # if self.verif_avant_mois_salon(self.salon_id.date_realisation):
    #     return


    def manage_out_refund(self, inv):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_pca_id = get_journal_provision_id_by_code('pca')
        journal_reprise_aae_id = get_journal_provision_id_by_code('reprise_aae')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in inv.invoice_line]

    def manage_in_refund(self, inv):
        get_journal_provision_id_by_code = self.env['account.journal']._get_journal_provision_id_by_code
        journal_cca_id = get_journal_provision_id_by_code('cca')
        journal_reprise_aar_id = get_journal_provision_id_by_code('reprise_aar')
        account_move_line_obj = self.env['account.move.line']
        codes = [x.account_id.code for x in inv.invoice_line]

        if self.verif_apres_mois_salon(self.salon_id.date_realisation):
            print "EX_AAR / EX_AAR_Echange"
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tag = "ex_aar_echange"
                modele = "aar_echange"
            else:
                tag = "ex_aar"
                modele = "aar"

            aml_from_po = {}
            aml_from_invoice = {}
            value = 0
            devis = self.env['gle.refund.to.receive'].search(
                [('invoice_new_id.id', '=', inv.id), ('move_id', '!=', False)])
            if len(devis) > 0:
                for po in devis:
                    for aml in po.move_id.line_id:
                        if aml.product_id and not aml.product_id.id in aml_from_po.keys():
                            aml_from_po[aml.product_id.id] = {'product_id': aml.product_id.id,
                                                              'account_id': aml.account_id.id,
                                                              'credit': aml.credit, }
                        elif aml.product_id:
                            aml_from_po[aml.product_id.id] = {'product_id': aml.product_id.id,
                                                              'account_id': aml.account_id.id,
                                                              'credit': aml_from_po[aml.product_id.id][
                                                                            'credit'] + aml.credit, }
                for aml in inv.move_id.line_id:
                    if aml.product_id and not aml.product_id.id in aml_from_invoice.keys():
                        aml_from_invoice[aml.product_id.id] = {'product_id': aml.product_id.id,
                                                               'account__id': aml.account_id.id,
                                                               'product_name': aml.product_id.name,
                                                               'credit': aml.credit, }
                    elif aml.product_id:
                        aml_from_invoice[aml.product_id.id] = {'product_id': aml.product_id.id,
                                                               'product_name': aml.product_id.name,
                                                               'account__id': aml.account_id.id,
                                                               'credit': aml_from_invoice[aml.product_id.id][
                                                                             'credit'] + aml.credit, }
                for key, value_dict in aml_from_invoice.items():
                    if key in aml_from_po.keys():
                        debit = value_dict['credit'] if value_dict['credit'] < aml_from_po[key]['credit'] else \
                            aml_from_po[key]['credit']
                        value = value + debit
                        vals = {
                            'journal_id': inv.journal_id.id,
                            'partner_id': inv.partner_id.id,
                            'move_id': inv.move_id.id,
                            'name': value_dict['product_name'],
                            'account_id': value_dict['account_id'],
                            'product_id': key,
                            'tag': tag,
                            'debit': debit,
                            'credit': 0,
                            'date': self.get_validation_date(),
                            'period_id': inv.move_id.period_id.id
                        }
                        account_move_line_obj.create(vals)

                vals = account_move_line_obj.get_additional_move_line_dict_from_devis(inv, modele)
                vals.update({'tag': tag, 'credit': value, 'debit': 0, 'move_id': inv.move_id.id})
                account_move_line_obj.create(vals)

    @api.multi
    def action_move_create(self):
        for inv in self:
            # CYCLE NORMAL D'ODOO
            res = super(AccountInvoice, self).action_move_create()
            if not inv.type_facturation == 'intercos':
                # RECHERCHE DES ECRITURES COMPTABLES GENEREES AVANT VALIDATION DE FACTURES
                if inv.salon_id:
                    if inv.move_id:
                        print "there is a move", inv.move_id.state
                        self.pool.get('account.move').button_cancel(self._cr, self._uid, [inv.move_id.id])

                    if inv.type == 'out_invoice':
                        # CAS D'UNE FACTURE DE VENTE CLASSIQUE
                        self.manage_out_invoice(inv)
                    elif inv.type == 'out_refund':
                        # CAS D'UNE FACTURE DE VENTE DE TYPE AVOIR
                        self.manage_out_refund(inv)
                    elif inv.type == 'in_invoice':
                        # CAS D'UNE FACTURE D'ACHAT CLASSIQUE
                        self.manage_in_invoice(inv)
                    elif inv.type == 'in_refund':
                        # CAS D'UNE FACTURE D'ACHAT DE TYPE AVOIR
                        self.manage_in_refund(inv)
                    self.pool.get('account.move').button_validate(self._cr, self._uid, [inv.move_id.id])
            return res

    @api.multi
    def action_number(self):
        for inv in self:
            return super(AccountInvoice, self).action_number()

    @api.multi
    def invoice_validate(self):
        """ Override method to change statut in function of type account.invoice """
        for inv in self:

            if not inv.date_invoice:
                inv.date_invoice = fields.Date.context_today(inv)

            if not inv.date_comptabilisation:
                inv.date_comptabilisation = self.get_validation_date()
            return super(AccountInvoice, self).invoice_validate()

    @api.onchange('type_facturation')
    @api.multi
    def onchange_Type_facturation(self):
        domain = []
        if self._context.get('type') in ['out_invoice', 'out_refund']:
            domain.extend([('is_company', '=', True), ('customer', '=', True)])
        else:
            domain.extend([('supplier', '=', True)])
        if self.type_facturation == 'normal':
            return {'domain': {'partner_id': domain, }}

        if not self.type_facturation:
            return {'domain': {'partner_id': domain, }}
        elif self.type_facturation == 'intercos':
            if self.partner_id and not self.partner_id.intercos:
                self.partner_id = False
            domain.append(('intercos', '=', True))
            return {'domain': {'partner_id': domain, }}
        elif self.type_facturation in ['r_echange', 'm_echange']:
            if self.partner_id and (self.partner_id.supplier == False or self.partner_id.customer == False):
                self.partner_id = False
            domain = [('supplier', '=', True), ('customer', '=', True), ('is_company', '=', True)]
            return {'domain': {'partner_id': domain,
                               }}

    @api.multi
    def action_a_facturer(self):
        for inv in self:
            if inv.state == 'draft':
                inv.state = 'a_facturer'
                # inv.date_invoice = fields.Date.context_today(self)
            else:
                inv.state = 'proforma_a_facturer'

            inv.refused = False

    @api.multi
    def action_cancel(self):
        for inv in self:
            super(AccountInvoice, self).action_cancel()
            inv.refused = False

    @api.model
    def create(self, vals):
        if 'type' in vals.keys() and vals['type'] in ['out_refund', 'in_refund',
                                                      'out_invoice', 'in_invoice'] and 'origin' in vals.keys():
            if vals['type'] in ['out_refund', 'in_refund']:
                origine = self.search([('number', '=', vals['origin'])], limit=1)
            elif vals['type'] == 'out_invoice':
                origine = self.env['sale.order'].search([('name', '=', vals['origin'])], limit=1)
            elif vals['type'] == 'in_invoice':
                origine = self.env['purchase.order'].search([('name', '=', vals['origin'])], limit=1)
            if origine and origine.salon_id:
                vals['salon_id'] = origine.salon_id.id

        invoice = super(AccountInvoice, self).create(vals)
        # if invoice.type == 'in_invoice' and invoice.amount_total < 0:
        #     invoice.type = 'in_refund'
        return invoice

    #     Vérification des étapes de validité
    @api.multi
    def default_validity(self):
        uid = self._context.get('uid')
        if uid:

            for account_invoice in self:
                profil = self.env['res.users'].browse(uid).profile_type_id.code
                code = account_invoice.salon_id.name.code
                code_sect = account_invoice.gle_section_id.code_sect
                if account_invoice.create_uid.id == 1:
                    account_invoice.step_validity_1 = True
                else:
                    if code in ['SDEPAR', 'SDENAN', 'CGRDAF']:
                        if code_sect in ['PRD200', 'MKD400', 'SDE600', 'SDE610'] and profil == "CS":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRD210', 'CMD410'] and profil == "CG":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRDW310'] and profil == "DO":
                            account_invoice.step_validity_1 = True
                        else:
                            account_invoice.step_validity_1 = False

                    elif code in ['SDELYO', 'SDEMAR', 'FORNAF']:
                        if code_sect in ['PRD200', 'MKD400', 'SDE600'] and profil == "CS":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRD210', 'CMD410', 'SDE610'] and profil == "CG":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRDW310'] and profil == "DO":
                            account_invoice.step_validity_1 = True
                        else:
                            account_invoice.step_validity_1 = False

                    elif code in ['DVLPMT', 'SDEW00'] and profil == "DO":
                        account_invoice.step_validity_1 = True

                    elif code in ['SDEHDP']:
                        if code_sect in ['PRD200', 'MKD400', 'SDE600'] and profil == "CS":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRD210', 'CMD410'] and profil == "CG":
                            account_invoice.step_validity_1 = True
                        elif code_sect in ['PRDW310', 'SDE610'] and profil == "DO":
                            account_invoice.step_validity_1 = True
                        else:
                            account_invoice.step_validity_1 = False
                    else:
                        account_invoice.step_validity_1 = False
        return True

    step_validity_1 = fields.Boolean(string='Step1 validity', compute="default_validity")

    @api.model
    def default_validity_2(self):
        uid = self._context.get('uid')
        if uid:
            for account_invoice in self:
                profil = self.env['res.users'].browse(uid).profile_type_id.code
                code = account_invoice.salon_id.name.code
                code_sect = account_invoice.gle_section_id.code_sect
                if account_invoice.create_uid.id == 1:
                    account_invoice.step_validity_2 = True
                else:
                    if code in ['DVLPMT', 'SDEW00', 'SDEHDP'] and profil == "DBU":
                        account_invoice.step_validity_2 = True

                    elif code in ['CGRDAF', 'SDENAN', 'SDEPAR']:
                        if code_sect in ['PRD200', 'PRD210', 'MKD400', 'CMD410', 'SDE600', 'SDE610'] and profil == "DO":
                            account_invoice.step_validity_2 = True
                        elif code_sect in ['PRDW310'] and profil == "DBU":
                            account_invoice.step_validity_2 = True
                        else:
                            account_invoice.step_validity_2 = False

                    elif code in ['SDELYO', 'SDEMAR']:
                        if code_sect in ['PRD200', 'PRD210', 'MKD400', 'SDE600'] and profil == "CG":
                            account_invoice.step_validity_2 = True
                        elif code_sect in ['CMD410', 'SDE610'] and profil == "DO":
                            account_invoice.step_validity_2 = True
                        elif code_sect in ['PRDW310'] and profil == "DBU":
                            account_invoice.step_validity_2 = True
                        else:
                            self.step_validity_2 = False
                    elif code in ['FORNAF']:
                        if code_sect in ['PRD200', 'MKD400', 'CMD410', 'SDE600'] and profil == "CG":
                            account_invoice.step_validity_2 = True
                        elif code_sect in ['PRD210', 'SDE610'] and profil == "DO":
                            account_invoice.step_validity_2 = True
                        elif code_sect in ['PRDW310'] and profil == "DBU":
                            account_invoice.step_validity_2 = True
                        else:
                            account_invoice.step_validity_2 = False
                    else:
                        account_invoice.step_validity_2 = False

    step_validity_2 = fields.Boolean(string='Step2 validity', compute="default_validity_2")

    @api.model
    def default_validity_3(self):
        uid = self._context.get('uid')
        if uid:
            for account_invoice in self:
                profil = self.env['res.users'].browse(uid).profile_type_id.code
                if account_invoice.state != "finalstep" and account_invoice.create_uid.id == 1:
                    account_invoice.step_validity_3 = True
                else:
                    if account_invoice.state != "finalstep" and profil == "DBU":
                        account_invoice.step_validity_3 = True
                    else:
                        account_invoice.step_validity_3 = False

    step_validity_3 = fields.Boolean(string='Step3 validity', compute="default_validity_3")

    @api.model
    def default_validity_4(self):
        uid = self._context.get('uid')
        if uid:
            for account_invoice in self:
                profil = self.env['res.users'].browse(uid).profile_type_id.code
                if account_invoice.state != "finalstep" and account_invoice.create_uid.id == 1:
                    account_invoice.step_validity_4 = True
                else:
                    if account_invoice.state != "finalstep" and profil == "DG":
                        account_invoice.step_validity_4 = True
                    else:
                        account_invoice.step_validity_4 = False

    step_validity_4 = fields.Boolean(string='Step4 validity', compute="default_validity_4")

    @api.multi
    def finalStep_action(self):
        self.state = 'finalstep'

    @api.multi
    def action_account_invoice_refus(self):
        return {
            'name': _('Refuser la Facture'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'gle.account.invoice.refus',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {'montant_ttc': self.amount_total}
        }

    @api.multi
    def finalize_invoice_move_lines(self, move_lines, ):
        move_lines = super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
        if self.type_facturation == 'm_echange' and self.type == 'out_invoice':
            for ml in move_lines:
                account_id = ml[2].get('account_id')
                if self.env['account.account'].browse([account_id]).type == 'receivable':
                    ml[2].update({'debit': self.amount_untaxed,
                                  'name': '/ HT'})
                    dupp = ml[2].copy()
                    break
            if dupp:
                dupp.update({'debit': self.amount_tax,
                             'name': '/ TVA'})
                move_lines.append((0, 0, dupp))
        return move_lines

    def get_validationdate(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()

    def compute_account_invoice_name(self, account_invoice, invoice_type):

        account_invoice_obj = self.env['account.invoice']

        if account_invoice.salon_id.id:
            salon_id = account_invoice.salon_id.id

            if invoice_type in ['out_invoice', 'out_refund']:
                account_invoice_count = account_invoice_obj.search_count(
                    [('salon_id', '=', salon_id), ('type', 'in', ['out_invoice', 'out_refund']),
                     ('state', 'in', ['open', 'paid'])])
            elif invoice_type in ['in_invoice', 'in_refund']:
                account_invoice_count = account_invoice_obj.search_count(
                    [('salon_id', '=', salon_id), ('type', 'in', ['in_invoice', 'in_refund']),
                     ('state', 'in', ['open', 'paid'])])

            chrono_projet_str = "00{0}".format(str(salon_id))

            if account_invoice_count == 0:
                account_invoice_str = "0001"
            else:
                account_invoice_str = "0000{0}".format(account_invoice_count + 1)

            if invoice_type == "out_invoice":
                return "F{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

            elif invoice_type == "out_refund":
                return "A{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

            elif invoice_type in ['in_invoice', 'in_refund']:
                return "H{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])
