# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import time
import datetime

import logging

_logger = logging.getLogger(__name__)

FIELDS_MODIFIED = ['quantity', 'price_unit', 'discount_type', 'invoice_line_tax_id', 'salon_id', 'gle_section_id',
                   'nature_id', 'type_id', 'prestation_id', 'product_id', 'account_id']

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    move_prov_origin_id = fields.Many2one('account.move', string="Piece comptable provision")

    @api.multi
    def write(self, vals):
        ret = super(AccountInvoiceLine, self).write(vals)
        for record in self:
            for _field in FIELDS_MODIFIED:
                if _field in vals.keys():
                    record.invoice_id._modifier = 'Yes'
        return ret

    @api.model
    def create(self, vals):
        ret = super(AccountInvoiceLine, self).create(vals)
        for _field in FIELDS_MODIFIED:
            if _field in vals.keys():
                if ret.invoice_id._modifier == 'no':
                    ret.invoice_id._modifier = 'Yes'
        return ret

    @api.multi
    def unlink(self):
        ret = super(AccountInvoiceLine, self).unlink()
        for record in self:
            record.invoice_id._modifier = 'Yes'
        return ret
