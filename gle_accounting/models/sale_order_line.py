# -*- coding: utf-8 -*-

from openerp import  models, fields, api
from openerp.tools.translate import _
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    type_facturation = fields.Selection(selection=[ ('normal', 'Classique'),
                                                    ('intercos', 'Intercos'),
                                                    ('r_echange', 'Echange des règlements'),
                                                    ('m_echange', 'Echange des marchandises')    ], string="Type de facturation")

    @api.onchange('type_facturation', 'salon_id')
    def onchange_type_facturation(self):
        domain = [('article_state_adv', '=', 'valid'),('sale_ok', '=', True)]
        if self.type_facturation == 'intercos':
            # domain.append(('categ_id.intercos', '=', True))
            domain.append(('intercos', '=', True))
        if self.salon_id:
            domain.append(('salon_id', '=', self.salon_id.id)) 
        return {'domain':{'product_id':  domain }} 