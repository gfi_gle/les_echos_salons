from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class SaleAdvancePaymentInv(osv.osv_memory):
    _inherit = "sale.advance.payment.inv"
    
    def _get_acompte_product(self, cr, uid, context=None):
        try:
            product = self.pool.get('product.product').search(cr, uid, [('for_acompte','=',True),('name','=','Acompte')], context=context, limit=1)
            if len(product)>0:
                product = product[0]
        except ValueError:
            return False
        return product
    _defaults = {
                 'product_id': lambda self, cr, uid, c=None: self._get_acompte_product(cr, uid, context=c) or self._get_advance_product(cr, uid, context=c),
    }
    