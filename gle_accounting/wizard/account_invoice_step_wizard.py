# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class AccountInvoiceStepOne(models.TransientModel):
    _name = 'account.invoice.step.one'
    _description = 'Step one validation'

    invoice_id = fields.Many2one('account.invoice', string='Invoice', default=lambda self: self.env.context.get('active_id', False), )
    comment = fields.Text(string='Commentaire', )

    @api.multi
    def step_one_action(self):
        self.invoice_id.comment_step1 = self.comment
        self.invoice_id.state = 'step1'
        self.invoice_id._modifier = 'no'


class AccountInvoiceStepTwo(models.TransientModel):
    _name = 'account.invoice.step.two'
    _description = 'Step two validation'

    invoice_id = fields.Many2one('account.invoice', string='Invoice', default=lambda self: self.env.context.get('active_id', False), )
    comment = fields.Text(string='Commentaire', )


    @api.multi
    def final_step_action(self):
        self.invoice_id.state = 'finalstep'

    @api.multi
    def step_two_action(self):
        self.invoice_id.comment_step2 = self.comment
        xamount_wf_validate = self.invoice_id.salon_id.name.montant_valid_two
        if xamount_wf_validate > 0:
            if self.invoice_id.amount_total < xamount_wf_validate:
                self.final_step_action()
            else:
                self.invoice_id.state = 'step2'
        else:
            self.final_step_action()

class AccountInvoiceStepThree(models.TransientModel):
    _name = 'account.invoice.step.three'
    _description = 'Step three validation'

    invoice_id = fields.Many2one('account.invoice', string='Invoice', default=lambda self: self.env.context.get('active_id', False), )
    comment = fields.Text(string='Commentaire', )


    @api.multi
    def final_step_action(self):
        self.invoice_id.state = 'finalstep'

    @api.multi
    def step_three_action(self):
        self.invoice_id.comment_step3 = self.comment
        xamount_wf_validate = self.invoice_id.salon_id.name.montant_valid_three
        if xamount_wf_validate > 0:
            if self.invoice_id.amount_total <= xamount_wf_validate:
                self.final_step_action()
            else:
                self.invoice_id.state = 'step3'
        else:
            self.final_step_action()


class AccountInvoiceStepFour(models.TransientModel):
    _name = 'account.invoice.step.four'
    _description = 'Step four validation'

    invoice_id = fields.Many2one('account.invoice', string='Invoice', default=lambda self: self.env.context.get('active_id', False), )
    comment = fields.Text(string='Commentaire', )


    @api.multi
    def final_step_action(self):
        self.invoice_id.state = 'finalstep'

    @api.multi
    def step_four_action(self):
        self.invoice_id.comment_step4 = self.comment
        xamount_wf_validate = self.invoice_id.salon_id.name.montant_valid_four
        if xamount_wf_validate > 0:
            if self.invoice_id.amount_total > xamount_wf_validate:
                self.final_step_action()
            else:
                self.invoice_id.state = 'step4'
                self.final_step_action()
        else:
            self.invoice_id.state = 'step4'
            self.final_step_action()