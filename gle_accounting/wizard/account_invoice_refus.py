from openerp import fields, models, api
from openerp.tools.translate import _


class AccountInvoiceRefus(models.Model):
    _name = "gle.account.invoice.refus"
        
    motif_refus_facture = fields.Text('Motif', required=True)
    
    @api.one
    def refus_facture(self):
        context = self._context
        if context.get("type") and context.get("type") == "in_invoice":
            montant = context.get('montant','')
            invoice_id = self.env["account.invoice"].browse(context.get("active_id"))
            if invoice_id:
                if montant < 10000:
                    invoice_id.state = "draft"
                        
                elif montant >= 10000 and montant <= 50000:
                    invoice_id.state = "draft"
                        
                else:
                    invoice_id.state = "draft"

        elif context.get("type") and context.get("type") == "out_invoice":
            invoice_id = self.env["account.invoice"].browse(context.get("active_id"))
            invoice_id.state = "draft"
            
        inv_obj = self.env['account.invoice']
        inv_ids = self._context.get('active_ids', [])
        inv_obj = inv_obj.search([('id', 'in', inv_ids)])
        inv_obj.write({'comment' : self.motif_refus_facture, 'refused' : True})
        return True
