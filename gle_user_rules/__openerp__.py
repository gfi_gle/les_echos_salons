# -*- coding: utf-8 -*-
{
    'name': "gle_user_rules",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'gle_adv',
                'gle_crm',
                'gle_salon',
                'gle_accounting',
                'gle_accounting_extended',
                'gle_products',
                'gle_reservation_salon',
                'sale',
                'crm',
                'gfi_state_fr',
                'account_followup',
                'gle_suivi_charges_ca',
                'hr',
                'partner_unique_id',
                'sale_invoice_check_payment',
                'sale_invoice_bank_payment',
                'super_calendar',
                'account',
                'purchase',
                'stock',
                'purchase_tracker',
                'account_voucher',
                'account_voucher_sepa',
                'account_payment',
                'sale_invoice_bank_tracker_afb120',
                'sale_invoice_bank_payment_extended',
                ],

    # always loaded
    'data': [
        'security/general_group_security.xml',
        'security/dgm620_group_security.xml',
        'security/sde600_group_security.xml',
        'security/sde630_group_security.xml',
        'security/dsi_group_security.xml',
        'security/sde610_group_security.xml',
        'security/adv_group_security.xml',
        'security/cg_group_security.xml',
        'security/prd200_group_security.xml',
        'security/prd210_group_security.xml',
        'security/mkd400_group_security.xml',
        'security/cmd410_group_security.xml',
        'security/gle_controle_group_security.xml',
        'security/account_group_security.xml',
        'security/sales_record_rules.xml',
        'security/salon_group_security.xml',
        'security/record_rules_security.xml',
        'security/ir.model.access.csv',
        'views/sale_menu_item.xml',
        'views/general_menu_item.xml',
        'views/salon_menu_item.xml',
        'views/crm_menu_item.xml',
        'views/purchase_menu_item.xml',
        'views/gle_products_menu_item.xml',
        'views/purchase_tracker_menu_item.xml',
        'views/gle_reservation_menu_item.xml',
        'views/account_menu_item.xml',
        'views/report_menu_item.xml',
    ],
    'demo': [
    ],
}