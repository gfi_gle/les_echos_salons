# -*- coding: utf-8 -*-
import logging

_logger = logging.getLogger(__name__)


class ComputeNumerotation(object):
    """ Class to compute all differentes numerotation """

    def __init__(self, env):
        self.env = env

    def compute_purchase_order_number(self, order):
        """ Compute numerotation number for purchase.order and refund.to.receive """

        po_obj = self.env['purchase.order'].sudo()
        refund_obj = self.env['gle.refund.to.receive'].sudo()

        po_count = po_obj.search_count([('salon_id', '=', order.salon_id.id)])
        refund_count = refund_obj.search_count([('salon_id', '=', order.salon_id.id)])

        chrono_project = "00{0}".format(order.salon_id.id)
        sum_count = int(po_count) + int(refund_count)

        order_number_str = "0000{0}".format(sum_count)

        if sum_count == 0:
            order_number_str = "0001"

        order_number_str_final = "B{0}{1}".format(chrono_project[-3:], order_number_str[-4:])

        while True:
            po_name_exist = po_obj.search([('name', '=', order_number_str_final)])
            refund_name_exist = refund_obj.search([('name', '=', order_number_str_final)])

            if not po_name_exist and not refund_name_exist:
                break

            new_count = "0000{0}".format(str(int(order_number_str_final[-4:]) + 1))

            order_number_str_final = "B{0}{1}".format(chrono_project[-3:], new_count[-4:])

        return order_number_str_final

    def compute_sale_order_aae_name(self, order, type_creation=None):
        """
            Compute sale.order and gle.account.refund name, type parameter defined if method compute is on_change method.
            Parameter type='change' is a parameter for _on_change method.
            Parameter type='write' is a parameter for write method of sale.order.
        """

        gle_account_refund_obj = self.env['gle.account.refund'].sudo()
        sale_order_obj = self.env['sale.order'].sudo()

        if order.salon_id.id:
            sale_order_count = sale_order_obj.search_count([('salon_id', '=', order.salon_id.id)])
            account_refund = gle_account_refund_obj.search_count([('salon_id', '=', order.salon_id.id), ('state', 'in', ['open', 'done'])])
            account_refund_cancel = gle_account_refund_obj.search_count([('salon_id', '=', order.salon_id.id), ('state', '=', 'cancel'), ('name', '!=', False)])

            chrono_project = "00{0}".format(order.salon_id.id)
            sum_count = sale_order_count + account_refund + account_refund_cancel

            if sum_count == 0:
                sale_order_number_str = "0001"
            elif sum_count > 0 and type_creation == 'create':
                sale_order_number_str = "0000{0}".format(sum_count)
            elif sum_count > 0 and type_creation in ['on_change', 'account_refund']:
                sale_order_number_str = "0000{0}".format(sum_count + 1)

            sale_order_number_str_final = "D{0}{1}".format(chrono_project[-3:], sale_order_number_str[-4:])

            while True:

                account_refund_name_exist = gle_account_refund_obj.search([('name', '=', sale_order_number_str_final)])
                sale_order_name_exist = sale_order_obj.search([('name', '=', sale_order_number_str_final)])

                if not account_refund_name_exist and not sale_order_name_exist:
                    break

                new_count = "0000{0}".format(str(int(sale_order_number_str_final[-4:]) + 1))
                sale_order_number_str_final = "D{0}{1}".format(chrono_project[-3:], new_count[-4:])

            return sale_order_number_str_final
