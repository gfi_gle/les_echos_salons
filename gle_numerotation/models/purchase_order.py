# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from .ComputeNumerotation import ComputeNumerotation
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'

    @api.model
    def create(self, vals):

        new_purchase_order = super(PurchaseOrder, self).create(vals)

        vals['order_line'] = False
        new_purchase_order.write(vals=vals, new_order=new_purchase_order)

        return new_purchase_order

    @api.multi
    def write(self, vals, new_order=None):

        if new_order:
            computar = ComputeNumerotation(env=self.env)
            vals['name'] = computar.compute_purchase_order_number(order=self)

        return super(PurchaseOrder, self).write(vals)

