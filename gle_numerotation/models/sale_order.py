# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp.addons.gle_numerotation.models.ComputeNumerotation import ComputeNumerotation
from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):

    _inherit = 'sale.order'

    name = fields.Char('Order Reference', required=True, copy=False, readonly=False, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, select=True)

    @api.model
    def create(self, vals):
        """ Override method create of sale.order to save the echo name pattern for quote """
        sale_order_created = super(SaleOrder, self).create(vals)
        # Use write method to save the echo name pattern for quote
        vals['order_line'] = False

        self.search_status_partner(new_obj=sale_order_created)

        return sale_order_created

    @api.onchange('name')
    def _on_change_name(self):
        """ Compute name of quote when name change """

        computar = ComputeNumerotation(env=self.env)
        self.name = computar.compute_sale_order_aae_name(order=self, type_creation='on_change')

    @api.onchange('salon_id')
    def _on_change_salon_id(self):
        """ Compute name of quote when salon is select or change """

        computar = ComputeNumerotation(env=self.env)
        self.name = computar.compute_sale_order_aae_name(order=self, type_creation='create')

    def compute_name(self):

        computar = ComputeNumerotation(env=self.env)
        return computar.compute_sale_order_aae_name(order=self, type_creation='create')
