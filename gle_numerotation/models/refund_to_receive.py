# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from .ComputeNumerotation import ComputeNumerotation
import logging

_logger = logging.getLogger(__name__)


class RefundToReceive(models.Model):

    _inherit = 'gle.refund.to.receive'

    @api.model
    def create(self, vals):

        new_refund = super(RefundToReceive, self).create(vals)
        vals['refund_line'] = False
        new_refund.write(vals=vals, new_refund=new_refund)
        return new_refund

    @api.multi
    def write(self, vals, new_refund=None):

        if new_refund:
            computar = ComputeNumerotation(env=self.env)
            vals['name'] = computar.compute_purchase_order_number(order=new_refund)

        return super(RefundToReceive, self).write(vals)


