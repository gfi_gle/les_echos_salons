# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.model
    def create(self, vals):

        new_obj = super(AccountInvoice, self).create(vals)
        vals['invoice_line'] = False
        invoice_type = vals.get('type', False)

        if not invoice_type:
            invoice_type = self._context.get('default_type')

        new_obj.write(vals, new_obj=new_obj, invoice_type=invoice_type)

        return new_obj

    @api.multi
    def write(self, vals, new_obj=None, invoice_type=None):

        if invoice_type:
            result = self.compute_account_invoice_number(new_obj, invoice_type=invoice_type)
            vals['invoice_line'] = False

        return super(AccountInvoice, self).write(vals)

    @api.multi
    def name_get(self):

        super(AccountInvoice, self).name_get()
        res = []

        for account_invoice in self:

            if account_invoice.type in ['in_invoice', 'in_refund']:
                name = account_invoice.supplier_invoice_number
            else:
                name = account_invoice.internal_number

            res.append((account_invoice.id, name))

        return res

    def compute_account_invoice_number(self, account_invoice, invoice_type):

        account_invoice_obj = self.env['account.invoice']

        if account_invoice.salon_id.id:
            salon_id = account_invoice.salon_id.id

            if invoice_type in ['out_invoice', 'out_refund']:
                account_invoice_count = account_invoice_obj.search_count(
                    [('salon_id', '=', salon_id), ('type', 'in', ['out_invoice', 'out_refund']),
                     ('state', 'in', ['open', 'paid'])])
            elif invoice_type in ['in_invoice', 'in_refund']:
                account_invoice_count = account_invoice_obj.search_count(
                    [('salon_id', '=', salon_id), ('type', 'in', ['in_invoice', 'in_refund']),
                     ('state', 'in', ['open', 'paid'])])

            chrono_projet_str = "00{0}".format(str(salon_id))

            if account_invoice_count == 0:
                account_invoice_str = "0001"
            else:
                account_invoice_str = "0000{0}".format(account_invoice_count)

            if invoice_type == "out_invoice":
                return "F{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

            elif invoice_type == "out_refund":
                return "A{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])

            elif invoice_type in ['in_invoice', 'in_refund']:
                return "H{0}{1}".format(chrono_projet_str[-3:], account_invoice_str[-4:])


