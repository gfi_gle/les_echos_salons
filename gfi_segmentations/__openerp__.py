# -*- coding: utf-8 -*-
{
    'name': "gfi_segmentation",

    'summary': """
       CRM -  Segmentation """,

    'description': """
        CRM -  Segmentation
    """,

    'author': "Gfi Informatique",
    'website': "www.gfi.world",

    'category': 'CRM',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'gle_crm',
                'gle_user_rules',
                ],

    # always loaded
    'data': [
        'security/gfi_segmentations_security.xml',
        'security/ir.model.access.csv',
        'views/crm_segmentation.xml',
        'views/contacts_segmentation.xml',
        'wizard/segmentation_wizard.xml',
        'views/crm_segmentation_type.xml',
    ],
    'demo': [
    ],
}
