# -*- coding: utf-8 -*-
import codecs
import datetime
from pytz import timezone
from openerp.addons.account_export.util.unicode_csv import (
    UnicodeWriter,
)
from openerp import models
from openerp import api
from openerp import fields
from openerp import exceptions
from openerp import _
from openerp.addons.account_export.util.timezone import (
    system_timezone_datetime
)
from openerp.exceptions import ValidationError

import base64
import datetime
from openerp.osv import osv, expression
from openerp.addons.web import http
from openerp.exceptions import except_orm, Warning
import openerp.addons.web.http as oeweb
from openerp import models, fields, api, _
import openerp
from operator import attrgetter

try:
    import xlwt
except ImportError:
    xlwt = None
import re
from cStringIO import StringIO

STATES = dict(readonly=False, states={'done': [('readonly', True)]})
FILTER = ['company_name_ids',
          'company_cp',
          'company_city',
          'company_state_ids',
          'company_parent_id',
          'company_country_ids',
          'company_type_company_id',
          'company_sector_one_lvl_id',
          'company_sector_two_lvl_id',
          'company_sector_three_lvl_id',
          'company_commercial_allocation_ids',
          'company_centers_of_interest_ids',
          'contact_name_ids',
          'contact_function',
          'contact_function',
          'contact_title',
          'contact_key_words_ids',
          'contact_type_contact',
          'contact_civility_ids',
          'contact_category_id',
          'contact_commercial_allocation_ids',
          'contact_date_departue',
          'segmentations_ids']


class CrmSegmentationsType(models.Model):
    _name = "crm.segmentations.type"
    _description = "Partner Segmentation type"

    name = fields.Char('Name', required=True)

    _sql_constraints = [
        ('unique_name', 'unique(name)', u"Type doit être unique"),
    ]


class CrmSegmentations(models.Model):
    _name = "crm.segmentations"
    _description = "Partner Segmentation"

    state = fields.Selection([('draft', 'Brouillon '),
                              ('done', 'Terminé')], 'Status', default='draft', widget="statusbar")

    name = fields.Char('Nom', required=True, help='Nom de la segmentation')
    type_id = fields.Many2one("crm.segmentations.type", required=True, string="Type")
    description = fields.Text('Description')

    # -COMPANY- #
    company_name_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='segmentations_partners_company',
        column1='segmentation_id',
        column2='partner_id',
        string="Raison sociale", domain=[('is_company', '=', True)], **STATES)
    company_cp = fields.Char('CP', **STATES)
    company_city = fields.Char('Ville', **STATES)
    company_state_ids = fields.Many2many('res.country.state', 'segmentation_state_rel', 'segmentation_id', 'state_id',
                                         'States', **STATES)
    company_parent_id = fields.Many2many(
        comodel_name='res.partner',
        relation='segmentation_company_parent',
        column1='segmentation_id',
        column2='parent_id_id',
        domain=[('is_company', '=', True)],
        string="company parent", **STATES)
    company_country_ids = fields.Many2many(
        comodel_name='res.country',
        relation='segmentations_countrys',
        column1='segmentation_id',
        column2='country_id',
        string="Countrys", **STATES)
    company_type_company_id = fields.Many2many(
        comodel_name='gle_crm.type_company',
        relation='company_segmentations_type',
        column1='segmentation_id',
        column2='contact_type_id',
        string="Type de société", **STATES)

    company_sector_one_lvl_id = fields.Many2many(
        comodel_name="gle_crm.sector",
        relation='segmentation_sectors_one',
        column1="segmentation_one_id",
        column2="secteurs_one_id",
        string="Secteur 1", **STATES)
    company_sector_two_lvl_id = fields.Many2many(
        comodel_name="gle_crm.sector",
        relation='segmentation_sectors_two',
        column1="segmentation_two_id",
        column2="secteurs_two_id",
        string="Secteur 2", **STATES)
    company_sector_three_lvl_id = fields.Many2many(
        comodel_name="gle_crm.sector",
        relation='segmentation_sectors_three',
        column1="segmentation_three_id",
        column2="secteurs_three_id",
        string="Secteur 3", **STATES)
    company_commercial_allocation_ids = fields.Many2many(
        comodel_name='res.users',
        relation='segmentations_commercials_allocation',
        column1='segmentation_id',
        column2='commercial_id',
        string="Commercials", **STATES)
    company_centers_of_interest_ids = fields.Many2many(
        comodel_name='gle_crm.centers_of_interest',
        relation='segmentations_interets',
        column1='segmentation_id',
        column2='interet_id',
        string="Centres d'intérêt", **STATES)

    # -CONTACTS- #
    contact_name_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='segmentations_contacts_partners',
        column1='segmentation_id',
        column2='partner_id',
        string="Nom", domain=[('is_company', '=', False)], **STATES)
    contact_function = fields.Many2many(
        comodel_name='gle_crm.function',
        relation="segmentations_function",
        column1='segmentation_id',
        column2='function_id', string="Fonction", **STATES)
    contact_title = fields.Char("Titre", **STATES)
    contact_key_words_ids = fields.Many2many(
        comodel_name='gle_crm.key_words',
        relation='segmentations_keys',
        column1='segmentation_id',
        column2='key_id',
        string="Mots clés", **STATES)
    contact_type_contact = fields.Many2many(
        comodel_name='gle_crm.type_contact',
        relation='segmentations_type_contact',
        column1='segmentation_id',
        column2='contact_id',
        string="Décideur", **STATES)
    contact_civility_ids = fields.Selection([('mlle', "Mademoiselle"),
                                             ('mme', "Madame"),
                                             ('mr', "Monsieur"),
                                             ('dr', "Docteur"),
                                             ('pr', "Professeur"),
                                             ('me', "Maître")], string="Civilité", **STATES)

    contact_category_id = fields.Many2many(
        comodel_name='res.partner.category',
        relation='res_segmentations_partner_category',
        column1='segmentation_id',
        column2='category_id',
        string="RP", **STATES)

    contact_commercial_allocation_ids = fields.Many2many(
        comodel_name='res.users',
        relation='crm_segmentations_contact_commercial',
        column1='segmentation_id',
        column2='commercial_id',
        string="Commercial allocation", **STATES)
    contact_date_departue = fields.Date(string="Date de départ", **STATES)

    # -segmentatios- #
    segmentations_ids = fields.Many2many(comodel_name='crm.segmentations',
                                         relation='segmentations_segmentations',
                                         column1='segmentation_ids',
                                         column2='segmentations_ids',
                                         string="Segmentations", **STATES)

    result_ids = fields.Many2many(comodel_name='res.partner',
                                  relation='segmentations_partner_id',
                                  column1='segmentation_ids',
                                  column2='partner_ids',
                                  string="result segmentations", store=True)
    add_ids = fields.Many2many(comodel_name='res.partner',
                               relation='segmentations_add_id',
                               column1='segmentation_ids',
                               column2='partner_add_ids',
                               string="result segmentations",
                               compute='_segmentation_count')

    del_ids = fields.Many2many(comodel_name='res.partner',
                               relation='segmentations_del_id',
                               column1='segmentation_ids',
                               column2='partner_add_ids',
                               string="result segmentations",
                               compute='_segmentation_count')

    @api.depends(lambda self: FILTER)
    @api.multi
    def _segmentation_count(self):
        for obj in self:
            partner_obj = obj.env['res.partner']
            segmentations_obj = self.env[('crm.segmentations')]
            segmentation = segmentations_obj.search([('name', '=', obj.name)])
            all_partners = partner_obj.search(obj.get_domain())
            obj.add_ids = all_partners - segmentation.result_ids
            obj.del_ids = all_partners & segmentation.result_ids

            obj.segmentation_count_add = len(obj.add_ids)
            obj.segmentation_count_del = len(obj.del_ids)

    segmentation_count_add = fields.Integer(compute='_segmentation_count')
    segmentation_count_del = fields.Integer(compute='_segmentation_count')
    segmentation_count = fields.Integer('result')

    @api.multi
    def open_contacts_list(self):
        for obj in self:
            [action] = self.env.ref("gle_crm.gle_action_partner_form").read()
            tree_id = self.env.ref("gfi_segmentations.crm_segmentation_contacts_tree_view").id
            action['context'] = {"default_segmentation_id": obj.id}
            action['domain'] = [('id', 'in', obj.result_ids.ids)]
            action['views'] = [(tree_id, "tree"), (False, "form")]
            return action

    @api.multi
    def get_domain(self):
        partner_obj = self.env['res.partner']
        for obj in self:
            domain = []
            # -*- COMPANY -*-#
            if obj.company_name_ids:
                domain.append(('parent_id.id', 'in', obj.company_name_ids.ids))
            if obj.company_cp:
                domain.append(('parent_id.zip', '=', obj.company_cp))
            if obj.company_city:
                domain.append(('parent_id.city', '=', obj.company_city))
            if obj.company_state_ids:
                domain.append(('parent_id.state_id.id', 'in', obj.company_state_ids.ids))
            if obj.company_parent_id:
                domain.append(('parent_id.parent_id.id', 'in', obj.company_parent_id.ids))
                print domain
            if obj.company_country_ids:
                domain.append(('country_id', 'in', obj.company_country_ids.ids))
            if obj.company_type_company_id:
                domain.append(('parent_id.type_company_id', 'in', obj.company_type_company_id.ids))
                print domain
            if obj.company_sector_one_lvl_id:
                domain.append(('parent_id.sector_lvl_one_id.id', 'in', obj.company_sector_one_lvl_id.ids))
                print domain
            if obj.company_sector_two_lvl_id:
                domain.append(('parent_id.sector_lvl_two_id.id', 'in', obj.company_sector_two_lvl_id.ids))
                print domain
            if obj.company_sector_three_lvl_id:
                domain.append(('parent_id.sector_lvl_three_id.id', 'in', obj.company_sector_three_lvl_id.ids))
                print domain
            if obj.company_centers_of_interest_ids:
                domain.append(('parent_id.centers_of_interest_ids.id', 'in', obj.company_centers_of_interest_ids.ids))
                print domain
            if obj.company_commercial_allocation_ids:
                domain.append(
                    ('parent_id.commercial_allocation_ids.id', 'in', obj.company_commercial_allocation_ids.ids))
                print domain
            # -*- CONTACTS -*-#
            if obj.contact_name_ids:
                domain.append(('id', 'in', obj.contact_name_ids.ids))
                print domain
            if obj.contact_function:
                domain.append(('gle_function.id', 'in', obj.contact_function.ids))
                print domain
            if obj.contact_title:
                domain.append(('title_echos', '=', obj.contact_title))
                print domain
            if obj.contact_key_words_ids:
                domain.append(('key_words_ids.id', 'in', obj.contact_key_words_ids.ids))
                print domain
            if obj.contact_category_id:
                domain.append(('category_id.id', 'in', obj.contact_category_id.ids))
            if obj.contact_type_contact:
                domain.append(('type_contact.id', 'in', obj.contact_type_contact.ids))
                print domain
            if obj.contact_civility_ids:
                domain.append(('civility', '=', obj.contact_civility_ids))
            if obj.segmentations_ids:
                domain = expression.OR([domain, [('id', 'in', obj.segmentations_ids.mapped('result_ids.id'))]])
            print domain
            if obj.contact_commercial_allocation_ids:
                domain.append(('commercial_allocation_ids.id', 'in', obj.contact_commercial_allocation_ids.ids))
                print domain
            if obj.contact_date_departue:
                domain.append(('date_departue', '=', obj.contact_date_departue))
                print domain
            if not domain:
                domain = [('id', '=', -1)]
            return domain

    @api.multi
    def action_add(self):
        partner_obj = self.env['res.partner']
        for obj in self:
            obj.result_ids = obj.result_ids | partner_obj.search(obj.get_domain())
            obj.segmentation_count = len(obj.result_ids)
            obj.vider_attr()

    @api.multi
    def action_del(self):
        partner_obj = self.env['res.partner']
        for obj in self:
            obj.result_ids = obj.result_ids - partner_obj.search(obj.get_domain())
            obj.segmentation_count = len(obj.result_ids)
            obj.vider_attr()

    @api.multi
    def vider_attr(self):
        for obj in self:
            for field in FILTER:
                obj[field] = False

    @api.multi
    def get_segmentation_file(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/export_segmentations/%s' % (self.id),
            'target': 'self',

        }

    @api.model
    def create(self, vals):
        if self.search([('name', '=', vals['name'])]):
            raise ValidationError(_("Le nom %s de la segmentation existe déjà" % vals['name']))
        return super(CrmSegmentations, self).create(vals)

    @api.multi
    @api.constrains('result_ids')
    def constrains_result_ids(self):
        for obj in self:
            if obj.state == 'done':
                raise ValidationError(_("Vous ne pouvez pas modifier une segmentation validé"))

    @api.multi
    def action_done(self):
        for obj in self:
            obj.state = 'done'

    _sql_constraints = [
        ('unique_name', 'unique(name)', u"Le nom de la segmentation doit être unique"),
    ]
