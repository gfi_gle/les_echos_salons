# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2012 Domsense srl (<http://www.domsense.com>)
#    Copyright (C) 2012-2013:
#        Agile Business Group sagl (<http://www.agilebg.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
try:
    import json
except ImportError:
    import simplejson as json

import openerp.http as http
from openerp.http import request
from openerp.addons.web.controllers.main import ExcelExport


class ExcelExportView(ExcelExport):
    def _get_civility(self, _civility):
        value = ''
        if _civility == 'mme':
            value = "Madame",
        if _civility == 'mr':
            value = "Monsieur",
        if _civility == 'dr':
            value = "Docteur",
        if _civility == 'pr':
            value = "Professeur",
        if _civility == 'me':
            value = "Maître",

        print  value
        print type(value)
        return value[0] if value else ''

    @http.route('/export_segmentations/<int:seg_id>', type='http', auth='user')
    def export_segmentations(self, seg_id):
        columns_headers = []
        rows = []
        seg_name = "vide.xls"
        if request.env.user.has_group('gfi_segmentations.group_segmentation_manager'):
            seg = request.env['crm.segmentations'].browse(int(seg_id))
            seg_name = seg.name
            columns_headers = ['CIVILITE', u'Prènom', 'Nom', 'Rue', 'Rue (suite)', 'Rue 3', 'Code Postal', 'Ville',
                               'Pays',
                               'Courriel', u'Tél', u'Tél. portable', 'Commercial allocation', 'Fonction', 'Titre',
                               u'Mots clés', 'Type de contact',
                               u'Société parente/Nom', u'Société parente/Rue', u'Société parente/Rue (suite)',
                               u'Société parente/Rue3',
                               u'Société parente/Code Postal', u'Société parente/Ville', u'Société parente/Pays',
                               u'Société parente/Secteur 1', u'Société parente/Secteur 2', u'Société parente/Secteur 3',
                               u'Société parente/Centres d\'intérêt', u'Société parente/Type de société',
                               u'Société parente/Tél', u'Société parente/Courriel']
            rows = []

            for res in seg.result_ids:
                key_words = ''
                commercial_allocation = ''
                sector_1 = ''
                sector_2 = ''
                sector_3 = ''
                centers_of_interest = ''
                for item in res.key_words_ids:
                    key_words = key_words + str(item.name) + ', '
                key_words = key_words.rstrip(', ')
                for item in res.commercial_allocation_ids:
                    commercial_allocation = commercial_allocation + str(item.name) + ', '
                commercial_allocation = commercial_allocation.rstrip(', ')
                for item in res.parent_id.sector_lvl_one_id:
                    sector_1 = sector_1 + str(item.name) + ', '
                sector_1 = sector_1.rstrip(', ')
                for item in res.parent_id.sector_lvl_two_id:
                    sector_2 = sector_2 + str(item.name) + ', '
                sector_2 = sector_2.rstrip(', ')
                for item in res.parent_id.sector_lvl_three_id:
                    sector_3 = sector_3 + str(item.name) + ', '
                sector_3 = sector_3.rstrip(', ')
                for item in res.parent_id.centers_of_interest_ids:
                    centers_of_interest = centers_of_interest + str(item.name) + ', '
                centers_of_interest = centers_of_interest.rstrip(', ')
                row = [res.civility and self._get_civility(res.civility).decode('utf-8') or '',
                       res.first_name and res.first_name.decode('utf-8') or '',
                       res.name_gle and res.name_gle.decode('utf-8') or '',
                       res.street and res.street.decode('utf-8') or '',
                       res.street2 and res.street2.decode('utf-8') or '',
                       res.street3 and res.street3.decode('utf-8') or '',
                       res.zip and res.zip.decode('utf-8') or '',
                       res.city and res.city.decode('utf-8') or '',
                       res.country_id.name and res.country_id.name.decode('utf-8') or '',
                       res.email and res.email.decode('utf-8') or '', res.phone or '', res.mobile or '',
                       commercial_allocation and commercial_allocation.decode('utf-8') or '',
                       res.gle_function.name and res.gle_function.name.decode('utf-8') or '',
                       res.title_echos and res.title_echos.decode('utf-8') or '',
                       key_words and key_words.decode('utf-8') or '',
                       res.type_contact.name and res.type_contact.name.decode('utf-8') or '',
                       res.parent_id.name and res.parent_id.name.decode('utf-8') or '',
                       res.parent_id.street and res.parent_id.street.decode('utf-8') or '',
                       res.parent_id.street2 and res.parent_id.street2.decode('utf-8') or '',
                       res.parent_id.street3 and res.parent_id.street3.decode('utf-8') or '',
                       res.parent_id.zip and res.parent_id.zip.decode('utf-8') or '',
                       res.parent_id.city and res.parent_id.city.decode('utf-8') or '',
                       res.parent_id.country_id.name and res.parent_id.country_id.name.decode('utf-8') or '',
                       sector_1 and sector_1.decode('utf-8') or '',
                       sector_2 and sector_2.decode('utf-8') or '', sector_3 and sector_3.decode('utf-8') or '',
                       centers_of_interest and centers_of_interest.decode('utf-8') or '',
                       res.parent_id.type_company_id.name and res.parent_id.type_company_id.name.decode('utf-8') or '',
                       res.parent_id.phone and res.parent_id.phone.decode('utf-8') or '',
                       res.parent_id.email and res.parent_id.email.decode('utf-8') or '']
                rows.append(row)
        return request.make_response(
            self.from_data(columns_headers, rows),
            headers=[
                ('Content-Disposition', 'attachment; filename="%s.xls"' % seg_name),
                ('Content-Type', self.content_type)
            ],
        )
