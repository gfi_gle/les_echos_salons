# -*- coding: utf-8 -*-


from openerp import api, models, fields


class SegmentationWizard(models.TransientModel):
    _name = "crm.segmentation.wizard"

    partners_ids = fields.Many2many(
        comodel_name='res.partner',
        relation='segmentations_wizard_partners',
        column1='segmentation_wizard_id',
        column2='partner_id',
        string="Contacts sélectionnés",
        default=lambda self: self.env['res.partner'].browse(self.env.context.get('active_ids')).mapped(
            'child_ids').filtered(lambda i: not i.is_company) | self.env['res.partner'].browse(
            self.env.context.get('active_ids')).filtered(lambda i: not i.is_company),
        domain=[('is_company', '=', False)]
    )
    segmentation_id = fields.Many2one('crm.segmentations', 'Segmentation', domain=[('state', '!=', 'done')])
    operation = fields.Selection([('add', 'Ajouter'), ('del', 'Supprimer')])

    @api.multi
    def valider(self):
        for obj in self:
            if obj.operation == 'add':
                obj.segmentation_id.result_ids |= obj.partners_ids
                obj.segmentation_id.segmentation_count = len(obj.segmentation_id.result_ids)
                print len(obj.segmentation_id.result_ids)
                print 'les partenaire sont bien ajouté à la segmentation'

            else:
                obj.segmentation_id.result_ids -= obj.partners_ids
                obj.segmentation_id.segmentation_count = len(obj.segmentation_id.result_ids)

                print 'les partenaire sont bien supprimer de la segmentation'
        [action] = self.env.ref("gfi_segmentations.crm_segmentation_list_action").read()
        action['context'] = {"default_segmentation_id": obj.id, "segmentation_id": obj.id, }
        action['res_id'] = obj.segmentation_id.id
        action['views'] = [(False, "form")]
        return action
