# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.gfi_import_base.wizard.import_base import filter_data, get_file_base64
import logging
import os

from openerp.exceptions import Warning as UserError

logger = logging.getLogger(__name__)

maps = {
    'NAME': 0,
    'NAME_GLE': 1,
    'STREET': 2,
    'STREET2': 3,
    'STREET3': 4,
    'ZIP': 5,
    'CITY': 6,
    'COUNTRY_NAME': 7,
    'DISPLAY_NAME': 8,
    'TYPE_COMPANY': 9,
    'CENTRES_OF_INTERESTS': 10,
    'COMMERCIAL_ALLOCATION': 11,
    'SECTOR_LVL_ONE': 12,
    'SECTOR_LVL_TWO': 13,
    'SECTOR_LVL_THREE': 14,
    'PARENT_NAME': 15,
    'EMAIL': 16,
    'PHONE': 17,
    'MOBILE': 18,
    'FAX': 19,
    'ACCOUNT_TWITTER': 20,
    'ACCOUNT_LINKEDIN': 21,
    'ACCOUNT_FACEBOOK': 22,
    'IS_COMPANY': 23,
    'CUSTOMER': 24,
    'SUPPLIER': 25,
    'EMPLOYEE': 26,
    'ACTIVE': 27,
    'TYPE': 28,
    'TYPE_CONTACT': 29,
    'USE_PARENT_ADDRESS': 30,
    'FIRSTNAME': 31,
    'LASTNAME': 32,
    'FIRST_NAME': 33,
    'CIVILITY': 34,
    'TITLE_ECHOS': 35,
    'FUNCTION': 36,
    'KEY_WORDS': 37,
    'CATEGORY': 38,
    'EXCEL_ID': 39,
}

PARTNER_SHEET_NAME = u'Feuil1'


class ImportBase(models.TransientModel):
    _inherit = 'import.base'

    @api.multi
    def _get_partner_data(self, data_line, archive_files):
        partner_data = {}

        """"NAME"""
        partner_name = filter_data(data_line, maps=maps, col='NAME', return_type='value', ctype='string')

        if not partner_name:
            return False

        """"NAME_GLE"""
        name_gle = filter_data(data_line, maps=maps, col='NAME_GLE', return_type='value', ctype='string')
        partner_data.update({
            'name_gle': name_gle,
        })

        """"STREET"""
        street = filter_data(data_line, maps=maps, col='STREET', return_type='value')
        partner_data.update({
            'street': street,
        })

        """"STREET2"""
        street2 = filter_data(data_line, maps=maps, col='STREET2', return_type='value')
        partner_data.update({
            'street2': street2,
        })

        """"STREET3"""
        street3 = filter_data(data_line, maps=maps, col='STREET3', return_type='value')
        partner_data.update({
            'street3': street3,
        })

        """"ZIP"""
        zip = filter_data(data_line, maps=maps, col='ZIP', return_type='value')
        partner_data.update({
            'zip': zip,
        })

        """"CITY"""
        city = filter_data(data_line, maps=maps, col='CITY', return_type='value')
        partner_data.update({
            'city': city,
        })

        """"COUNTRY"""
        country_name = filter_data(data_line, maps=maps, col='COUNTRY_NAME', return_type='value')
        country = self.env['res.country'].search([('name', 'ilike', country_name)], limit=1)
        if country:
            partner_data.update({
                'country_id': country.id,
            })

        """TYPE_COMPANY"""
        type_company_name = filter_data(data_line, maps=maps, col='TYPE_COMPANY', return_type='value')
        type_company_id = self.env['gle_crm.type_company'].search([('name', '=', type_company_name)])
        if type_company_id:
            partner_data.update({
                'type_company_id': type_company_id.id
            })

        """"CENTRERS_OF_INTEREST"""
        centers_of_interest_names = filter_data(data_line, maps=maps, col='CENTRES_OF_INTERESTS', return_type='value')
        centers_of_interests = centers_of_interest_names and centers_of_interest_names.split(';') or []
        centers_of_interests = self.env['gle_crm.centers_of_interest'].search([('name', 'in', centers_of_interests)])
        if centers_of_interests:
            partner_data.update({
                'centers_of_interest_ids': [(6, 0, centers_of_interests.ids)],
            })

        """"COMMERCIAL_ALLOCATION"""
        commercial_allocation_names = filter_data(data_line, maps=maps, col='COMMERCIAL_ALLOCATION',
                                                  return_type='value')
        commercial_allocations = commercial_allocation_names and commercial_allocation_names.split(';') or []
        commercial_allocations = [item.strip() for item in commercial_allocations]
        commercial_allocations = self.env['res.users'].search([('name', 'in', commercial_allocations)])
        if commercial_allocations:
            partner_data.update({
                'commercial_allocation_ids': [(6, 0, commercial_allocations.ids)],
            })

        """"SECTOR LVL ONE"""
        sector_lvl_one_name = filter_data(data_line, maps=maps, col='SECTOR_LVL_ONE', return_type='value')
        sector_lvl_one = sector_lvl_one_name and sector_lvl_one_name.split(';') or []
        sector_lvl_one = [item.strip() for item in sector_lvl_one]
        sector_lvl_one = self.env['gle_crm.sector'].search([('name', 'in', sector_lvl_one), ('sector_level', '=', '1')])
        if sector_lvl_one:
            partner_data.update({
                'sector_lvl_one_id': [(6, 0, sector_lvl_one.ids)],
            })

        """"SECTOR LVL TWO"""
        sector_lvl_two_name = filter_data(data_line, maps=maps, col='SECTOR_LVL_TWO', return_type='value')
        sector_lvl_two = sector_lvl_two_name and sector_lvl_two_name.split(';') or []
        sector_lvl_two = [item.strip() for item in sector_lvl_two]
        sector_lvl_two = self.env['gle_crm.sector'].search([('name', 'in', sector_lvl_two), ('sector_level', '=', '2')])
        if sector_lvl_two:
            partner_data.update({
                'sector_lvl_two_id': [(6, 0, sector_lvl_two.ids)],
            })

        """"SECTOR LVL THREE"""
        sector_lvl_three_name = filter_data(data_line, maps=maps, col='SECTOR_LVL_THREE', return_type='value')
        sector_lvl_three = sector_lvl_three_name and sector_lvl_three_name.split(';') or []
        sector_lvl_three = [item.strip() for item in sector_lvl_three]
        sector_lvl_three = self.env['gle_crm.sector'].search(
            [('name', 'in', sector_lvl_three), ('sector_level', '=', '3')])
        if sector_lvl_three:
            partner_data.update({
                'sector_lvl_three_id': [(6, 0, sector_lvl_three.ids)],
            })

        """"PARENT"""
        parent_name = filter_data(data_line, maps=maps, col='PARENT_NAME', return_type='value')
        parent = self.env['res.partner'].search([('name', 'ilike', parent_name)], limit=1)
        if parent:
            partner_data.update({
                'parent_id': parent.id,
            })

        """"EMAIL"""
        email = filter_data(data_line, maps=maps, col='EMAIL', return_type='value')
        partner_data.update({
            'email': email,
        })

        """"PHONE"""
        phone = filter_data(data_line, maps=maps, col='PHONE', return_type='value')
        partner_data.update({
            'phone': phone,
        })

        """"MOBILE"""
        mobile = filter_data(data_line, maps=maps, col='MOBILE', return_type='value')
        partner_data.update({
            'mobile': mobile,
        })

        """"FAX"""
        fax = filter_data(data_line, maps=maps, col='FAX', return_type='value')
        partner_data.update({
            'fax': fax,
        })
        """"ACCOUNT_TWITTER"""
        account_twitter = filter_data(data_line, maps=maps, col='ACCOUNT_TWITTER', return_type='value')
        partner_data.update({
            'account_twitter': account_twitter,
        })
        """"ACCOUNT_LINKEDIN"""
        account_linkedin = filter_data(data_line, maps=maps, col='ACCOUNT_LINKEDIN', return_type='value')
        partner_data.update({
            'account_linkedin': account_linkedin,
        })
        """"ACCOUNT_FACEBOOK"""
        account_facebook = filter_data(data_line, maps=maps, col='ACCOUNT_FACEBOOK', return_type='value')
        partner_data.update({
            'account_facebook': account_facebook,
        })

        """"IS_COMPANY"""
        is_company = filter_data(data_line, maps=maps, col='IS_COMPANY', return_type='value', ctype='bool')
        partner_data.update({
            'is_company': is_company,
        })

        """"CUSTOMER"""
        customer = filter_data(data_line, maps=maps, col='CUSTOMER', return_type='value', ctype='bool')
        partner_data.update({
            'customer': customer,
        })

        """"SUPPLIER"""
        supplier = filter_data(data_line, maps=maps, col='SUPPLIER', return_type='value', ctype='bool')
        partner_data.update({
            'supplier': supplier,
        })

        """"EMPLOYEE"""
        employee = filter_data(data_line, maps=maps, col='EMPLOYEE', return_type='value', ctype='bool')
        partner_data.update({
            'employee': employee,
        })

        """"ACTIVE"""
        active = filter_data(data_line, maps=maps, col='ACTIVE', return_type='value', ctype='bool')
        partner_data.update({
            'active': active,
        })

        """"TYPE"""
        type = filter_data(data_line, maps=maps, col='TYPE', return_type='value')
        partner_data.update({
            'type': type,
        })

        """"TYPE_CONTACT"""
        type_contact = filter_data(data_line, maps=maps, col='TYPE_CONTACT', return_type='value')

        if type_contact == 'A QUALIFIER':
            pass

        type_contact_id = self.env['gle_crm.type_contact'].search([('name', '=', type_contact)])

        if type_contact_id:
            partner_data.update({
                'type_contact': type_contact_id.id,
            })

        """"USE_PARENT_ADDRESS"""
        use_parent_address = filter_data(data_line, maps=maps, col='USE_PARENT_ADDRESS', return_type='value',
                                         ctype='bool')
        partner_data.update({
            'use_parent_address': use_parent_address,
        })

        """"FIRSTNAME"""
        firstname = filter_data(data_line, maps=maps, col='FIRSTNAME', return_type='value')
        partner_data.update({
            'firstname': firstname,
        })

        """"LASTNAME"""
        lastname = filter_data(data_line, maps=maps, col='LASTNAME', return_type='value')
        partner_data.update({
            'lastname': lastname,
        })

        """"FIRST_NAME"""
        first_name = filter_data(data_line, maps=maps, col='FIRST_NAME', return_type='value')
        partner_data.update({
            'first_name': first_name,
        })

        """"CIVILITY"""
        civility = filter_data(data_line, maps=maps, col='CIVILITY', return_type='value')
        partner_data.update({
            'civility': civility,
        })

        """"TITLE_ECHOS"""
        title_echos = filter_data(data_line, maps=maps, col='TITLE_ECHOS', return_type='value')
        partner_data.update({
            'title_echos': title_echos,
        })

        """"FUNCTION"""
        function = filter_data(data_line, maps=maps, col='FUNCTION', return_type='value')
        function_id = self.env['gle_crm.function'].search([('name', '=', function)])
        if function_id:
            partner_data.update({
                'function': function_id.id,
            })

        """"KEY_WORDS"""
        key_words_names = filter_data(data_line, maps=maps, col='KEY_WORDS',
                                      return_type='value')
        key_words = key_words_names and key_words_names.split(';') or []
        key_words = [item.strip() for item in key_words]
        key_words = self.env['gle_crm.key_words'].search([('name', 'in', key_words)])
        if key_words:
            partner_data.update({
                'key_words_ids': [(6, 0, key_words.ids)],
            })

        """"CATEGORY"""
        category_name = filter_data(data_line, maps=maps, col='CATEGORY', return_type='value')
        category = self.env['res.partner.category'].search([('name', 'ilike', category_name)], limit=1)
        if category:
            partner_data.update({
                'category_id': category.id,
            })

        """"EXCEL_ID"""
        excel_id = filter_data(data_line, maps=maps, col='EXCEL_ID', return_type='value')
        if excel_id:
            partner_data.update({
                'excel_id': int(excel_id)
            })

        return partner_data

    @api.multi
    def process_partner(self, datas, archive_files):
        partner_env = self.env['res.partner']
        datas = filter_data(datas, sheet_name=PARTNER_SHEET_NAME, row_start=1)
        all_partners = partner_env

        for row_idx in range(datas.get_nrows(PARTNER_SHEET_NAME)):
            data_line = filter_data(datas, row=row_idx)  # We selected the row
            partner_data = self._get_partner_data(data_line, archive_files)

            if not partner_data:
                continue
            # logger.debug('-------------< NAME')
            # logger.debug(partner_data.get('name'))

            partners = partner_env.create(partner_data)
            logger.info('create the partner #%s, row=%s', partners.id, row_idx)

            all_partners |= partners
        return all_partners

    @api.multi
    def show(self, label='', model='', records=None):
        if self.doc_type == 'partner':
            return super(ImportBase, self).show('Clients importés', 'res.partner', records)
        else:
            return super(ImportBase, self).show(label, model, records)

    @api.multi
    def process_data(self, datas, archive_files):
        res = super(ImportBase, self).process_data(datas, archive_files)
        if self.doc_type == 'partner':
            return self.process_partner(datas, archive_files)
        return res
