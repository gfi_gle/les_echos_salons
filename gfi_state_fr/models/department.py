# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.tools import float_compare
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class Department(models.Model):

    _name = 'department'

    name = fields.Char(string="Nom du département")

    number_department = fields.Char(string="Numéro de département")
    state_id = fields.Many2one(comodel_name='res.country.state', string="Région")