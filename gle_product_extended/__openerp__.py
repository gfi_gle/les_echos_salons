# -*- coding: utf-8 -*-
{
    'name': "gle_product_extended",

    'summary': """Extends odoo's products""",

    'description': """

    """,

    'author': "Gfi",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'gle_salon','gle_reservation_salon','gle_crm'],

    # always loaded
    'data': [
        'views/menu.xml',
        'security/ir.model.access.csv',
        'views/product.xml',
        'views/type_article_echos_views.xml',
        'views/product_template_kanban.xml',
        # 'data/article_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/article_demo_data.xml',
    ],
}