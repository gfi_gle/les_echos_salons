# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.osv import expression
from openerp.tools.translate import _
from operator import itemgetter
from openerp import SUPERUSER_ID
import logging

_logger = logging.getLogger(__name__)

class ProductProduct(models.Model):
    """"""

    _inherit = 'product.template'

