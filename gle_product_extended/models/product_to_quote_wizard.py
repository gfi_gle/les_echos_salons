# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class ProductToQuoteWizard(models.TransientModel):
    """ Model representing model transient wizard to create a quote from product view or add a product in existing quote """

    _name = 'gle_product_extended.product_to_quote_wizard'

    def _init_product_id(self):
        return self._context.get('product_id')

    partner_id = fields.Many2one('res.partner', string="Société")
    sale_order_id = fields.Many2one('sale.order', string="Devis")

    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    product_id = fields.Many2one('product.product', default=_init_product_id, string="Article")
    quantity = fields.Integer(string="Quantité")
    discount_line = fields.Float(string="Remise")

    @api.onchange('discount_line')
    def on_change(self):
        """"""

        if self.product_id.commision_state == 'noncommissionnable' and self.discount_line != 0:
            self.discount_line = None

            res = {'warning': {
                'title': _('Warning'),
                'message': _(("Vous ne pouvez pas faire une remise sur un produit non commissionnable"))
            }}

            return res

    @api.multi
    def action_add_product_to_quote(self):
        """ Method triggered by button on wizard view to add a product in existing quote """

        sale_order_line_obj = self.env['sale.order.line']
        sale_order_line_list =  sale_order_line_obj.search([('order_id', '=', self.sale_order_id.id), ('product_id', '=', self.product_id.id)])

        test_upadte = False
        for sale_order_line in sale_order_line_list:

            vals = {
                'product_uos_qty': self.quantity,
                'product_uom_qty': self.quantity,
            }
            test_upadte = True
            sale_order_line.write(vals)

        if test_upadte is False:
            vals = {
                'order_id': self.sale_order_id.id,
                'order_partner_id': self.partner_id.id,
                'product_id': self.product_id.id,
                'product_uos_qty': self.quantity,
                'product_uom_qty': self.quantity,
            }

            sale_order_line_obj.create(vals)

    @api.multi
    def action_create_quote(self):
        """  Method triggered by button on wizard view to create a quote and add product """

        vals_order = {
            'partner_id': self.partner_id.id,
            'salon_id': self.salon_id.id
        }

        sale_order_created = self.env['sale.order'].create(vals_order)

        vals_order_line = {
            'name': self.product_id.name,
            'product_id': self.product_id.id,
            'order_id': sale_order_created.id,
            'product_uom_qty': self.quantity,
            'discount_line': self.discount_line
        }

        self.env['sale.order.line'].create(vals_order_line)

        return {
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'res_id': sale_order_created.id
        }