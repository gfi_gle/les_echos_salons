# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)


class Product(models.Model):
    """ Inherit product.template Model to add attributes """

    _inherit = 'product.template'
    _description = "Inherit product.template and adds new fields"

    salon_id = fields.Many2one('gle_salon.salon', string="Salon")
    salon_concat = fields.Char(string="salon_concat")

    intercos = fields.Boolean('Intercos', help="", default=False)

    purchase_ok = fields.Boolean('Can be Purchased', help="Specify if the product can be selected in a purchase order line.", default=False)
    sale_ok = fields.Boolean('Can be Sold', help="Specify if the product can be selected in a sales order line.", default=True)


    article_state_adv = fields.Selection([('draft', "Brouillon"),('valid', "Validé")], default="draft", string='Statut ADV')

    state_article = fields.Selection([('recurrent', "Récurrent"),('surmesure', "Sur mesure"),], string='Statut d\'article')
    commision_state = fields.Selection([('commissionnable', "Commissionnable"),('noncommissionnable', "Non commissionnable"),('commissionnable_a', "Non Commissionnable Aménagement")], default='', string="Article commissionnable")
    type_article_echos = fields.Many2one(comodel_name='gle_product_extended.type_article_echos', string="Type d'article Les echos")

    line_article_crm_id = fields.One2many(comodel_name='gle_crm.line_articles', inverse_name='product_product_id')

    type = fields.Selection([('product', 'Stockable Product'), ('consu', 'Consumable'), ('service', 'Service')],
                             'Product Type', required=True, default='service',
                             help="Consumable: Will not imply stock management for this product. \nStockable product: Will imply stock management for this product.")

    @api.onchange('salon_id')
    def _salon_concat(self):
        self.salon_concat = '{}{}{}'.format(self.salon_id.name.name, ' - ', self.salon_id.millesime.name)

class TypeArticleEchos(models.Model):
    """ Model representing type d'article les Echos """

    _name = 'gle_product_extended.type_article_echos'

    _sql_constraints = [('name_unique',
                         'UNIQUE(name)',
                         "Le libellé du type d'article est unique.")]

    name = fields.Char(string="Libellé du type d'article")


class ValidateProduct(models.TransientModel):
    _name = "validate.product"

    @api.multi
    def test_validate(self):
        list_ids = self._context.get('active_ids')
        for article_id in self.env['product.template'].browse(list_ids):
            article_id.article_state_adv = 'valid'



class DuplicateProduct(models.TransientModel):
    _name = "duplicate.product"
    salon_id = fields.Many2one('gle_salon.salon')

    @api.multi
    def article_dupplication(self):
        list_ids = self._context.get('active_ids')
        article_object = self.env['product.template']
        for article_id in self.env['product.template'].browse(list_ids):
            vals={
                "warranty":article_id.warranty,
                "uos_id":article_id.uos_id.id,
                "list_price":article_id.list_price,
                "weight":article_id.weight,
                "color":article_id.color,
                "image":article_id.image,
                "mes_type":article_id.mes_type,
                "uom_id":article_id.uom_id.id,
                "description_purchase":article_id.description_purchase,
                "uos_coeff":article_id.uos_coeff,
                "sale_ok":article_id.sale_ok,
                "categ_id":article_id.categ_id.id,
                "product_manager":article_id.product_manager,
                "message_last_post":article_id.message_last_post,
                "company_id":article_id.company_id.id,
                "state":article_id.state,
                "uom_po_id":article_id.uom_po_id.id,
                "description_sale":article_id.description_sale,
                "description":article_id.description,
                "weight_net":article_id.weight_net,
                "volume":article_id.volume,
                "active":article_id.active,
                "rental":article_id.rental,
                "image_medium":article_id.image_medium,
                "name":article_id.name,
                "type":article_id.type,
                "image_small":article_id.image_small,
                "track_all":article_id.track_all,
                "loc_row":article_id.loc_row,
                "track_outgoing":article_id.track_outgoing,
                "loc_rack":article_id.loc_rack,
                "loc_case":article_id.loc_case,
                "track_incoming":article_id.track_incoming,
                "sale_delay":article_id.sale_delay,
                "purchase_ok":article_id.purchase_ok,
                "commision_state":article_id.commision_state,
                "article_state_adv":"draft",
                "type_article_echos":article_id.type_article_echos.id,
                "salon_id":self.salon_id.id,
                "state_article":article_id.state_article}

            new_object_article=article_object.create(vals)
            new_object_article.write({'name':article_id.name})


