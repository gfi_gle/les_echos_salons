# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class ProductReservationFermeWizard(models.TransientModel):
    """"""

    _name = 'gle_product_extended.product_reservation_ferme_wizard'

    product_id = fields.Many2one('product.template', string="Article")
    partner_id = fields.Many2one('res.partner', string="Société")
    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    main_conference_partner_id = fields.Many2one('res.partner', string="Contact conférence principale")
    conference_partner_ids = fields.Many2many(comodel_name='res.partner', relation="gle_reservation_salon_res_partner_conference_wizard_rel", column1="reservation_id", column2="res_partner_id",string="Autre(s) Contact(s) Conférence")

    product_category_echo = fields.Char(compute='_compute_category_product')
    state_ware = fields.Selection([('void', ''), ('yes', 'Oui'), ('no', 'Non')], default='void', string="Etat (Echange marchandise)")

    @api.multi
    def action_ferme_reservation_salon(self):
        """"""

        sql_qery_insert = 'INSERT INTO gle_reservation_salon_res_partner_conference_rel (reservation_id, res_partner_id) VALUES ({},{});'

        reservation_obj = self.env['gle_reservation_salon.reservation_salon'].search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id), ('product_id', '=', self.product_id.id)])

        if reservation_obj.state == 'ferme':
            raise ValidationError("La réservation que vous éssayez de modifier est déja en état FERME")

        if self.state_ware == 'void':
            raise ValidationError("Veuillez saisir une valeur pour le champs Etat 'Echange marchandise'.")

        if reservation_obj:
            vals = {}

            if self.main_conference_partner_id:
                main_conference_partner = self.main_conference_partner_id.id

                vals.update({'main_conference_partner_id': main_conference_partner})

            vals.update({'state_ware': self.state_ware,'state': 'ferme'})
            _logger.debug(vals)
            reservation_obj.write(vals)

        else:

            raise ValidationError("Aucune réservation ne correspond à votre recherche.")

    @api.onchange('product_id', 'partner_id', 'salon_id')
    def _compute_category_product(self):
        """"""

        reservation_obj = self.env['gle_reservation_salon.reservation_salon'].search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id),('product_id', '=', self.product_id.id)])

        if reservation_obj:
            self.product_category_echo = reservation_obj.product_category_echo
