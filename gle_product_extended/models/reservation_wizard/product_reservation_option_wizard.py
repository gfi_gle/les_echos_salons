# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class ProductReservationOptionWizard(models.TransientModel):
    """ Model representing model transient wizard to create a reservation.salon from product view """

    _name = 'gle_product_extended.product_reservation_wizard'

    salon_id = fields.Many2one('gle_salon.salon', string="Salon")

    product_salon_code = fields.Char(compute='compute_salon_code', string="Code produit")
    project_code = fields.Char(compute='compute_project_code', string="Code projet")

    partner_id = fields.Many2one('res.partner', string="Client (Donneur d'ordre)")

    product_id = fields.Many2one('product.template', string="Article")

    commercial_partner_id = fields.Many2one('res.partner', string="Contact commercial")

    commercial_echo_id = fields.Many2one('res.users', string="Commercial les Echos")

    product_category_echo = fields.Selection([('atelier', 'Atelier'), ('conference', 'Conférence'), ('pleniere', 'Plénière')])

    @api.depends('salon_id')
    def compute_salon_code(self):

        self.product_salon_code =  self.salon_id.name.code

    @api.depends('salon_id')
    def compute_project_code(self):

        self.project_code = self.salon_id.millesime.code

    @api.multi
    def action_create_reservation_salon(self):
        """  Method triggered by button on wizard view to create a quote and add product """

        reservation_obj = self.env['gle_reservation_salon.reservation_salon'].search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id), ('product_id', '=', self.product_id.id)])

        if not reservation_obj:

            vals = {
                'salon_id': self.salon_id.id,
                'product_salon_code': self.product_salon_code,
                'project_code': self.project_code,
                'partner_id': self.partner_id.id,
                'product_id': self.product_id.id,
                'commercial_partner_id': self.commercial_partner_id.id,
                'commercial_echo_id': self.commercial_echo_id.id,
                'product_category_echo': self.product_category_echo,
                'state': 'option'
            }

            self.env['gle_reservation_salon.reservation_salon'].create(vals)

        else:

            raise ValidationError("Une réservation pour cet article et ce client éxiste déja.")

    @api.onchange('partner_id')
    def on_change_partner(self):

        self.commercial_partner_id = None
