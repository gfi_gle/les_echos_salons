# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
import logging

_logger = logging.getLogger(__name__)

class ProductReservationFermeToOptionWizard(models.TransientModel):
    """"""

    _name = 'gle_product_extended.product_reservation_ferme_to_option_wizard'

    salon_id = fields.Many2one('gle_salon.salon', string="Projet/Produit")
    partner_id = fields.Many2one('res.partner', string="Société")
    product_id = fields.Many2one('product.product', string="Article")

    product_salon_code = fields.Char(compute='compute_salon_code', string="Code produit")
    project_code = fields.Char(compute='compute_project_code', string="Code projet")


    @api.depends('salon_id')
    def compute_salon_code(self):
        self.product_salon_code = self.salon_id.name.code

    @api.depends('salon_id')
    def compute_project_code(self):
        self.project_code = self.salon_id.millesime.code

    def button_action_ferme_to_option_product(self):
        """"""

        _logger.debug('------------>TEST 01')
        reservation_obj = self.env['gle_reservation_salon.reservation_salon'].search([('salon_id', '=', self.salon_id.id), ('partner_id', '=', self.partner_id.id), ('product_id', '=', self.product_id.id)])
        _logger.debug(reservation_obj)
        if reservation_obj:
            _logger.debug('------------>TEST 02')
            reservation_obj.action_ferme_to_option_product()
        else:

            raise ValidationError("Aucune réservation ne correspond à votre recherche.")