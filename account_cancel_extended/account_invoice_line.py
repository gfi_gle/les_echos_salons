from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic





class account_invoice_line(models.Model):
    __metaclass__ = MetaAnalytic
    _name = "account.invoice.line"
    _inherit = "account.invoice.line"

    _analytic = 'account_invoice_line'
