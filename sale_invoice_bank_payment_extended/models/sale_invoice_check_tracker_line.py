# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from .sales_invchecktracker import OUT_INVOICE, OUT_REFUND, INVOICE_STATES
from .sales_invchecktracker import is_refund, is_invoice
from openerp import exceptions


class SaleInvoiceCheckTrackerLine(models.Model):
    _inherit = 'sale.invoice_check_tracker.line'

    move_line_id = fields.Many2one('account.move.line', string='Account move line', required=False, )
    date_due = fields.Date(related='move_line_id.date_maturity', )
    residual = fields.Float(compute='_compute_residual', )
    amount_total = fields.Float(related='move_line_id.amount_currency', )

    @api.multi
    @api.depends('move_line_id')
    def _compute_residual(self):
        for obj in self:
            obj.residual = - obj.move_line_id.amount_residual if is_refund(
                obj.invoice_id) else obj.move_line_id.amount_residual

    @api.constrains('tracker_id', 'invoice_id')
    def tracker_invoice_unicity(self):
        for record in self:
            # if len(
            #         record.tracker_id.tracker_line_ids.
            #                 filtered(lambda r: r.invoice_id.id == self.invoice_id.id)
            # ) > 1:
            #     raise ValueError(
            #         _(
            #             "There must be only one tracker line per invoice."
            #         )
            #     )

            t = record.tracker_id
            i = record.invoice_id

            valid_invoice = (
                (
                    (
                        t.tracker_validated and
                        i.state in ['open', 'paid']
                    ) or
                    i.state == 'open'
                ) and i.type in INVOICE_STATES and
                (
                    i.partner_id.id == t.client_id.id or
                    not t.has_client_id
                )
            )

            if not valid_invoice:
                raise exceptions.ValidationError(_(
                    'An invalid invoice has been inserted.\n \n'
                    'An invoice must be in the open state. \n'
                    'It must be an invoice or a refund accordingly to the '
                    'type of invoice, which has been selected. \n'
                    'It must have been made in the name of the client, who '
                    'has been selected.'
                ))

    @api.onchange('invoice_id')
    def _onchange_invoice_id(self):
        if self.invoice_id:
            self.move_line_id = False

    @api.onchange('move_line_id')
    def _onchange_move_line_id(self):
        if self.move_line_id and self.invoice_id:
            move_line = self.move_line_id
            invoice = self.invoice_id
            self.amount_retained = -move_line.amount_residual if is_refund(invoice) else move_line.amount_residual
        else:
            self.amount_retained = 0
