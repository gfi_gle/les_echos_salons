# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp import exceptions
from openerp.tools import float_is_zero

OUT_REFUND, OUT_INVOICE = 'out_refund', 'out_invoice'
INVOICE_STATES = [OUT_REFUND, OUT_INVOICE]
is_refund = lambda inv: inv.type == OUT_REFUND
is_invoice = lambda inv: inv.type == OUT_INVOICE


class SaleInvoiceBankTracker(models.Model):
    _inherit = 'sale.invoice_bank_tracker'

    invoice_type = fields.Selection([
        ('out_invoice', 'Facture'),
        ('out_refund', 'Avoir et facture'),
    ], string='Invoice type', )

    computed_invoice_total_amount = fields.Float(compute='_get_total_invoice_amounts', )

    @api.depends('tracker_line_ids.invoice_id.residual')
    @api.one
    def _get_total_invoice_amounts(self):
        """Sum sale invoice amounts and compute a delta."""
        for record in self:
            sum_refund = sum(record.account_invoice_ids.filtered(lambda i: i.type == OUT_REFUND).mapped('residual'))
            sum_invoice = sum(record.account_invoice_ids.filtered(lambda i: i.type == OUT_INVOICE).mapped('residual'))
            record.computed_invoice_total_amount = sum_invoice - sum_refund

    @api.multi
    def delete_tracker_lines(self):
        self.tracker_line_ids.unlink()

    @api.multi
    def fill_tracker_lines(self):
        """Create tracker lines for all available invoices associated to a
        client.
        """

        self.ensure_one()

        if self.client_id:
            invoice_type = INVOICE_STATES if self.invoice_type == 'out_refund' else ['out_invoice']
            domain = [
                ('state', '=', 'open'),
                ('type', 'in', invoice_type),
                ('partner_id', '=', self.client_id.id),
            ]

            invoices = \
                self.env['account.invoice'].search(domain)

            self.tracker_line_ids = [(5,)]
            tracker_line_ids = []
            for invoice in invoices:
                for move_line in invoice.mapped('move_id.line_id').filtered(
                        lambda line: line.account_id.type == 'receivable'):
                    tracker_line_ids.append((0, 0, {
                        'invoice_id': invoice.id,
                        'move_line_id': move_line.id,
                        'amount_retained': -move_line.amount_residual if invoice.type == OUT_REFUND else move_line.amount_residual,
                    }))

            self.tracker_line_ids = tracker_line_ids

    def _get_name_move_bank(self):

        account_voucher_obj = self.env['account.voucher']
        get_journal_bnk_by_code = self.env['account.journal']._get_journal_bnk_by_code

        encais_cb_les_id = get_journal_bnk_by_code('5BKEC')
        encais_cheque_les_id = get_journal_bnk_by_code('5BKEH')
        encais_virm_etra_id = get_journal_bnk_by_code('5BKEVE')
        encais_virm_fran_id = get_journal_bnk_by_code('5BKEVF')
        decais_cb_les_id = get_journal_bnk_by_code('5BKDC')
        decais_cheque_les_id = get_journal_bnk_by_code('5BKDH')
        decais_virm_id = get_journal_bnk_by_code('5BKDV')

        pay_encais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cb_les_id)])
        pay_encais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', encais_cheque_les_id)])
        pay_encais_virm_etra_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_etra_id)])
        pay_encais_virm_fran_ids = account_voucher_obj.search([('journal_id', '=', encais_virm_fran_id)])

        pay_decais_cb_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cb_les_id)])
        pay_decais_cheque_les_ids = account_voucher_obj.search([('journal_id', '=', decais_cheque_les_id)])
        pay_decais_virm_ids = account_voucher_obj.search([('journal_id', '=', decais_virm_id)])

        bank_invoice_count = sum(
            [len(pay_encais_cb_les_ids), len(pay_encais_cheque_les_ids), len(pay_encais_virm_etra_ids),
             len(pay_encais_virm_fran_ids), len(pay_decais_cb_les_ids), len(pay_decais_cheque_les_ids),
             len(pay_decais_virm_ids)])

        bank_invoice_count += 1

        chrono_projet_str = "000"

        if bank_invoice_count == 0:
            bank_invoice_str = "0001"
        else:
            bank_invoice_str = "0000{0}".format(bank_invoice_count)

        return "B{0}{1}".format(chrono_projet_str[-3:], bank_invoice_str[-4:])

    @api.one
    def pay_invoices(self):
        """Generate a payment when all invoices are validated.
        """

        if self.state != 'validated':
            raise exceptions.Warning(_('Non-validated bank tracker.'))

        # Ensure all accounting invoices are ready for payment.
        if self.account_invoice_ids.filtered(
                lambda invoice: invoice.state == 'draft'
        ):
            raise exceptions.Warning(_('Some invoices are still draft.'))
        if self.account_invoice_ids.filtered(
                lambda invoice: invoice.state == 'paid'
        ):
            raise exceptions.Warning(_(
                'Some invoices have already been paid.'
            ))

        # Gather some data.
        first_invoice = self.account_invoice_ids[0]
        payment_method = self.bank_tracker_payment_method_id

        if not payment_method:
            raise exceptions.Warning(_(
                'No payment method has been defined for bank trackers.'
            ))

        amount = self.amount

        journal = payment_method.sales_invoices_bank_payment_journal_id
        partner = self.env['res.partner']._find_accounting_partner(
            first_invoice.partner_id,
        )

        # Init accessors with various potentially useful context values.
        context = {
            'invoice_id': self.id,
            'invoice_type': first_invoice.type,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_expected_currency': first_invoice.currency_id.id,
            'type': 'receipt',
        }
        voucher_obj = self.env['account.voucher'].with_context(context)
        vline_obj = self.env['account.voucher.line'].with_context(context)

        payment_method = self.env['crm.payment.mode'].search(
            [('technical_name', '=', 'VIR')], limit=1,
        )

        # Values to create the voucher with.
        voucher_values = {
            'account_id': journal.default_debit_account_id.id,
            'amount': amount,
            'currency_id': first_invoice.currency_id.id,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_option': 'without_writeoff',
            'pre_line': True,
            'reference': self._get_name_move_bank(),
            'number': self._get_name_move_bank(),
            'name': first_invoice.name,
            'type': 'receipt',
            'date_origin_document': self.date,
            'sales_payment_method_id': payment_method.id or False,
        }

        # When a write-off accounting account is defined, allow write-offs.
        if payment_method.sales_invoices_bank_payment_write_off_account_id:
            voucher_values.update({
                'comment': _('Write-off'),
                'payment_option': 'with_writeoff',
                'writeoff_acc_id': (
                    payment_method.
                        sales_invoices_bank_payment_write_off_account_id.id
                ),
            })

        voucher = voucher_obj.create(voucher_values)

        # Include lines by hand, to avoid the voucher selection magic. Done in
        # a secondary step so workflows update correctly.
        # Mostly imitate account.voucher::recompute_voucher_lines for the
        # details of account.voucher.line creation.
        for tracker_line in self.tracker_line_ids:

            invoice = tracker_line.invoice_id

            lines = invoice.move_id.line_id.filtered(
                lambda r: not (
                    r.state != 'valid' or
                    r.account_id.type != 'receivable' or
                    r.reconcile_id
                )
            )

            if not lines:
                continue

            line = lines[0]

            vline_vals = {
                'account_id': line.account_id.id,
                'amount': abs(tracker_line.amount_retained),
                'currency_id': line.currency_id.id,
                'date_due': line.date_maturity,
                'date_original': line.date,
                'move_line_id': line.id,
                'name': line.name,
                'reconcile': True,
                'type': 'cr' if tracker_line.amount_retained > 0 else 'dr',
                'voucher_id': voucher.id,
            }
            vline_obj.create(vline_vals)
        voucher.signal_workflow('proforma_voucher')
        if voucher.move_id.state == 'draft':
            voucher.move_id.button_validate()

    @api.one
    def wkf_validated(self):
        """ Override method to skip gap control """

        if not self.account_invoice_ids:
            raise exceptions.Warning(_('No invoice to pay.'))

        if float_is_zero(self.amount, 2):
            raise exceptions.Warning(_('Impossible de valider le tracker, car sa valeur est nulle.'))

        self.state = 'validated'

        self.pay_invoices()
