# -*- coding: utf-8 -*-

{
    'name': 'Bank Tracker Extended',
    'version': '8.0.1.0.0',
    'category': 'Accounting',
    'sequence': 75,
    'summary': 'Bank tracker Extended',
    'author': 'GFI Informatique, (GFI Informatique)',
    'website': 'http://gfi.world',
    'images': [],
    'description': """
Bank Tracker Extended
=====================
Extend the bank tracker
        """,
    'depends': ['sale_invoice_check_payment'],
    'data': [
        'views/sale_invoice_bank_tracker.xml',
        'views/sale_invoice_check_tracker.xml',
        # 'workflows/sales_invchecktracker.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
