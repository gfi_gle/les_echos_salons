from openerp import fields, models, api
from openerp.tools.translate import _

class PurchaseOrderRefus(models.Model):
    _name = "purchase.order.refus"
        
    motif_refus = fields.Text('Motif', required=True)
    
    @api.one
    def action_refus(self):
        inv_obj = self.env['purchase.order']
        inv_ids = self._context.get('active_ids', [])
        inv_obj = inv_obj.search([('id', 'in', inv_ids)])
        inv_obj.write({'comment' : self.motif_refus, 'refused' : True})
        return True
    
    