from openerp.tests.common import SingleTransactionCase, TransactionCase


class TestSalonFy(TransactionCase):
    def setUp(self):
        super(TestSalonFy, self).setUp()
        self.fy = self.env['account.fiscalyear']
        self.salon = self.env['gle_salon.salon']
        self.ml = self.env['gle_salon.millesime']
        self.produit = self.env['gle_salon.produit']
        self.partner = self.env.user.partner_id

    def test_2020(self):
        fy2020 = self.fy.create({
            'name': '2020',
            'code': '2020',
            'date_start': '2020-01-01',
            'date_stop': '2020-12-31',
        })
        fy2020.state = 'done'
        self.assertEqual(fy2020.state, 'done', 'Fy not done')
        self.assertEqual(fy2020.name, '2020', 'Not 2020')
        ml2020 = self.ml.create({
            'name': '2020',
            'code': '2020',
        })
        self.assertEqual(ml2020.name, '2020', 'Not 2020')
        produit2020 = self.produit.create({
            'name': '2020',
            'code': '2020',
        })
        self.assertEqual(produit2020.name, '2020', 'Not 2020')
        salon2020_data = {
            'name': produit2020.id,
            'millesime': ml2020.id,
            'date_realisation': '2020-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2020 = self.salon.create(salon2020_data)

        # Main Tests
        self.assertTrue(salon2020.is_clos, 'Salon is not clos')

    def test_2021(self):
        fy2021 = self.fy.create({
            'name': '2021',
            'code': '2021',
            'date_start': '2021-01-01',
            'date_stop': '2021-12-31',
        })
        self.assertEqual(fy2021.state, 'draft', 'Fy not draft')
        self.assertEqual(fy2021.name, '2021', 'Not 2021')
        ml2021 = self.ml.create({
            'name': '2021',
            'code': '2021',
        })
        self.assertEqual(ml2021.name, '2021', 'Not 2021')
        produit2021 = self.produit.create({
            'name': '2021',
            'code': '2021',
        })
        self.assertEqual(produit2021.name, '2021', 'Not 2021')
        salon2021_data = {
            'name': produit2021.id,
            'millesime': ml2021.id,
            'date_realisation': '2021-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2021 = self.salon.create(salon2021_data)

        # Main Tests
        self.assertFalse(salon2021.is_clos, 'Salon is not clos')

    def test_annee(self):
        ml2021 = self.ml.create({
            'name': '2021',
            'code': '2021',
        })
        produit2021 = self.produit.create({
            'name': '2021',
            'code': '2021',
        })
        salon2021_data = {
            'name': produit2021.id,
            'millesime': ml2021.id,
            'date_realisation': '2021-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2021 = self.salon.create(salon2021_data)

        # Main Tests
        self.assertEqual(salon2021.annee, ml2021.name, 'Year of salon and mellisime are not the same')

    def test_next(self):
        ml2020 = self.ml.create({
            'name': '2020',
            'code': '2020',
        })
        produit = self.produit.create({
            'name': '2020',
            'code': '2020',
        })
        salon2020_data = {
            'name': produit.id,
            'millesime': ml2020.id,
            'date_realisation': '2020-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2020 = self.salon.create(salon2020_data)

        ml2021 = self.ml.create({
            'name': '2021',
            'code': '2021',
        })

        salon2021_data = {
            'name': produit.id,
            'millesime': ml2021.id,
            'date_realisation': '2021-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2021 = self.salon.create(salon2021_data)

        ml2022 = self.ml.create({
            'name': '2022',
            'code': '2022',
        })

        salon2022_data = {
            'name': produit.id,
            'millesime': ml2022.id,
            'date_realisation': '2022-05-08',
            'user_mail_from_id': self.partner.id,
            'user_mail_to_ids': [(6, 0, [self.partner.id])],
        }
        salon2022 = self.salon.create(salon2022_data)
        # Main Tests
        self.assertEqual(salon2020.next(), salon2021, 'Next of 2020 should be 2021, found %s' % salon2020.next().annee)
        self.assertEqual(salon2021.next(), salon2022, 'Next of 2020 should be 2021, found %s' % salon2021.next().annee)
        self.assertEqual(salon2020.next().next(), salon2022, 'Next of next of 2020 should be 2022, found %s' % salon2020.next().next().annee)
        self.assertEqual(salon2022.previous().previous(), salon2020, 'Previous of previous of 2022 should be 2020, found %s' % salon2022.previous().previous().annee)
