# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime 
class GleNature(models.Model):

    _name = 'gle.nature'
    _sql_constraints = [
        ('code_uniq', 'unique(code_analytique, section_id)', _("Le code Analytique des natures doit être unique par section!")),
        ('code_size', "check(code_analytique in ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','S','T','U','V','W','X','Y','Z' ))", _("Le code Analytique doit être une lettre alphabétique !"))
    ]

    def _get_code(self):
        for obj in self:
            code = ''
            if obj.section_id:
                code = code + str(obj.section_id.code) + '.' + str(obj.code_analytique) 
                obj.code_compose =code
                
    code_compose = fields.Char('Code Composé', compute='_get_code')
    code_analytique = fields.Char(string="Ordre d'affichage", required=True)
    name = fields.Char("Libellé opérationnel", required=True)
    section_id = fields.Many2one("gle.section", string="Section", ondelete='cascade')
    categ_ids = fields.One2many("product.category",'nature_id', string="Categories")   
    type_ids = fields.One2many("gle.type",'nature_id', string="Natures")
    
    nature_comptable_id = fields.Many2one("gle.nature.comptable", string="Nature comptable")
    code_nature = fields.Char(related='nature_comptable_id.name_nature',string='Code Nature',)
    name_nature = fields.Char(related='nature_comptable_id.code_nature',string="Libellé Nature")
