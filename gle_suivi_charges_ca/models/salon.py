# -*- coding: utf-8 -*-
from openerp import models, fields, api, _


class Salon(models.Model):
    _inherit = 'gle_salon.salon'

    is_clos = fields.Boolean(string='Is clos', compute='_compute_is_clos', )
    annee = fields.Char(string='Année', related='millesime.name', store=True, )

    @api.multi
    def _compute_is_clos(self):
        for obj in self:
            is_clos = False
            dt = "{year}-01-01".format(year=obj.annee)
            fy_id = self.env['account.fiscalyear'].find(dt=dt, exception=False)
            fy = self.env['account.fiscalyear'].browse(fy_id) if fy_id else False
            if fy and fy.state == 'done':
                is_clos = True
            obj.is_clos = is_clos


    @api.multi
    def next(self):
        self.ensure_one()
        year = int(self.millesime.name) + 1
        produit = self.name
        return self.search([
            ('name', '=', produit.id),
            ('millesime.name', '=', str(year))
        ])


    @api.multi
    def previous(self):
        self.ensure_one()
        year = int(self.millesime.name) - 1
        produit = self.name
        return self.search([
            ('name', '=', produit.id),
            ('millesime.name', '=', str(year))
        ])

