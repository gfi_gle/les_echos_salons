# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime 
from openerp.exceptions import ValidationError


class GlePrestation(models.Model):

    _name = 'gle.prestation'
    _sql_constraints = [
        ('code_uniq', 'unique(code, type_id)', _("Le code Analytique doit être unique par Type!")),
        ('code_size', 'check(code >= 1 and code <= 99)', _("Le code Analytique doit être compris entre 1 et 99 !")),
    ]

    def _get_code(self):
        for obj in self:
            code = ''
            if obj.type_id:
                code = code + str(obj.type_id.code_compose) + '.' 
                if obj.code > 9:
                    code = code + str(obj.code) 
                else: 
                    code = code + '0' + str(obj.code)
                obj.code_compose = code

    code_compose = fields.Char('Code Composé', compute='_get_code')
    code = fields.Integer("Ordre d'affichage", required=True)
    name = fields.Char('Libellé opérationnel', required=True)
    type_id = fields.Many2one("gle.type", string="Type")
    product_ids = fields.One2many("product.product", "prestation_id", "Produits")
    section_related_id = fields.Many2one(related='type_id.section_related_id', string="Related section", store=True)

    @api.model
    def create(self, vals):
        if vals.get('type_id'):
            type_id = self.env['gle.type'].search([('id', '=', vals.get('type_id'))])
            
            if type_id and type_id.account_id:
                vals['product_ids'] = [(0, 0, {
                                        'name': vals.get('name'),
                                        'type': "service",
                                        'uom_id': 1,
                                        'property_account_expense': type_id.account_id.id,
                                        'sale_ok': False,
                                        'purchase_ok': True,
                                        })]

        return super(GlePrestation, self).create(vals)
