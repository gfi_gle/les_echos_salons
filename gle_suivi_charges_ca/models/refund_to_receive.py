# -*- coding: utf-8 -*-
from openerp import models, fields, api
import datetime


class RefundToReceive(models.Model):
    _inherit = 'gle.refund.to.receive'

    def get_order_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()

    @api.multi
    def action_confirm(self):
        res = super(RefundToReceive, self).action_confirm()
        for order in self:
            reference = "AAR %s, Validation" % (order.name or '')
            for line in order.refund_line:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                if order.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=line.prestation_id.type_id.nature_id.section_id,
                                                     nature=line.prestation_id.type_id.nature_id,
                                                     type=line.prestation_id.type_id,
                                                     prestation=line.prestation_id,
                                                     amount=line.price_subtotal,
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)
        return res


    @api.multi
    def tsc_action_cancel(self):
        for order in self:
            reference = "AAR %s, Annulation" % (order.name or '')
            for line in order.refund_line.filtered(lambda r: not r.tsc_canceled and (r.tsc_last_engage != 0 or r.tsc_total_engage != 0)):
                if line.salon_id.is_clos:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line)
                    amount = line.tsc_total_engage
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         facture=True,
                                                         type=line.type_id,
                                                         prestation=line.prestation_id,
                                                         amount=-amount,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True,
                                                         store=False)
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         type=line.type_id,
                                                         facture=True,
                                                         prestation=line.prestation_id,
                                                         amount=amount,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)
                else:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "ch2_ce"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         type=line.type_id,
                                                         prestation=line.prestation_id,
                                                         amount=-line.tsc_last_engage,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)

        # elif year_of_salon_in_line == current_fiscal_year - 1:
        #     suivi_charg_ex_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
        #         [('gle_produit_id', '=', line.salon_id.name.id), ('millesime', '=', line.salon_id.millesime.id)])
        #     if suivi_charg_ex_fiscal_year:
        #         suivi_charg_ex_fiscal_year.update_suivi_charge(tableau='ch3_ep',
        #                                                        section=line.gle_section_id,
        #                                                        amount=line.price_subtotal,
        #                                                        facture=True,
        #                                                        nature=line.nature_id,
        #                                                        type=line.type_id,
        #                                                        prestation=line.prestation_id,
        #                                                        reference=reference
        #                                                        )
        #
        #     suivi_charg_current_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
        #         [('gle_produit_id', '=', line.salon_id.name.id), ('millesime.name', '=', str(current_fiscal_year))])
        #     if suivi_charg_current_fiscal_year:
        #         suivi_charg_current_fiscal_year.update_suivi_charge(tableau='ch3_ep',
        #                                                             section=line.gle_section_id,
        #                                                             amount=-(line.price_subtotal),
        #                                                             facture=True,
        #                                                             nature=line.nature_id,
        #                                                             type=line.type_id,
        #                                                             prestation=line.prestation_id,
        #                                                             reference=reference
        #                                                             )

    @api.multi
    def action_cancel(self):
        res = super(RefundToReceive, self).action_cancel()
        for order in self:
            order.tsc_action_cancel()
        return res

    @api.multi
    def action_refus(self):
        res = super(RefundToReceive, self).action_refus()
        for order in self:
            order.tsc_action_cancel()
        return res
