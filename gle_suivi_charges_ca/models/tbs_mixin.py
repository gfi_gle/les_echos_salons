# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.addons.decimal_precision import decimal_precision as dp

from openerp.exceptions import MissingError

FIELDS = [
    'tsc_ecart',
    'tsc_touched',
    'tsc_canceled',
    'tsc_last_engage',
    'tsc_total_engage',
    'tsc_last_facture',
    'tsc_total_facture',
]


class TSCLineMixin(models.AbstractModel):
    _name = 'tsc.line.mixin'

    tsc_ecart = fields.Float(string='Ecart dans le TSC', digits=dp.get_precision('Account'), copy=False)
    tsc_total_facture = fields.Float(string='Total facturé dans le TSC', digits=dp.get_precision('Account'), copy=False)
    tsc_last_facture = fields.Float(string='Dernier facturé dans le TSC', digits=dp.get_precision('Account'),
                                    copy=False)
    tsc_total_engage = fields.Float(string='Total engagé dans le TSC', digits=dp.get_precision('Account'), copy=False)
    tsc_last_engage = fields.Float(string='Dernier engagé dans le TSC', digits=dp.get_precision('Account'), copy=False)

    tsc_touched = fields.Boolean(string='Touched', copy=False)
    tsc_canceled = fields.Boolean(string='Is canceled', copy=False)

    related_tsc_total_facture = fields.Float(string='R/ Total facturé dans le TSC', digits=dp.get_precision('Account'),
                                             compute='_compute_related_tsc_fields', )
    related_tsc_last_facture = fields.Float(string='R/ Dernier facturé dans le TSC', digits=dp.get_precision('Account'),
                                            compute='_compute_related_tsc_fields', )
    related_tsc_total_engage = fields.Float(string='R/ Total engagé dans le TSC', digits=dp.get_precision('Account'),
                                            compute='_compute_related_tsc_fields', )
    related_tsc_last_engage = fields.Float(string='R/ Dernier engagé dans le TSC', digits=dp.get_precision('Account'),
                                           compute='_compute_related_tsc_fields', )

    related_tsc_touched = fields.Boolean(string='R/ Touched', compute='_compute_related_tsc_fields', )
    related_tsc_canceled = fields.Boolean(string='R/ Is canceled', compute='_compute_related_tsc_fields', )

    diff_tsc_total_on_facture = fields.Float(string='F-E total facturé dans le TSC', digits=dp.get_precision('Account'),
                                             compute='_compute_related_tsc_fields', )
    diff_tsc_last_on_facture = fields.Float(string='F-E Dernier facturé dans le TSC',
                                            digits=dp.get_precision('Account'),
                                            compute='_compute_related_tsc_fields', )
    diff_tsc_total_on_engage = fields.Float(string='F-E Total engagé dans le TSC', digits=dp.get_precision('Account'),
                                            compute='_compute_related_tsc_fields', )
    diff_tsc_last_on_engage = fields.Float(string='F-E Dernier engagé dans le TSC', digits=dp.get_precision('Account'),
                                           compute='_compute_related_tsc_fields', )

    """ XML
        <field name="tsc_touched"/>
        <field name="tsc_canceled"/>                                    
        <field name="tsc_last_engage"/>
        <field name="tsc_total_engage"/>
        <field name="tsc_last_facture"/>                                    
        <field name="tsc_total_facture"/>  
        <field name="related_tsc_touched"/>
        <field name="related_tsc_canceled"/>                                    
        <field name="related_tsc_last_engage"/>
        <field name="related_tsc_total_engage"/>
        <field name="related_tsc_last_facture"/>                                    
        <field name="related_tsc_total_facture"/>
        <field name="diff_tsc_total_on_facture"/>
        <field name="diff_tsc_last_on_facture"/>
        <field name="diff_tsc_total_on_engage"/>                                    
        <field name="diff_tsc_last_on_engage"/>                                  
        <field name="tsc_ecart"/>                                  
    """

    @api.model
    def create(self, vals):
        for key in vals.keys():
            if key in FIELDS:
                del vals[key]
        return super(TSCLineMixin, self).create(vals)

    @api.multi
    def _get_related_line(self):
        self.ensure_one()
        raise MissingError('Not implemented')


    @api.multi
    @api.depends('tsc_touched', 'tsc_canceled', 'tsc_last_engage', 'tsc_total_engage', 'tsc_last_facture',
                 'tsc_total_facture', )
    def _compute_related_tsc_fields(self):
        for obj in self:
            related_line = obj._get_related_line()
            if related_line:
                obj.related_tsc_total_facture = related_line.tsc_total_facture
                obj.related_tsc_last_facture = related_line.tsc_last_facture
                obj.related_tsc_total_engage = related_line.tsc_total_engage
                obj.related_tsc_last_engage = related_line.tsc_last_engage
                obj.related_tsc_touched = related_line.tsc_touched
                obj.related_tsc_canceled = related_line.tsc_canceled
                obj.diff_tsc_total_on_facture = obj.tsc_total_facture - related_line.tsc_total_engage
                obj.diff_tsc_last_on_facture = obj.tsc_last_facture - related_line.tsc_last_engage
                obj.diff_tsc_total_on_engage = related_line.tsc_total_facture - obj.tsc_total_engage
                obj.diff_tsc_last_on_engage = related_line.tsc_last_facture - obj.tsc_last_engage
                obj.tsc_ecart = related_line.tsc_ecart
            else:
                obj.related_tsc_total_facture = 0
                obj.related_tsc_last_facture = 0
                obj.related_tsc_total_engage = 0
                obj.related_tsc_last_engage = 0
                obj.related_tsc_touched = 0
                obj.related_tsc_canceled = 0
                obj.diff_tsc_total_on_facture = obj.tsc_total_facture
                obj.diff_tsc_last_on_facture = obj.tsc_last_facture
                obj.diff_tsc_total_on_engage = obj.tsc_total_engage
                obj.diff_tsc_last_on_engage = obj.tsc_last_engage
                obj.tsc_ecart = 0
