# -*- coding: utf-8 -*-



from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'
    
    date_order_demo = fields.Date('date order demo')
    
    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()
        
    def get_order_date(self):
        valid_date = self.env["res.company"].search([['id','=',1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()
        
    @api.model
    def _get_test_affichage_btn_refuse(self):
        uid = self._context.get('uid')

        if uid:
            if self.refused == False:
                user_connecte = self.env['res.users'].search([('id' , '=', uid)])
                if self.create_uid.id == 1 and self.state in ['draft', 'bid', 'validated', 'validated_bid', 'sent']:
                    self.test_affichage_btn_refuse = True
                else:
                    if self.state in ['draft', 'bid', 'sent'] and user_connecte.profile_type_id and user_connecte.profile_type_id.code == 'SALAR':
                        self.test_affichage_btn_refuse = True
                    elif self.state in ['validated', 'validated_bid', 'sent'] and user_connecte.profile_type_id and user_connecte.profile_type_id.code == 'CS':
                        self.test_affichage_btn_refuse = True
                    else:
                        self.test_affichage_btn_refuse = False
            else:
                self.test_affichage_btn_refuse = False
            
    test_affichage_btn_refuse = fields.Boolean(compute=_get_test_affichage_btn_refuse)

    @api.model
    def _get_test_affichage_btn_vpc(self):
        uid = self._context.get('uid')
        if uid:
            user_connecte = self.env['res.users'].search([('id' , '=', uid)])
            if self.create_uid.id == 1 and self.state in ['draft', 'bid', 'sent']:
                self.test_affichage_btn_vpc = True
            else: 
                if user_connecte.profile_type_id and user_connecte.profile_type_id.code == 'SALAR' and self.state in ['draft', 'sent', 'bid']:
                    self.test_affichage_btn_vpc = True
                else:
                    self.test_affichage_btn_vpc = False
            
    test_affichage_btn_vpc = fields.Boolean(compute=_get_test_affichage_btn_vpc)

    @api.model
    def _get_test_affichage_btn_confirm(self):
        uid = self._context.get('uid')
        if uid:
            user_connecte = self.env['res.users'].search([('id' , '=', uid)])
            if self.create_uid.id == 1 and self.state == 'validated':
                self.test_affichage_btn_confirm = True
            else: 
                if user_connecte.profile_type_id and  user_connecte.profile_type_id.code == 'CS' and self.state == 'validated':
                        self.test_affichage_btn_confirm = True
                else:
                    self.test_affichage_btn_confirm = False
            
    test_affichage_btn_confirm = fields.Boolean(compute=_get_test_affichage_btn_confirm)

    @api.model
    def _get_test_affichage_btn_confirm_bid(self):
        uid = self._context.get('uid')
        if uid:
            user_connecte = self.env['res.users'].search([('id' , '=', uid)])
            if self.create_uid.id == 1 and self.state == 'validated_bid':
                self.test_affichage_btn_confirm_bid = True
            else: 
                if user_connecte.profile_type_id and user_connecte.profile_type_id.code == 'CS' and self.state == 'validated_bid':
                        self.test_affichage_btn_confirm_bid = True
                else:
                    self.test_affichage_btn_confirm_bid = False
                
    test_affichage_btn_confirm_bid = fields.Boolean(compute=_get_test_affichage_btn_confirm_bid)
        
    @api.multi
    def valider_par_collaborateur(self):
        if self.state == 'sent' or self.state == 'bid':
            self.state = 'validated_bid'
        if self.state == 'draft':
            self.state = 'validated'
        self.refused = False
        self.comment = ''
    
    @api.multi
    def confirm_order(self):
        self.signal_workflow('bid_received')
        self.signal_workflow('purchase_confirm')
        self.wkf_approve_order()

    
    def wkf_confirm_order(self, cr, uid, ids, context=None):
        super(PurchaseOrder, self).wkf_confirm_order(cr, uid, ids, context)
        for po in self.browse(cr, uid, ids, context=context):
            self.write(cr, uid, [po.id], {'refused': False, 'comment': ''})
        return True
    
    refused = fields.Boolean('refused', default=False)
    comment = fields.Text('Motif de refus')
    state = fields.Selection(selection=[
                                        ('draft', 'Draft PO'),
                                        ('sent', 'RFQ'),
                                        ('bid', 'Bid Received'),
                                        ('confirmed', 'Waiting Approval'),
                                        ('validated', 'Validé par Collaborateur'),
                                        ('validated_bid', 'Validé par Collaborateur'),
                                        ('approved', 'Purchase Confirmed'),
                                        ('except_picking', 'Shipping Exception'),
                                        ('except_invoice', 'Invoice Exception'),
                                        ('done', 'Done'),
                                        ('passage_sans_suite', 'Passage sans suite'),
                                        ('cancel', 'Cancelled')])
    
    @api.multi
    def wkf_approve_order(self):
        """
        Un bon de commande Achat, ne peut etre etablit sur salon clos
        """
        res = super(PurchaseOrder, self).wkf_approve_order()
        for order in self:
            reference = u'Achat %s' % (order.name or '')
            for line in order.order_line:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                if order.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=line.prestation_id.type_id.nature_id.section_id,
                                                     nature=line.prestation_id.type_id.nature_id,
                                                     type=line.prestation_id.type_id,
                                                     prestation=line.prestation_id,
                                                     amount=line.price_subtotal,
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)
        return res

    @api.multi
    def tsc_action_cancel(self):
        reference = u'Achat %s, passage sans suite' % (self.name or '')
        for order in self:
            for line in order.order_line.filtered(lambda r: not r.tsc_canceled and (r.tsc_total_engage != 0 or r.tsc_last_engage != 0)):
                if line.salon_id.is_clos:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line)
                    amount = line.tsc_total_engage
                    if order.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         facture=True,
                                                         type=line.type_id,
                                                         prestation=line.prestation_id,
                                                         amount=-amount,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True,
                                                         store=False)
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                    if order.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         facture=True,
                                                         type=line.type_id,
                                                         prestation=line.prestation_id,
                                                         amount=amount,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)
                else: # Not clos
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                    if order.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "ch2_ce"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=line.gle_section_id,
                                                         nature=line.nature_id,
                                                         type=line.type_id,
                                                         prestation=line.prestation_id,
                                                         amount=-line.tsc_total_engage,
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)
        # 
        # elif year_of_salon_in_line == current_fiscal_year - 1:
        #     suivi_charg_ex_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
        #         [('gle_produit_id', '=', line.salon_id.name.id), ('millesime', '=', line.salon_id.millesime.id)])
        #     if suivi_charg_ex_fiscal_year:
        #         suivi_charg_ex_fiscal_year.update_suivi_charge(tableau='ch3_ep',
        #                                                        section=line.gle_section_id,
        #                                                        amount=line.price_subtotal,
        #                                                        facture=True,
        #                                                        nature=line.nature_id,
        #                                                        type=line.type_id,
        #                                                        prestation=line.prestation_id,
        #                                                        reference=reference)
        # 
        #     suivi_charg_current_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
        #         [('gle_produit_id', '=', line.salon_id.name.id), ('millesime.name', '=', str(current_fiscal_year))])
        #     if suivi_charg_current_fiscal_year:
        #         suivi_charg_current_fiscal_year.update_suivi_charge(tableau='ch3_ep',
        #                                                             section=line.gle_section_id,
        #                                                             amount=-(line.price_subtotal),
        #                                                             facture=True,
        #                                                             nature=line.nature_id,
        #                                                             type=line.type_id,
        #                                                             prestation=line.prestation_id,
        #                                                             reference=reference
        #                                                             )

    @api.multi
    def action_pss(self):
        for order in self:
            order.state = "passage_sans_suite"
            order.tsc_action_cancel()


    @api.multi
    def action_refus(self):
        for order in self:
            order.state = 'draft'
            order.tsc_action_cancel()

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        vals = super(PurchaseOrder, self)._prepare_inv_line(cr, uid, account_id, order_line, context=context)
        res = {'salon_id' : order_line.salon_id.id,
                     'gle_section_id' : order_line.gle_section_id.id,
                     'nature_id' : order_line.nature_id.id,
                     'type_id' : order_line.type_id.id,
                     'prestation_id' : order_line.prestation_id.id,
                     }
        if order_line.type_id:
            if order_line.type_id.account_id:
                res['account_id'] = order_line.type_id.account_id.id
        vals.update(res)
        return vals

    def _prepare_invoice(self, cr, uid, order, line_ids, context=None):

        invoice_vals = super(PurchaseOrder, self)._prepare_invoice(cr, uid, order, line_ids, context=context)
        user_id = self.pool.get('res.users').browse(cr, uid, [uid], context)

        if user_id and len(user_id.section_ids) > 0:
            invoice_vals.update({'gle_section_id': user_id.section_ids.ids[0]})

        return invoice_vals


