# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import datetime

import logging

_logger = logging.getLogger(__name__)

OUT_INVOICE, OUT_REFUND, IN_INVOICE, IN_REFUND = ['out_invoice', 'out_refund', 'in_invoice', 'in_refund']
INS, OUTS = [IN_INVOICE, IN_REFUND], [OUT_INVOICE, OUT_REFUND]


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()

    @api.model
    def _get_emp_domain(self):
        user_id = self.env['res.users'].browse([self.env.context.get("uid")])
        if user_id:

            if user_id.profile_type_id.code == 'ADV':
                domain = "[]"
            else:
                domain = [('id', 'in', user_id.section_ids.ids)]
        return domain

    def get_section_id(self):

        user_id = self.env['res.users'].browse([self._uid])

        if user_id and len(user_id.section_ids) > 0:
            return user_id.section_ids[0].id
        else:
            return self.env['gle.section']

    gle_section_id = fields.Many2one("gle.section", string="Section", domain=_get_emp_domain, default=get_section_id,
                                     help="La section Comptable est utilisé dans le circuit de validation dans les factures d'achat. Quand aux factures de vente, elle n'est utile que pour le remplissage du tableau de suivi des charges/ CA.")
    gle_prestation_id = fields.Many2one("gle.prestation", string="Prestation")

    @api.model
    def _prepare_refund(self, invoice, date=None, period_id=None, description=None, journal_id=None):
        """ Inherit method to add origin_invoice """

        values = super(AccountInvoice, self)._prepare_refund(invoice, date=None, period_id=None, description=None,
                                                             journal_id=None)

        values.update({
            'supplier_invoice_number': self.supplier_invoice_number
        })

        return values

    @api.multi
    def invoice_validate(self):
        """
        Gérer les factures clients (cas des intercos, les avoirs clients (cas des intercos)

        Gérer les factures fournisseurs et leurs avoirs

        Cas 1 : pas d'écart, juste enregister les montants sur le tableau de l'exercice de salon, aucune
                vérification ne sera pas faite par la suite
        Cas 2 : écart sur la facture, enregister les montants dans le tableau de l'exercice de salon,
                après enregister la différence dans le bloc écarts des provisions dans l'exercice de
                salon et le négatif de la différénce dans le même bloc mais lié à l'exercice de la facture
        Cas 3 : quand un engagé est annulé, disant c'est le bon de commande fournisseur, les montants
                seront enregistés dans le facturé dans le bloc des écarts de provision, le premier montant
                dans le tableau de l'exercice de salon et son inverse dans le tableau de l'exercice de la
                facture, ca cas doit être normalement implémenté côté engagé.
        Cas 4 : réception d'une facture non approvisionnée, le montant est enregistré dans le facturé
                après ce même montant est enregistrée dans les ecarts dans les deux exercice, mais en
                valeur négative sur le tableau de l'ercice de salon, ca cas est automatiquement
                considéré comme cas 1 puisque l'écart est égal à zéro
        """
        res = super(AccountInvoice, self).invoice_validate()
        for inv in self:
            for line in inv.invoice_line:
                if inv.type in OUTS and (inv.type_facturation == 'intercos' or inv.partner_id.intercos):
                    line.tsc_intercos_not_clos(cancel=False)
                    if line.diff_tsc_total_on_facture and inv.salon_id.is_clos:
                        line.tsc_intercos_clos(cancel=False, ecart=line.diff_tsc_total_on_facture)
                elif inv.type in INS:
                    line.tsc_in_not_clos(cancel=False)
                    if line.diff_tsc_total_on_facture and line.salon_id.is_clos:  # Si pas d'écart, tout sera normal
                        line.tsc_in_clos(cancel=False, ecart=line.diff_tsc_total_on_facture)

        return res

    @api.multi
    def tsc_action_cancel(self):
        for inv in self:
            for line in inv.invoice_line.filtered(
                    lambda r: not r.tsc_canceled and (r.tsc_last_facture != 0 or r.tsc_total_facture != 0)):
                if inv.type in OUTS and (inv.type_facturation == 'intercos' or inv.partner_id.intercos):
                    line.tsc_intercos_not_clos(cancel=True)
                    if line.tsc_ecart and inv.salon_id.is_clos:
                        line.tsc_intercos_clos(cancel=True, ecart=line.tsc_ecart)

                elif inv.type in INS:
                    line.tsc_in_not_clos(cancel=True)
                    if line.tsc_ecart and line.salon_id.is_clos:
                        line.tsc_in_clos(cancel=True, ecart=line.tsc_ecart)

    @api.multi
    def action_cancel(self):
        res = super(AccountInvoice, self).action_cancel()
        for inv in self:
            origins = [] if not inv.origin else inv.origin.strip().split(',')
            print origins

            devis_aar_line = self.env['gle.refund.to.receive.line'].search(
                [('refund_id.name', 'in', origins), ('move_id', '!=', False), ('move_id.extourne', '=', False)])
            purchase_order_lines = self.env['purchase.order.line'].search(
                [('order_id.name', 'in', origins), ('move_id', '!=', False),
                 ('move_id.extourne', '=', False)]).filtered(
                lambda r: r.salon_id in inv.invoice_line.mapped('salon_id'))
            print devis_aar_line
            if inv.type == 'in_invoice' and purchase_order_lines:
                inv.extourne_fnp(purchase_order_lines)
            if inv.type == 'in_refund' and devis_aar_line:
                inv.extourne_aar(devis_aar_line)
            inv.tsc_action_cancel()
        return res

        # La première fois cette fonction est surchargée pour l'imputation des engégs et
        # des facturé, par la suite, la fonction est modifiée pour la prise en charges des
        # avoir fournisseur et client.
        #         coeff = -1 if self.type and self.type.endswith('refund') else 1
        #         if self.type in ['in_invoice', 'in_refund']:
        #             p_order = self.env['purchase.order'].search([('name', '=', self.origin)])
        #             # Si une commande d'achat est liée à la facture
        #             if p_order:
        #                 annee_exercice = self.get_validation_date().year
        # #                annee_order=p_order.date_order_demo.year
        #                # annee_order = datetime.datetime.strptime(p_order.date_order_demo, '%Y-%m-%d').date().year
        #                 p_order.date_order_demo = p_order.get_order_date()
        #                 annee_order = datetime.datetime.strptime(p_order.date_order_demo, '%Y-%m-%d').date().year
        #                 # annee_order = '2017'
        #                 # Si l'année d'aujourd'hui et celle dans la commande d'achat
        #                 if annee_exercice == annee_order:
        #                     if self.type_facturation in ['m_echange', 'r_echange']:
        #                         type_tab = "ch4_em"
        #                     else:
        #                         type_tab = "ch2_ce"
        #                     for line in self.invoice_line:
        #                         suivi_charge = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', line.salon_id.name.id),('millesime.name', '=', str(annee_exercice))],limit=1)
        #                         if suivi_charge:
        #                             suivi_charge.update_suivi_charge(tableau=type_tab,
        #                                                              section=line.gle_section_id,
        #                                                              amount=abs(line.price_subtotal) * coeff,
        #                                                              facture=True,
        #                                                              nature=line.nature_id,
        #                                                              type=line.type_id,
        #                                                              prestation=line.prestation_id,
        #                                                              reference=reference)
        #                 # Si l'année d'aujourd'hui n'est ce celle de la commande ET
        #                 # ET la commande d'achat est celle de l'année dernière
        #                 elif annee_order == annee_exercice - 1:
        #                     aml_from_po = {}
        #                     aml_from_invoice = {}
        #                     for aml in p_order.order_line:
        #                         if aml.product_id and not aml.product_id.id in aml_from_po.keys() :
        #                             aml_from_po[aml.product_id.id] = { 'amount' : aml.price_subtotal,}
        #                         elif aml.product_id:
        #                             aml_from_po[aml.product_id.id] = {  'amount' : aml_from_po[aml.product_id.id]['amount'] + aml.price_subtotal,}
        #
        #                     for line in self.invoice_line:
        #                         if line.product_id and not line.product_id.id in aml_from_invoice.keys() :
        #                             aml_from_invoice[aml.product_id.id] = { "amount" : line.price_subtotal,}
        #                         elif line.product_id:
        #                             aml_from_invoice[aml.product_id.id] = { 'amount' : aml_from_invoice[aml.product_id.id]['amount'] + line.price_subtotal,}
        #
        #                     for key, value_dict in aml_from_invoice.items():
        #                         if key in aml_from_po.keys():
        #                             product_p_id = self.env['product.product'].search([['id','=',key]])
        #
        #     #     ----      Montant produit Bon de commande inférieur Montant produit facture
        #                             if value_dict['amount'] > aml_from_po[key]['amount']:
        #                                 if self.type_facturation in ['m_echange', 'r_echange']:
        #                                     type_tab = "ch4_em"
        #                                 else:
        #                                     type_tab = "ch2_ce"
        #     #     ----      Ecriture 1 sur Tableau Année Bon de commande
        #                                 suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', product_p_id.salon_id.name.id),('millesime.name', '=', str(annee_order))],limit=1)
        #                                 if suivi_charge_1:
        #                                     suivi_charge_1.update_suivi_charge(tableau=type_tab,
        #                                                                      section=product_p_id.prestation_id.type_id.nature_id.section_id,
        #                                                                      amount= line.price_subtotal * coeff,
        #                                                                      facture=True,
        #                                                                      nature=product_p_id.prestation_id.type_id.nature_id,
        #                                                                      type=product_p_id.prestation_id.type_id,
        #                                                                      prestation=product_p_id.prestation_id,
        #                                                                        reference=reference)
        #         #     ----      Ecriture 2 sur Tableau Année Bon de commande
        #     #                                 suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', product_p_id.salon_id.name.id),('millesime.name', '=', str(annee_order))])
        #                                     suivi_charge_1.update_suivi_charge(tableau="ch3_ep",
        #                                                                      section=product_p_id.prestation_id.type_id.nature_id.section_id,
        #                                                                      amount= (aml_from_po[key]['amount'] - value_dict['amount']) * coeff,
        #                                                                      facture=True,
        #                                                                      nature=product_p_id.prestation_id.type_id.nature_id,
        #                                                                      type=product_p_id.prestation_id.type_id,
        #                                                                      prestation=product_p_id.prestation_id,
        #                                                                        reference=reference)
        #
        #     #     --     --      Ecriture 3 sur Tableau année Exercice comptable facture
        #                                 suivi_charge_2 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', product_p_id.salon_id.name.id),('millesime.name', '=', str(annee_exercice))],limit=1)
        #                                 if suivi_charge_2:
        #                                     suivi_charge_2.update_suivi_charge(tableau="ch3_ep",
        #                                                                      section=product_p_id.prestation_id.type_id.nature_id.section_id,
        #                                                                      amount= abs(aml_from_po[key]['amount'] - value_dict['amount']) * coeff,
        #                                                                      facture=True,
        #                                                                      nature=product_p_id.prestation_id.type_id.nature_id,
        #                                                                      type=product_p_id.prestation_id.type_id,
        #                                                                      prestation=product_p_id.prestation_id,
        #                                                                        reference=reference)
        #
        #     #     ----      Montant produit Bon de commande égal Montant produit facture
        #                             else:
        #                                 if self.type_facturation in ['m_echange', 'r_echange']:
        #                                     type_tab = "ch4_em"
        #                                 else:
        #                                     type_tab = "ch2_ce"
        #
        #                                 suivi_charge = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', product_p_id.salon_id.name.id),('millesime.name', '=', str(annee_order))],limit=1)
        #                                 if suivi_charge:
        #                                     suivi_charge.update_suivi_charge(tableau=type_tab,
        #                                                                      section=product_p_id.prestation_id.type_id.nature_id.section_id,
        #                                                                      amount= value_dict['amount'],
        #                                                                      facture=True,
        #                                                                      nature=product_p_id.prestation_id.type_id.nature_id,
        #                                                                      type=product_p_id.prestation_id.type_id,
        #                                                                      prestation=product_p_id.prestation_id,
        #                                                                      reference=reference)
        #             #Pas de commande achat liée à la facture
        #             else:
        #                 exercice_en_cours= self.get_validation_date().year
        #                 exercice_salon = int(self.salon_id.millesime.name)
        #                 # Baser sur la date de salon est voir est ce que :
        #                 # L'année de salon est celle de l'année passée
        #                 if str(exercice_en_cours) == str(exercice_salon + 1):
        #                     for line in self.invoice_line:
        #                         if self.type_facturation in ['m_echange', 'r_echange']:
        #                             type_tab = "ch4_em"
        #                         else:
        #                             type_tab = "ch2_ce"
        #                         suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', line.product_id.salon_id.name.id),('millesime.name', '=', line.product_id.salon_id.millesime.name)])
        #                         if suivi_charge_1:
        #                             suivi_charge_1.update_suivi_charge(tableau=type_tab,
        #                                                              section=line.product_id.prestation_id.type_id.nature_id.section_id,
        #                                                              amount= line.price_subtotal * coeff,
        #                                                              facture=True,
        #                                                              nature=line.product_id.prestation_id.type_id.nature_id,
        #                                                              type=line.product_id.prestation_id.type_id,
        #                                                              prestation=line.product_id.prestation_id,
        #                                                                reference=reference)
        #
        #                             suivi_charge_1.update_suivi_charge(tableau="ch3_ep",
        #                                                              section=line.product_id.prestation_id.type_id.nature_id.section_id,
        #                                                              amount= - line.price_subtotal * coeff,
        #                                                              facture=True,
        #                                                              nature=line.product_id.prestation_id.type_id.nature_id,
        #                                                              type=line.product_id.prestation_id.type_id,
        #                                                              prestation=line.product_id.prestation_id,
        #                                                                reference=reference)
        #
        #                         suivi_charge_2 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', line.product_id.salon_id.name.id),('millesime.name', '=', str(exercice_salon + 1))],limit=1)
        #                         if suivi_charge_2:
        #                             suivi_charge_2.update_suivi_charge(tableau="ch3_ep",
        #                                                              section=line.product_id.prestation_id.type_id.nature_id.section_id,
        #                                                              amount= abs(line.price_subtotal) * coeff,
        #                                                              facture=True,
        #                                                              nature=line.product_id.prestation_id.type_id.nature_id,
        #                                                              type=line.product_id.prestation_id.type_id,
        #                                                              prestation=line.product_id.prestation_id,
        #                                                                reference=reference)
        #
        #         elif self.type in ["out_invoice", "out_refund"]:
        #             if self.type_facturation == 'intercos':
        #                 force_string = '4.F.10.01'
        #                 coeff = - coeff
        #             else:
        #                 force_string = None
        #             # Constatation du CA sur le tableau du salon approprié
        #             if self.period_id:
        #                 p_order = self.env['sale.order'].search([('name','=',self.origin)])
        #                 # Cherche le bon de commande, et si tu le trouves :
        #                 if p_order:
        #
        #                     annee_exercice = self.get_validation_date().year
        #                     #TODO add field date_order_demo to sale_order and get annee_order
        #                     # annee_order = datetime.datetime.strptime(p_order.date_order_demo, '%Y-%m-%d').date().year
        #                     p_order.date_order_demo = p_order.get_order_date()
        #                     annee_order = datetime.datetime.strptime(p_order.date_order_demo, '%Y-%m-%d').date().year
        #                     # annee_order = 2017
        #                     # Test si l'année d'aujourd'hui est celle de la commande vente
        #                     if annee_exercice == annee_order:
        #                         if self.type_facturation in ['m_echange', 'r_echange']:
        #                             type_tab = "ch4_em"
        #                         else:
        #                             type_tab = "pdt2_pe"
        #
        #                         for line in self.invoice_line:
        #                             suivi_charge = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', str(annee_exercice))],limit=1)
        #                             if suivi_charge:
        #                                 if self.type_facturation == 'intercos':
        #                                     suivi_charge.update_suivi_charge(tableau= 'ch2_ce',
        #                                                                  section=self.gle_section_id,
        #                                                                  amount=abs(line.price_subtotal) * coeff,
        #                                                                  facture=True,
        #                                                                  famille=False,
        #                                                                  force_string=force_string,
        #                                                                  reference=reference,
        #                                                                  record=line,
        #                                                                  )
        #                                 else:
        #                                     suivi_charge.update_suivi_charge(tableau= type_tab,
        #                                                                  section=self.gle_section_id,
        #                                                                  amount=abs(line.price_subtotal) * coeff,
        #                                                                  facture=True,
        #                                                                  famille=line.product_id.categ_id,
        #                                                                  force_string=force_string,
        #                                                                  reference=reference,
        #                                                                  record=line,
        #                                                                  )
        #                     # Voir si l'année de la commande de vente est celle de l'année passée
        #                     elif annee_order == annee_exercice - 1:
        #                         aml_from_po = {}
        #                         aml_from_invoice = {}
        #                         for aml in p_order.order_line:
        #                             if aml.product_id and not aml.product_id.id in aml_from_po.keys() :
        #                                 aml_from_po[aml.product_id.id] = { 'amount' : aml.price_subtotal,}
        #                             elif aml.product_id:
        #                                 aml_from_po[aml.product_id.id] = {  'amount' : aml_from_po[aml.product_id.id]['amount'] + aml.price_subtotal,}
        #
        #                         for line in self.invoice_line:
        #                             if line.product_id and not line.product_id.id in aml_from_invoice.keys() :
        #                                 aml_from_invoice[aml.product_id.id] = { "amount" : line.price_subtotal,}
        #                             elif line.product_id:
        #                                 aml_from_invoice[aml.product_id.id] = { 'amount' : aml_from_invoice[aml.product_id.id]['amount'] + line.price_subtotal,}
        #
        #                         for key, value_dict in aml_from_invoice.items():
        #                             if key in aml_from_po.keys():
        #                                 product_p_id = self.env['product.product'].search([['id','=',key]])
        #
        #         #     ----      Montant produit Bon de commande inférieur Montant produit facture
        #                                 if value_dict['amount'] > aml_from_po[key]['amount']:
        #                                     if self.type_facturation in ['m_echange', 'r_echange']:
        #                                         type_tab = "ch4_em"
        #                                     else:
        #                                         type_tab = "pdt2_pe"
        #         #     ----      Ecriture 1 sur Tableau Année Bon de commande
        #                                     suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', str(annee_order))],limit=1)
        #                                     if suivi_charge_1:
        #                                         suivi_charge_1.update_suivi_charge(tableau=type_tab,
        #                                                                          section=self.gle_section_id,
        #                                                                          amount=abs(line.price_subtotal) * coeff,
        #                                                                          facture=True,
        #                                                                          famille=line.product_id.categ_id,
        #                                                                          force_string=force_string,
        #                                                                            reference=reference
        #                                                                          )
        #             #     ----      Ecriture 2 sur Tableau Année Bon de commande
        #         #                                 suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', product_p_id.salon_id.name.id),('millesime.name', '=', str(annee_order))])
        #                                         suivi_charge_1.update_suivi_charge(tableau="pdt3_pe",
        #                                                                          section=self.gle_section_id,
        #                                                                          amount= (aml_from_po[key]['amount'] - value_dict['amount']) * coeff,
        #                                                                          facture=True,
        #                                                                          famille=line.product_id.categ_id,
        #                                                                          force_string=force_string,
        #                                                                            reference=reference)
        #
        #         #     --     --      Ecriture 3 sur Tableau année Exercice comptable facture
        #                                     suivi_charge_2 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', str(annee_exercice))],limit=1)
        #                                     if suivi_charge_2:
        #                                         suivi_charge_2.update_suivi_charge(tableau="pdt3_pe",
        #                                                                          section=self.gle_section_id,
        #                                                                          amount= abs(aml_from_po[key]['amount'] - value_dict['amount']) * coeff,
        #                                                                          facture=True,
        #                                                                          famille=line.product_id.categ_id,
        #                                                                          force_string=force_string,
        #                                                                            reference=reference)
        #
        #         #     ----      Montant produit Bon de commande égal Montant produit facture
        #                                 else:
        #                                     if self.type_facturation in ['m_echange', 'r_echange']:
        #                                         type_tab = "ch4_em"
        #                                     else:
        #                                         type_tab = "pdt2_pe"
        #
        #                                     suivi_charge = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', str(annee_order))],limit=1)
        #                                     if suivi_charge:
        #                                         suivi_charge.update_suivi_charge(tableau=type_tab,
        #                                                                          section=self.gle_section_id,
        #                                                                          amount= value_dict['amount'] * coeff,
        #                                                                          facture=True,
        #                                                                          famille=line.product_id.categ_id,
        #                                                                          force_string=force_string,
        #                                                                          reference=reference)
        #                 else:
        #                     exercice_en_cours= self.get_validation_date().year
        #                     exercice_salon = int(self.salon_id.millesime.name)
        #                     if str(exercice_en_cours) == str(exercice_salon + 1):
        #                         for line in self.invoice_line:
        #                             if self.type_facturation in ['m_echange', 'r_echange']:
        #                                 type_tab = "ch4_em"
        #                             else:
        #                                 type_tab = "pdt2_pe"
        #                             suivi_charge_1 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', line.product_id.salon_id.millesime.name)])
        #                             if suivi_charge_1:
        #                                 suivi_charge_1.update_suivi_charge(tableau=type_tab,
        #                                                                  section=self.gle_section_id,
        #                                                                  amount= line.price_subtotal * coeff,
        #                                                                  facture=True,
        #                                                                  famille=line.product_id.categ_id,
        #                                                                    reference=reference)
        #
        #                                 suivi_charge_1.update_suivi_charge(tableau="pdt3_pe",
        #                                                                  section=self.gle_section_id,
        #                                                                  amount= - line.price_subtotal * coeff,
        #                                                                  facture=True,
        #                                                                  famille=line.product_id.categ_id,
        #                                                                    reference=reference)
        #
        #                             suivi_charge_2 = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', self.salon_id.name.id),('millesime.name', '=', str(exercice_salon + 1))],limit=1)
        #                             if suivi_charge_2:
        #                                 suivi_charge_2.update_suivi_charge(tableau="pdt3_pe",
        #                                                                  section=self.gle_section_id,
        #                                                                  amount= abs(line.price_subtotal) * coeff,
        #                                                                  facture=True,
        #                                                                  famille=line.product_id.categ_id,
        #                                                                    reference=reference)
        #             else:
        #                 raise ValidationError("Attention! Veuillez définir une période, avant la validation.")
        #             if self.type_facturation in ['m_echange', 'r_echange']:
        #                 # CREATION D'UN BdC
        #                 vals = {
        #                     'origin' : self.number,
        #                        'partner_id' : self.partner_id.id,
        #                        'salon_id': self.salon_id.id,
        #                        'contact_fournisseur_id' : self.contact_client_id.id,
        #                        'location_id' : self.partner_id.property_stock_customer.id or self.contact_client_id.property_stock_customer.id,
        #                        'date_order' : self.get_validation_date(),
        #                        'pricelist_id' : self.partner_id.property_product_pricelist_purchase.id,
        #                        'currency_id' : self.partner_id.property_product_pricelist_purchase.currency_id.id,
        #                        'dest_address_id' : self.invoice_address_ids[0].id,
        #                        'type_facturation' : self.type_facturation,
        #                        'order_line' : [(0,0,{
        #                             'prestation_id': self.gle_prestation_id.id,
        #                             'product_id' : self.gle_prestation_id.product_ids[0].id or False,
        #                             'salon_id': self.salon_id.id,
        #                             'name': self.gle_prestation_id.name,
        #                             'price_unit': self.amount_untaxed,
        #                             'product_uom': self.env.ref('product.product_uom_unit').id ,
        #                             'date_planned' : self.get_validation_date(),
        #                             'taxes_id': False,
        #                            })]
        #                        }
        #                 self.env['purchase.order'].create(vals)
        #         res = super(AccountInvoice, self).invoice_validate()
        #
        #        return res
