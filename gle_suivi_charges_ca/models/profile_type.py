from openerp import models, fields, api
from openerp.tools.translate import _ 

class ProfileType(models.Model):
    _name = 'profile.type'

    code = fields.Char(string="Code")
    name = fields.Char(string="Nom", required=True)
    user_ids = fields.One2many("res.users", 'profile_type_id' , string="Utilisateurs")
