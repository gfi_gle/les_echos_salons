# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime 
from lxml import etree
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):

    _inherit = 'product.product'

    prestation_id = fields.Many2one("gle.prestation", string="Prestation")

    @api.onchange('prestation_id')
    def onchange_prestation_id(self):
        if self.prestation_id:
            self.property_account_expense = self.prestation_id.type_id.account_id.id


class ProductTemplate(models.Model):

    _inherit = 'product.template'
    
    def _get_salon(self):

        if self._context.get('default_millesime') and self._context.get('default_salon'):
            salon_id = self.env['gle_salon.salon'].search([('millesime.id', '=', self._context.get('default_millesime')), ('name.id', '=', self._context.get('default_salon'))])
            return salon_id and salon_id.id 
        else:
            return self.env['gle_salon.salon']

    salon_id = fields.Many2one('gle_salon.salon', default=_get_salon)
    prestation_id = fields.Many2one("gle.prestation", string="Prestation")


class ProductCategory(models.Model):

    _inherit = 'product.category'

    _sql_constraints = [
        ('code_uniq', 'unique(code, nature_id)', _("Le code Analytique des catégories d'article doit être unique par Nature!")),
    ]

    def _get_code(self):
        for obj in self:
             obj.code_compose = obj.code
                
    nature_id = fields.Many2one("gle.nature", string="Nature")
    nature_comptable_id = fields.Many2one("gle.nature.comptable", string="Nature Comptable")
    code = fields.Integer("Code Analytique")
    code_compose = fields.Char('Code Composé', compute='_get_code')

    @api.model
    def create(self, vals):
        code = vals.get('code', '')
        if code:
            if code >= 1 and code <=99:
                return super(ProductCategory,self).create(vals)
            else:
                raise ValidationError(_("Le code Analytique doit être compris entre 1 et 99 !"))
        else:
            return super(ProductCategory,self).create(vals)
    
    @api.multi
    def write(self, vals):
        code = vals.get('code','')
        if code:
            if code >= 1 and code <=99:
                return super(ProductCategory,self).write(vals)
            else:
                raise ValidationError(_("Le code Analytique doit être compris entre 1 et 99 !"))
        else:
            return super(ProductCategory,self).write(vals)


