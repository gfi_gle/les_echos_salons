# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning,ValidationError
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrderLine(models.Model):
    _inherit = ['tsc.line.mixin', 'purchase.order.line']
    _name = 'purchase.order.line'

    @api.model
    def _init_prestation_domain(self):
        """
            Method to init domain associated to attribute prestation_id,
            domain is generate in function of section associated to current user
        """

        user_id = self.env['res.users'].browse([self.env.context.get("uid")])

        if user_id.profile_type_id.code in ['ADV', 'CF']:
            return []
        else:
            return [('section_related_id', 'in', user_id.section_ids.ids)]

    salon_id = fields.Many2one(
        comodel_name='gle_salon.salon',
        string="Salon")

    gle_section_id = fields.Many2one(
        comodel_name='gle.section',
        string="Section",
        compute='compute_analytic',
        store=True)

    nature_id = fields.Many2one(
        comodel_name='gle.nature',
        string="Nature",
        compute='compute_analytic',
        store=True)

    type_id = fields.Many2one(
        comodel_name='gle.type',
        string="Type",
        compute='compute_analytic',
        store=True)

    prestation_id = fields.Many2one(
        comodel_name='gle.prestation',
        string="Prestation",
        domain=_init_prestation_domain)

    product_id = fields.Many2one(
        comodel_name='product.product',
        string="Product",
        change_default=True)

    @api.multi
    def _get_related_line(self):
        self.ensure_one()
        return False

    @api.onchange('salon_id')
    def on_change_salon_id(self):

        if self.salon_id:
            self.empty_attributes_product(type_change='salon')

    @api.depends('prestation_id')
    def compute_analytic(self):
        for purchase_order_line in self:
            if purchase_order_line.prestation_id and purchase_order_line.salon_id:
                purchase_order_line.type_id = purchase_order_line.prestation_id.type_id.id
                purchase_order_line.nature_id = purchase_order_line.type_id.nature_id.id
                purchase_order_line.gle_section_id = purchase_order_line.prestation_id.section_related_id.id
                purchase_order_line.date_planned = datetime.datetime.now()

    @api.onchange('prestation_id')
    def onchange_prestation_id(self):
        domain_dict = {}

        if self.prestation_id and self.salon_id:

            # add domain to product_id field
            domain_dict.update({'domain': {'product_id': [('purchase_ok', '=', True),
                                                          ('property_account_expense', '!=', None),
                                                          ('prestation_id', '=', self.prestation_id.id)]
                                           }})

            self.product_id = self.env['product.product'].search([('purchase_ok', '=', True),
                                                                  ('property_account_expense', '!=', None),
                                                                  ('prestation_id', '=', self.prestation_id.id)])

        return domain_dict

    def empty_attributes_product(self, type_change=None):
        """ Method to empty product's attributes in po line """

        self.date_planned = None
        self.gle_section_id = None
        self.product_id = None
        self.name = None
        self.product_qty = None
        self.price_unit = None
        self.discount_type = None
        self.discount = None

        if type_change == 'salon':
            self.type_id = None
            self.nature_id = None
            self.gle_section_id = None
            self.prestation_id = None

    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
                            name=False, price_unit=False, state='draft', context=None):

        if not context.get('default_salon_id', False):
            raise except_orm(_('Pas de salon défini!'), _("Vous devez le définir avant d'ajouter une ligne!"))

        return super(PurchaseOrderLine, self).onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
                                                                  partner_id, date_order, fiscal_position_id, date_planned,
                                                                  name, price_unit, state='draft', context=context)
