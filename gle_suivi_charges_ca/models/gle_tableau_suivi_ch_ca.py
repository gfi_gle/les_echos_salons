# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
from openerp.exceptions import ValidationError
# from numpy.ma.core import ids
from jinja2.ext import do
from lxml import etree
from collections import defaultdict
import logging

_logger = logging.getLogger(__name__)

PROVIVISION_STATES = [('draft', 'Brouillon'), ('done', 'Validé')]


class GleTableauSuiviChCA(models.Model):
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _name = 'gle.tableau_suivi_ch_ca'

    # type = fields.Selection(selection=[('ca','CA'),('ch','Charge')])
    gle_produit_id = fields.Many2one("gle_salon.produit", string="Produit", required=True)
    millesime = fields.Many2one('gle_salon.millesime', string='Projet', required=True)

    provision0_date = fields.Date('Date prevision 0')
    provision0_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision0_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision0_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision0_he_total = fields.Float('Total hors échange', readonly=True, )
    provision0_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision1_date = fields.Date('Date prevision 1')
    provision1_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision1_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision1_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision1_he_total = fields.Float('Total hors échange', readonly=True, )
    provision1_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision2_date = fields.Date('Date prevision 2')
    provision2_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision2_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision2_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision2_he_total = fields.Float('Total hors échange', readonly=True, )
    provision2_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision3_date = fields.Date('Date prevision 3')
    provision3_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision3_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision3_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision3_he_total = fields.Float('Total hors échange', readonly=True, )
    provision3_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision4_date = fields.Date('Date prevision 4')
    provision4_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision4_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision4_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision4_he_total = fields.Float('Total hors échange', readonly=True, )
    provision4_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision5_date = fields.Date('Date prevision 5')
    provision5_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision5_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision5_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision5_he_total = fields.Float('Total hors échange', readonly=True, )
    provision5_ae_total = fields.Float('Total avec échange', readonly=True, )
    provision6_date = fields.Date('Date prevision 6')
    provision6_state = fields.Selection(PROVIVISION_STATES, default='draft', readonly=True, )
    provision6_readonly = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision6_invisible = fields.Boolean(compute='_compute_provision_readonly_invisible')
    provision6_he_total = fields.Float('Total hors échange', readonly=True, )
    provision6_ae_total = fields.Float('Total avec échange', readonly=True, )

    facture_he_total = fields.Float('Total facturé hors échange', readonly=True, )
    facture_ae_total = fields.Float('Total facturé avec échange', readonly=True, )
    engage_he_total = fields.Float('Total engagé hors échange', readonly=True, )
    engage_ae_total = fields.Float('Total engagé avec échange', readonly=True, )

    line_ids = fields.One2many("gle.suivi_ch_ca.line", 'tableau_id', string="Lignes")

    section_line_ids = fields.Many2many(comodel_name='gle.suivi_ch_ca.line', compute='_compute_section_line_ids', string="Lignes")

    list_code_for_css = fields.Char(string="list_code_css", compute="_compute_list_code_for_css")

    @api.multi
    def _compute_section_line_ids(self):

        user_id = self.env.user
        ca_line_obj = self.env['gle.suivi_ch_ca.line']

        for section in user_id.section_ids:
            if user_id.profile_type_id.code in ['ADV', 'CF', 'DC', 'DBU', 'DAF']:
                section_id = section
            else:
                section_id = section

        for tableau in self:
            if user_id.profile_type_id.code in ['ADV', 'CF', 'DC', 'DBU', 'DAF', 'CTG', 'DO', 'CG']:
                line_ids = tableau.line_ids
            else:
                line_ids = ca_line_obj.search([('section_id', '=', section_id.id),
                                               ('tableau_id', '=', tableau.id)])

            tableau.section_line_ids = line_ids

    @api.multi
    def _compute_provision_readonly_invisible(self):
        for obj in self:
            for i in range(7):
                prev_state_field = 'provision%s_state' % (i - 1)
                current_ro_field = 'provision%s_readonly' % i
                current_state_field = 'provision%s_state' % i
                current_invisible_field = 'provision%s_invisible' % i
                current_state_value = getattr(obj, current_state_field, False)
                prev_state_value = hasattr(obj, prev_state_field) and getattr(obj, prev_state_field, 'done') or 'done'
                ro_value = False
                if prev_state_value == 'draft':
                    ro_value = True
                if current_state_value == 'done':
                    ro_value = True
                can_write = self.env['gle.suivi_ch_ca.line'].check_access_rights('write', raise_exception=False)
                invisible_value = not can_write and current_state_value == 'draft' and True or False
                setattr(obj, current_ro_field, ro_value)
                setattr(obj, current_invisible_field, invisible_value)

    @api.depends('line_ids')
    def _compute_list_code_for_css(self):
        """ get list of code_analytique_compose for css in lines"""
        for xtableau in self:
            _logger.debug(xtableau)
            list = []
            for line in xtableau.line_ids:
                if len(str(line.code_analytique_compose)) < 8:
                    list.append(str(line.code_analytique_compose))
            xtableau.list_code_for_css = list

    @api.multi
    def validate_provision(self):
        if not self.env.context.get('provision_state_field', False):
            return False
        provision_state_field = self.env.context.get('provision_state_field')
        for obj in self:
            setattr(obj, provision_state_field, 'done')
            prev_index = int(provision_state_field[9:10])
            if prev_index >= 0:
                prev_field_name = 'provision%s' % prev_index
                curr_field_name = 'provision%s' % (prev_index + 1)
                for line in obj.line_ids:
                    setattr(line, curr_field_name, getattr(line, prev_field_name, 0))

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        res = super(GleTableauSuiviChCA, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                               submenu=submenu)
        self.calcule_tableau()
        return res

    @api.model
    def find(self, obj, year=None, dt=None, delta=None):
        if dt and not year:
            year = dt[:4]
        if year and delta:
            year = int(year) + delta
        new_year = False
        if obj._name == 'account.invoice':
            new_year = obj.date_invoice and obj.date_invoice[:4] or False
            try:
                new_year = obj.move_id.mapped('date')[0][:4]
            except:
                pass
            salon = obj.salon_id
        elif obj._name == 'account.invoice.line':
            new_year = obj.invoice_id.date_invoice and obj.invoice_id.date_invoice[:4] or False
            try:
                new_year = obj.invoice_id.move_id.mapped('date')[0][:4]
            except:
                pass
            salon = obj.salon_id or obj.invoice_id.salon_id
        elif obj._name == 'purchase.order':
            new_year = fields.Date.today()[:4]
            salon = obj.salon_id
        elif obj._name == 'purchase.order.line':
            new_year = fields.Date.today()[:4]
            salon = obj.salon_id
        elif obj._name == 'gle.refund.to.receive.line':
            new_year = fields.Date.today()[:4]
            salon = obj.salon_id
        elif obj._name == 'gle.refund.to.receive':
            new_year = fields.Date.today()[:4]
            salon = obj.salon_id
        elif obj._name == 'gle_salon.salon':
            salon = obj
        elif obj._name == 'sale.order':
            new_year = obj.date_order[:4] if obj.date_order else fields.Date.today()[:4]
            salon = obj.salon_id
        elif obj._name == 'gle.account.refund':
            new_year = obj.date[:4] if obj.date else fields.Date.today()[:4]
            salon = obj.salon_id
        year = year or new_year
        if year:
            return self.search([
                ('gle_produit_id', '=', salon.name.id),
                ('millesime.name', '=', str(year))
            ])
        else:
            return self.search([
                ('gle_produit_id', '=', salon.name.id),
                ('millesime', '=', salon.millesime.id)
            ])


    @api.onchange('gle_produit_id', 'millesime')
    def on_change_produit_projet(self):
        if self.gle_produit_id or self.millesime:
            if self.search([('gle_produit_id', '=', self.gle_produit_id.id), ('millesime', '=', self.millesime.id)]):
                self.millesime = None
                self.gle_produit_id = None
                res = {
                    'warning': {
                        'title': _('Warning'),
                        'message': _(("Un tableau a été déjà créé pour ce Produit/Projet."))
                    }
                }
                return res

    def get_dict_line(self, type_tableau, section_id=None, famille_id=None, nature_id=None, type_id=None,
                      prestation_id=None):
        vals = {
            'type_tableau': type_tableau,
            'identifiant_ligne': type_tableau,
        }

        if section_id:

            vals.update({
                'parent': vals['identifiant_ligne'],
                'type_tableau': type_tableau,
                'section_id': section_id.id,
                'identifiant_ligne': type_tableau + str(section_id.code),
                'code_analytique_compose': str(section_id.code),
                'code_analytique_compose2': str(section_id.code),
                'nom_ligne': section_id.name,
            })
            if famille_id:
                code = "%s.%s" % (section_id.code, famille_id.code)
                vals.update({
                    'parent': vals['identifiant_ligne'],
                    'famille_id': famille_id.id,
                    'nom_ligne': famille_id.name,
                    'type_line': 'N',
                    'identifiant_ligne': type_tableau + code,
                    'code_analytique_compose': code,
                    'code_analytique_compose2': code,
                })
            if nature_id:
                vals.update({
                    'parent': vals['identifiant_ligne'],
                    'nature_id': nature_id.id,
                    'nom_ligne': nature_id.name,
                    'identifiant_ligne': type_tableau + nature_id.code_compose,
                    'code_analytique_compose': nature_id.code_compose,
                    'code_analytique_compose2': nature_id.code_compose,
                })

            if type_id:
                vals.update({
                    'parent': vals['identifiant_ligne'],
                    'type_id': type_id.id,
                    'nom_ligne': type_id.name,
                    'identifiant_ligne': type_tableau + type_id.code_compose,
                    'code_analytique_compose': type_id.code_compose,
                    'code_analytique_compose2': type_id.code_compose,

                })

            if prestation_id:
                vals.update({
                    'parent': vals['identifiant_ligne'],
                    'prestation_id': prestation_id.id,
                    'nom_ligne': prestation_id.name,
                    'type_line': 'N',
                    'identifiant_ligne': type_tableau + prestation_id.code_compose,
                    'code_analytique_compose': prestation_id.code_compose,
                    'code_analytique_compose2': prestation_id.code_compose,
                })

        return vals

    @api.one
    def generate_lines(self):
        lines = []
        for tab in self.search([]):

            codes = []
            if not self.gle_produit_id or not self.millesime:
                continue
            self._cr.execute(
                "SELECT l.identifiant_ligne FROM gle_suivi_ch_ca_line l, gle_tableau_suivi_ch_ca t WHERE l.tableau_id = t.id AND t.id=%s",
                (tab.id,))
            codes = [row[0] for row in self._cr.fetchall()]

            if 'ch2_ce' not in codes:
                lines.append((0, 0, {
                    'type_tableau': 'ch2_ce',
                    'identifiant_ligne': 'ch2_ce',
                    'code_analytique_compose': "Charges d'Exploitation",
                    'code_analytique_compose2': '0'
                }))
            # if 'pdt2_pe' not in codes:
            #     lines.append((0, 0, {
            #         'type_tableau': 'pdt2_pe',
            #         'identifiant_ligne': 'pdt2_pe',
            #         'code_analytique_compose': "Produits d'Exploitation",
            #         'code_analytique_compose2': '0'
            #     }))
            if 'ch4_em' not in codes:
                lines.append((0, 0, {
                    'type_tableau': 'ch4_em',
                    'identifiant_ligne': 'ch4_em',
                    'code_analytique_compose': "Echange de marchandises",
                    'code_analytique_compose2': '0'
                }))
            if 'ch3_ep' not in codes:
                lines.append((0, 0, {
                    'type_tableau': 'ch3_ep',
                    'identifiant_ligne': 'ch3_ep',
                    'code_analytique_compose': "Ecarts de Provisions de charges",
                    'code_analytique_compose2': '0'
                }))
            # if 'pdt3_pe' not in codes:
            #     lines.append((0, 0, {
            #         'type_tableau': 'pdt3_pe',
            #         'identifiant_ligne': 'pdt3_pe',
            #         'code_analytique_compose': "Ecarts de Provisions de CA",
            #         'code_analytique_compose2': '0'
            #     }))

            for section in self.env['gle.section'].search([]):
                if not self._context.get('line_familly'):
                    if section.categ_ids:
                        identifiant = 'pdt2_pe' + str(section.code)
                        if identifiant not in codes:
                            lines.append((0, 0, self.get_dict_line(section_id=section, type_tableau="pdt2_pe")))

                        if not self._context.get('line_familly'):
                            for famille in section.categ_ids:
                                identifiant = 'pdt2_pe' + famille.code_compose
                                if identifiant not in codes:
                                    lines.append((0, 0, self.get_dict_line(section_id=section, famille_id=famille,
                                                                           type_tableau="pdt2_pe")))

                if section.nature_ids:

                    identifiant = 'ch2_ce' + str(section.code)
                    if identifiant not in codes:
                        lines.append((0, 0, self.get_dict_line(section_id=section, type_tableau="ch2_ce")))

                    for nature in section.nature_ids:

                        identifiant = 'ch2_ce' + nature.code_compose
                        if identifiant not in codes:
                            lines.append(
                                (0, 0, self.get_dict_line(section_id=section, nature_id=nature, type_tableau="ch2_ce")))

                        for type in nature.type_ids:

                            identifiant = 'ch2_ce' + type.code_compose
                            if identifiant not in codes:
                                lines.append((0, 0,
                                              self.get_dict_line(section_id=section, nature_id=nature, type_id=type,
                                                                 type_tableau="ch2_ce")))

                            for prestation in type.prestation_ids:
                                identifiant = 'ch2_ce' + prestation.code_compose
                                if identifiant not in codes:
                                    lines.append((0, 0,
                                                  self.get_dict_line(section_id=section, nature_id=nature, type_id=type,
                                                                     prestation_id=prestation, type_tableau="ch2_ce")))

            if lines:
                tab.write({
                    'gle_produit_id': tab.gle_produit_id.id,
                    'millesime': tab.millesime.id,
                    'line_ids': lines
                })
            else:
                tab.write({
                    'gle_produit_id': tab.gle_produit_id.id,
                    'millesime': tab.millesime.id
                })

    @api.multi
    def recompute_lines_total(self):

        millesime_id = self.millesime
        produit_id = self.gle_produit_id

        purchase_order_ids = self.env['purchase.order'].search([
            ('salon_id.name', '=', produit_id.id),
            ('salon_id.millesime', '=', millesime_id.id),
            ('state', '=', 'approved')])

        sale_order_ids = self.env['sale.order'].search([
            ('salon_id.name', '=', produit_id.id),
            ('salon_id.millesime', '=', millesime_id.id),
            ('state', 'not in', ['cancel', 'draft', 'sent', 'shipping_except', 'invoice_except', 'passage_sans_suite', 'waiting_date']),
            ('type_facturation', '=', 'intercos')])

        # Recompute for purchase.order
        for order in purchase_order_ids:
            reference = u'Achat %s' % (order.name or '')
            for line in order.order_line:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
                if order.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=line.prestation_id.type_id.nature_id.section_id,
                                                     nature=line.prestation_id.type_id.nature_id,
                                                     type=line.prestation_id.type_id,
                                                     prestation=line.prestation_id,
                                                     amount=line.price_subtotal,
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)

        for order in sale_order_ids:
            reference = u'Bon de commande %s, intercos' % (order.name or '')
            if order.type_facturation != 'intercos' and not order.partner_id.intercos:
                continue
            for line in order.order_line:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(order.salon_id)
                if order.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=order.gle_section_id,
                                                     amount=-line.price_subtotal,
                                                     famille=False,
                                                     force_string='4.F.10.01',
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)

    @api.multi
    def get_section_lines(self):
        return {
            'name': self.gle_produit_id.name + " / " + self.millesime.name,
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'gle.suivi_ch_ca.line',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('tableau_id', '=', self.id), '|', ('code_analytique_compose2', '=', '0'),
                       ('section_id', '=', self.env['res.users'].browse([self._uid]).section_ids.ids)],
        }



    @api.multi
    def update_suivi_charge(self, tableau, section, amount, facture=False, nature=None, type=None, prestation=None,
                            famille=None, force_string=None, reference=None, record=None, cancel=None, ecart=False,
                            create_ecart=False, store=True):
        assert record, 'please specify the record'
        assert cancel in [True, False], 'please specify the cancel attribute'
        self = self.sudo()
        result = {}
        def croisement(lval, lvals):
            for __, __, lval_data in lvals:
                if lval.get('code_analytique_compose', False) and lval_data.get('code_analytique_compose', False):
                    if lval.get('code_analytique_compose', False) == lval_data.get('code_analytique_compose', False):
                        return True
            return False

        suivi_obj = self.env['gle.suivi_ch_ca.line']
        if force_string:
            s_code, n_code, t_code, p_code = force_string.strip().replace(' ', '').split('.')
            s_code, n_code, t_code, p_code = int(s_code), n_code, int(t_code), int(p_code)
            prestation = self.env['gle.prestation'].search([
                ('code', '=', p_code),
                ('type_id.code', '=', t_code),
                ('type_id.nature_id.code_analytique', '=', n_code),
                ('type_id.nature_id.section_id.code', '=', s_code),
            ], limit=1)
            if prestation:
                type = prestation.type_id
                nature = type.nature_id
                section = nature.section_id
                famille = False
        #ids = self.line_ids.mapped('id')
        domain = [('tableau_id', '=', self.id),
                  ('type_tableau', '=', tableau),
                  ('section_id', '=', section.id)]
        vals = {
            'type_tableau': tableau,
            'section_id': section,
        }
        if nature:
            vals['nature_id'] = nature
            domain.append(('nature_id', '=', nature.id))
        if type:
            vals['type_id'] = type
            domain.append(('type_id', '=', type.id))
        if prestation:
            vals['prestation_id'] = prestation
            domain.append(('prestation_id', '=', prestation.id))
        if famille:
            vals['famille_id'] = famille
            domain.append(('famille_id', '=', famille.id))

        line = suivi_obj.search(domain, order='id asc')

        line = line.unlink_doublon()

        if line:
            if facture:
                if amount:
                    line.facture += amount
                    line.tsc_touched = True
                    result['tsc_last_facture'] = amount
            else:
                if amount:
                    line.engage += amount
                    line.tsc_touched = True
                    result['tsc_last_engage'] = amount

        else:
            list_vals = []
            line_values = self.get_dict_line(type_tableau=vals['type_tableau'],
                                             section_id=vals['section_id'],
                                             famille_id=vals.get('famille_id', False),
                                             nature_id=vals.get('nature_id', False),
                                             type_id=vals.get('type_id', False),
                                             prestation_id=vals.get('prestation_id', False))
            if facture:
                if amount:
                    line_values['facture'] = amount
                    line_values['tsc_touched'] = True
                    result['tsc_last_facture'] = amount
            else:
                if amount:
                    line_values['engage'] = amount
                    line_values['tsc_touched'] = True
                    result['tsc_last_engage'] = amount
            list_vals.append((0, 0, line_values))
            if prestation:
                identifiant = '%s%s' % (tableau, type.code_compose)
                if not suivi_obj.search([('identifiant_ligne', '=', identifiant)]):
                    # LIGNE POUR LE TYPE
                    line_values = self.get_dict_line(type_tableau=vals['type_tableau'],
                                                     section_id=vals['section_id'],
                                                     famille_id=vals.get('famille_id', False),
                                                     nature_id=vals.get('nature_id', False),
                                                     type_id=vals.get('type_id', False),
                                                     prestation_id=False)
                    line_values.update({'facture': 0, 'engage': 0})
                    list_vals.append((0, 0, line_values))
                identifiant = '%s%s' % (tableau, nature.code_compose)
                if not suivi_obj.search([('identifiant_ligne', '=', identifiant)]):
                    # LIGNE POUR LA NATURE
                    line_values = self.get_dict_line(type_tableau=vals['type_tableau'],
                                                     section_id=vals['section_id'],
                                                     famille_id=vals.get('famille_id', False),
                                                     nature_id=vals.get('nature_id', False),
                                                     type_id=False,
                                                     prestation_id=False)
                    line_values.update({'facture': 0, 'engage': 0})
                    list_vals.append((0, 0, line_values))
                identifiant = '%s%s' % (tableau, section.code)
                if not suivi_obj.search([('identifiant_ligne', '=', identifiant)]):
                    # LIGNE POUR LA SECTION
                    line_values = self.get_dict_line(type_tableau=vals['type_tableau'],
                                                     section_id=vals['section_id'],
                                                     famille_id=vals.get('famille_id', False),
                                                     nature_id=False,
                                                     type_id=False,
                                                     prestation_id=False)
                    line_values.update({'facture': 0, 'engage': 0})
                    list_vals.append((0, 0, line_values))
            if famille:
                identifiant = '%s%s' % (tableau, section.code)
                if not suivi_obj.search([('identifiant_ligne', '=', identifiant)]):
                    # LIGNE POUR LA SECTION
                    line_values = self.get_dict_line(type_tableau=vals['type_tableau'],
                                                     section_id=vals['section_id'],
                                                     famille_id=False,
                                                     nature_id=vals.get('nature_id', False),
                                                     type_id=vals.get('type_id', False),
                                                     prestation_id=vals.get('prestation_id', False))
                    line_values.update({'facture': 0, 'engage': 0})
                    if not croisement( line_values, list_vals):
                        list_vals.append((0, 0, line_values))
            self.write({'line_ids': list_vals})
        # POST A MESSAGE
        message_body = u"""
                <strong>Référence</strong> : {reference}<br />
                <strong>Heure</strong> : {heure:%d/%m/%Y :%H:%M:%S}<br />
                <strong>Montant</strong> : {amount:.2f}<br />
                <strong>Tableau</strong> : {tableau}<br />
                <strong>Colonne</strong> : {facture}<br />
                <strong>Section</strong> : {section}<br />
                <strong>Prestation</strong> : {prestation}<br />
                <strong>Line</strong> : {line}<br />
                <strong>Force line</strong> : {force_string}<br />
                <strong>Number of line created</strong> : {nbr}<br />
                <br /><br /><br /><br /><br /><br /><br /><br />
                <strong>Created lines</strong> : {created_lines}<br />
                """
        message_empty = u'*' * 10
        message_data = {
            'heure': datetime.datetime.now(),
            'tableau': tableau or message_empty,
            'section': section and section.name or message_empty,
            'amount': amount,
            'facture': facture and u'Facturé' or u'Engagé',
            'nature': nature and nature.name or message_empty,
            'type': type and type.name or message_empty,
            'prestation': prestation and prestation.name or message_empty,
            'famille': famille and famille.name or message_empty,
            'nbr': 0,
            'line': '',
            'force_string': force_string,
            'created_lines': '',
            'reference': reference or u'Non trouvée',
        }
        try:
            message_data['nbr'] = len(line_values)
        except:
            pass
        try:
            message_data['line'] = line and line.code_analytique_compose or ''
        except:
            pass
        try:
            message_data['created_lines'] = line_values
        except:
            pass

        message_final = message_body.format(**message_data)
        self.message_post(message_final)
        self.calcule_tableau()
        if store and record and result:
            if ecart and create_ecart:
                if result.get('tsc_last_engage', 0):
                    result['tsc_ecart'] = result.get('tsc_last_engage')
                    del result['tsc_last_engage']
                if result.get('tsc_last_facture', 0):
                    result['tsc_ecart'] = result.get('tsc_last_facture')
                    del result['tsc_last_facture']
                for record_item in record:
                    record_item.sudo().write(result)
            if not ecart:
                if cancel:
                    result['tsc_canceled'] = True
                else:
                    result['tsc_canceled'] = False
                for record_item in record:
                    if result.get('tsc_last_engage', 0) != 0:
                        result['tsc_total_engage'] = record_item.tsc_total_engage + result['tsc_last_engage']
                        result['tsc_touched'] = True
                    if result.get('tsc_last_facture', 0) != 0:
                        result['tsc_total_facture'] = record_item.tsc_total_facture + result['tsc_last_facture']
                        result['tsc_touched'] = True
                    record_item.sudo().write(result)

    @api.multi
    def write(self, vals):
        res = super(GleTableauSuiviChCA, self).write(vals)

        if vals.get('line_ids'):
            self.calcule_tableau()

        return res

    @api.constrains('provision0_date', 'provision1_date', 'provision2_date', 'provision3_date', 'provision4_date', 'provision5_date', 'provision6_date')
    def constrains_profile_attribute(self):
        user_id = self.env.user

        if self.env.context.get('is_table', False):
            if user_id.profile_type_id.name == 'ADV':
                pass
            else:
                raise ValidationError(
                    "Impossible de valider le tableau de suivi des charges. Seul l'ADV possède ce droit.")

    @api.multi
    def calcule_tableau(self):
        groups = defaultdict(list)
        without_parent = self.env['gle.suivi_ch_ca.line']

        # Recherhce doublons
        doublons = set()
        all_codes = set()
        for obj in self.line_ids.filtered(lambda line: line.type_line == 'N'):
            if obj.code_analytique_compose in all_codes:
                doublons.add(obj.code_analytique_compose)
            all_codes.add(obj.code_analytique_compose)
        for doublon_item in doublons:
            doublon_lines = self.line_ids.sudo().search([
                ('tableau_id', '=', self.id),
                ('code_analytique_compose', '=', doublon_item),
            ])
            doublon_lines.unlink_doublon()

        # groupe les lignes qui ont le meme parent
        for obj in self.line_ids:
            if obj.parent:
                groups[obj.parent].append(obj)
                if obj.type_line == 'N':
                    without_parent += obj

        groups_list = groups.values()
        i = 0
        while i < 5:
            for group in groups_list:
                prov0 = prov1 = prov2 = prov3 = prov4 = prov5 = prov6 = eng = fact = 0

                for l in group:
                    prov0 += l.provision0
                    prov1 += l.provision1
                    prov2 += l.provision2
                    prov3 += l.provision3
                    prov4 += l.provision4
                    prov5 += l.provision5
                    prov6 += l.provision6
                    prov6 += l.provision6
                    eng += l.engage
                    fact += l.facture

                line = self.line_ids.filtered(lambda r: r.identifiant_ligne == l.parent)
                line.write({
                    'provision0': prov0, 'provision1': prov1, 'provision2': prov2,
                    'provision3': prov3, 'provision4': prov4, 'provision5': prov5,
                    'provision6': prov6, 'engage': eng, 'facture': fact
                })
            i = i + 1
        to_sum_hors_echange = without_parent.filtered(
            lambda r: r.type_tableau not in ['ch0_total_he', 'ch0_total', 'ch4_em'])
        self.write({
            'provision0_he_total': sum(to_sum_hors_echange.mapped('provision0')),
            'provision1_he_total': sum(to_sum_hors_echange.mapped('provision1')),
            'provision2_he_total': sum(to_sum_hors_echange.mapped('provision2')),
            'provision3_he_total': sum(to_sum_hors_echange.mapped('provision3')),
            'provision4_he_total': sum(to_sum_hors_echange.mapped('provision4')),
            'provision5_he_total': sum(to_sum_hors_echange.mapped('provision5')),
            'provision6_he_total': sum(to_sum_hors_echange.mapped('provision6')),
            'engage_he_total': sum(to_sum_hors_echange.mapped('engage')),
            'facture_he_total': sum(to_sum_hors_echange.mapped('facture')),
        })
        to_sum_avec_echange = without_parent.filtered(lambda r: r.type_tableau == 'ch4_em')
        self.write({
            'provision0_ae_total': sum(to_sum_avec_echange.mapped('provision0')) + self.provision0_he_total,
            'provision1_ae_total': sum(to_sum_avec_echange.mapped('provision1')) + self.provision1_he_total,
            'provision2_ae_total': sum(to_sum_avec_echange.mapped('provision2')) + self.provision2_he_total,
            'provision3_ae_total': sum(to_sum_avec_echange.mapped('provision3')) + self.provision3_he_total,
            'provision4_ae_total': sum(to_sum_avec_echange.mapped('provision4')) + self.provision4_he_total,
            'provision5_ae_total': sum(to_sum_avec_echange.mapped('provision5')) + self.provision5_he_total,
            'provision6_ae_total': sum(to_sum_avec_echange.mapped('provision6')) + self.provision6_he_total,
            'engage_ae_total': sum(to_sum_avec_echange.mapped('engage')) + self.engage_he_total,
            'facture_ae_total': sum(to_sum_avec_echange.mapped('facture')) + self.facture_he_total,
        })


    # Three debugging methods :
    @api.multi
    def clean_all_chatter_messages(self):
        self.env['mail.message'].search([
            ('model', '=', self._name),
            ('res_id', 'in', self.ids),
        ]).unlink()

    @api.multi
    def clean_all_tab_values(self):
        for tab in self:
            for line in tab.line_ids:
                line.engage = line.facture = 0

    @api.multi
    def clean_all_line_ids(self):
        for tab in self:
            tab.line_ids.unlink()

class GleSuiviChCALine(models.Model):
    _name = 'gle.suivi_ch_ca.line'
    _order = 'type_tableau, code_analytique_compose2'

    parent = fields.Char('Parent')
    identifiant_ligne = fields.Char('identifiant')
    tableau_id = fields.Many2one("gle.tableau_suivi_ch_ca", string="Tableau que Suivi ch/CA", ondelete="cascade")
    tableau_name = fields.Char(string="Tableau", compute='_compute_tableau_name',  )

    type_tableau = fields.Selection(selection=[('ch0_total_he', 'Total hors Echange'),
                                               ('ch0_total', 'Total avec Echange'),
                                               ('ch2_ce', "Charges d'Exploitation"),
                                               ('ch3_ep', 'Ecarts de Provisions de charges'),
                                               ('ch4_em', "Echange de marchandises"),
                                               ('pdt2_pe', "Produits d'Exploitation"),
                                               ('pdt3_pe', "Ecarts de Provisions de CA"),
                                               ], required=True)
    type_line = fields.Selection(selection=[('N', 'Normal'), ('V', 'Vue')], default='V')
    commentaire = fields.Char(string="Commentaire")
    code_analytique_compose = fields.Char('code analytique', readonly=True, required=True)
    code_analytique_compose2 = fields.Char('code analytique', readonly=True, required=True)
    nom_ligne = fields.Char('Description', readonly=True)

    section_id = fields.Many2one("gle.section", 'Section', ondelete='restrict')
    famille_id = fields.Many2one("product.category", 'Famille', domain="[('section_id.id','=', section_id.id)]",
                                 ondelete='restrict')
    nature_id = fields.Many2one("gle.nature", 'Nature', domain="[('section_id.id','=', section_id.id)]",
                                ondelete='restrict')
    type_id = fields.Many2one("gle.type", 'Type', domain="[('nature_id.id','=', nature_id.id)]", ondelete='restrict')
    prestation_id = fields.Many2one("gle.prestation", 'Prestation', domain="[('type_id.id','=', type_id.id)]",
                                    ondelete='restrict')

    provision0 = fields.Integer("Prev 0")
    provision0_readonly = fields.Boolean(related='tableau_id.provision0_readonly', store=False)
    provision0_invisible = fields.Boolean(related='tableau_id.provision0_invisible', store=False, related_sudo=False)
    provision1 = fields.Integer("Prev 1")
    provision1_readonly = fields.Boolean(related='tableau_id.provision1_readonly', store=False)
    provision1_invisible = fields.Boolean(related='tableau_id.provision1_invisible', store=False, related_sudo=False)
    provision2 = fields.Integer("Prev 2")
    provision2_readonly = fields.Boolean(related='tableau_id.provision2_readonly', store=False)
    provision2_invisible = fields.Boolean(related='tableau_id.provision2_invisible', store=False, related_sudo=False)
    provision3 = fields.Integer("Prev 3")
    provision3_readonly = fields.Boolean(related='tableau_id.provision3_readonly', store=False)
    provision3_invisible = fields.Boolean(related='tableau_id.provision3_invisible', store=False, related_sudo=False)
    provision4 = fields.Integer("Prev 4")
    provision4_readonly = fields.Boolean(related='tableau_id.provision4_readonly', store=False)
    provision4_invisible = fields.Boolean(related='tableau_id.provision4_invisible', store=False, related_sudo=False)
    provision5 = fields.Integer("Prev 5")
    provision5_readonly = fields.Boolean(related='tableau_id.provision5_readonly', store=False)
    provision5_invisible = fields.Boolean(related='tableau_id.provision5_invisible', store=False, related_sudo=False)
    provision6 = fields.Integer("Prev 6")
    provision6_readonly = fields.Boolean(related='tableau_id.provision6_readonly', store=False)
    provision6_invisible = fields.Boolean(related='tableau_id.provision6_invisible', store=False, related_sudo=False)

    bool_red = fields.Boolean("bool_red", compute="_compute_bool_red")

    total = fields.Boolean(string='Total', default=False, )

    engage = fields.Float('Engage')
    facture = fields.Float('Facture')
    tsc_touched = fields.Boolean(string='Touched', default=False,   )

    @api.multi
    @api.depends('tableau_id')
    def _compute_tableau_name(self):
        for line in self:
            line.tableau_name = line.tableau_id.gle_produit_id.name + ' - ' + line.tableau_id.millesime.name

    @api.multi
    @api.depends('tableau_id')
    def _compute_bool_red(self):
        for line in self:
            if line.code_analytique_compose in eval(line.tableau_id.list_code_for_css):
                line.bool_red = True
            else:
                line.bool_red = False

    @api.multi
    def write(self, vals):
        res = super(GleSuiviChCALine, self).write(vals)

        user_id = self.env.user

        if self.env.context.get('is_table', False):
            if user_id.profile_type_id.name == 'ADV':
                pass
            else:
                raise ValidationError(
                    "Impossible de valider le tableau de suivi des charges. Seul l'ADV possède ce droit.")

        return res

    @api.multi
    def unlink_doublon(self):
        line = self
        suivi_obj = self.env['gle.suivi_ch_ca.line']
        if len(line) <= 1:
            return line
        if len(line) > 1:
            line = self[0]
            to_delete = suivi_obj
            for tmp_line in self:
                if tmp_line.engage == 0 and tmp_line.facture == 0:
                    to_delete |= tmp_line
                else:
                    line = tmp_line
            if len(to_delete) == len(self):
                line = to_delete[0]
                to_delete = to_delete[1:]
            if to_delete:
                _logger.info('return #%s and delete lines %s', line.id, ' #'.join([str(x.id) for x in to_delete]))
                to_delete.sudo().unlink()
            return line

