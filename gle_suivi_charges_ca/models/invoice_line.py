# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning, ValidationError

from openerp.addons.purchase_tracker.model.invoicegenerator import (
    WithPOInvoiceBuilder,
)
import logging

_logger = logging.getLogger(__name__)

OUT_INVOICE, OUT_REFUND, IN_INVOICE, IN_REFUND = ['out_invoice', 'out_refund', 'in_invoice', 'in_refund']
INS, OUTS = [IN_INVOICE, IN_REFUND], [OUT_INVOICE, OUT_REFUND]


def _prepare_invoice(self):
    """ Builds the dict containing the values for the invoice
    """

    invoice_vals = super(WithPOInvoiceBuilder, self)._prepare_invoice()
    if self.tracker.purchase_order_ids:
        po_br = self.tracker.purchase_order_ids[0]
        po_names = ','.join(self.tracker.purchase_order_ids.filtered(lambda r: r.name).mapped('name'))
        invoice_vals['origin'] = po_names
        invoice_vals['salon_id'] = po_br.salon_id.id
        if not self.tracker.company_id.tracker_disable_payment_term:
            invoice_vals['payment_term'] = po_br.payment_term_id.id

    if self.tracker.refund_to_receive_ids:
        po_first_record = self.tracker.refund_to_receive_ids[0]
        po_aar_name = ','.join(self.tracker.refund_to_receive_ids.filtered(lambda r: r.name).mapped('name'))
        invoice_vals['origin'] = po_aar_name
        invoice_vals['salon_id'] = po_first_record.salon_id.id

    if self.tracker.due_date:
        invoice_vals['date_due'] = self.tracker.due_date
    if not invoice_vals.get('payment_term') and self.tracker.partner_id.operational_supplier_payment_terms_id.id:
        invoice_vals['payment_term'] = self.tracker.partner_id.operational_supplier_payment_terms_id.id

    return invoice_vals


def _prepare_invoice_line(self, order_line):
    """ Builds the dict containing the values for the invoice line
    """

    res = {
        'name': order_line.name,
        'origin': '',
        'invoice_id': self.invoice.id,
        'product_id': order_line.product_id.id,
        'salon_id': order_line.salon_id.id,
        'prestation_id': order_line.prestation_id.id,
        'account_id': order_line.product_id.property_account_expense.id,
        'price_unit': order_line.price_unit,
        'quantity': order_line.product_qty,
        'currency_id': self.tracker.invoice_currency_id.id,
        'invoice_line_tax_id': self._get_tax(order_line),
        'discount': order_line.discount,
        'discount_type': order_line.discount_type,
    }
    if order_line and order_line._name == 'purchase.order.line':
        res['purchase_line_id'] = order_line.id
    elif order_line and order_line._name == 'gle.refund.to.receive.line':
        res['refund_to_receive_line_id'] = order_line.id
    res.update(self.pool['analytic.structure'].extract_values(
        self.cr, self.uid, order_line, 'purchase_order_line',
        dest_model='account_invoice_line'
    ))
    return res


WithPOInvoiceBuilder._prepare_invoice_line = _prepare_invoice_line
WithPOInvoiceBuilder._prepare_invoice = _prepare_invoice


class InvoiceLine(models.Model):
    _inherit = ['tsc.line.mixin', 'account.invoice.line']
    _name = 'account.invoice.line'

    salon_id = fields.Many2one(
        comodel_name='gle_salon.salon',
        string="Salon")

    gle_section_id = fields.Many2one(
        comodel_name='gle.section',
        compute='compute_analytic',
        string="Section",
        store=True)

    nature_id = fields.Many2one(
        comodel_name='gle.nature',
        string="Nature",
        compute='compute_analytic',
        store=True)

    type_id = fields.Many2one(
        comodel_name='gle.type',
        string="Type",
        compute='compute_analytic',
        store=True)

    prestation_id = fields.Many2one("gle.prestation", string="Prestation")

    product_id = fields.Many2one('product.product', 'Product',
                                 domain=[('purchase_ok', '=', True), ('prestation_id', '!=', None),
                                         ('salon_id', '!=', None)], change_default=True)

    invoice_type = fields.Selection([('out_invoice', 'Customer Invoice'),
                                     ('in_invoice', 'Supplier Invoice'),
                                     ('out_refund', 'Customer Refund'),
                                     ('in_refund', 'Supplier Refund'), ],
                                    string='Invoice Type',
                                    default=lambda self: self._context.get('type', 'in_invoice'), )

    refund_to_receive_line_id = fields.Many2one('gle.refund.to.receive.line', string='Refund to receive line',
                                                ondelete='set null', )

    @api.multi
    def _get_related_line(self):
        self.ensure_one()
        if self.invoice_id:
            if self.invoice_id.type == OUT_INVOICE:
                sql = """
                SELECT order_line_id, invoice_id 
                FROM sale_order_line_invoice_rel
                WHERE invoice_id = %s
                """
                self.env.cr.execute(sql, (self.id,))
                order_line_ids = [x[0] for x in self.env.cr.fetchall()]
                order_line_id = order_line_ids[0] if order_line_ids else False
                return self.env['sale.order.line'].browse(order_line_id)  if order_line_id else False
            elif self.invoice_id.type == OUT_REFUND:
                return self.gle_account_refund_line_id
            elif self.invoice_id.type == IN_INVOICE:
                return self.purchase_line_id
            elif self.invoice_id.type == IN_REFUND:
                return self.refund_to_receive_line_id
        return False

    @api.model
    def _get_emp_domain(self):
        user_id = self.env['res.users'].browse([self.env.context.get("uid")])
        if user_id:
            if self.env['gle.section'].search([('name', '=', 'ADV')]).id in user_id.section_ids.ids:
                domain = "[]"
            else:
                domain = [('section_related_id', 'in', user_id.section_ids.ids)]
        return domain

    @api.multi
    def product_id_change(self, product, uom_id, qty=0, name='', type='out_invoice',
                          partner_id=False, fposition_id=False, price_unit=False, currency_id=False,
                          company_id=None):
        if self._context.get('default_salon_id', False) == False and type == "in_invoice":
            raise except_orm(_('Pas de salon défini!'), _("Vous devez le définir avant d'ajouter une ligne!"))

        return super(InvoiceLine, self).product_id_change(product, uom_id, qty, name, type,
                                                          partner_id, fposition_id, price_unit, currency_id, company_id)


    # @api.onchange('nature_id')
    # def onchange_nature_id(self):
    #     if (self.nature_id):
    #         self.gle_section_id = self.nature_id.section_id.id
    #
    # @api.onchange('type_id')
    # def onchange_type_id(self):
    #     if (self.type_id):
    #         self.nature_id = self.type_id.nature_id.id
    #         self.gle_section_id = self.type_id.nature_id.section_id.id

    @api.depends('prestation_id')
    def compute_analytic(self):

        for invoice_line in self:
            invoice_line.type_id = invoice_line.prestation_id.type_id.id
            invoice_line.nature_id = invoice_line.type_id.nature_id.id
            invoice_line.gle_section_id = invoice_line.nature_id.section_id.id
            invoice_line.date_planned = datetime.datetime.now()

    @api.onchange('prestation_id', 'salon_id')
    def onchange_prestation_id(self):
        if self.prestation_id and self.salon_id:

            if not self.product_id:
                if len(self.prestation_id.product_ids) == 1:
                    self.product_id = self.prestation_id.product_ids[0].id
                    self.name = self.product_id.name
                    self.price_unit = self.product_id.lst_price
                    self.account_id = self.product_id.property_account_expense.id
                    # self.date_planned = datetime.datetime.now()

                    if self.salon_id and not self.product_id.salon_id:
                        self.product_id.write({'salon_id': self.salon_id.id})
                    elif self.product_id.salon_id and self.product_id.salon_id != self.salon_id.id:
                        self.salon_id = self.prestation_id.product_ids[0].salon_id.id

            # product = self.env["product.product"].search([('prestation_id', '=', self.prestation_id.id),
            #                                               ('property_account_expense', '!=', None),
            #                                               ('purchase_ok', '=', True),
            #                                               ('salon_id', '!=', None)])

            return {'domain': {'product_id': [('prestation_id', '=', self.prestation_id.id),
                                              ('property_account_expense', '!=', None),
                                              ('purchase_ok', '=', True),
                                              ('salon_id', '=', self.salon_id.id)],
                               'nature_id': [],
                               'prestation_id': [],
                               'type_id': [],
                               'gle_section_id': [],
                               }}
        domain = {'domain': {'prestation_id': self._get_emp_domain(), }}
        return domain

    @api.model
    def create(self, vals):
        prestation_id = self.env['gle.prestation'].browse(vals.get('prestation_id'))
        vals_2 = {
            'nature_id': prestation_id.type_id.nature_id.id or False,
            'type_id': prestation_id.type_id.id or False,
            'gle_section_id': prestation_id.type_id.nature_id.section_id.id or False,
        }
        account_id = prestation_id.type_id.account_id.id
        if account_id:
            vals_2.update({'account_id': account_id})
        vals.update(vals_2)
        return super(InvoiceLine, self).create(vals)

    # @api.multi
    # def write(self, vals):
    #     for obj in self:
    #         prestation_id = obj.prestation_id
    #         vals_2 = {
    #             'nature_id': prestation_id.type_id.nature_id.id or False,
    #             'type_id': prestation_id.type_id.id or False,
    #             'gle_section_id': prestation_id.type_id.nature_id.section_id.id or False,
    #         }
    #         vals.update(vals_2)
    #     return super(InvoiceLine, self).write(vals)

    @api.multi
    def tsc_intercos_not_clos(self, cancel):
        for line in self:
            inv = line.invoice_id
            reference = u'Facture intercos ID=%s Name=%s Origine=%s' % (inv.id, inv.name, inv.origin or '')
            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(inv.salon_id)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "ch2_ce"
            if suivi_charge:
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=inv.gle_section_id,
                                                 amount=-line.price_subtotal if not cancel else -line.tsc_last_facture,
                                                 famille=False,
                                                 facture=True,
                                                 force_string='4.F.10.01',
                                                 reference=reference,
                                                 record=line,
                                                 cancel=cancel)


    @api.multi
    def tsc_intercos_clos(self, cancel, ecart):
        for line in self:
            inv = line.invoice_id
            reference = u'Facture intercos ID=%s Name=%s Origine=%s' % (inv.id, inv.name, inv.origin or '')
            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(inv.salon_id)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "pdt3_pe"
            if suivi_charge:
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=inv.gle_section_id,
                                                 amount=-ecart,
                                                 famille=False,
                                                 facture=True,
                                                 force_string='4.F.10.01',
                                                 reference=reference,
                                                 record=line,
                                                 cancel=cancel,
                                                 ecart=True,
                                                 create_ecart=True)
            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(inv)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "pdt3_pe"
            if suivi_charge:
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=inv.gle_section_id,
                                                 amount=ecart,
                                                 famille=False,
                                                 facture=True,
                                                 force_string='4.F.10.01',
                                                 reference=reference,
                                                 record=line,
                                                 cancel=cancel,
                                                 ecart=True)


    @api.multi
    def tsc_in_not_clos(self, cancel):
        for line in self:
            inv = line.invoice_id
            reference = u'Facture fournisseur ID=%s Name=%s Origine=%s' % (inv.id, inv.name, inv.origin or '')
            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "ch2_ce"
            if suivi_charge:
                amount = line.price_subtotal if inv.type == IN_INVOICE else -line.price_subtotal
                amount = amount if not cancel else -line.tsc_last_facture
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=line.prestation_id.type_id.nature_id.section_id,
                                                 nature=line.prestation_id.type_id.nature_id,
                                                 type=line.prestation_id.type_id,
                                                 prestation=line.prestation_id,
                                                 amount=amount,
                                                 reference=reference,
                                                 facture=True,
                                                 record=line,
                                                 cancel=cancel)

    @api.multi
    def tsc_in_clos(self, cancel, ecart):
        for line in self:
            inv = line.invoice_id
            reference = u'Facture fournisseur ID=%s Name=%s Origine=%s' % (inv.id, inv.name, inv.origin or '')
            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line.salon_id)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "pdt3_pe"
            if suivi_charge:
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=line.prestation_id.type_id.nature_id.section_id,
                                                 nature=line.prestation_id.type_id.nature_id,
                                                 type=line.prestation_id.type_id,
                                                 prestation=line.prestation_id,
                                                 amount=-ecart,
                                                 reference=reference,
                                                 facture=True,
                                                 record=line,
                                                 cancel=cancel,
                                                 create_ecart=True,
                                                 ecart=True)

            suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(line)
            if inv.type_facturation in ['m_echange', 'r_echange']:
                tableau = "ch4_em"
            else:
                tableau = "pdt3_pe"
            if suivi_charge:
                suivi_charge.update_suivi_charge(tableau=tableau,
                                                 section=line.prestation_id.type_id.nature_id.section_id,
                                                 nature=line.prestation_id.type_id.nature_id,
                                                 type=line.prestation_id.type_id,
                                                 prestation=line.prestation_id,
                                                 amount=ecart,
                                                 reference=reference,
                                                 facture=True,
                                                 record=line,
                                                 cancel=cancel,
                                                 ecart=True)


