# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime
import logging

_logger = logging.getLogger(__name__)


class GleType(models.Model):

    _name = 'gle.type'
    
    _sql_constraints = [
        ('code_uniq', 'unique(code, nature_id)', _("Le code Analytique doit être unique par nature!")),
        ('code_size', 'check(code >= 10 and code <= 99)', _("Le code Analytique doit être compris entre 10 et 99 !")),
        #('code_dizaine', 'check(code%10 )', _("Le code Analytique doit être une dizaine!"))
    ]

    def _get_code(self):
        for obj in self:
            code = ''
            if obj.nature_id:
                code = code + str(obj.nature_id.code_compose) + '.' + str(obj.code) 
                obj.code_compose =code
                
    def get_default_account(self):

        return self.env['account.account'].search([('code', '=', '607100')])

    code_compose = fields.Char('Code Composé', compute='_get_code')
    code = fields.Integer("Ordre d'affichage",required=True)
    name = fields.Char("Libellé opérationnel", required=True)
    nature_id = fields.Many2one("gle.nature", string="Nature",  ondelete='cascade')
    prestation_ids = fields.One2many("gle.prestation", 'type_id', string="Types")
    account_id = fields.Many2one( 'account.account', "Compte de charges", default=get_default_account)
    section_related_id = fields.Many2one(related='nature_id.section_id', string="Related section", store=True)
