# -*- coding: utf-8 -*-

from openerp import models, fields, api
class GleNatureComptable(models.Model):
    _name = 'gle.nature.comptable'

    code_nature =fields.Char("Libellé Nature", required=True)
    name_nature = fields.Char('Code Nature', required=True)
   
    @api.multi
    def _get_code(self):
        for cmp in self:
            cmp.name = "["+cmp.code_nature+"]"+cmp.name_nature
 
    name = fields.Char('Libellé',compute="_get_code")
