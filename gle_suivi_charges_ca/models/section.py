# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import datetime 


class ResUser(models.Model):

    _inherit = 'res.users'
   
    section_ids = fields.Many2many(comodel_name="gle.section", relation='section_user_rel', column1='section_id', column2='user_id' , string ="Sections Comptables")
    profile_type_id = fields.Many2one("profile.type", "Type du profil")
    
    @api.model
    def update_groups_section(self):
        user_obj = self.env['res.users']
        group_obj = self.env['res.groups']
        section_obj = self.env['gle.section']
        profile_type_obj = self.env['profile.type']


class GleSection(models.Model):

    _name = 'gle.section'
    
    _sql_constraints = [
        ('code_uniq', 'unique(code)', _("Le code Analytique doit être unique !")),
        ('code_size', 'check(code >= 1 and code <= 9)', _("Le code Analytique doit être compris entre 1 et 9 !"))
    ]
    user_ids = fields.Many2many(comodel_name="res.users", relation='section_user_rel', column1 = 'user_id', column2 = 'section_id' , string="Utilisateurs")
    code = fields.Integer("Ordre d'affichage" ,  required=True)
    name =fields.Char("Libellé opérationnel", required=True)
    nature_ids = fields.One2many("gle.nature", 'section_id', string="Natures") 
    code_sect = fields.Char("Code section" ,  required=True)
    name_sect =fields.Char("Libellé section" ,  required=True)
