# -*- coding: utf-8 -*-
from openerp import models, fields, api, _


class GleAccountRefundLine(models.Model):
    _inherit = ['tsc.line.mixin', 'gle.account.refund.line']
    _name = 'gle.account.refund.line'

    @api.multi
    def _get_related_line(self):
        return False


class GleAccountRefund(models.Model):
    _inherit = 'gle.account.refund'

    @api.multi
    def tsc_action_cancel(self):
        for refund in self:
            reference = u'AAE %s, passage sans suite' % (refund.name or '')
            for line in self.refund_line_ids.filtered(lambda r: not r.tsc_canceled and (r.tsc_last_engage != 0 or r.tsc_total_engage != 0)):
                if refund.salon_id.is_clos:
                    amount = line.tsc_last_engage
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(refund)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=-amount,
                                                         famille=False,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True,
                                                         store=False)
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(refund.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=amount,
                                                         famille=False,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)
                else:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(refund.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "ch2_ce"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=-line.tsc_last_engage,
                                                         famille=False,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)

    @api.multi
    def action_pss(self):
        res = super(GleAccountRefund, self).action_pss()
        for refund in self:
            if refund.type_facturation != 'intercos' and not refund.partner_id.intercos:
                continue
            refund.tsc_action_cancel()
        return res

    @api.multi
    def refus_refund(self):
        res = super(GleAccountRefund, self).refus_refund()
        for refund in self:
            if refund.type_facturation != 'intercos' and not refund.partner_id.intercos:
                continue
            refund.tsc_action_cancel()
        return res

    @api.multi
    def refund_director_co_confirmed(self):
        res = super(GleAccountRefund, self).refund_director_co_confirmed()
        for refund in self:
            if refund.type_facturation != 'intercos' and not refund.partner_id.intercos:
                continue
            reference = u'AAE %s, intercos' % (refund.name or '')
            for line in refund.refund_line_ids:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(self.salon_id)
                if refund.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=refund.gle_section_id,
                                                     amount=-line.price_subtotal,
                                                     famille=False,
                                                     force_string='4.F.10.01',
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)
        return res
