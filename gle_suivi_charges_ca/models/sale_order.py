# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import logging
import datetime
aujourdhui = datetime.datetime.now()

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    _inherit = ['tsc.line.mixin', 'sale.order.line']
    _name = 'sale.order.line'

    @api.multi
    def _get_related_line(self):
        return False


class SaleOrder(models.Model):

    _inherit = 'sale.order'
    date_order_demo = fields.Date('Date order demo')

    @api.model
    def _get_emp_domain(self):
        user_id = self.env['res.users'].browse([self.env.context.get("uid")])

        # Add 09/06 JIRA 300 domain
        if user_id:
            if user_id.profile_type_id.code == 'ADV':
                domain = "[]"
            else:
                domain = [('id', 'in', user_id.section_ids.ids)]

        return domain

    @api.multi
    def get_section_id(self):
        user_id = self.env['res.users'].browse([self._uid])
        if user_id and len(user_id.section_ids)>0:
            return user_id.section_ids[0].id
        
    _defaults = {
        'gle_section_id' : get_section_id,
        }
    gle_section_id = fields.Many2one("gle.section", string="Section", domain=_get_emp_domain)

    def get_validation_date(self):
        valid_date = self.env["res.company"].search([['id', '=', 1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return datetime.datetime.now().date()
    
    def get_order_date(self):
        valid_date = self.env["res.company"].search([['id','=',1]]).date_day_test
        if valid_date:
            return datetime.datetime.strptime(valid_date, '%Y-%m-%d').date()
        else:
            return aujourdhui.date()
    # L'alimentation de tableau de suivi de charges se fait
    # lors de validation d'un commercial sur un devis intercos
    # @api.one
    # def action_button_confirm(self):
    #     reference = 'Bon de commande %s' % (self.name or '')
    #     if self.type_facturation == 'intercos':
    #         return super(SaleOrder, self).action_button_confirm()
    #     self.date_order_demo=self.get_order_date()
    #     millesime=datetime.datetime.strptime(self.date_order_demo, '%Y-%m-%d').date().year
    #     for line in self.order_line:
    #         suivi_charge = self.env['gle.tableau_suivi_ch_ca'].search([('gle_produit_id', '=', line.salon_id.name.id), ('millesime.name', '=', millesime)])
    #         if self.type_facturation in ['m_echange', 'r_echange']:
    #             tableau = "ch4_em"
    #         else:
    #             tableau = "pdt2_pe"
    #         if suivi_charge :
    #             suivi_charge.update_suivi_charge(tableau=tableau,
    #                                              section=self.gle_section_id,
    #                                              amount=line.price_subtotal,
    #                                              famille=line.product_id.categ_id,
    #                                              reference=reference)
    #
    #     return super(SaleOrder, self).action_button_confirm()

    @api.one
    def validate_quotation(self):
        res = super(SaleOrder, self).validate_quotation()
        for order in self:
            reference = u'Bon de commande %s, intercos' % (order.name or '')
            if order.type_facturation != 'intercos' and not order.partner_id.intercos:
                continue
            for line in self.order_line:
                suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(order.salon_id)
                if order.type_facturation in ['m_echange', 'r_echange']:
                    tableau = "ch4_em"
                else:
                    tableau = "ch2_ce"
                if suivi_charge:
                    suivi_charge.update_suivi_charge(tableau=tableau,
                                                     section=order.gle_section_id,
                                                     amount=-line.price_subtotal,
                                                     famille=False,
                                                     force_string='4.F.10.01',
                                                     reference=reference,
                                                     record=line,
                                                     cancel=False)
        return res
    
    @api.multi
    def action_cancel(self):
        res = super(SaleOrder, self).action_cancel()
        for order in self:
            if order.type_facturation != 'intercos' and not order.partner_id.intercos:
                continue
            order.tsc_action_cancel()
        return res

    @api.multi
    def action_pss(self):
        res = super(SaleOrder, self).action_pss()
        for order in self:
            if order.type_facturation != 'intercos' and not order.partner_id.intercos:
                continue
            order.tsc_action_cancel()
        return res

    @api.multi
    def tsc_action_cancel(self):
        for order in self:
            reference = u'Bon de commande %s, passage sans suite' % (order.name or '')
            for line in self.order_line.filtered(lambda r: not r.tsc_canceled and (r.tsc_last_engage != 0 or r.tsc_total_engage != 0)):
                if order.salon_id.is_clos:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(order)
                    amount = line.tsc_last_engage
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=-amount,
                                                         famille=False,
                                                         facture=True,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True,
                                                         store=False)
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(order.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "pdt3_pe"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=amount,
                                                         famille=False,
                                                         facture=True,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)
                else:
                    suivi_charge = self.env['gle.tableau_suivi_ch_ca'].find(order.salon_id)
                    if self.type_facturation in ['m_echange', 'r_echange']:
                        tableau = "ch4_em"
                    else:
                        tableau = "ch2_ce"
                    if suivi_charge:
                        suivi_charge.update_suivi_charge(tableau=tableau,
                                                         section=self.gle_section_id,
                                                         amount=-line.tsc_last_engage,
                                                         famille=False,
                                                         force_string='4.F.10.01',
                                                         reference=reference,
                                                         record=line,
                                                         cancel=True)

            # elif year_of_salon_in_line == current_fiscal_year - 1:
            #     suivi_charg_ex_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
            #         [('gle_produit_id', '=', self.salon_id.name.id),
            #          ('millesime', '=', self.salon_id.millesime.id)])
            #     if suivi_charg_ex_fiscal_year:
            #         suivi_charg_ex_fiscal_year.update_suivi_charge(tableau='pdt3_pe',
            #                                                        section=self.gle_section_id,
            #                                                        amount=line.price_subtotal,
            #                                                        famille=line.product_id.categ_id,
            #                                                        facture=True,
            #                                                        reference=reference)
            #
            #     suivi_charg_current_fiscal_year = self.env['gle.tableau_suivi_ch_ca'].search(
            #         [('gle_produit_id', '=', self.salon_id.name.id),
            #          ('millesime.name', '=', str(current_fiscal_year))])
            #     if suivi_charg_current_fiscal_year:
            #         suivi_charg_current_fiscal_year.update_suivi_charge(tableau='pdt3_pe',
            #                                                             section=self.gle_section_id,
            #                                                             amount=-(line.price_subtotal),
            #                                                             facture=True,
            #                                                             famille=line.product_id.categ_id,
            #                                                             reference=reference)

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        invoice_vals = super(SaleOrder, self)._prepare_invoice(cr, uid, order, lines, context=context)
        invoice_vals.update({'gle_section_id' : order.gle_section_id.id})
        return invoice_vals

    @api.multi
    def action_invoice_create(self, grouped=False, states=None, date_invoice=False, context=None):

        res = super(SaleOrder, self).action_invoice_create(grouped=False, states=None, date_invoice=False, context=None)

        self.write({'state': 'done'})
        return res
