# -*- coding: utf-8 -*-

from openerp import models, fields, api, _


class RefundToReceiveLine(models.Model):
    _inherit = ['tsc.line.mixin', 'gle.refund.to.receive.line']
    _name = 'gle.refund.to.receive.line'

    @api.multi
    def _get_related_line(self):
        return False
