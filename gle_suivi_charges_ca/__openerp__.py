# -*- coding: utf-8 -*-
{
    'name': "gle suivi des charges et CA",

    'summary': """
        Module de suivi des charges/ CA des Echos""",

    'description': """
        Module de suivi des charges/CA pour Les Echos  """,

    'author': "GFI",
    'website': "http://www.yourcompany.com",
    'category': 'Accounting & Finance',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'gle_product_extended',
        'gle_accounting',
        'account_accountant',
        'purchase_tracker',
    ],

    # always loaded
    'data': [
        'data/profile_type_data.xml',

        'views/account_invoice.xml',
        'wizard/purchase_order_refus.xml',
        'views/purchase_order.xml',
        'views/sale_order.xml',
        'views/gle_account_refund.xml',
        'views/product.xml',
        'views/nature_comptable.xml',
        'views/section.xml',
        'views/nature.xml',
        'views/type.xml',
        'views/prestation.xml',
        'views/profile_type.xml',
        'views/res_users.xml',
        'views/res_partner_view.xml',
        'views/tableau_suivi_ch_ca.xml',
        'views/menu.xml',
        'views/templates.xml',
        'views/product_template.xml',

    ],
    'js': ['gle_suivi_charges_ca/static/src/js/suivie_charge.js'],
    'css': ['static/src/css/suivie_charge.css','static/src/css/tableau.css'],
    'application': True,
    # 'update_xml': ['sql/constraint.sql'],
    'post_init_hook': 'post_init_update_users',
}
