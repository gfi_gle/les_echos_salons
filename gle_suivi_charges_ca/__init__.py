import models
import wizard
from openerp import SUPERUSER_ID
from openerp.api import Environment


def post_init_update_users(cr,registry):
    env = Environment(cr, SUPERUSER_ID, {})
    user_obj = env['res.users'].search([])
    user_obj.update_groups_section()
    
from . import models