# -*- coding: utf-8 -*-
{
    'name': "gle_reservation_salon",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'gle_salon'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'security/security.xml',
        'views/templates.xml',
        'views/reservation_salon_views/reservation_salon_view.xml',
        'views/reservation_salon_views/reservation_salon_marketing_view_tree.xml',
        'views/reservation_salon_views/reservation_salon_workshop_view_tree.xml',

        'views/reservation_views_tree/reservation_stand_view_tree.xml',
        'views/reservation_views_tree/reservation_marketing_view_tree.xml',
        'views/reservation_views_tree/reservation_workshop_view_tree.xml',

        'views/menus.xml',
        'views/advanced_menu.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}