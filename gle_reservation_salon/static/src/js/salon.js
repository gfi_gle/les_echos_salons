var root = document.getElementsByTagName( 'html' )[0];
root.setAttribute( 'class', 'salonhtml' );
$( "div" ).remove( ".oe_view_manager_buttons" );
$( "table" ).remove( ".oe_view_manager_header" );

$.fn.waitUntilExistssecurity    = function (handler, shouldRunHandlerOnce, isChild) {
    var found       = 'found';
    var $this       = $(this.selector);
    var $elements   = $this.not(function () { return $(this).data(found); }).each(handler).data(found, true);
    if (!isChild)
    {
        (window.waitUntilExistssecurity_Intervals = window.waitUntilExistssecurity_Intervals || {})[this.selector] =
            window.setInterval(function () { $this.waitUntilExistssecurity(handler, shouldRunHandlerOnce, true); }, 100)
        ;
    }
    else if (shouldRunHandlerOnce && $elements.length)
    {
    }
    return $this;
}

$('.oe_form_field_many2many_list_row_add').waitUntilExistssecurity(function(){
    setTimeout(function(){
        $( "td" ).remove( ".oe_form_field_many2many_list_row_add" );
        $( "td.oe_list_record_delete" ).remove();
    }, 100);
});