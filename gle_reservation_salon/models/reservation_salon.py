# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class ReservationSalon(models.Model):
    """ Model representing reservation salon """

    _name = 'gle_reservation_salon.reservation_salon'

    name = fields.Char(string="Name")
    salon_id = fields.Many2one('gle_salon.salon', string="Couple projet/produit")

    village_id_filter = fields.Many2one(comodel_name='gle_salon.village', string="Village")

    product_salon_code = fields.Char(string="Code produit")

    project_code = fields.Char(string="Code projet")

    partner_id = fields.Many2one('res.partner', string="Client (Donneur d'ordre)")

    partner_workshop_ids = fields.Many2many(comodel_name='res.partner',
                                       relation='gle_salon_product_workshop_res_partner_reservation_relation',
                                       column1="reservation_id", column2='partner_id',
                                       string="Clients (Donneurs d'ordres)")

    stand_id = fields.Many2one('gle_salon.stand', string="Stand")

    stand_type = fields.Char(string="Type stand")
    stand_area = fields.Float(string="Surface")
    stand_angle = fields.Char(string="Angle(s)")
    stand_enseigne = fields.Char(string="Enseigne(s)")
    stand_communication = fields.Selection([('1', "Oui"), ('0', "Non")], string="Affichage communication")
    village = fields.Char(string="Village")

    product_marketing_id = fields.Many2one('gle_products.product_marketing', string="Produit Marketing / Communication")
    product_workshop_id = fields.Many2one('gle_products.product_workshop', string="Produit Conférences / Ateliers")

    commercial_partner_id = fields.Many2one('res.partner', string="Contact commercial")
    commercial_partner_workshop_ids = fields.Many2many(comodel_name='res.partner',
                                            relation='gle_salon_product_workshop_res_partner_commercial_rel',
                                            column1="reservation_id", column2='partner_id',
                                            string="Contact commercial")


    commercial_echo_id = fields.Many2one('res.users', string="Commercial les Echos")
    commercial_echo_workshop_ids = fields.Many2many(comodel_name='res.users',
                                                       relation='gle_salon_product_workshop_res_users_echos_rel',
                                                       column1="reservation_id", column2='res_users_id',
                                                       string="Commercial les Echos")



    state_ware = fields.Selection([('void', ''), ('yes', 'Oui'), ('no', 'Non')], string="Etat (Echange marchandise)")

    state = fields.Selection([('void', 'Brouillon'), ('option', 'Option'), ('ferme', 'Ferme')])

    reservation_type = fields.Selection([('stand', 'Stand'), ('marketing', "Marketing / Communication"),
                                         ('workshop', "Catégories Conférences / Ateliers")], string="Type de produit")

    category_workshop_id = fields.Many2one(comodel_name='gle_products.product_workshop_category',
                                            string="Categorie de l'article")

    category_marketing_id = fields.Many2one(comodel_name='gle_products.product_marketing_category',
                                            string="Categorie de l'article")


    main_conference_partner_id = fields.Many2one('res.partner', string="Contact conférence principale")
    conference_partner_ids = fields.Many2many(comodel_name='res.partner', relation="gle_reservation_salon_res_partner_conference_rel", column1="reservation_id", column2="res_partner_id", string="Autre(s) Contact(s) Conférence")

    qty_ordered = fields.Integer(string="Quantité commandé")

    @api.multi
    def action_ferme_reservation_salon(self):
        """  """

        product_workshop_id = False
        product_marketing_id = False
        partner_id = False

        list_partner_workshop_ids = [partner_workshop_id.id for partner_workshop_id in self.partner_workshop_ids]


        if self.partner_id.id:
            partner_id = self.partner_id.id

        if self.product_marketing_id.id:
            product_marketing_id = self.product_marketing_id.id

        if self.product_workshop_id.id:
            product_workshop_id = self.product_workshop_id.id

        if self.salon_id.id:
            salon_id = self.salon_id.id

        if self.stand_id.id:
            stand_id = self.stand_id.id

        if self.stand_id and not self.stand_id.redistribution_required:
            try:
                self.stand_id.action_ferme_stand()
            except:

                view_id = self.env['ir.model.data'].get_object_reference('gle_salon', 'salon_stand_wizard_adv_view')

                return {
                    'name': "Veuillez remplire les champs suivant pour le passage en ferme du stand.",
                    'view_mode': 'form',
                    'view_id': view_id[1],
                    'view_type': 'form',
                    'res_model': 'gle_salon.stand_ferme_wizard',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': {'default_partner_id': partner_id, 'default_stand_id': stand_id}
                }

        elif self.stand_id.id:
            raise ValidationError("Le stand est en attente de redécoupage, vous ne pouvez pas le passer en Ferme.")

        if product_workshop_id or product_marketing_id:

            view_id = self.env['ir.model.data'].get_object_reference('gle_products', 'products_reservation_ferme_wizard_view')

            return {
                'name': "Veuillez remplire les champs suivant pour le passage en ferme de l'article.",
                'view_mode': 'form',
                'view_id': view_id[1],
                'view_type': 'form',
                'res_model': 'gle_products.products_ferme_wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {'default_partner_id': partner_id,
                            'default_partner_workshop_ids':list_partner_workshop_ids,
                            'default_product_marketing_id': product_marketing_id,
                            'default_product_workshop_id': product_workshop_id,
                            'default_salon_id': salon_id,
                            'default_reservation_id': self.id,
                            'default_reservation_type': self.reservation_type}
            }

    @api.multi
    def action_ferme_to_option_product(self):
        """"""

        sql_query_delete_conference_partner_ids = 'DELETE FROM gle_reservation_salon_res_partner_conference_rel WHERE reservation_id = {} AND res_partner_id = {};'

        for partner_id in self.conference_partner_ids:
            self.env.cr.execute(sql_query_delete_conference_partner_ids.format(self.id, partner_id.id))

        vals = {
            'main_conference_partner_id': '',
            'state_ware': '',
            'state': 'option'
        }

        self.write(vals)

    @api.multi
    def action_ferme_to_option_reservation_stand(self):
        """"""

        self.stand_id.button_action_ferme_to_option()

        vals = {
            'state_ware': '',
            'state': 'option'
        }
        self.write(vals)

    @api.multi
    def action_option_to_libre_stand(self):

        self.stand_id.action_libre_stand()

    @api.multi
    def action_delete_product_option(self):
        """"""

        self.unlink()


class ReservationSalonWizard(models.TransientModel):
    """ Model representing reservation salon wizard"""
    _name = 'gle_reservation_salon.wizardreportingreservation'
    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")

    @api.multi
    def action_tree_position_follow(self):
        view_id = self.env['ir.model.data'].get_object_reference('gle_crm', 'position_follow_view_sale_evol')
        return {
            'view_mode': 'tree',
            'view_id': view_id[1],
            'view_type': 'form',
            'res_model': 'gle_reservation_salon.reservation_salon',
            'type': 'ir.actions.act_window',
            'context': {'group_by':'village'},
            'domain': [('salon_id', '=', self.salon_id.id), ('reservation_type', '=', 'stand')]
        }



