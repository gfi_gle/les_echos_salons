# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class ReservationStandTree(models.TransientModel):
    """"""

    _name = 'gle_reservation_salon.reservation_stand_tree'

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")

    village_id_filter = fields.Many2one(comodel_name='gle_salon.village', string="Village")

    partner_id = fields.Many2one(comodel_name='res.partner', string="Client (Donneur d'ordre)")

    commercial_echo_partner_id = fields.Many2one(comodel_name='res.users', string="Les echos")

    state_reservation = fields.Selection([('option', 'Option'), ('ferme', 'Ferme')])

    reservation_ids = fields.Many2many(comodel_name='gle_reservation_salon.reservation_salon',
                                       relation='gle_salon_stand_tree_reservation_relation',
                                       column1="reservation_wizard_id", column2='reservation_id' ,
                                       string="Reservation",
                                       default = lambda self : self._init_reservation_ids())

    @api.onchange('salon_id')
    def _onchange_salon_id_filter(self):
        self.village_id_filter = False

    @api.multi
    def _init_reservation_ids(self):
        """"""
        return self.env['gle_reservation_salon.reservation_salon'].search([('stand_id', '!=', False)])

    @api.onchange('salon_id', 'village_id_filter', 'partner_id', 'commercial_echo_partner_id', 'state_reservation')
    def on_change_attributes(self):
        """"""

        query_search = self.generate_search_query()

        record_set = self.env['gle_reservation_salon.reservation_salon'].search(query_search)

        self.reservation_ids = record_set

    def generate_search_query(self):
        """"""

        query_search = [('stand_id', '!=', False)]

        if self.salon_id.id:
            query_search.append(('salon_id', '=', self.salon_id.id))
        if self.village_id_filter.id != False:
            query_search.append(('village', '=', self.village_id_filter.name))
        if self.partner_id.id:
            query_search.append(('partner_id', '=', self.partner_id.id))
        if self.commercial_echo_partner_id.id:
            query_search.append(('commercial_echo_id', '=', self.commercial_echo_partner_id.id))
        if self.state_reservation != False:
            query_search.append(('state', '=', self.state_reservation))

        return query_search
