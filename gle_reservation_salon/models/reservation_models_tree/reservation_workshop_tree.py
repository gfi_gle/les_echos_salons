# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class ReservationWorkshopTree(models.TransientModel):
    """"""

    _name = 'gle_reservation_salon.reservation_workshop_tree'

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")

    category_workshop_id = fields.Many2one(comodel_name='gle_products.product_workshop_category',
                                            string="Categorie de l'article")

    partner_ids = fields.Many2many(comodel_name='res.partner',
                                 relation='gle_reservation_salon_reservation_workshop_tree',
                                 column1='wizard_id',
                                 column2='reservation_id',
                                 string="Client (Donneur d'ordre)")

    commercial_echo_partner_id = fields.Many2one(comodel_name='res.users', string="Les echos")

    state_reservation = fields.Selection([('option', 'Option'), ('ferme', 'Ferme')])

    reservation_ids = fields.Many2many(comodel_name='gle_reservation_salon.reservation_salon',
                                       relation='gle_salon_product_workshop_tree_reservation_relation',
                                       column1="workshop_wizard_id", column2='reservation_id',
                                       string="Reservation",
                                       default=lambda self: self._init_reservation_ids())

    @api.multi
    def _init_reservation_ids(self):
        """"""
        return self.env['gle_reservation_salon.reservation_salon'].search([('product_workshop_id', '!=', False)])

    @api.onchange('salon_id', 'partner_ids', 'commercial_echo_partner_id', 'state_reservation', 'category_workshop_id')
    def on_change_attributes(self):
        """"""

        query_search = self.generate_search_query()

        record_set = self.env['gle_reservation_salon.reservation_salon'].search(query_search)

        self.reservation_ids = record_set

    def generate_search_query(self):
        """"""

        query_search = [('product_workshop_id', '!=', False)]

        if self.salon_id.id:
            query_search.append(('salon_id', '=', self.salon_id.id))
        if self.category_workshop_id.id:
            query_search.append(('category_workshop_id', '=', self.category_workshop_id.name))
        if len(self.partner_ids) > 0:
            list_ids = [partner.id for partner in self.partner_ids]
            query_search.append(('partner_workshop_ids', 'in', list_ids))
        if self.commercial_echo_partner_id.id:
            query_search.append(('commercial_echo_id', '=', self.commercial_echo_partner_id.id))
        if self.state_reservation != False:
            query_search.append(('state', '=', self.state_reservation))

        return query_search