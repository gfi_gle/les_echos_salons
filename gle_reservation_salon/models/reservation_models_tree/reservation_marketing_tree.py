# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import except_orm
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)

class ReservationMarketingTree(models.TransientModel):
    """"""

    _name = 'gle_reservation_salon.reservation_marketing_tree'

    salon_id = fields.Many2one(comodel_name='gle_salon.salon', string="Salon")

    category_marketing_id = fields.Many2one(comodel_name='gle_products.product_marketing_category',
                                            string="Categorie produit")

    partner_id = fields.Many2one(comodel_name='res.partner', string="Client (Donneur d'ordre)")

    commercial_echo_partner_id = fields.Many2one(comodel_name='res.users', string="Les echos")

    state_reservation = fields.Selection([('option', 'Option'), ('ferme', 'Ferme')])

    reservation_ids = fields.Many2many(comodel_name='gle_reservation_salon.reservation_salon',
                                       relation='gle_salon_product_marketing_tree_reservation_relation',
                                       column1="marketing_wizard_id", column2='reservation_id',
                                       string="Reservation",
                                       default=lambda self: self._init_reservation_ids())

    @api.multi
    def _init_reservation_ids(self):
        """"""
        return self.env['gle_reservation_salon.reservation_salon'].search([('product_marketing_id', '!=', False)])

    @api.onchange('salon_id', 'partner_id', 'commercial_echo_partner_id', 'state_reservation', 'category_marketing_id')
    def on_change_attributes(self):
        """"""

        query_search = self.generate_search_query()

        record_set = self.env['gle_reservation_salon.reservation_salon'].search(query_search)

        self.reservation_ids = record_set

    def generate_search_query(self):
        """"""

        query_search = [('product_marketing_id', '!=', False)]

        if self.salon_id.id:
            query_search.append(('salon_id', '=', self.salon_id.id))
        if self.category_marketing_id.id:
            query_search.append(('category_marketing_id', '=', self.category_marketing_id.name))
        if self.partner_id.id:
            query_search.append(('partner_id', '=', self.partner_id.id))
        if self.commercial_echo_partner_id.id:
            query_search.append(('commercial_echo_id', '=', self.commercial_echo_partner_id.id))
        if self.state_reservation != False:
            query_search.append(('state', '=', self.state_reservation))

        return query_search