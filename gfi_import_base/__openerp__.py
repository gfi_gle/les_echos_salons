# -*- coding: utf-8 -*-
{
    'name': "Excel Importation files",
    'summary': "Import Excel Files",
    'description': """
Base Module to Import Excel Files
=================================

    """,
    'author': "GFI",
    'website': "http://www.gfi.com",
    'category': 'Tools',
    'sequence': 1,
    'version': '0.1',
    'depends': [
        # 'gfi_message_center',
    ],
    'data': [
        'security/security.xml',
        'views/menu.xml',
        # 'views/message.xml',
        'wizard/import_base.xml',
    ],
    'external_dependencies': {
        'bin': [

        ],
        'python': [
            'xlrd',
            'patoolib',
            'shutil',
        ],
    },
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
