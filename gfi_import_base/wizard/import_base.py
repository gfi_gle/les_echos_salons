# -*- coding: utf-8 -*-
import base64
import os
import patoolib
import shutil
from xlrd.sheet import ctype_text
from openerp import tools
from openerp import models, fields, api, _
import tempfile
from xlrd import open_workbook
import logging

from openerp.exceptions import Warning as UserError

logger = logging.getLogger(__name__)


def get_file_base64(file_path, archive_files):
    path = archive_files.get(file_path)
    with open(path, "rb") as file:
        return tools.image_resize_image_medium(base64.b64encode(file.read()), avoid_if_small=False)
    return False


def parse(value, ctype):
    try:
        if long(value) == value:
            value = int(value)
    except:
        pass
    if ctype == 'bool':
        if not value: return False
        value = value.strip().upper() if isinstance(value, basestring) else value
        value = str(int(value)) if isinstance(value, (int, long, float)) else value
        if value in ['1', 'VRAI', 'TRUE']:
            return True
        else:
            return False
    elif ctype in ['int', 'long']:
        try:
            return long(value.strip())
        except:
            return value
    elif ctype in ['float']:
        try:
            return float(value.strip())
        except:
            return value
    elif ctype in ['string']:
        if isinstance(value, (int, long, float, bool)):
            value = unicode(value).upper()
        try:
            value = value.strip()
        except:
            pass
        return value
    return value


def filter_data(datas, maps={}, sheet_name=None, col=None, col_start=None, col_stop=None, row=None,
                row_start=None, row_stop=None, not_empty=False, return_type='datas', ctype=None):
    if isinstance(col, basestring):
        assert col in maps.keys(), 'The key %s is not the maps' % col
        col = maps.get(col)
    _datas = Data()

    for _sheet_name, _lines in datas.get_datas().iteritems():
        if sheet_name and sheet_name != _sheet_name:
            continue
        _datas.copy_from(datas, _sheet_name)
        _datas.add_sheet(_sheet_name)
        for line in _lines:
            if col is not None and line.col != col: continue
            if row is not None and line.row != row: continue
            if col_start is not None and line.col < col_start: continue
            if col_stop is not None and line.col > col_stop: continue
            if row_start is not None and line.row < row_start: continue
            if row_stop is not None and line.row > row_stop: continue
            if not_empty is not None and not line.value: continue
            _datas.add_line(_sheet_name, line)
    if return_type == 'datas':
        return _datas
    for line in _datas.get_lines():
        if return_type == 'value':
            return parse(line.value, ctype)
        elif return_type == 'row':
            return parse(line.row, ctype)
        elif return_type == 'col':
            return parse(line.col, ctype)
        elif return_type == 'type':
            return parse(line.type, ctype)
        elif return_type == 'obj':
            return parse(line.obj, ctype)
    return False


class DataLine(object):
    def __init__(self, line):
        super(DataLine, self).__init__()
        if isinstance(line, DataLine):
            line = [line.row, line.col, line.obj, line.value, line.type]
        self.line = line

    @property
    def row(self):
        return self.line[0]

    @property
    def col(self):
        return self.line[1]

    @property
    def obj(self):
        return self.line[2]

    @property
    def value(self):
        return self.line[3]

    @property
    def type(self):
        return self.line[4]


class Data(object):
    def __init__(self):
        super(Data, self).__init__()
        self.datas = {}
        self.dimension = {}

    def add_sheet(self, sheet_name):
        if sheet_name not in self.datas:
            self.datas[sheet_name] = []
            self.dimension[sheet_name] = {'ncols': False, 'nrows': False}

    def add_line(self, sheet_name, line):
        self.add_sheet(sheet_name)
        self.datas[sheet_name].append(DataLine(line))

    def get_lines(self, sheet_name=False):
        assert sheet_name or len(self.get_sheet_names()) == 1, 'Specify the sheet name'
        if not sheet_name:
            sheet_name = self.get_sheet_names()[0]
        return self.datas[sheet_name]

    def get_datas(self):
        return self.datas

    def get_sheet_names(self):
        return self.datas.keys()

    def set_ncols(self, sheet_name, ncols):
        self.dimension[sheet_name]['ncols'] = ncols

    def get_ncols(self, sheet_name):
        return self.dimension[sheet_name]['ncols']

    def set_nrows(self, sheet_name, nrows):
        self.dimension[sheet_name]['nrows'] = nrows

    def get_nrows(self, sheet_name):

        return self.dimension[sheet_name]['nrows']

    def copy_from(self, old_datas, sheet_name):
        self.add_sheet(sheet_name)
        self.set_ncols(sheet_name, old_datas.get_ncols(sheet_name))
        self.set_nrows(sheet_name, old_datas.get_nrows(sheet_name))


class ImportBase(models.TransientModel):
    _name = 'import.base'
    _description = 'Importation'

    file = fields.Binary(string='Fichier Excel', required=True, )
    filename = fields.Char(string='Libellé', size=64, )
    archive = fields.Binary(string='Archive', required=False, )
    archive_name = fields.Char(string='Libellé', size=64, )
    doc_type = fields.Char(string='Type du document', size=20, required=True, )

    @api.multi
    def action_import(self):
        tf = tempfile.NamedTemporaryFile(mode='wb+', delete=False, suffix='_import_base_%s' % self.filename,
                                         prefix='import')
        tf.write(base64.decodestring(self.file))
        tf.close()
        archive_files = {}
        archive_dir = False
        if self.archive:
            archive_tf = tempfile.NamedTemporaryFile(mode='wb+', delete=False,
                                                     suffix='_import_base_%s' % self.archive_name, prefix='archive')
            archive_tf.write(base64.decodestring(self.archive))
            archive_tf.close()
            archive_dir = tempfile.mkdtemp(suffix='_archive', prefix='import_base_')
            patoolib.extract_archive(archive_tf.name, outdir=archive_dir)
            for root, dirs, files in os.walk(archive_dir):
                for name in files:
                    extracted_file = os.path.join(root, name)
                    archive_files[extracted_file[len(archive_dir) + 1:]] = os.path.join(root, name)
        book = open_workbook(tf.name)
        sheet_names = book.sheet_names()
        datas = Data()

        for sheet_name in sheet_names:
            datas.add_sheet(sheet_name)
            sheet = book.sheet_by_name(sheet_name)
            num_cols = sheet.ncols
            datas.set_ncols(sheet_name, num_cols)
            datas.set_nrows(sheet_name, sheet.nrows)
            for row_idx in range(0, sheet.nrows):
                for col_idx in range(0, num_cols):
                    cell_obj = sheet.cell(row_idx, col_idx)
                    line = (row_idx, col_idx, cell_obj, cell_obj.value, ctype_text.get(cell_obj.ctype, 'unknown'))
                    datas.add_line(sheet_name, line)

        records = self.process_data(datas, archive_files)
        os.remove(tf.name)
        if archive_dir:
            shutil.rmtree(archive_dir)
        return self.show(label='', model='', records=records)

    @api.multi
    def process_data(self, datas, archive_files):
        pass

    @api.multi
    def show(self, label='', model='', records=[]):
        return {
            'name': label,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': model,
            'domain': [('id', 'in', records._ids)],
            'context': {},
            'type': 'ir.actions.act_window',
        }
