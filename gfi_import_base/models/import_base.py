# -*- coding: utf-8 -*-

from openerp import models, fields, api, _


class ImportBaseMixin(models.AbstractModel):
    _name = 'import.base.mixin'

    excel_id = fields.Char(string='Excel Extern ID', size=64, )
