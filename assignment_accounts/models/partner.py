from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

import logging
logger = logging.getLogger(__name__)
# New accounting flags that require the partner to be a client.
CLIENT_FLAGS = [
    'is_operational_client',
]

# New accounting flags that require the partner to be a supplier.
SUPPLIER_FLAGS = [
    'is_operational_supplier',
    'has_referrer_fees',
]


class Partner(models.Model):
    """Manage partner accounting accounts related to subscriptions.
    """

    _inherit = 'res.partner'

    # ===
    # Fields in the "Accounting" tab.
    # ===

    # Group: Operational client.

    is_operational_client = fields.Boolean(
        string='Operational client',
        help=(
            'When enabled, flag this partner as being a client relevant to '
            'current operations; and enable client accounting set-up.'
        ),
    )

    operational_client_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Operational client account',
        domain=[('type', '=', 'receivable')],
        ondelete='restrict',
        help='Accounting account generated for current client operations.',
    )

    operational_client_credit_limit = fields.Float(
        string='Operational client credit limit',
        help='Credit limit for current client operations.',
    )

    operational_client_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Operational client payment terms',
        ondelete='restrict',
        help='Payment terms for current client operations.',
    )

    # Group: Operational supplier.

    is_operational_supplier = fields.Boolean(
        string='Operational supplier',
        help=(
            'When enabled, flag this partner as being a supplier relevant to '
            'current operations; and enable supplier accounting set-up.'
        ),
    )

    operational_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Operational supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for current supplier operations.',
    )

    operational_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Operational supplier payment terms',
        ondelete='restrict',
        help='Payment terms for current supplier operations.',
    )

    # Group: Referrer fees.

    has_referrer_fees = fields.Boolean(
        string='Referrer fees',
        help=(
            'When enabled, flag this partner as benefiting from referrer '
            'fees; and enable supplier accounting set-up.'
        ),
    )

    referrer_fees_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Referrer fees account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for referrer fees.',
    )

    referrer_fees_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Referrer fees payment terms',
        ondelete='restrict',
        help='Payment terms for referrer fees.',
    )

    # ===
    # Fields in the "Unique identification" tab.
    # ===

    partner_info_required = fields.Boolean(
        string='Unique information required',
        help=(
            'When enabled, a unique information record must be defined for '
            'this partner.'
        ),
    )

    # Group: Third-party share fee client.

    is_third_party_share_fee_client = fields.Boolean(
        string='Third-party share fee client',
        help=(
            'When enabled, flag this partner as being a client with '
            'third-party share fees; and enable client accounting set-up.'
        ),
    )

    third_party_share_fee_client_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Third-party share fee client account',
        domain=[('type', '=', 'receivable')],
        ondelete='restrict',
        help='Accounting account generated for third-party share fees.',
    )

    third_party_share_fee_client_credit_limit = fields.Float(
        string='Third-party share fee client credit limit',
        help='Credit limit for third-party share fees.',
    )

    third_party_share_fee_client_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Third-party share fee client payment terms',
        ondelete='restrict',
        help='Payment terms for third-party share fees.',
    )

    # Group: Third-party share supplier.

    is_third_party_share_supplier = fields.Boolean(
        string='Third-party share supplier',
        help=(
            'When enabled, flag this partner as being a supplier of '
            'third-party shares; and enable supplier accounting set-up.'
        ),
    )

    third_party_share_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Third-party share supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for third-party supplies.',
    )

    third_party_share_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Third-party share supplier payment terms',
        ondelete='restrict',
        help='Payment terms for third-party supplies.',
    )

    # Group: Asset supplier.

    is_asset_supplier = fields.Boolean(
        string='Asset supplier',
        help=(
            'When enabled, flag this partner as being a supplier of assets; '
            'and enable supplier accounting set-up.'
        ),
    )

    asset_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Asset supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for asset supplies.',
    )

    asset_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Asset supplier payment terms',
        ondelete='restrict',
        help='Payment terms for asset supplies.',
    )

    # Group: Author.

    is_author = fields.Boolean(
        string='Author',
        help=(
            'When enabled, flag this partner as being an author; and enable '
            'supplier accounting set-up.'
        ),
    )

    author_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Author account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for being an author.',
    )

    author_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Author payment terms',
        ondelete='restrict',
        help='Payment terms for being an author.',
    )

    # ===
    # Fields in the "Legal notice specifics" tab.
    # ===

    has_compensation = fields.Boolean(
        string='Compensation',
        help='When enabled, flag this partner as allowing compensation.',
    )

    invoicing_frequency = fields.Selection(
        selection=[
            ('order', 'On orders'),
            ('monthly', 'Monthly'),
        ],
        string='Invoicing frequency',
        help=(
            'How invoicing is handled for this partner. Only partners flagged '
            'as being large accounts may have monthly invoicing.'
        ),
        required=True,
        default='order',
    )

    is_large_account = fields.Boolean(
        string='Large account',
        help='When enabled, flag this partner as being a large account.',
    )

    is_referrer = fields.Boolean(
        string='Referrer',
        help=(
            'When enabled, flag this partner as being a referrer benefiting '
            'from fees.'
        ),
    )

    referrer_fees_base = fields.Selection(
        selection=[
            ('whole_invoice', 'Whole invoice'),
            ('space_sale', 'Space sale'),
        ],
        string='Referrer fees base',
        help=(
            'Defines how referrer fees are computed: On whole invoices or '
            'based on the "Space sale" product.'
        ),
    )

    referrer_fees_invoicing_type = fields.Selection(
        selection=[
            ('regular', 'Regular provision'),
            ('direct_cut_on_sales', 'Direct cut on the sales invoice'),
        ],
        string='Referrer fees invoicing type',
        help='Defines the invoicing type of referrer fees.',
    )

    @api.constrains(*(
        ['customer', 'supplier'] +
        CLIENT_FLAGS + SUPPLIER_FLAGS
    ))
    @api.one
    def _check_client_supplier_flags(self):
        """Ensure "Client" and "Supplier" flags are correctly set when some
        flags that require them are.
        """

        if (
            any(getattr(self, client_flag) for client_flag in CLIENT_FLAGS) and
            not self.customer
        ):
            raise exceptions.ValidationError(_(
                'The "%s" partner must be marked as a client.'
            ) % self.name)

        if (
            any(getattr(self, sup_flag) for sup_flag in SUPPLIER_FLAGS) and
            not self.supplier
        ):
            raise exceptions.ValidationError(_(
                'The "%s" partner must be marked as a supplier.'
            ) % self.name)

    @api.multi
    def write(self, vals):
        """Override to:
        - Save operational client / supplier accounts into the base ones.
        - Generate accounts when enabling an account-related setting after
        partner information has been generated.
        """
	
        # Save operational client / supplier accounts into the base ones.
        account_id = vals.get('operational_client_account_id')
        if account_id:
            vals['property_account_receivable'] = account_id
        account_id = vals.get('operational_supplier_account_id')
        if account_id:
            vals['property_account_payable'] = account_id
        ret = super(Partner, self).write(vals)

        # Generate accounts when enabling an account-related setting after
        # partner information has been generated.
        if any(vals.get(flag) for flag in CLIENT_FLAGS + SUPPLIER_FLAGS):
            self.generate_accounts()

        return ret

    @api.one
    def generate_accounts(self):
        """Generate accounts based on rules."""

        if self.partner_info_status != 'validated':
                #Partner information is needed to generate accounts.
            return

        account_gen_rules = self.env['account.generation_rule'].search([])

        for account_gen_rule in account_gen_rules:
            account_gen_rule.generate_account(
                self, self.partner_info_id.code, self.partner_info_official_name,
            )

    @api.onchange(*(CLIENT_FLAGS + SUPPLIER_FLAGS))
    def on_change_accounting_flags(self):
        """Auto-enable some flags when enabling specific accounting ones.
        """

        if any(getattr(self, client_flag) for client_flag in CLIENT_FLAGS):
            self.customer = True
            self.partner_info_required = True

        if any(getattr(self, sup_flag) for sup_flag in SUPPLIER_FLAGS):
            self.supplier = True
            self.partner_info_required = True

    @api.multi
    def wkf_partner_info_generated(self):
        """Override to generate accounts based on rules."""

        ret = super(Partner, self).wkf_partner_info_generated()

        self.generate_accounts()

        return ret

    @api.constrains('invoicing_frequency', 'is_large_account')
    @api.one
    def _check_invoicing_frequency(self):
        """Ensure no forbidden invoicing frequency has been selected when the
        partner is not a large account.
        """

        if not self.is_large_account and self.invoicing_frequency != 'order':
            raise exceptions.ValidationError(_(
                'Invalid invoicing frequency set on the "%s" partner (it is '
                'not a large account so invoicing must be on orders).'
            ) % self.name)