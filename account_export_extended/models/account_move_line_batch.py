# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import _
from openerp import models, fields, api
from openerp.addons.account_export.models.account_move_line_batch import (
    AccountMoveLineField, ExportedField, AnalyticCodeField
)
import StringIO
import logging
import codecs

from openerp.addons.account_export.util.unicode_csv import (
    UnicodeWriter,
)

_logger = logging.getLogger(__name__)


class AccountMoveLineBatch(models.Model):

    _inherit = 'account.move.line_batch'

    def get_exported_fields(self):

        today = fields.Date.today()

        # Get analytic fields of accounting entries.
        aentry_analytics = (
            self.env['analytic.structure'].get_structures('account_move_line')
        )

        def formatPeriod(period):
            """Format the period code (01/2016 -> 201601)."""
            if not period:
                return u''
            month, year = period.code.split('/')
            return year + month

        def formatName(name):
            """ Format name to delete /n """
            if not name:
                return u''

            return unicode(name).split('\n')[0][:64]

        # Soft dependency on the "sale_payment_method" module: Only export the
        # field when it is available.
        optional_sales_payment_method_field = []
        if hasattr(self.env['account.move.line'], 'sales_payment_method_id'):
            optional_sales_payment_method_field.append(ExportedField(
                'crm.payment.mode', 'sales_payment_method_id.technical_name',
                label=_('Payment method'), typ='char',
            ))

        return [
                   AccountMoveLineField('company_id', label='Company'),
                   AccountMoveLineField('id', label='ID'),
                   AccountMoveLineField('move_state', label='status'),
                   ExportedField('account.journal', 'journal_id.code',
                                 label=_('Journal')),
                   ExportedField(
                       '', '', label=_('Period'), typ='char',
                       value_getter=lambda field, record, field_type: (
                           formatPeriod(record.period_id)
                       ),
                   ),
                   AccountMoveLineField('date_origin_document', label='Date of transaction'),
                   AccountMoveLineField('date_created', label='Effective date'),
                   AccountMoveLineField('internal_sequence_number', label='Internal Number'),
                   AccountMoveLineField('move_id', label='Journal Entry'),
                   AccountMoveLineField('ref', label='Reference'),
                   ExportedField('account.account', 'account_id.code',
                                 label=_('Compte')),
                   ExportedField('', '', label="Name", typ='text',
                                 value_getter=lambda field, record, field_type: (
                                     formatName(record.name)
                                 )),
                   AccountMoveLineField('debit', label='Debit'),
                   AccountMoveLineField('debit_curr', label='Debit T'),
                   AccountMoveLineField('credit', label='Credit'),
                   AccountMoveLineField('credit_curr', label='Credit T'),
                   AccountMoveLineField('currency_id', label='Currency'),
                   AccountMoveLineField('date_maturity', label='Due Date'),
                   AccountMoveLineField('partner_id', label='Partner'),
                   ExportedField('res.partner', 'partner_id.partner_info_id', label='Unique identification'),
                   ExportedField(
                       '', '', label=_('Compte de taxe'), typ='char',
                       value_getter=lambda field, record, field_type: (
                           record.tax_id.description or u''
                       ),
                   ),
                   # add field from regie expo
                   AccountMoveLineField('folder', label='Dossier'),
                   AccountMoveLineField('edition', label='Edition'),
                   ExportedField(
                        '', '', label=_('Section'), typ='char',
                       value_getter=lambda field, record, field_type: (
                           record.section_id.code_sect or u'' if hasattr(record.section_id, 'code_sect') else record.section_id.code or u''
                       ),
                   ),
                   ExportedField('gle_salon.millesime', 'produit_id.code', label='Produit'),
                   ExportedField('gle.nature.comptable', 'nature_id.name_nature', label='Nature analytique'),
                   AccountMoveLineField('code_interco', label='Interco'),
                   ExportedField('gle_salon.produit', 'projet_id.code', label='Projet'),
               ] + [
                   # Analytics.
                   AnalyticCodeField(
                       'a%s_id.name' % a_structure.ordering,
                       label=a_structure.nd_id.name,
                       typ='char',
                       )
                   for a_structure in aentry_analytics
                   ] + [
                   AccountMoveLineField('date_reconcile', label='Reconcile Date'),
                   AccountMoveLineField('reconcile_ref', label='Reconcile Ref'),
                   ExportedField(
                       '', 'extraction_date', label=_('Date d\'extraction'),
                       typ='date', value_getter=lambda *a: today,
                   ),
                   AccountMoveLineField('reconciliation_marker', label='Reconcilation marker'),
               ] + optional_sales_payment_method_field

    def generate_export(self):
        """Override to customize export generation.
        By default, build a CSV file with 1 line per account move line.
        """

        exported_fields = self.get_exported_fields()

        # Usually this runs via an automated task so there is no lang info.
        export_context = self.env.context.copy()
        if not export_context.get('lang'):
            export_context['lang'] = self.env.user.lang

        # Get information about the fields to export.
        # Odoo 7 style to call "fields_get" without error...
        field_info = {
            model: self.pool[model].fields_get(self.env.cr, self.env.user.id, [
                field.name for field in exported_fields if field.model == model
            ], context=export_context)
            for model in (
                'account.account',
                'account.journal',
                'account.move.line',
                'res.partner',
                'account.invoice',
                'gle.section',
                'gle_salon.millesime',
                'gle.nature.comptable',
                'gle_salon.produit',
                'crm.payment.mode',
            )
        }

        # Labels - either explicit or deduced from field information.
        data = [[
            field.label or field_info[field.model][field.name]['string']
            for field in exported_fields
        ]]

        # The actual data.
        data.extend([
            [
                field.get_value(
                    field, line,
                    field.typ or field_info[field.model][field.name]['type'],
                )
                for field in exported_fields
            ]
            for line in self.line_ids
        ])

        # Prepare the CSV data.
        csv_stream = StringIO.StringIO()
        csv_stream.write(codecs.BOM_UTF8)  # Make Excel happy...
        csv_writer = UnicodeWriter(csv_stream, delimiter=';', quotechar='"')
        csv_writer.writerows(data)
        csv_data = csv_stream.getvalue()
        csv_stream.close()

        return csv_data
