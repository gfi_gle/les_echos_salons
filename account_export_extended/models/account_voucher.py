# -*- coding: utf-8 -*-
from openerp import models, fields, api

import logging

_logger = logging.getLogger(__name__)


class AccountVoucher(models.Model):

    _inherit = 'account.voucher'

    date_origin_document = fields.Date()

    sales_payment_method_id = fields.Many2one(
        comodel_name='crm.payment.mode',
    )

    @api.multi
    def proforma_voucher(self):
        """Override to:
        - Propagate the sales payment method to accounting elements.
        """

        ret = super(AccountVoucher, self).proforma_voucher()

        payment_method = self.env['crm.payment.mode'].search(
            [('technical_name', '=', 'VIR')], limit=1,
        )
        if not payment_method:
            return ret

        for voucher in self:
            # Get the accounting documents linked to the accounting entries,
            # and from there, all of their entries. This assumes some about
            # payment (no mixed payments).
            payment_docs = voucher.move_ids.mapped('move_id')
            payment_entries = payment_docs.mapped('line_id')

            # Ignore those that already have a payment method.
            payment_entries = payment_entries.filtered(
                lambda entry: not entry.sales_payment_method_id
            )

            #Include our payment method onto the accounting entries.
            payment_entries.write({
                'sales_payment_method_id': voucher.sales_payment_method_id.id,
            })

            for account_move_line in voucher.move_id.line_id:
                account_move_line.write({
                    'date_origin_document': voucher.date_origin_document,
                    'sales_payment_method_id': voucher.sales_payment_method_id.id,
                })

        return ret

    def first_move_line_get(self, cr, uid, voucher_id, move_id, company_currency, current_currency, context=None):

        voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
        debit = credit = 0.0
        # TODO: is there any other alternative then the voucher type ??
        # ANSWER: We can have payment and receipt "In Advance".
        # TODO: Make this logic available.
        # -for sale, purchase we have but for the payment and receipt we do not have as based on the bank/cash journal we can not know its payment or receipt
        if voucher.type in ('purchase', 'payment'):
            credit = voucher.paid_amount_in_company_currency
        elif voucher.type in ('sale', 'receipt'):
            debit = voucher.paid_amount_in_company_currency
        if debit < 0: credit = -debit; debit = 0.0
        if credit < 0: debit = -credit; credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        #set the first line of the voucher

        move_line = {
                'name': voucher.name or '/',
                'debit': debit,
                'credit': credit,
                'account_id': voucher.account_id.id,
                'move_id': move_id,
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'partner_id': voucher.partner_id.id,
                'currency_id': company_currency <> current_currency and  current_currency or False,
                'amount_currency': (sign * abs(voucher.amount) # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': voucher.date,
                'date_maturity': voucher.date_due,
                'date_origin_document': voucher.date_origin_document,
                'sales_payment_method_id': voucher.sales_payment_method_id.id,
            }

        return move_line

    def writeoff_move_line_get(self, cr, uid, voucher_id, line_total, move_id, name, company_currency, current_currency, context=None):
        '''
        Set a dict to be use to create the writeoff move line.

        :param voucher_id: Id of voucher what we are creating account_move.
        :param line_total: Amount remaining to be allocated on lines.
        :param move_id: Id of account move where this line will be added.
        :param name: Description of account move line.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: mapping between fieldname and value of account move line to create
        :rtype: dict
        '''
        currency_obj = self.pool.get('res.currency')
        move_line = {}

        voucher = self.pool.get('account.voucher').browse(cr,uid,voucher_id,context)
        current_currency_obj = voucher.currency_id or voucher.journal_id.company_id.currency_id

        if not currency_obj.is_zero(cr, uid, current_currency_obj, line_total):
            diff = line_total
            account_id = False
            write_off_name = ''
            if voucher.payment_option == 'with_writeoff':
                account_id = voucher.writeoff_acc_id.id
                write_off_name = voucher.comment
            elif voucher.partner_id:
                if voucher.type in ('sale', 'receipt'):
                    account_id = voucher.partner_id.property_account_receivable.id
                else:
                    account_id = voucher.partner_id.property_account_payable.id
            else:
                # fallback on account of voucher
                account_id = voucher.account_id.id
            sign = voucher.type == 'payment' and -1 or 1
            move_line = {
                'name': write_off_name or name,
                'account_id': account_id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                'date': voucher.date,
                'credit': diff > 0 and diff or 0.0,
                'debit': diff < 0 and -diff or 0.0,
                'amount_currency': company_currency <> current_currency and (sign * -1 * voucher.writeoff_amount) or 0.0,
                'currency_id': company_currency <> current_currency and current_currency or False,
                'analytic_account_id': voucher.analytic_id and voucher.analytic_id.id or False,
                'date_maturity': voucher.date_due,
                'date_origin_document': voucher.date_origin_document,
                'sales_payment_method_id': voucher.sales_payment_method_id.id,
            }

        return move_line
