import base64
import codecs
import functools
import StringIO
from openerp import _
from openerp import api
from openerp import fields
from openerp import models

from openerp.addons.account_export.util.unicode_csv import (
    UnicodeWriter,
)


class ResPartnerInfoBatch(models.Model):
    """Maintain links to multiple partners.
    """

    _inherit = 'res.partner_info_batch'

    name = fields.Char(
        string='Name',
        help='The name of this batch.',
        required=True,
    )

    @api.depends('partner_info_ids')
    @api.one
    def _get_partner_info_count(self):
        """Count partners covered by the specified batch.
        """

        self.partner_info_count = len(self.partner_info_ids.ids)

    partner_info_count = fields.Integer(
        compute=_get_partner_info_count,
        string='Partner identifications count',
        help='The amount of partner identifications covered by this batch.',
        readonly=True,
        store=True,
    )

    partner_info_ids = fields.Many2many(
        comodel_name='res.partner.info',
        relation='res_partner_info_batch_rel',
        column1='batch_id',
        column2='partner_info_id',
        string='Partner identifications',
        help='Partners identifications covered by this batch.',
        readonly=True,
    )

    def get_exported_fields(self):
        """Override to customize the fields that are to be exported.
        :return: List of "ExportedField" instances.
        """

        partner_info_analytics = (
            self.env['analytic.structure'].get_structures('res_partner_info')
        )

        return [

                   ExportedField('res.partner', 'partner_id.company_id.name', label='Company'),
                   # ExportedField('res.partner', 'partner_id.id' or '', label='ID'),
                   PartnerInfoField('id', label='ID'),
                   PartnerInfoField('label', label='Unique identification'),
                   ExportedField('res.partner', 'partner_id.name' or '', label='Name'),
                   PartnerInfoField('official_name', label='Official name'),
                   PartnerInfoField('email'),
                   PartnerInfoField('street1', label='Street'),
                   PartnerInfoField('street2', label='Street2'),
                   PartnerInfoField('street3', label='Street3'),
                   PartnerInfoField('zip_code', label='Zip'),
                   PartnerInfoField('city', label='City'),
                   ExportedField('res.country', 'country_id.code',
                                 label=_('Country')),
                   PartnerInfoField('entity_type_id', label='Entity type'),
                   PartnerInfoField('entity_identifier', label='Entity identifier'),
                   PartnerInfoField('phone', label='Phone'),
                   # PartnerInfoField('label'),
               ] + [
            # Analytics.
            AnalyticCodeField(
                'a%s_id.name' % a_structure.ordering,
                label=a_structure.nd_id.name,
                typ='char',
            )
            for a_structure in partner_info_analytics
        ] + [
            # # TODO The following come from another addon, but keeping them here
            # #for now is fine for now.
                   ExportedField('res.partner', 'partner_id.is_author'),
                   ExportedField('res.partner', 'partner_id.author_account_id'),
                   ExportedField('res.partner', 'partner_id.author_payment_terms_id'),
                   ExportedField('res.partner', 'partner_id.is_operational_client'),
                   ExportedField('res.partner', 'partner_id.operational_client_account_id',
                                 label='Operational client account'),
                   ExportedField('res.partner', 'partner_id.operational_client_payment_terms_id',
                                 label='Operational client payment terms'),
                   ExportedField('res.partner', 'partner_id.is_asset_supplier'),
                   ExportedField('res.partner', 'partner_id.asset_supplier_account_id'),
                   ExportedField('res.partner', 'partner_id.asset_supplier_payment_terms_id'),
                   ExportedField('res.partner', 'partner_id.is_operational_supplier'),
                   ExportedField('res.partner', 'partner_id.operational_supplier_account_id',
                                 label='Operational supplier account'),
                   ExportedField('res.partner', 'partner_id.operational_supplier_payment_terms_id',
                                 label='Operational supplier payment terms'),
                   ExportedField('res.partner', 'partner_id.is_third_party_share_fee_client'),
                   ExportedField('res.partner', 'partner_id.third_party_share_fee_client_account_id'),
                   ExportedField('res.partner', 'partner_id.third_party_share_fee_client_payment_terms_id'),

                   ExportedField('res.partner', 'partner_id.has_referrer_fees'),
                   ExportedField('res.partner', 'partner_id.referrer_fees_account_id', label='Referrer fees account'),
                   ExportedField('res.partner', 'partner_id.referrer_fees_payment_terms_id'),
                   ExportedField('res.partner', 'partner_id.is_third_party_share_supplier'),
                   ExportedField('res.partner', 'partner_id.third_party_share_supplier_account_id'),
                   ExportedField('res.partner', 'partner_id.third_party_share_supplier_payment_terms_id'),
        ]

    def generate_export(self):
        """Override to customize export generation.
        By default, build a CSV file with 1 line per partner.
        """

        exported_fields = self.get_exported_fields()

        # Usually this runs via an automated task so there is no lang info.
        export_context = self.env.context.copy()
        if not export_context.get('lang'):
            export_context['lang'] = self.env.user.lang

        # Get information about the fields to export.
        # Odoo 7 style to call "fields_get" without error...
        field_info = {
            model: self.pool[model].fields_get(self.env.cr, self.env.user.id, [
                field.name for field in exported_fields if field.model == model
            ], context=export_context)
            for model in (
                'res.company',
                'res.country',
                'res.partner',
                'res.partner.info',
            )
        }

        # Labels - either explicit or deduced from field information.
        data = [[
            field.label or field_info[field.model][field.name]['string']
            for field in exported_fields
        ]]

        # The actual data.

        # if not field.get_value( field, partner_info, field.typ or field_info['res.partner']['operational_client_account_id']['type'], ):
        #     pass

        data.extend([
            [
                field.get_value(
                    field, partner_info,
                    field.typ or field_info[field.model][field.name]['type'],
                )
                for field in exported_fields
            ]
            for partner_info in self.partner_info_ids
        ])

        # Prepare the CSV data.
        csv_stream = StringIO.StringIO()
        csv_stream.write(codecs.BOM_UTF8)  # Make Excel happy...
        csv_writer = UnicodeWriter(csv_stream, delimiter=';', quotechar='"')
        csv_writer.writerows(data)
        csv_data = csv_stream.getvalue()
        csv_stream.close()

        return csv_data

    def get_export_name(self):
        """Override to define the filename of the export.
        By default, use the name of the batch and build a CSV file.
        :rtype: String.
        """

        return self.name + '.csv'

    @api.model
    def create(self, vals):
        """- Generate an export file when creating a partner info batch.
        - Mark unique ID records as exported.
        """

        batch = super(ResPartnerInfoBatch, self).create(vals)

        self.env['ir.attachment'].write({
            'datas_fname': batch.get_export_name(),
            'datas': base64.b64encode(batch.generate_export()),
            'name': batch.get_export_name(),
            'res_id': batch.id,
            'res_model': self._name,
        })

        # Mark unique ID records as exported.
        batch.partner_info_ids.write({'to_export': False})

        return batch

    @api.multi
    def generate_file(self):

        self.ensure_one()

        att_obj = self.env['ir.attachment']

        data = self.generate_export()
        name = self.get_export_name()

        files = att_obj.search([
            ('res_model', '=', self._name),
            ('res_id', '=', self.id),
        ])

        files.unlink()

        att_obj.create({
            'datas_fname': name,
            'datas': base64.b64encode(data),
            'name': name,
            'res_id': self.id,
            'res_model': self._name,
        })

        return True


class ExportedField(object):
    """Information about an exported field.
    """

    def __init__(self, model, path, label=None, typ=None, value_getter=None):
        """Init the field information holder.

        :param model: Technical name of the Odo model this field is in.

        :param path: Technical path to the field, from a partner -
        may contain nested field names, separated by dots.

        :param label: Custom label for the exported field; by default, the Odoo
        label will be used.

        :param typ: Type of the exported field; by default, the Odoo field
        type will be used.

        :param value_getter: Custom value getter function; by default, the
        value will directly be fetched from the Odoo record.
        Signature: def value_getter(exported_field, record, field_type).
        """

        self.model = model
        self.path = path
        self.label = label
        self.typ = typ
        self.get_value = value_getter or self.default_value_getter

        # Extract the name of the this field in its model (after the last dot).
        last_dot_pos = self.path.rfind('.')
        if last_dot_pos > 0:
            self.name = self.path[last_dot_pos + 1:]
        else:
            self.name = self.path

    @staticmethod
    def default_value_getter(self, record, field_type):
        """By default, just call "getattr" to read the field and format
        accordingly.

        Although this is a static method, the first parameter is effectively a
        "self".
        """

        # import wdb
        # wdb.set_trace()
        field_name = self.path.split('.')[-1] or ''
        value = ExportedField.format_value(
            functools.reduce(getattr, self.path.split('.'), record),
            field_type,
        )

        if value:
            return value
        if field_name in ['is_author', 'is_operational_client', 'is_operational_supplier', 'is_asset_supplier',
                          'is_operational_supplier', 'is_third_party_share_fee_client',
                          'has_referrer_fees', 'is_third_party_share_supplier']:
            return False
        else:
            return ''

    @staticmethod
    def format_value(value, field_type):
        """Format a value according to an Odoo field type, so it shows up
        nicely in the exported file.

        :return: The formatted value.
        """

        if field_type == 'boolean':
            # Return bools as they are.
            return value

        if field_type in ('float', 'integer'):
            # Return numbers as they are.
            return value

        if field_type in ('many2one', 'one2many', 'many2many'):
            # Call the "name_get" method to stringify relational values.
            if not value:
                return u''
            names = dict(value.name_get())
            return u', '.join(names[val_iter.id] for val_iter in value)

        # Stringify values of fields in unhandled types and encode them as
        # Unicode.
        if value in (None, False):
            return u''
        return unicode(value)


# Define shortcuts for some model fields.


class AnalyticCodeField(ExportedField):
    def __init__(self, *args, **kwargs):
        ExportedField.__init__(self, 'analytic.code', *args, **kwargs)


class PartnerInfoField(ExportedField):
    def __init__(self, *args, **kwargs):
        ExportedField.__init__(self, 'res.partner.info', *args, **kwargs)
