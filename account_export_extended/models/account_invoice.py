# -*- coding: utf-8 -*-
from openerp import models, fields, api

import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    def line_get_convert(self, line, part, date):
        """
            Override method to add fields in account.move.line
            Dictionary used  to create account.move.line
        """

        vals = super(AccountInvoice, self).line_get_convert(line, part, date)

        if self.partner_id.intercos:
            code_interco = self.partner_id.code_intercos
        else:
            code_interco = "9Z99"

        vals.update({
            'section_id': line.get('section_id'),
            'nature_id': line.get('nature_id'),
            'date_origin_document': self.date_invoice,
            'tax_id': line.get('tax_id')
        })

        if line.get('account_id', False):
            account_record = self.env['account.account'].search([('id', '=', line.get('account_id'))])
            try:
                if account_record and int(account_record.code[0]) in [6, 7]:

                    if self.type in ['in_invoice', 'in_refund']:
                        salon_record = self.env['gle_salon.salon'].search([('id', '=', line.get('salon_id'))])

                        vals.update({
                            'projet_id': salon_record.millesime.id,
                            'produit_id': salon_record.name.id,
                            'code_interco': code_interco,
                        })

                    elif self.type in ['out_invoice', 'out_refund']:

                        vals.update({
                            'projet_id': self.salon_id.millesime.id,
                            'produit_id': self.salon_id.name.id,
                            'code_interco': code_interco,
                        })
                elif account_record and int(account_record.code[0:3]) == 445:
                    account_tax_ids = self.env['account.tax'].search([('account_collected_id', '=', account_record.id)])

                    for account_tax in account_tax_ids:
                        if account_tax.with_context(lang='fr_FR').name == line.get('name') \
                                or account_tax.name == line.get('name'):

                            vals.update({
                                'tax_id': account_tax.id,
                            })

            except:
                _logger.debug('-------------> EXCPTION')

        return vals
