# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from openerp import models
from openerp import api
from openerp import fields
from openerp import exceptions
from openerp import _

import datetime

from openerp.addons.account_export.util.timezone import (
    system_timezone, system_timezone_datetime, system_timezone_now
)
import logging
log = logging.getLogger('account.move.line')


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    folder = fields.Char(string="Dossier")
    edition = fields.Char(string="Edition")

    tax_id = fields.Many2one(comodel_name='account.tax')

    date_origin_document = fields.Date(string="Date origine document")

    @api.model
    def export_all(self, filename, journal_codes, max_date, test_date=None):
        """Export all accounting entries there are to export, provided they
        reside within the specified accounting journals.

        :param filename: Filename; may contain date / time formatters.
        :type filename: String.

        :type journal_codes: List.
        """

        if not isinstance(journal_codes, (list, tuple)) or not journal_codes:
            raise exceptions.Warning(
                'Accounting entry export: Invalid journal codes.'
            )

        log.info(u"Exports de lots d'écritures comptables")

        domain = [('journal_id.code', 'in', journal_codes)]

        if not test_date:
            now = system_timezone_now()
        else:
            now = datetime.datetime.strptime(test_date, "%Y-%m-%d")

        log.info(u'Date : %s. Fuseau horaire : %s.', now, system_timezone)

        if max_date == 'prev_day':

            limit = (now - datetime.timedelta(days=1)). \
                strftime('%Y-%m-%d 23:59:59')

            domain.append(('date', '<=', limit))

            log.info(
                u"Exports de lots d'écritures comptables - Chaque jour : "
                u"avant le %s", limit
            )

        elif max_date == 'prev_month':

            max_day = (
                datetime.datetime(now.year, now.month, 1, 23, 59, 59) -
                datetime.timedelta(days=1)
            )

            limit = max_day.strftime('%Y-%m-%d 23:59:59')

            domain.append(('date', '<=', limit))

            log.info(
                u"Exports de lots d'écritures comptables - Provisions "
                u"- Chaque mois : avant le %s", limit
            )

        elif max_date != 'none':

            raise exceptions.Warning(_(u'Invalid date limit argument.'))

        account_move_ids = self.env['account.move'].search(domain)
        exportable_lines = self.search([('move_id', 'in', account_move_ids._ids)]).filter_exportable_lines()

        log.info(u"%s lignes à exporter.", len(exportable_lines))

        if exportable_lines:
            exportable_lines.export(filename)

            log.info(u"Terminé : Fichier d'export %s", filename)

