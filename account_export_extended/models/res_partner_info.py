import datetime
from pytz import timezone

from openerp import models
from openerp import api
from openerp import fields
from openerp import exceptions
from openerp import _

from openerp.addons.account_export.util.timezone import (
    system_timezone_datetime
)

import logging

log = logging.getLogger('res.partner.info')


class ResPartnerInfo(models.Model):
    _inherit = 'res.partner.info'

    to_export = fields.Boolean(
        u"To Export",
        default=True,
    )

    partner_id = fields.Many2one('res.partner', compute='_compute_partner_id', string='Partenaire')

    @api.multi
    @api.depends('partner_ids')
    def _compute_partner_id(self):
        for obj in self:
            obj.partner_id = obj.partner_ids[0] if obj.partner_ids else False

    @api.multi
    def write(self, vals):
        """Override to:
        - Mark as "to export" on any change.
        """

        if vals:
            vals['to_export'] = True

        return super(ResPartnerInfo, self).write(vals)

    @api.multi
    def filter_exportable_partner_infos(self):
        # return self.filtered(
        #     lambda r: r.to_export and getattr(r, 'status', None) == 'validated'
        # )

        return self.filtered(
            lambda r: r.to_export and getattr(r, 'state',
                                              None) == 'generated' and (r.partner_id.operational_client_account_id
                                                                   or r.partner_id.operational_supplier_account_id)
        )

    @api.model
    def export_all(self, filename):
        """Export all partner infos there are to export.

        :param filename: Filename; may contain date / time formatters.
        :type filename: String.
        """

        exportable_infos = self.search([]).filter_exportable_partner_infos()
        if exportable_infos:

            log.info(
                'Found %s partners identifications to export.',
                len(exportable_infos)
            )

            exportable_infos.export(filename)

    @api.multi
    def export(self, filename):
        """Export the specified partners.

        :param filename: Filename; may contain date / time formatters.
        :type filename: String.
        """

        if not self:
            raise exceptions.Warning(
                "The selected partners can't be packed into a batch."
            )

        now = system_timezone_datetime(datetime.datetime.utcnow())

        # Generate the batch.
        return self.env['res.partner_info_batch'].create({
            'name': now.strftime(filename),
            'partner_info_ids': [(6, 0, self.ids)],
        })

    @api.multi
    def generate_batch(self):
        """Generate a partner batch for the
         selected partner infos.
        """

        batch = self.browse(
            self.env.context['active_ids']
        ).filter_exportable_partner_infos().export()

        # Show the generated batch.
        return {
            'context': self.env.context,
            'name': _('Partners batch'),
            'res_id': batch.id,
            'res_model': 'res.partner_info_batch',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
        }
