# -*- coding: utf-8 -*-
from openerp import models, fields, api

import logging

_logger = logging.getLogger(__name__)


class AccountInvoiceLine(models.Model):

    _inherit = 'account.invoice.line'

    @api.model
    def move_line_get_item(self, line):
        """ Override method to add regie expo fields, before create account.move.line """

        res = super(AccountInvoiceLine, self).move_line_get_item(line)

        if line.invoice_line_tax_id.id:
            res.update({
                'account_tax_id': line.invoice_line_tax_id.id,
                'tax_id': line.invoice_line_tax_id.id
            })

        if line.invoice_id.type in ['in_invoice', 'in_refund']:
            res.update({
                'section_id': line.gle_section_id.id,
                'nature_id': line.nature_id.nature_comptable_id.id,
                'salon_id': line.salon_id.id,
            })

        elif line.invoice_id.type in ['out_invoice', 'out_refund']:

            res.update({
                'section_id': line.invoice_id.gle_section_id.id,
                'nature_id': line.product_id.product_tmpl_id.categ_id.nature_comptable_id.id,
            })

        return res
