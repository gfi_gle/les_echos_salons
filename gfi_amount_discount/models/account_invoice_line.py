# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class AccountInvoiceLine(models.Model):
    """ Class inherit from account.invoice.line to add fields and function """
    _inherit = 'account.invoice.line'

    discount_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', 'Absolue')], string="Type de remise")

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price(self):
        """ Method override from account.invoice.line and test new discount type """

        tax_id = self.get_taxe_id()

        if tax_id:
            price = self._calc_base_price(tax_id=tax_id)
            taxes = self.invoice_line_tax_id.compute_all(price, self.quantity, product=self.product_id,
                                                     partner=self.invoice_id.partner_id)
            self.price_subtotal = taxes['total']

            if self.invoice_id:
                self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)

    def get_taxe_id(self):
        """ Return the value of the first tax associated to line """

        for taxe in self.invoice_line_tax_id:
            return taxe

    @api.multi
    def _calc_base_price(self, tax_id):

        price_unit = self.price_unit

        if self.discount_type:

            if not tax_id.price_include:
                if self.discount_type == 'percentage':
                    price_unit = self.price_unit * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit = self.price_unit * (1 - (self.calc_percentage_with_amount(price_unit=self.price_unit) or 0.0) / 100)

            elif tax_id.price_include:

                result = tax_id.compute_all(
                    price_unit=self.price_unit,
                    quantity=self.quantity,
                    product=self.product_id,
                    partner=self.partner_id)

                price_unit_without_tax = result['taxes'][0].get('price_unit')

                if self.discount_type == 'percentage':
                    price_unit_with_discount = price_unit_without_tax * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit_with_discount = (price_unit_without_tax * (1 - (self.calc_percentage_with_amount(price_unit=price_unit_without_tax) or 0.0) / 100))

                price_unit = price_unit_with_discount / (100 / (100 + tax_id.amount * 100))

        return price_unit

    def calc_percentage_with_amount(self, price_unit):

        amount = price_unit * self.quantity

        return (self.discount / amount) * 100

