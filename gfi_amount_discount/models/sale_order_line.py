# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from openerp import models, fields, api
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    """ Class inherit from sale.order.line to add new field and new method """

    _inherit = 'sale.order.line'

    discount_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', 'Absolue')], string="Type de remise",
                                     readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})

    @api.onchange('discount', 'discount_type')
    def _on_change_discount(self):
        """ Method on change to test if article is commisionable or not """

        if self.product_id.id:
            if (self.product_id.commision_state == 'noncommissionnable') or (self.product_id.commision_state == 'commissionnable_a'):

                self.discount_type = None
                self.discount = None

                res = {
                    'warning': {
                        'title': _('Warning'),
                        'message': _(("Cet article est non-commisionnable."))
                    }
                }

                return res

            elif not self.check_amount_discount() and self.discount_type == 'absolut':

                self.discount = None
                res = {
                    'warning': {
                        'title': _('Warning'),
                        'message': _(("La remise ne peut être supérieure au sous-total"))
                    }
                }

                return res

    def check_amount_discount(self):

        if self.discount_type == 'absolut':
            if self.discount > (self.price_unit * self.product_uom_qty):
                return False

        return True

    def _calc_line_base_price(self, cr, uid, line, context=None):
        """ Method override from sale.order.line model to add a calculator in case discount_type is absolut"""

        tax_id = line.get_taxes_include()
        return line.calc_base_price(tax_id=tax_id)

    def get_taxes_include(self):
        """ Return the value of the first tax associated to line """

        for taxe in self.tax_id:
            return taxe

    @api.multi
    def calc_base_price(self, tax_id):

        price_unit = self.price_unit

        if self.discount_type:

            if not tax_id.price_include:
                if self.discount_type == 'percentage':
                    price_unit = self.price_unit * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit = self.price_unit * (
                        1 - (self.calc_percentage_with_amount(price_unit=self.price_unit) or 0.0) / 100)

            elif tax_id.price_include:

                result = tax_id.compute_all(
                    price_unit=self.price_unit,
                    quantity=self.product_uom_qty,
                    product=self.product_id,
                    partner=self.order_id.partner_id, )

                price_unit_without_tax = result['taxes'][0].get('price_unit')

                if self.discount_type == 'percentage':
                    price_unit_with_discount = price_unit_without_tax * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit_with_discount = (price_unit_without_tax * (
                        1 - (self.calc_percentage_with_amount(price_unit=price_unit_without_tax) or 0.0) / 100))

                price_unit = price_unit_with_discount / (100 / (100 + tax_id.amount * 100))

        return price_unit

    def calc_percentage_with_amount(self, price_unit):

        amount = price_unit * self.product_uom_qty
        return (self.discount / amount) * 100

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        """Prepare the dict of values to create the new invoice line for a
           sales order line. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record line: sale.order.line record to invoice
           :param int account_id: optional ID of a G/L account to force
               (this is used for returning products including service)
           :return: dict of values to create() the invoice line
        """

        res = {}
        if not line.invoiced:
            if not account_id:
                if line.product_id:
                    account_id = line.product_id.property_account_income.id
                    if not account_id:
                        account_id = line.product_id.categ_id.property_account_income_categ.id
                    if not account_id:
                        raise osv.except_osv(_('Error!'),
                                             _('Please define income account for this product: "%s" (id:%d).') % \
                                             (line.product_id.name, line.product_id.id,))
                else:
                    prop = self.pool.get('ir.property').get(cr, uid,
                                                            'property_account_income_categ', 'product.category',
                                                            context=context)
                    account_id = prop and prop.id or False
            uosqty = self._get_line_qty(cr, uid, line, context=context)
            uos_id = self._get_line_uom(cr, uid, line, context=context)
            pu = 0.0
            if uosqty:
                pu = round(line.price_unit * line.product_uom_qty / uosqty,
                           self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
            fpos = line.order_id.fiscal_position or False
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            if not account_id:
                raise osv.except_osv(_('Error!'),
                                     _(
                                         'There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))

            res = {
                'name': line.name,
                'sequence': line.sequence,
                'origin': line.order_id.name,
                'account_id': account_id,
                'price_unit': pu,
                'quantity': uosqty,
                'discount_type': line.discount_type,
                'discount': line.discount,
                'uos_id': uos_id,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
            }

        return res