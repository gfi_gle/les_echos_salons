.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

========
gfi_amount_discount
========

Functionnality
===============

This module add a new discount type "absolut" for sale.order.line model.
Compute a sub-total value with a good type.
This value is transfer in account.invoice model.

Dependencies
------------

-base, sale, gle_salon, sale_stock, product, gfi_amount_discount

Bug Tracker
============

Incident tracking is done with the GFI Jira platform.

Credits
=======

Contributors
------------

* alexandre.matis@gfi.fr
* william.musy@gfi.fr

Maintainer
----------

.. image:: image url (http://www.gfi....image.png)
   :alt:  Odoo by GFI
   :target: http://http://www.gfi.world/

This module is maintained by GFI.