# -*- encoding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    discount_type = fields.Selection([('percentage', "Pourcentage"), ('absolut', 'Absolue')], string="Type de remise",
                                     readonly=True,
                                     states={'draft': [('readonly', False)]})

    discount = fields.Float(string='Discount', digits_compute=dp.get_precision('Discount'))

    price_unit = fields.Float(string="Unit Price", required=True, digits_compute=dp.get_precision('Product Price'))

    @api.model
    def _calc_line_base_price(self, line):
        """ Override method to calculate the base price unit in function of discount_type and taxes_id """

        tax_id = line.get_taxes_include()
        return line.calc_base_price(tax_id=tax_id)

    def get_taxes_include(self):
        """ Return the value of the first tax associated to line """

        for taxe in self.taxes_id:
            return taxe

    @api.multi
    def calc_base_price(self, tax_id):

        price_unit = self.price_unit

        if self.discount_type:

            if not tax_id.price_include:
                if self.discount_type == 'percentage':
                    price_unit = self.price_unit * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit = self.price_unit * (1 - (self.calc_percentage_with_amount(price_unit=self.price_unit) or 0.0) / 100)

            elif tax_id.price_include:

                result = tax_id.compute_all(
                    price_unit=self.price_unit,
                    quantity=self.product_qty,
                    product=self.product_id,
                    partner=self.partner_id,)

                price_unit_without_tax = result['taxes'][0].get('price_unit')

                if self.discount_type == 'percentage':
                    price_unit_with_discount = price_unit_without_tax * (1 - (self.discount or 0.0) / 100)
                elif self.discount_type == 'absolut':
                    price_unit_with_discount = (price_unit_without_tax * (1 - (self.calc_percentage_with_amount(price_unit=price_unit_without_tax) or 0.0) / 100))

                price_unit = price_unit_with_discount / (100 / (100 + tax_id.amount * 100))

        return price_unit

    def calc_percentage_with_amount(self, price_unit):

        amount = price_unit * self.product_qty

        return (self.discount / amount) * 100

    @api.onchange('discount', 'discount_type')
    def _on_change_discount(self):

        if not self.check_amount_absolut_discount():

            self.discount = None
            res = {
                'warning': {
                    'title': _('Warning'),
                    'message': _(("La remise ne peut être supérieure au sous-total (HT)"))
                }
            }

            return res

        if not self.check_percentage_discount():

            self.discount = None
            res = {
                'warning': {
                    'title': _('Warning'),
                    'message': _(("Le pourcentage doit être comprit entre 0 et 100."))
                }
            }

            return res

    def check_amount_absolut_discount(self):

        tax_id = self.get_taxes_include()

        if self.discount_type == 'absolut':
            price_without_tax = (self.price_unit * (100 / ((tax_id.amount * 100) + 100)))

            if self.discount > (price_without_tax * self.product_qty):
                return False

        return True

    def check_percentage_discount(self):

        if self.discount_type == 'percentage':
            if not 0 < self.discount < 100 and self.discount:
                return False

        return True


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.model
    def _prepare_inv_line(self, account_id, order_line):
        result = super(PurchaseOrder, self)._prepare_inv_line(
            account_id, order_line)
        result['discount'] = order_line.discount or 0.0
        return result

    @api.model
    def _prepare_order_line_move(self, order, order_line, picking_id,
                                 group_id):
        res = super(PurchaseOrder, self)._prepare_order_line_move(
            order, order_line, picking_id, group_id)
        for vals in res:
            vals['price_unit'] = (vals.get('price_unit', 0.0) *
                                  (1 - (order_line.discount / 100)))
        return res

    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        """ Override method to add discount in dict """
        
        vals = super(PurchaseOrder, self)._prepare_inv_line(cr, uid, account_id, order_line, context=context)

        vals.update({
            'discount': order_line.discount,
            'discount_type': order_line.discount_type
        })

        return vals

