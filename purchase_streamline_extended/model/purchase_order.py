# -*- coding: utf-8 -*-
##############################################################################
#
#    Purchase management improvements for Odoo
#    Copyright (C) 2013-2015 GFI Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from lxml import etree
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models, fields
from openerp.osv import fields as odoo7_fields

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic
from openerp.addons.purchase.purchase import purchase_order as base_po_class
from openerp.addons.purchase_streamline.util.odoo import default_context


# Defined here so it can be fiddled with by other modules.

class PurchaseOrder(models.Model):
    """Customize purchase orders and add analytics.
    """

    _inherit = 'purchase.order'

    state = fields.Selection(selection_add= [("confirmed_large_value", "En attente de confirmation (montant limite dépassé)")])
    department_id = fields.Many2one(comodel_name='hr.department', required=False)
    operational_department_id = fields.Many2one(required=False)

    # we override onchange_partner_id to add a field to set when a product is
    # selected
    # def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
    #     res = super(PurchaseOrder, self).onchange_partner_id(
    #         cr, uid, ids, partner_id,context
    #     )
    #
    #     company_partner_id = self._get_company_partner_id(
    #         cr, uid, ids, context=context)
    #     res['value']['company_partner_id'] = company_partner_id
    #     return res

    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        partner = self.pool.get('res.partner')
        if not partner_id:
            return {'value': {
                'fiscal_position': False,
                'payment_term_id': False,
                }}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = context.get('company_id', user.company_id.id)
        company_partner_id = self._get_company_partner_id(
            cr, uid, ids, context=context)
        # company_id = context.get('company_id') or self.pool.get('res.users')._get_company(cr, uid, context=context)
        if not company_id:
            raise osv.except_osv(_('Error!'), _('There is no default company for the current user!'))
        fp = self.pool['account.fiscal.position'].get_fiscal_position(cr, uid, company_id, partner_id, context=context)
        supplier_address = partner.address_get(cr, uid, [partner_id], ['default'], context=context)
        supplier = partner.browse(cr, uid, partner_id, context=context)

        return {'value': {
            'fiscal_position': fp or supplier.property_account_position and supplier.property_account_position.id,
            'payment_term_id': supplier.property_supplier_payment_term.id or False,
            'company_partner_id': company_partner_id
        }}
